﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using System.Net;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CrystalDecisions;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using System.Web;

namespace InventoryMS.Models
{
    public static class clsMailProcess
    {
        private static EventLog eventLog = new EventLog();
        private static bool _autoStart = false;
        private static DataTable dtCommon;
        //public static bool autoStart 
        //{
        //    set
        //    {
        //        if (value && value != _autoStart)
        //        {
        //            _autoStart = value;

        //            //WriteLog("Start PI Prepare", EventLogEntryType.Information);
        //            PreparePI();

        //            //WriteLog("Start PI Sending", EventLogEntryType.Information);
        //            SendMail();
        //        }
        //        else
        //            _autoStart = value;
        //    }
        //    get
        //    {
        //        return _autoStart;
        //    }
        //}

        public static void SendMail()
        {
            //dtCommon = new DataTable();
            //dtCommon = SelectQuery("Select * From EmailCube.dbo.ReportConfiguration");

            //eventLog.Source = "EmailService";
            //eventLog.Log = "Automated Email Service";

            //Thread thread = new Thread(() =>
            //    {
            //        while (_autoStart)
            //        {
                        DataTable dataTable = SelectQuery("Exec prcGetPendingEmailList 'IPMSS'", "Email");
                        int rowPosition = 0;

                        while (rowPosition < dataTable.Rows.Count)
                        {
                            ExecuteProcess(dataTable.Rows[rowPosition++]);
                            //Console.WriteLine("H : " + rowPosition);
                        }

                        //Thread.Sleep(60000);

                        //try
                        //{
                        //    //WriteLog("Mail List A", EventLogEntryType.Information);
                        //    dataTable = SelectQuery("Exec prcMailListForDTMNotification ''", "Sales");
                        //    //WriteLog("Mail List B", EventLogEntryType.Information);
                        //}
                        //catch (Exception exx)
                        //{
                        //    WriteLog(exx.Message + " <DTM Notification>", EventLogEntryType.Error);
                        //}

                        //Thread.Sleep(30000);

                        //try
                        //{
                        //    //WriteLog("Mail List A", EventLogEntryType.Information);
                        //    dataTable = SelectQuery("Exec prcMailListForDTMNotification ''", "Sales");
                        //    //WriteLog("Mail List B", EventLogEntryType.Information);
                        //}
                        //catch (Exception exx)
                        //{
                        //    WriteLog(exx.Message + " <DTM Notification>", EventLogEntryType.Error);
                        //}

                        //Thread.Sleep(10000);
            //        }
            //    });
            //thread.Start();
        }

        public static void PreparePI()
        {
            //WriteLog("Start Prepare P P", EventLogEntryType.Information);
            Thread thread2 = new Thread(() =>
            {
                //WriteLog("Start Prepare P " + _autoStart.ToString(), EventLogEntryType.Information);
                while (_autoStart)
                {
                    //WriteLog("Start Prepare", EventLogEntryType.Information);
                    try
                    {
                        //WriteLog("CS Prepare : " + clsDBConfig.getConnectionString("Sales"), EventLogEntryType.Information);
                        DataTable dataTable = SelectQuery("Execute prcGetQueuedPIList", "Sales");
                        int rowPosition = 0;
                        //WriteLog("Total Pending Preparation : " + dataTable.Rows.Count.ToString(), EventLogEntryType.Information);
                        while (rowPosition < dataTable.Rows.Count)
                        {
                            try
                            {
                                //WriteLog("A Prepare", EventLogEntryType.Information);
                                ExecutePIPreparation(dataTable.Rows[rowPosition++]);
                                //Console.WriteLine("H Prepare: " + rowPosition);
                            }
                            catch (Exception ex)
                            {
                                WriteLog("Execute PI preparation failed. \nReason: " + ex.Message, EventLogEntryType.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Execute PI preparation failed. \nReason: " + ex.Message, EventLogEntryType.Error);
                    }

                    Thread.Sleep(30000);
                }
            });
            thread2.Start();
        }

        private static void ExecutePIPreparation(DataRow dataRow)
        {
            try
            {
                //WriteLog("Preparation Start.\nTransaction Id : " + dataRow["BookingNo"].ToString(), EventLogEntryType.Information);
                //Thread thread = new Thread(() =>
                //{
                //    Thread.Sleep(100);
                SqlConnection sCon = new SqlConnection(clsDBConfig.getConnectionString("Sales"));

                if (sCon.State == System.Data.ConnectionState.Open)
                {
                    sCon.Close();
                }
                sCon.Open();

                SqlTransaction sTran = sCon.BeginTransaction();
                try
                {
                    //string emailBody = EmailText[(dataRow["LocationId"].ToString().Equals("2") ? 1 : 0)].Replace("@PINo", dataRow["PINo"].ToString());
                    //WriteLog("SMSText : " + " " + SMSText, EventLogEntryType.Information);
                    SqlCommand sCom = new SqlCommand("Execute prcPINotification " + dataRow["PIId"].ToString() + ", " +
                        "'" + dataRow["PINo"].ToString() + "', " + dataRow["LocationId"].ToString() + ", " +
                        "'" + dataRow["ContactEmail"].ToString() + "', NULL", sCon);

                    //WriteLog("AT Preparation", EventLogEntryType.Information);
                    sCom.Transaction = sTran;
                    sCom.ExecuteNonQuery();

                    //WriteLog("BT Preparation", EventLogEntryType.Information);
                    sCom.CommandText = "Update PIMain Set IsInQueue = 0, IsSentToCustomer = 1 Where PIId = " + dataRow["PIId"].ToString();

                    //WriteLog(sCom.CommandText, EventLogEntryType.Information);
                    sCom.ExecuteNonQuery();

                    sTran.Commit();
                    WriteLog("PI preparation successful.\nTransaction Id : " + dataRow["PINo"].ToString(), EventLogEntryType.Information);
                }
                catch (Exception ex)
                {
                    WriteLog("PI preparation Failed.\nTransaction Id : " + dataRow["PINo"].ToString() + "; \nReason: " + ex.Message, EventLogEntryType.Error);
                    sTran.Rollback();
                }
                finally
                {
                    sCon.Close();
                    //Thread.CurrentThread.Join();
                }
                //});
                //thread.Start();
            }
            catch (Exception ex)
            {
                WriteLog("PI preparation failed. \nTransaction ID: " + dataRow["PINo"].ToString() + "; \nReason: " + ex.Message, EventLogEntryType.Error);
            }
        }

        private static DataTable SelectQuery(string sqlString, string connectionType)
        {
            SqlConnection sCon = new SqlConnection(clsDBConfig.getConnectionString(connectionType));
            DataTable dataTable = new DataTable();

            if (sCon.State == System.Data.ConnectionState.Open)
            {
                sCon.Close();
            }

            try
            {
                sCon.Open();
                SqlDataAdapter sAdapter = new SqlDataAdapter(sqlString, sCon);
                sAdapter.Fill(dataTable);
                sCon.Close();
            }
            catch (Exception ex)
            {
                sCon.Close();
                WriteLog("Database Retrieve Function Error. Reason : " + ex.Message + "\n Query : " + sqlString, EventLogEntryType.Error);
            }

            return dataTable;
        }

        private static void ExecuteProcess(DataRow dataRow)
        {
            if (!dataRow["ModuleName"].ToString().Equals(""))
            {
                try
                {
                    //Thread thread = new Thread(() =>
                    //{
                    //    Thread.Sleep(100);
                    SqlConnection sCon = new SqlConnection(clsDBConfig.getConnectionString());

                    if (sCon.State == System.Data.ConnectionState.Open)
                    {
                        sCon.Close();
                    }
                    sCon.Open();
                    SqlCommand sCom = new SqlCommand();

                    if (dataRow["ModuleName"].ToString().Equals("SALES") && !dataRow["EmailBCC"].ToString().Contains("mohammad.noman@kdsgroup.net"))
                    {
                        dataRow["EmailBCC"] = dataRow["EmailBCC"].ToString() + ";mohammad.noman@kdsgroup.net";
                        //WriteLog(dataRow["EmailSubject"].ToString() + ", Add Noman As BCC", EventLogEntryType.Warning);
                    }
                    else if (!dataRow["EmailFrom"].ToString().Contains("kdsgroup.net"))
                    {
                        sCom = new SqlCommand("Update EmailCube.dbo.EmailCollection " +
                            "Set LastProcessingDate = GetDate(), SendingCount = SendingCount + 1, LastSentBy = 1, SendingStatus = 2, SendingDate = GetDate() " +
                            "Where EmailId = " + dataRow["EmailId"].ToString(), sCon);

                        sCom.ExecuteNonQuery();
                        sCon.Close();
                        WriteLog("Mail disapproved due to problematic from email, Email Id: " + dataRow["EmailId"].ToString(), EventLogEntryType.Error);
                        
                        return;
                        //WriteLog(dataRow["EmailSubject"].ToString() + ", Do Not Add Noman As BCC", EventLogEntryType.Warning);
                    }
                    else
                    {
                        sCom = new SqlCommand("Update EmailCube.dbo.EmailCollection " +
                                "Set LastProcessingDate = GetDate(), SendingCount = SendingCount + 1, LastSentBy = 1 " +
                                "Where EmailId = " + dataRow["EmailId"].ToString(), sCon);

                        sCom.ExecuteNonQuery();
                    }

                    String rptName = "";
                    SqlTransaction sTran = sCon.BeginTransaction();
                    try
                    {
                        sCom = new SqlCommand("Update EmailCube.dbo.EmailCollection " +
                                "Set SendingStatus = 1, SendingDate = GetDate() " +
                                "Where EmailId = " + dataRow["EmailId"].ToString(), sCon);

                        sCom.Transaction = sTran;
                        sCom.ExecuteNonQuery();

                        if (dataRow["ModuleName"].ToString().ToUpper().Equals("CRYSTALMAIL"))
                            dataRow["EmailFrom"] = "CUBE (Automated Mailing System)<" + dataRow["EmailFrom"] + ">;";


                        if (int.Parse(dataRow["HasAttachment"].ToString()) == 0 && int.Parse(dataRow["CreateAttachment"].ToString()) == 0)
                        {
                            MailSend(dataRow["EmailFrom"].ToString(), dataRow["EmailTo"].ToString().Split(';'), dataRow["EmailCC"].ToString().Split(';'),
                                dataRow["EmailBCC"].ToString().Split(';'), dataRow["EmailSubject"].ToString(), dataRow["EmailBody"].ToString());
                        }
                        else if (int.Parse(dataRow["HasAttachment"].ToString()) == 1 && dataRow["Attachment"] != null)
                        {
                            Stream attachmentStream = new MemoryStream(dataRow["Attachment"] as byte[]);
                            Attachment attachment = new Attachment(attachmentStream, dataRow["AllatchmentFileName"].ToString());

                            MailSend(dataRow["EmailFrom"].ToString(), dataRow["EmailTo"].ToString().Split(';'), dataRow["EmailCC"].ToString().Split(';'),
                                        dataRow["EmailBCC"].ToString().Split(';'), dataRow["EmailSubject"].ToString(), dataRow["EmailBody"].ToString(), null, new Attachment[] { attachment });
                        }
                        else if (int.Parse(dataRow["HasAttachment"].ToString()) == 0 && int.Parse(dataRow["CreateAttachment"].ToString()) == 1)
                        {
                            Stream attachmentStream = new MemoryStream(); 

                            if (dataRow["ReportName"].ToString().EndsWith(".rpt"))
                            {
                                //ReportDocument reportDocument = new ReportDocument();
                                
                                //rptName = "EmailService.Reports." + dataRow["ReportName"].ToString().Replace(".rpt", "") + "";
                                rptName = System.Web.HttpContext.Current.Server.MapPath("~/Reports/" + dataRow["ReportName"].ToString()); //Application.StartupPath + @"\Reports\" + dataRow["ReportName"].ToString();
                                //reportDocument.Load(rptName);
                                //reportDocument = (ReportDocument)Activator.CreateInstance(Type.GetType("EmailService.Reports." + dataRow["ReportName"].ToString().Replace(".rpt", "") + "", true, false));

                                //String[] ParameterList = dataRow["ParameterList"].ToString().Split(',');
                                //String[] ParameterValueList = dataRow["ParameterValueList"].ToString().Split(',');

                                //for (int Counter = 0; Counter < ParameterList.Count(); Counter++)
                                //{
                                //    reportDocument.SetParameterValue(ParameterList.ElementAt(Counter).Trim(), ParameterValueList.ElementAt(Counter).Trim());
                                //}
                                ////reportDocument.DataDefinition.FormulaFields["UserId"].Text = "'" + clsMain.sb.ShortName + "'";

                                //clsServerConfig ServerConfig = clsDBConfig.GetReportLogon();
                                //ConnectionInfo rptConnection = new ConnectionInfo();
                                //rptConnection.ServerName = ServerConfig.Server;
                                //rptConnection.DatabaseName = ServerConfig.Database;
                                //rptConnection.UserID = ServerConfig.UserId;
                                //rptConnection.Password = ServerConfig.Password;

                                //foreach (ReportDocument subRpt in reportDocument.Subreports)
                                //{
                                //    foreach (Table crTable in subRpt.Database.Tables)
                                //    {
                                //        TableLogOnInfo logInfo = crTable.LogOnInfo;
                                //        logInfo.ConnectionInfo = rptConnection;
                                //        crTable.ApplyLogOnInfo(logInfo);
                                //    }
                                //}

                                //foreach (Table crTable in reportDocument.Database.Tables)
                                //{
                                //    TableLogOnInfo loi = crTable.LogOnInfo;
                                //    loi.ConnectionInfo = rptConnection;
                                //    crTable.ApplyLogOnInfo(loi);
                                //}

                                //attachmentStream = reportDocument.ExportToStream(ExportFormatType.PortableDocFormat);

                                //reportDocument.Close(); reportDocument.Dispose();
                            }
                            else if (dataRow["ReportName"].ToString().EndsWith(".mrt"))
                            {
                                StiReport report = new StiReport();

                                //rptName = "EmailService.Reports." + dataRow["ReportName"].ToString().Replace(".rpt", "") + "";
                                rptName = System.Web.HttpContext.Current.Server.MapPath("~/Reports/" + dataRow["ReportName"].ToString()); //Application.StartupPath + @"\Reports\" + dataRow["ReportName"].ToString();

                                if (!System.IO.File.Exists(rptName))
                                {
                                    eventLog.WriteEntry("The specified report does not exist\n" + rptName, EventLogEntryType.Error);
                                    sTran.Rollback();
                                    return;
                                }
                                else
                                {
                                    report.Load(rptName);

                                    List<String> ParameterList = dataRow["ParameterList"].ToString().Split(',').ToList();
                                    List<String> ParameterValueList = dataRow["ParameterValueList"].ToString().Split(',').ToList();

                                    Dictionary<string, string> Parameters = new Dictionary<string, string>();

                                    for (int ri = 0; ri < ParameterList.Count; ri++)
                                    {
                                        Parameters.Add(ParameterList[ri].Replace("@", ""), ParameterValueList[ri]);
                                    }

                                    foreach (StiVariable pf in report.Dictionary.Variables)
                                    {
                                        if (Parameters.ContainsKey(pf.Name.Replace("_", "")))
                                        {
                                            var param = Parameters.Where(l => l.Key == pf.Name.Replace("_", "")).FirstOrDefault();

                                            if (!String.IsNullOrEmpty(param.Value))
                                            {
                                                if (pf.Type == typeof(string))
                                                {
                                                    report.Dictionary.Variables[pf.Name].Value = "\"" + param.Value + "\"";
                                                }
                                                else if (pf.Type.BaseType == typeof(System.ValueType))
                                                {
                                                    if (pf.Type == typeof(DateTime))
                                                    {
                                                        report.Dictionary.Variables[pf.Name].Value = "" + DateTime.Parse(param.Value).ToString("yyyy-MM-dd") + "";
                                                    }
                                                    else
                                                    {
                                                        report.Dictionary.Variables[pf.Name].Value = param.Value;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                                                {
                                                    pf.Value = "0";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                                            {
                                                pf.Value = "0";
                                            }
                                        }
                                    }

                                    StiSqlDatabase db = (StiSqlDatabase)report.Dictionary.Databases["IMPS"];

                                    if (db == null)
                                        db = (StiSqlDatabase)report.Dictionary.Databases["IPMS"];

                                    db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=InventoryCube;User Id=sa;Password=dbsrv170@sdk;";
                                    db.PromptUserNameAndPassword = false;
                                    report.Render(); 
                                    report.ExportDocument(StiExportFormat.Pdf, attachmentStream);
                                    attachmentStream.Seek(0, SeekOrigin.Begin);
                                }
                            }

                            try
                            {
                                Attachment attachment = new Attachment(attachmentStream, dataRow["AllatchmentFileName"].ToString());

                                MailSend(dataRow["EmailFrom"].ToString(), dataRow["EmailTo"].ToString().Split(';'), dataRow["EmailCC"].ToString().Split(';'),
                                    dataRow["EmailBCC"].ToString().Split(';'), dataRow["EmailSubject"].ToString(), dataRow["EmailBody"].ToString(), null, new Attachment[] { attachment });
                            }
                            catch (Exception ex)
                            {
                                eventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                                sTran.Rollback();

                                if (ex.Message.Contains("A recipient must be specified"))
                                {
                                    try
                                    {
                                        sCom = new SqlCommand("Update EmailCube.dbo.EmailCollection " +
                                            "Set SendingStatus = 2, SendingDate = GetDate() " +
                                            "Where EmailId = " + dataRow["EmailId"].ToString(), sCon);

                                        sCom.ExecuteNonQuery();

                                        WriteLog("Mail reject due to invalid recipient. Number: " + dataRow["EmailId"].ToString() + "; Subject: " + dataRow["EmailSubject"].ToString(), EventLogEntryType.Warning);
                                    }
                                    catch (Exception exx)
                                    {
                                    }
                                }
                            }
                        }
                        else if (int.Parse(dataRow["HasAttachment"].ToString()) == 1 && int.Parse(dataRow["CreateAttachment"].ToString()) == 0 && dataRow["Attachment"] == null)
                        {
                            MailSend(dataRow["EmailFrom"].ToString(), dataRow["EmailTo"].ToString().Split(';'), dataRow["EmailCC"].ToString().Split(';'),
                                dataRow["EmailBCC"].ToString().Split(';'), dataRow["EmailSubject"].ToString(), dataRow["EmailBody"].ToString(), dataRow["EmailAttachmentLink"].ToString().Split(';'), null);
                        }
                        else if (int.Parse(dataRow["HasAttachment"].ToString()) == 1 && int.Parse(dataRow["CreateAttachment"].ToString()) == 1 && dataRow["Attachment"] == null)
                        {
                            MailSend(dataRow["EmailFrom"].ToString(), dataRow["EmailTo"].ToString().Split(';'), dataRow["EmailCC"].ToString().Split(';'),
                                dataRow["EmailBCC"].ToString().Split(';'), dataRow["EmailSubject"].ToString(), dataRow["EmailBody"].ToString());
                        }

                        sTran.Commit();
                        WriteLog("Mail send. Number: " + dataRow["EmailId"].ToString() + "; Subject: " + dataRow["EmailSubject"].ToString(), EventLogEntryType.Information);
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Failed due to: " + ex.Message + ", Email Id: " + dataRow["EmailId"].ToString(), EventLogEntryType.Error);
                        sTran.Rollback();

                        if (ex.Message.Contains("A recipient must be specified"))
                        {
                            try
                            {
                                sCom = new SqlCommand("Update EmailCube.dbo.EmailCollection " +
                                    "Set SendingStatus = 2, SendingDate = GetDate() " +
                                    "Where EmailId = " + dataRow["EmailId"].ToString(), sCon);

                                sCom.ExecuteNonQuery();

                                WriteLog("Mail reject due to invalid recipient. Number: " + dataRow["EmailId"].ToString() + "; Subject: " + dataRow["EmailSubject"].ToString(), EventLogEntryType.Warning);
                            }
                            catch (Exception exx)
                            {
                            }
                        }
                    }
                    finally
                    {
                        sCon.Close();
                        //Thread.CurrentThread.Join();
                    }
                    //});
                    //thread.Start();
                }
                catch (Exception ex)
                {
                    WriteLog(ex.Message + ", Email Id: " + dataRow["EmailId"].ToString(), EventLogEntryType.Error);
                }
            }
        }

        public static void MailSend(string[] mailTo, string subject, string body, string[] attachmentLink, Attachment[] attachmentList)
        {
            MailSend("CUBE | SDMS (Sales & Distribution Management System)<" + "cube.sdms@kdsgroup.net" + ">;", mailTo, null, null, subject, body, attachmentLink, attachmentList);
        }

        public static void MailSend(string mailFrom, string[] mailCC, string[] mailBCC, string subject, string body, string[] attachmentLink, Attachment[] attachmentList)
        {
            MailSend(mailFrom, null, mailCC, mailBCC, subject, body, attachmentLink, attachmentList);
        }

        //public static void MailSend(string mailFrom, string[] mailTo, string[] mailCC, string subject, string body, string[] attachmentLink)
        //{
        //    MailSend(mailFrom, mailTo, mailCC, null, subject, body, attachmentLink);
        //}

        //public static void MailSend(string mailFrom, string[] mailTo, string[] mailBCC, string subject, string body, string[] attachmentLink)
        //{
        //    MailSend(mailFrom, mailTo, null, mailBCC, subject, body, attachmentLink);
        //}

        public static void MailSend(string mailFrom, string[] mailTo, string[] mailCC, string[] mailBCC, string subject, string body)
        {
            MailSend(mailFrom, mailTo, mailCC, mailBCC, subject, body, null, null);
        }

        public static void MailSend(string mailFrom, string mailTo, string subject, string body)
        {
            MailSend(mailFrom, new string[] { mailTo }, null, null, subject, body, null, null);
        }

        public static void MailSend(string mailFrom, string[] mailTo, string[] mailCC, string[] mailBCC, string subject, string body, string[] attachmentLink, Attachment[] attachmentList)
        {
            try
            {
                //if (!clsDBConfig.GetReportLogon().Server.ToUpper().Equals("192.168.0.181") && !clsDBConfig.GetReportLogon().Server.ToUpper().Equals("203.82.207.170")) return;

                if (mailFrom == null)
                    return;
                if (subject == null)
                    subject = "=N/A=";
                if (body == null)
                    body = "-";

                if (mailTo == null)
                    mailTo = new string[] { };
                if (mailCC == null)
                    mailCC = new string[] { };
                if (mailBCC == null)
                    mailBCC = new string[] { };
                if (attachmentLink == null)
                    attachmentLink = new string[] { };
                if (attachmentList == null)
                    attachmentList = new Attachment[] { };

                MailMessage mail = new MailMessage();

                //if (!mailFrom.Contains("<"))
                //    mailFrom = "CUBE | SDMS (Sales & Distribution Management System)<" + mailFrom + ">;";

                mail.From = new MailAddress(mailFrom.Trim());

                foreach (string address in mailTo.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.To.Add(address.Trim());
                }
                foreach (string cc in mailCC.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.CC.Add(cc.Trim());
                }
                foreach (string bcc in mailBCC.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.Bcc.Add(bcc.Trim());
                }
                //mail.Bcc.Add("mohammad.noman@kdsgroup.net");

                mail.Subject = subject.Replace(Environment.NewLine, "").Replace("\n", "").Trim();
                mail.Body = body.Trim();
                mail.IsBodyHtml = true;

                foreach (string strAttachment in attachmentLink.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.Attachments.Add(new Attachment(strAttachment.Trim()));
                }

                foreach (Attachment attachment in attachmentList.Where(o => o != null))
                {
                    mail.Attachments.Add(attachment);
                }

                SmtpClient SmtpServer = new SmtpClient("203.82.207.6");
                SmtpServer.Credentials = new System.Net.NetworkCredential("mohammad.noman@kdsgroup.net", "bakalam");
                SmtpServer.Port = 25;
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);

                SmtpServer = null;
                //MessageBox.Show("Mail Send Successfully.");
            }
            catch (Exception ex)
            {
                //WriteLog(ex.Message + ", Mail Subject: " + subject, EventLogEntryType.Error);
                throw new Exception("Mail Send Failed. " + ex.Message + ", Mail Subject: " + subject);
            }
        }

        public static void WriteLog(string sEvent, EventLogEntryType eventLogEntryType)
        {
            //if (!EventLog.SourceExists(eventLog.Source))
            //    EventLog.CreateEventSource(eventLog.Source, eventLog.Log);

            //eventLog.WriteEntry(sEvent, eventLogEntryType, 123);
            Console.WriteLine(sEvent);
        }

        public static bool IsFileInUse(string fileFullPath, bool throwIfNotExists)
        {
            if (System.IO.File.Exists(fileFullPath))
            {
                try
                {
                    using (System.IO.FileStream fileStream = System.IO.File.OpenWrite(fileFullPath))
                    {
                        if (fileStream == null)
                            return true;
                        else
                        {
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                    }
                    return false;
                }
                catch
                {
                    return true;
                }
            }
            else if (!throwIfNotExists)
            {
                return false;
            }
            else
            {
                throw new System.IO.FileNotFoundException("Specified path is not exsists", fileFullPath);
            }
        }    
    }
}
