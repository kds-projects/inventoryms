﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace InventoryMS.Models
{
    public class WharfRentAttatchModel
    {
        public int WharfrentId { get; set; }
        [Display(Name = "Lc No")]
        public int LcFileId { get; set; }
        [Display(Name = "Lot No")]
        public string LotNo { get; set; }
        [Display(Name = "Bill Date")]
        public System.DateTime BillDate { get; set; }
        [Display(Name = "Brething Date")]
        public System.DateTime BrethingDate { get; set; }
        [Display(Name = "Delivery Date")]
        public System.DateTime DeliveryDate { get; set; }
        [Display(Name = "From Date")]
        public System.DateTime WhrafRentFromDate { get; set; }
        [Display(Name = "To Date")]
        public System.DateTime WhrafRentToDate { get; set; }
        [Display(Name = "Amount")]
        public decimal WhrafRentAmount { get; set; }
        [Display(Name = "Amount")]
        public decimal ContainerDetentionCharge { get; set; }
        public string Remarks { get; set; }
        public short CreatedBy { get; set; }
        public System.DateTime CreatedTime { get; set; }
        public short RevisedBy { get; set; }
        public System.DateTime RevisedTime { get; set; }
        public string IPAddress { get; set; }
        public string PCName { get; set; }
        [Display(Name = "Port Landing Date")]
        public System.DateTime PortLandingDate { get; set; }
        [Display(Name = "From Date")]
        public System.DateTime DetentionFromDate { get; set; }
        [Display(Name = "To Date")]
        public System.DateTime DetentionToDate { get; set; }
        [Display(Name = "Container Return Date")]
        public System.DateTime ContainerReturnDate { get; set; }
        public string FilePath { get; set; }

        public virtual LCMain LCMain { get; set; }
    }
}