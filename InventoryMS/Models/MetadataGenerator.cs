﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects.DataClasses;

namespace InventoryMS.Models
{
	[MetadataType(typeof(BondMetadata))]
	public partial class Bond
	{
		internal sealed class BondMetadata
		{
		
			[Required(ErrorMessage="Bond is required")]
    		public Int16 BondId { get; set; }

			[Required(ErrorMessage="Bond Code is required")]
			[StringLength(50)]
    		public String BondCode { get; set; }

			[Required(ErrorMessage="Bond Name is required")]
			[StringLength(50)]
    		public String BondName { get; set; }

			[Required(ErrorMessage="Location is required")]
    		public Int16 LocationId { get; set; }

			[Required(ErrorMessage="License Number is required")]
			[StringLength(50)]
    		public String LicenseNumber { get; set; }

			[Required(ErrorMessage="License Renewal Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime LicenseRenewalDate { get; set; }

			[Required(ErrorMessage="License Period is required")]
			[StringLength(50)]
    		public String LicensePeriod { get; set; }

			[Required(ErrorMessage="Start Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime StartDate { get; set; }

			[Required(ErrorMessage="End Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EndDate { get; set; }

			[Required(ErrorMessage="Is Active is required")]
    		public Byte IsActive { get; set; }

			[Required(ErrorMessage="Rank is required")]
    		public Int16 RankId { get; set; }

			[Required(ErrorMessage="Created By is required")]
    		public Int16 CreatedBy { get; set; }

			[Required(ErrorMessage="Created Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedTime { get; set; }

			[Required(ErrorMessage="Updated By is required")]
    		public Int16 UpdatedBy { get; set; }

			[Required(ErrorMessage="Update Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime UpdateTime { get; set; }

			[Required(ErrorMessage="P C Name is required")]
			[StringLength(50)]
    		public String PCName { get; set; }

			[Required(ErrorMessage="I P Address is required")]
			[StringLength(50)]
    		public String IPAddress { get; set; }

    		public EntityCollection<BondCapacity> BondCapacities { get; set; }

		}
	}
	
	[MetadataType(typeof(BondCapacityMetadata))]
	public partial class BondCapacity
	{
		internal sealed class BondCapacityMetadata
		{
		
			[Required(ErrorMessage="Capacity is required")]
    		public Int32 CapacityId { get; set; }

			[Required(ErrorMessage="Bond is required")]
    		public Int16 BondId { get; set; }

			[Required(ErrorMessage="Bond Serial is required")]
    		public Int16 BondSerialId { get; set; }

			[Required(ErrorMessage="Bond Serial No is required")]
			[StringLength(10)]
    		public String BondSerialNo { get; set; }

			[Required(ErrorMessage="R M Title is required")]
			[StringLength(250)]
    		public String RMTitle { get; set; }

			[Required(ErrorMessage="Capacity is required")]
    		public Decimal Capacity { get; set; }

			[Required(ErrorMessage="Expire Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ExpireDate { get; set; }

			[Required(ErrorMessage="End Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EndDate { get; set; }

			[Required(ErrorMessage="Is Active is required")]
    		public Byte IsActive { get; set; }

			[Required(ErrorMessage="Rank is required")]
    		public Int16 RankId { get; set; }

			[Required(ErrorMessage="Created By is required")]
    		public Int16 CreatedBy { get; set; }

			[Required(ErrorMessage="Created Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedTime { get; set; }

			[Required(ErrorMessage="Updated By is required")]
    		public Int16 UpdatedBy { get; set; }

			[Required(ErrorMessage="Update Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime UpdateTime { get; set; }

			[Required(ErrorMessage="P C Name is required")]
			[StringLength(50)]
    		public String PCName { get; set; }

			[Required(ErrorMessage="I P Address is required")]
			[StringLength(50)]
    		public String IPAddress { get; set; }

    		public EntityCollection<Bond> Bond { get; set; }

		}
	}
	
	[MetadataType(typeof(BondProductMetadata))]
	public partial class BondProduct
	{
		internal sealed class BondProductMetadata
		{
		
			[Required(ErrorMessage="B S P is required")]
    		public Int32 BSPId { get; set; }

			[Required(ErrorMessage="Bond Serial is required")]
    		public Int16 BondSerialId { get; set; }

			[Required(ErrorMessage="Product is required")]
    		public Int16 ProductId { get; set; }

		}
	}
	
	[MetadataType(typeof(SupplerContactMetadata))]
	public partial class SupplerContact
	{
		internal sealed class SupplerContactMetadata
		{
		
			[Required(ErrorMessage="S Contact is required")]
    		public Int32 SContactId { get; set; }

			[Required(ErrorMessage="Supplier is required")]
    		public Int16 SupplierId { get; set; }

			[Required(ErrorMessage="Name is required")]
			[StringLength(150)]
    		public String Name { get; set; }

			[Required(ErrorMessage="Designation is required")]
			[StringLength(50)]
    		public String Designation { get; set; }

			[Required(ErrorMessage="Phone No is required")]
			[StringLength(50)]
			[DataType(DataType.PhoneNumber)]
    		public String PhoneNo { get; set; }

			[Required(ErrorMessage="Mobile No is required")]
			[StringLength(150)]
    		public String MobileNo { get; set; }

			[Required(ErrorMessage="e Mail is required")]
			[StringLength(75)]
			[DataType(DataType.EmailAddress)]
    		public String eMail { get; set; }

			[Required(ErrorMessage="Is Auto Mail Received is required")]
    		public Byte IsAutoMailReceived { get; set; }

			[Required(ErrorMessage="Is Active is required")]
    		public Byte IsActive { get; set; }

			[Required(ErrorMessage="Rank is required")]
    		public Int16 RankId { get; set; }

			[Required(ErrorMessage="Created By is required")]
    		public Int16 CreatedBy { get; set; }

			[Required(ErrorMessage="Created Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedTime { get; set; }

			[Required(ErrorMessage="Updated By is required")]
    		public Int16 UpdatedBy { get; set; }

			[Required(ErrorMessage="Update Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime UpdateTime { get; set; }

			[Required(ErrorMessage="P C Name is required")]
			[StringLength(50)]
    		public String PCName { get; set; }

			[Required(ErrorMessage="I P Address is required")]
			[StringLength(50)]
    		public String IPAddress { get; set; }

    		public EntityCollection<Supplier> Supplier { get; set; }

		}
	}
	
	[MetadataType(typeof(SupplierMetadata))]
	public partial class Supplier
	{
		internal sealed class SupplierMetadata
		{
		
			[Required(ErrorMessage="Supplier is required")]
    		public Int16 SupplierId { get; set; }

			[StringLength(5)]
    		public String SupplierCode { get; set; }

			[Required(ErrorMessage="Supplier Name is required")]
			[StringLength(100)]
    		public String SupplierName { get; set; }

			[Required(ErrorMessage="Short Name is required")]
			[StringLength(50)]
    		public String ShortName { get; set; }

			[Required(ErrorMessage="T I N No is required")]
			[StringLength(50)]
    		public String TINNo { get; set; }

			[Required(ErrorMessage="V A T No is required")]
			[StringLength(50)]
    		public String VATNo { get; set; }

			[Required(ErrorMessage="B I N No is required")]
			[StringLength(50)]
    		public String BINNo { get; set; }

			[Required(ErrorMessage="Origin Country is required")]
    		public Int16 OriginCountryId { get; set; }

			[Required(ErrorMessage="Founding Year is required")]
    		public Int16 FoundingYear { get; set; }

			[Required(ErrorMessage="Business Type is required")]
    		public Int16 BusinessTypeId { get; set; }

			[Required(ErrorMessage="Purchase Type is required")]
    		public Int16 PurchaseTypeId { get; set; }

			[Required(ErrorMessage="Citizenship is required")]
    		public Int16 CitizenshipId { get; set; }

			[Required(ErrorMessage="Legal Structure is required")]
    		public Int16 LegalStructureId { get; set; }

			[Required(ErrorMessage="House No is required")]
			[StringLength(50)]
    		public String HouseNo { get; set; }

			[Required(ErrorMessage="City is required")]
			[StringLength(50)]
    		public String City { get; set; }

			[Required(ErrorMessage="State is required")]
			[StringLength(50)]
    		public String State { get; set; }

			[Required(ErrorMessage="Z I P Code is required")]
			[StringLength(50)]
    		public String ZIPCode { get; set; }

			[Required(ErrorMessage="Country is required")]
			[StringLength(50)]
    		public String Country { get; set; }

			[Required(ErrorMessage="Phone No is required")]
			[StringLength(50)]
			[DataType(DataType.PhoneNumber)]
    		public String PhoneNo { get; set; }

			[Required(ErrorMessage="Mobile No is required")]
			[StringLength(150)]
    		public String MobileNo { get; set; }

			[Required(ErrorMessage="Fax is required")]
			[StringLength(50)]
    		public String Fax { get; set; }

			[Required(ErrorMessage="e Mail Address is required")]
			[StringLength(75)]
			[DataType(DataType.EmailAddress)]
    		public String eMailAddress { get; set; }

			[Required(ErrorMessage="Web Address is required")]
			[StringLength(75)]
    		public String WebAddress { get; set; }

			[Required(ErrorMessage="Payment Address is required")]
			[StringLength(500)]
    		public String PaymentAddress { get; set; }

			[Required(ErrorMessage="Local Address is required")]
			[StringLength(500)]
    		public String LocalAddress { get; set; }

			[Required(ErrorMessage="Manpower is required")]
    		public Int16 Manpower { get; set; }

			[Required(ErrorMessage="Annual Revenue is required")]
    		public Decimal AnnualRevenue { get; set; }

			[Required(ErrorMessage="Certificates Avail is required")]
			[StringLength(250)]
    		public String CertificatesAvail { get; set; }

			[Required(ErrorMessage="Award Avail is required")]
			[StringLength(250)]
    		public String AwardAvail { get; set; }

			[Required(ErrorMessage="Preferred Port is required")]
    		public Int16 PreferredPortId { get; set; }

			[Required(ErrorMessage="Is Auto Mail Enabled is required")]
    		public Byte IsAutoMailEnabled { get; set; }

			[Required(ErrorMessage="Is Auto S M S Enabled is required")]
    		public Byte IsAutoSMSEnabled { get; set; }

			[Required(ErrorMessage="Business Start Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime BusinessStartDate { get; set; }

			[Required(ErrorMessage="Is Active is required")]
    		public Byte IsActive { get; set; }

			[Required(ErrorMessage="Rank is required")]
    		public Int16 RankId { get; set; }

			[Required(ErrorMessage="Created By is required")]
    		public Int16 CreatedBy { get; set; }

			[Required(ErrorMessage="Created Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedTime { get; set; }

			[Required(ErrorMessage="Updated By is required")]
    		public Int16 UpdatedBy { get; set; }

			[Required(ErrorMessage="Update Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime UpdateTime { get; set; }

			[Required(ErrorMessage="P C Name is required")]
			[StringLength(50)]
    		public String PCName { get; set; }

			[Required(ErrorMessage="I P Address is required")]
			[StringLength(50)]
    		public String IPAddress { get; set; }

    		public EntityCollection<SupplerContact> SupplerContacts { get; set; }

    		public EntityCollection<SupplierProduct> SupplierProducts { get; set; }

		}
	}
	
	[MetadataType(typeof(SupplierProductMetadata))]
	public partial class SupplierProduct
	{
		internal sealed class SupplierProductMetadata
		{
		
			[Required(ErrorMessage="S Product is required")]
    		public Int32 SProductId { get; set; }

			[Required(ErrorMessage="Supplier is required")]
    		public Int16 SupplierId { get; set; }

			[Required(ErrorMessage="Category is required")]
    		public Int16 CategoryId { get; set; }

			[Required(ErrorMessage="Is Default is required")]
    		public Byte IsDefault { get; set; }

			[Required(ErrorMessage="Is Active is required")]
    		public Byte IsActive { get; set; }

			[Required(ErrorMessage="Rank is required")]
    		public Int16 RankId { get; set; }

			[Required(ErrorMessage="Created By is required")]
    		public Int16 CreatedBy { get; set; }

			[Required(ErrorMessage="Created Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedTime { get; set; }

			[Required(ErrorMessage="Updated By is required")]
    		public Int16 UpdatedBy { get; set; }

			[Required(ErrorMessage="Update Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime UpdateTime { get; set; }

			[Required(ErrorMessage="P C Name is required")]
			[StringLength(50)]
    		public String PCName { get; set; }

			[Required(ErrorMessage="I P Address is required")]
			[StringLength(50)]
    		public String IPAddress { get; set; }

    		public EntityCollection<Supplier> Supplier { get; set; }

		}
	}
	
	[MetadataType(typeof(Sys_User_NameMetadata))]
	public partial class Sys_User_Name
	{
		internal sealed class Sys_User_NameMetadata
		{
		
			[Required(ErrorMessage="User is required")]
    		public Int16 UserId { get; set; }

			[Required(ErrorMessage="Employee is required")]
    		public Int32 EmployeeId { get; set; }

			[Required(ErrorMessage="User Name is required")]
			[StringLength(10)]
    		public String UserName { get; set; }

			[Required(ErrorMessage="User P W D is required")]
			[StringLength(15)]
    		public String UserPWD { get; set; }

			[Required(ErrorMessage="User Title is required")]
			[StringLength(100)]
    		public String UserTitle { get; set; }

			[Required(ErrorMessage="Department is required")]
    		public Int16 DepartmentId { get; set; }

			[Required(ErrorMessage="Department Name is required")]
			[StringLength(150)]
    		public String DepartmentName { get; set; }

			[Required(ErrorMessage="Short Name is required")]
			[StringLength(25)]
    		public String ShortName { get; set; }

			[Required(ErrorMessage="e Mail is required")]
			[StringLength(100)]
			[DataType(DataType.EmailAddress)]
    		public String eMail { get; set; }

			[Required(ErrorMessage="Location is required")]
    		public Int16 LocationId { get; set; }

			[Required(ErrorMessage="Login Key is required")]
			[StringLength(50)]
    		public String LoginKey { get; set; }

			[Required(ErrorMessage="Key Start Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime KeyStartDate { get; set; }

			[Required(ErrorMessage="Key End Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime KeyEndDate { get; set; }

			[Required(ErrorMessage="User Type is required")]
    		public Int16 UserTypeId { get; set; }

			[Required(ErrorMessage="User Level is required")]
    		public Int16 UserLevelId { get; set; }

			[Required(ErrorMessage="Is Logged is required")]
    		public Byte IsLogged { get; set; }

			[Required(ErrorMessage="Is Active is required")]
    		public Byte IsActive { get; set; }

		}
	}
	
	[MetadataType(typeof(viewDecriptionListMetadata))]
	public partial class viewDecriptionList
	{
		internal sealed class viewDecriptionListMetadata
		{
		
			[Required(ErrorMessage="Table Name is required")]
			[StringLength(128)]
    		public String TableName { get; set; }

			[StringLength(128)]
    		public String ColumnName { get; set; }

			[Required(ErrorMessage="Property Name is required")]
			[StringLength(128)]
    		public String PropertyName { get; set; }

			[StringLength(100)]
    		public String DescriptionText { get; set; }

		}
	}
	
	[MetadataType(typeof(viewDefaultListMetadata))]
	public partial class viewDefaultList
	{
		internal sealed class viewDefaultListMetadata
		{
		
			[Required(ErrorMessage="Table is required")]
			[StringLength(128)]
    		public String Table { get; set; }

			[StringLength(128)]
    		public String Column { get; set; }

    		public String DefaultValue { get; set; }

		}
	}
	
	
}

