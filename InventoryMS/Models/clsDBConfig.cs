﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Win32;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace InventoryMS.Models
{
    public static class clsDBConfig
    {
        private static DataSet dsDBConfig;
        private static string sqlConnectionString = "";

        private static string dbName = "EMailCube";
        public static string userId = "";
        private static string userName = "";
        private static string srvName = "";

        private static string srvNameL = "192.168.0.181";
        private static string srvNameR = "203.82.207.170";
        //private static string dbUser = "db_client";//"sa";//
        //private static string dbPass = "#KDS-10288/dbclient";//dbsrv170@sdk";//

        ////private static string srvNameL = "192.168.0.199";
        ////private static string srvNameR = "203.82.207.173";
        private static string dbUser = "sa";//"db_client";//
        private static string dbPass = "dbsrv170@sdk";//"#KDS-10288/dbclient";//

        //private static string srvNameL = "192.168.0.50";
        //private static string srvNameR = "192.168.0.50";
        //private static string dbUser = "sa";
        //private static string dbPass = "saif123";

        private const string dbConFileName = "DBConfig";

        private static string dbConFilePath = "";

        public static string getConnectionString()
        {
            return getConnectionString("Email");
        }

        public static string getConnectionString(String ConnectionType)
        {
            dsDBConfig = new DataSet();
            dbConFilePath = Environment.CurrentDirectory + @"\";
            DataTable dt = new DataTable();
            try
            {
                //GetDBConfigDetails(ref dsDBConfig);
                //dt = dsDBConfig.Tables[0];

                //if (dt.Rows.Count != 0)
                //{
                //    DataRow[] dr = dt.Select("IsDefault = 1");
                //    if (dr.Length > 0)
                //    {
                //        sqlConnectionString = "Data Source = " + dr[0]["ServerName"].ToString() + ";";
                //        sqlConnectionString += "User Id = " + dr[0]["UserName"].ToString() + ";";
                //        sqlConnectionString += "Password = " + clsUtil.fncDecryptWord(dr[0]["Password"].ToString()) + ";";
                //        sqlConnectionString += "Initial Catalog = " + clsUtil.fncDecryptWord(dr[0]["DatabaseName"].ToString()) + ";";
                //    }
                //}
                //else
                //{
                //}

                if (IsServerExists(srvNameL))
                    srvName = srvNameL;
                else //if (IsServerExists(srvNameR))
                    srvName = srvNameR;
                //else
                //    throw new Exception("Server Is Not Alive, Try Later.");

                if (ConnectionType.Equals("Sales"))
                {
                    sqlConnectionString = "Data Source = " + srvName + "; User Id = " + dbUser + "; Password = " + dbPass + "; Initial Catalog = " + "SalesCube" + "";
                    //sqlConnectionString = "Data Source = NOMANSOFT; Initial Catalog = SalesCube; Integrated Security = true;";
                }
                else if (ConnectionType.Equals("SMS"))
                {
                    //sqlConnectionString = "Data Source = " + "NomanSoft" + "; User Id = " + "sa" + "; Password = " + "sys" + "; Initial Catalog = " + "SMSCube" + "";
                    sqlConnectionString = "Data Source = " + srvName + "; User Id = " + dbUser + "; Password = " + dbPass + "; Initial Catalog = " + "SMSCube" + "";
                }
                else if (ConnectionType.Equals("Email"))
                {
                    //sqlConnectionString = "Data Source = " + srvName + "; User Id = " + dbUser + "; Password = " + dbPass + "; Initial Catalog = " + "EMailCube" + "";
                    sqlConnectionString = "Data Source = " + "192.168.0.181" + "; User Id = " + dbUser + "; Password = " + dbPass + "; Initial Catalog = " + "EMailCube" + "";
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return sqlConnectionString;
        }

        private static void GetDBConfigDetails(ref DataSet dsDBConfig)
        {
            try
            {
                if (File.Exists(dbConFilePath + dbConFileName + ".xsd"))
                {
                    dsDBConfig.ReadXmlSchema(dbConFilePath + dbConFileName + ".xsd");
                }
                else
                {
                    throw new Exception("A required file: " + dbConFileName + ".xsd is not found is the location: " + dbConFilePath);
                }
                if (File.Exists(dbConFilePath + dbConFileName + ".xml"))
                {
                    dsDBConfig.ReadXml(dbConFilePath + dbConFileName + ".xml");
                }
                else
                {
                    throw new Exception("A required file: " + dbConFileName + ".xml is not found is the location: " + dbConFilePath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static bool IsServerExists(string srvName)
        {
            try
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();
                options.DontFragment = true;
                string data = "Testing Server Existance";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = (srvName.Substring(0, 3).Equals("192") ? 120 : 1000), success = 0, insuccess = 0;
                for (int i = 0; i < 4; i++)
                {
                    PingReply reply = pingSender.Send(srvName, 1000);
                    if (reply.Status == IPStatus.Success)
                        success++;
                    else
                        insuccess++;
                }

                if (success >= 3)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static clsServerConfig GetReportLogon()
        {
            try
            {
                if (IsServerExists(srvNameL))
                    srvName = srvNameL;
                else if (IsServerExists(srvNameR))
                    srvName = srvNameR;
                else
                    throw new Exception("Server Is Not Alive.");

                clsServerConfig ServerConfig = new clsServerConfig();

                ServerConfig.Server = srvName;
                ServerConfig.UserId = dbUser;
                ServerConfig.Password = dbPass;
                ServerConfig.Database = "SalesCube";

                return ServerConfig;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    //public class clsServerConfig
    //{
    //    public string Server { get; set; }
    //    public string UserId { get; set; }
    //    public string Password { get; set; }
    //    public string Database { get; set; }
    //}
}
