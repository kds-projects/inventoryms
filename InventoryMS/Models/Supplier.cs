//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(SupplierMetaData))]
    public partial class Supplier : IFIMSContext
    {
    	
        public Supplier()
        {
            this.PriceApprovalSubs = new HashSet<PriceApprovalSub>();
            this.SupplierContacts = new HashSet<SupplierContact>();
            this.SupplierFactories = new HashSet<SupplierFactory>();
            this.PRSubs = new HashSet<PRSub>();
            this.SupplierProducts = new HashSet<SupplierProduct>();
        }
    
    	public Supplier(IFIMSContext viewModel)
        {
            this.PriceApprovalSubs = new HashSet<PriceApprovalSub>();
            this.SupplierContacts = new HashSet<SupplierContact>();
            this.SupplierFactories = new HashSet<SupplierFactory>();
            this.PRSubs = new HashSet<PRSub>();
            this.SupplierProducts = new HashSet<SupplierProduct>();
    
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("Supplier", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("Supplier", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	[Display(Name="ID")]
    	public short SupplierId { get; set; }
    	[Display(Name="Code")]
    	public string SupplierCode { get; set; }
    	[Display(Name="Name")]
    	public string SupplierName { get; set; }
    	[Display(Name="Short Name")]
    	public string ShortName { get; set; }
    	public string ManufacturerName { get; set; }
    	[Display(Name="TIN")]
    	public string TINNo { get; set; }
    	[Display(Name="VAT")]
    	public string VATNo { get; set; }
    	[Display(Name="BIN")]
    	public string BINNo { get; set; }
    	[Display(Name="BIN")]
    	public string RegistrationNo { get; set; }
    	public short BrandId { get; set; }
    	public short LeadTime { get; set; }
    	[Display(Name="Country of Origin")]
    	public short OriginCountryId { get; set; }
    	[Display(Name="Founding Year")]
    	public short FoundingYear { get; set; }
    	[Display(Name="Business Type")]
    	public short BusinessTypeId { get; set; }
    	[Display(Name="Puchase Type")]
    	public short PurchaseTypeId { get; set; }
    	[Display(Name="Nationality")]
    	public short CitizenshipId { get; set; }
    	[Display(Name="Legal Structure")]
    	public short LegalStructureId { get; set; }
    	[Display(Name="Legal Structure")]
    	public decimal RatingScore { get; set; }
    	[Display(Name="House #")]
    	public string HouseNo { get; set; }
    	[Display(Name="City")]
    	public string City { get; set; }
    	[Display(Name="State")]
    	public string State { get; set; }
    	[Display(Name="ZIP/Postal Code")]
    	public string ZIPCode { get; set; }
    	public short AddressCountryId { get; set; }
    	[Display(Name="Phone")]
    	public string PhoneNo { get; set; }
    	[Display(Name="Mobile")]
    	public string MobileNo { get; set; }
    	[Display(Name="Fax")]
    	public string Fax { get; set; }
    	[Display(Name="E-Mail")]
    	public string eMailAddress { get; set; }
    	[Display(Name="Web")]
    	public string WebAddress { get; set; }
    	[Display(Name="Payment Address")]
    	public string PaymentAddress { get; set; }
    	[Display(Name="Local Address")]
    	public string LocalAddress { get; set; }
    	[Display(Name="Manpower")]
    	public short Manpower { get; set; }
    	[Display(Name="Annual Revenue")]
    	public decimal AnnualRevenue { get; set; }
    	[Display(Name="Certificates")]
    	public string CertificatesAvail { get; set; }
    	[Display(Name="Awards")]
    	public string AwardAvail { get; set; }
    	[Display(Name="Awards")]
    	public string CategoryList { get; set; }
    	[Display(Name="Preferred Port")]
    	public short PreferredPortId { get; set; }
    	[Display(Name="Auto Mail ?")]
    	public byte IsAutoMailEnabled { get; set; }
    	[Display(Name="Auto SMS ?")]
    	public byte IsAutoSMSEnabled { get; set; }
    	[Display(Name="Business Start")]
    	public System.DateTime BusinessStartDate { get; set; }
    	public short SupervisorUserId { get; set; }
    	public string SupplierReferredBy { get; set; }
    	public string SupplierNegotiationBy { get; set; }
    	[Display(Name="Active ?")]
    	public byte IsActive { get; set; }
    	[Display(Name="SL #")]
    	public short RankId { get; set; }
    	public short ApprovedType { get; set; }
    	public short ApprovedBy { get; set; }
    	public System.DateTime ApprovedTime { get; set; }
    	public short CreatedBy { get; set; }
    	public System.DateTime CreatedTime { get; set; }
    	public short UpdatedBy { get; set; }
    	public System.DateTime UpdateTime { get; set; }
    	public string PCName { get; set; }
    	public string IPAddress { get; set; }
    	public Nullable<short> NotificationFromID { get; set; }
    	public string NotificationToEmail { get; set; }
    	public string NotificationCCEmailList { get; set; }
    	public Nullable<byte> IsNotificationMail { get; set; }
    	public string NotificationToConcern { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual ICollection<PriceApprovalSub> PriceApprovalSubs { get; set; }
        public virtual ICollection<SupplierContact> SupplierContacts { get; set; }
        public virtual ICollection<SupplierFactory> SupplierFactories { get; set; }
        public virtual ICollection<PRSub> PRSubs { get; set; }
        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }
    }
    
    //public class SupplierMetaData
    //{
    //}
}
