//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(WarehouseMetaData))]
    public partial class Warehouse : IFIMSContext
    {
    	
        public Warehouse()
        {
            this.GRRMains = new HashSet<GRRMain>();
            this.SPGRRMains = new HashSet<SPGRRMain>();
        }
    
    	public Warehouse(IFIMSContext viewModel)
        {
            this.GRRMains = new HashSet<GRRMain>();
            this.SPGRRMains = new HashSet<SPGRRMain>();
    
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("Warehouse", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("Warehouse", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	public short WarehouseId { get; set; }
    	[Display(Name="ID")]
    	public short BondId { get; set; }
    	public string WarehouseName { get; set; }
    	public short LocationId { get; set; }
    	[Display(Name="Activity")]
    	public byte IsActive { get; set; }
    	[Display(Name="SL #")]
    	public short RankId { get; set; }
    	public short CreatedBy { get; set; }
    	public System.DateTime CreatedTime { get; set; }
    	public short UpdatedBy { get; set; }
    	public System.DateTime UpdateTime { get; set; }
    	public string PCName { get; set; }
    	public string IPAddress { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual Bond Bond { get; set; }
        public virtual ICollection<GRRMain> GRRMains { get; set; }
        public virtual ICollection<SPGRRMain> SPGRRMains { get; set; }
    }
    
    //public class WarehouseMetaData
    //{
    //}
}
