﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class InventoryCubeEntities : DbContext
    {
        //public InventoryCubeEntities()
        //    : base("name=InventoryCubeEntities")
        //{
        //}
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Bond> Bonds { get; set; }
        public virtual DbSet<BondCapacity> BondCapacities { get; set; }
        public virtual DbSet<BondPeriod> BondPeriods { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<ConsumptionAgIssue> ConsumptionAgIssues { get; set; }
        public virtual DbSet<ConsumptionMain> ConsumptionMains { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Custom_Type> Custom_Type { get; set; }
        public virtual DbSet<EmailLibrary> EmailLibraries { get; set; }
        public virtual DbSet<GRRMain> GRRMains { get; set; }
        public virtual DbSet<IssueMain> IssueMains { get; set; }
        public virtual DbSet<IssueReturnAgIssue> IssueReturnAgIssues { get; set; }
        public virtual DbSet<IssueReturnMain> IssueReturnMains { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<MenuDetail> MenuDetails { get; set; }
        public virtual DbSet<MenuType> MenuTypes { get; set; }
        public virtual DbSet<MenuWiseAction> MenuWiseActions { get; set; }
        public virtual DbSet<NewArea> NewAreas { get; set; }
        public virtual DbSet<NewLocation> NewLocations { get; set; }
        public virtual DbSet<Payer> Payers { get; set; }
        public virtual DbSet<PipelineMain> PipelineMains { get; set; }
        public virtual DbSet<PipelinePackingList> PipelinePackingLists { get; set; }
        public virtual DbSet<PORequisition> PORequisitions { get; set; }
        public virtual DbSet<PriceApprovalMain> PriceApprovalMains { get; set; }
        public virtual DbSet<PriceApprovalSub> PriceApprovalSubs { get; set; }
        public virtual DbSet<PRMain> PRMains { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public virtual DbSet<PurchaseOrderSub> PurchaseOrderSubs { get; set; }
        public virtual DbSet<QuantityApprovalMain> QuantityApprovalMains { get; set; }
        public virtual DbSet<QuantityApprovalSub> QuantityApprovalSubs { get; set; }
        public virtual DbSet<QuotationMain> QuotationMains { get; set; }
        public virtual DbSet<QuotationSub> QuotationSubs { get; set; }
        public virtual DbSet<ReceivedReturnMain> ReceivedReturnMains { get; set; }
        public virtual DbSet<SRRMain> SRRMains { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<SupplierContact> SupplierContacts { get; set; }
        public virtual DbSet<SupplierFactory> SupplierFactories { get; set; }
        public virtual DbSet<Sys_User_Name> Sys_User_Name { get; set; }
        public virtual DbSet<TransferMain> TransferMains { get; set; }
        public virtual DbSet<UnitNameConversion> UnitNameConversions { get; set; }
        public virtual DbSet<User_Action> User_Action { get; set; }
        public virtual DbSet<User_Controller> User_Controller { get; set; }
        public virtual DbSet<User_Permission> User_Permission { get; set; }
        public virtual DbSet<User_Role> User_Role { get; set; }
        public virtual DbSet<User_Role_Details> User_Role_Details { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<EmailFramework> EmailFrameworks { get; set; }
        public virtual DbSet<viewDecriptionList> viewDecriptionLists { get; set; }
        public virtual DbSet<viewDefaultList> viewDefaultLists { get; set; }
        public virtual DbSet<viewCompany> viewCompanies { get; set; }
        public virtual DbSet<viewFactory> viewFactories { get; set; }
        public virtual DbSet<viewPort> viewPorts { get; set; }
        public virtual DbSet<CurrencyConversionRate> CurrencyConversionRates { get; set; }
        public virtual DbSet<SupplierEvaluation> SupplierEvaluations { get; set; }
        public virtual DbSet<SupplierSelectionSub> SupplierSelectionSubs { get; set; }
        public virtual DbSet<SPConsumptionAgIssue> SPConsumptionAgIssues { get; set; }
        public virtual DbSet<SPConsumptionMain> SPConsumptionMains { get; set; }
        public virtual DbSet<SPGRRMain> SPGRRMains { get; set; }
        public virtual DbSet<SPIssueMain> SPIssueMains { get; set; }
        public virtual DbSet<SPIssueReturnAgIssue> SPIssueReturnAgIssues { get; set; }
        public virtual DbSet<SPIssueReturnMain> SPIssueReturnMains { get; set; }
        public virtual DbSet<SPSRRMain> SPSRRMains { get; set; }
        public virtual DbSet<BondProduct> BondProducts { get; set; }
        public virtual DbSet<ConsumptionAgainstLC> ConsumptionAgainstLCs { get; set; }
        public virtual DbSet<ConsumptionSub> ConsumptionSubs { get; set; }
        public virtual DbSet<GRRSub> GRRSubs { get; set; }
        public virtual DbSet<IssueAgainstLC> IssueAgainstLCs { get; set; }
        public virtual DbSet<IssueReturnAgainstLC> IssueReturnAgainstLCs { get; set; }
        public virtual DbSet<IssueReturnSub> IssueReturnSubs { get; set; }
        public virtual DbSet<IssueSub> IssueSubs { get; set; }
        public virtual DbSet<PipelineSub> PipelineSubs { get; set; }
        public virtual DbSet<PRSub> PRSubs { get; set; }
        public virtual DbSet<ReceivedReturnSub> ReceivedReturnSubs { get; set; }
        public virtual DbSet<RemarksDetail> RemarksDetails { get; set; }
        public virtual DbSet<SPConsumptionAgainstLC> SPConsumptionAgainstLCs { get; set; }
        public virtual DbSet<SPConsumptionSub> SPConsumptionSubs { get; set; }
        public virtual DbSet<SPGRRSub> SPGRRSubs { get; set; }
        public virtual DbSet<SPIssueAgainstLC> SPIssueAgainstLCs { get; set; }
        public virtual DbSet<SPIssueReturnAgainstLC> SPIssueReturnAgainstLCs { get; set; }
        public virtual DbSet<SPIssueReturnSub> SPIssueReturnSubs { get; set; }
        public virtual DbSet<SPIssueSub> SPIssueSubs { get; set; }
        public virtual DbSet<SPSRRAgainstPlan> SPSRRAgainstPlans { get; set; }
        public virtual DbSet<SPSRRSub> SPSRRSubs { get; set; }
        public virtual DbSet<SRRAgainstPlan> SRRAgainstPlans { get; set; }
        public virtual DbSet<SRRSub> SRRSubs { get; set; }
        public virtual DbSet<SupplierProduct> SupplierProducts { get; set; }
        public virtual DbSet<SupplierSelectionMain> SupplierSelectionMains { get; set; }
        public virtual DbSet<TransferSub> TransferSubs { get; set; }
        public virtual DbSet<PSIssueReturnAgainstLC> PSIssueReturnAgainstLCs { get; set; }
    }
}
