//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(IssueAgainstLCMetaData))]
    public partial class IssueAgainstLC : IFIMSContext
    {
    	
        public IssueAgainstLC()
        {
            this.ConsumptionAgainstLCs = new HashSet<ConsumptionAgainstLC>();
            this.IssueReturnAgainstLCs = new HashSet<IssueReturnAgainstLC>();
        }
    
    	public IssueAgainstLC(IFIMSContext viewModel)
        {
            this.ConsumptionAgainstLCs = new HashSet<ConsumptionAgainstLC>();
            this.IssueReturnAgainstLCs = new HashSet<IssueReturnAgainstLC>();
    
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("IssueAgainstLC", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("IssueAgainstLC", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	public int IssueLCId { get; set; }
    	public int IssueId { get; set; }
    	public int IssueSubId { get; set; }
    	[Display(Name="ID")]
    	public int GRRSubId { get; set; }
    	public int LCFileId { get; set; }
    	public int FSCId { get; set; }
    	public int ProductId { get; set; }
    	public decimal Quantity { get; set; }
    	public short RowNo { get; set; }
    	public string Remarks { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual ICollection<ConsumptionAgainstLC> ConsumptionAgainstLCs { get; set; }
        public virtual IssueMain IssueMain { get; set; }
        public virtual IssueSub IssueSub { get; set; }
        public virtual ICollection<IssueReturnAgainstLC> IssueReturnAgainstLCs { get; set; }
    }
    
    //public class IssueAgainstLCMetaData
    //{
    //}
}
