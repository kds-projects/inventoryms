//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(CategoryVariantMetaData))]
    public partial class CategoryVariant : IFIMSContext
    {
        public CategoryVariant()
        {
        }
    
    	public CategoryVariant(IFIMSContext viewModel)
        {
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getPCDefaultValue("CategoryVariant", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	[Display(Name="ID")]
    	public int CatVarId { get; set; }
    	[Display(Name="Category")]
    	public short CategoryId { get; set; }
    	[Display(Name="Factory")]
    	public short FactoryId { get; set; }
    	[Display(Name="Variant")]
    	public short VariantId { get; set; }
    	[Display(Name="Active ?")]
    	public byte IsActive { get; set; }
    	[Display(Name="SL #")]
    	public short RankId { get; set; }
    	public short CreatedBy { get; set; }
    	public System.DateTime CreatedTime { get; set; }
    	public short UpdatedBy { get; set; }
    	public System.DateTime UpdateTime { get; set; }
    	public string PCName { get; set; }
    	public string IPAddress { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual ItemCategory ItemCategory { get; set; }
    }
}
