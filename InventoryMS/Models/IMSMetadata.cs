﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Resources;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Web.Mvc;


    public class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();

            base.OnResultExecuting(filterContext);
        }
    }

    public partial class InventoryCubeEntities
    {
        public InventoryCubeEntities()
            : base("name=InventoryCubeEntities")
        {
            //((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += ObjectContext_ObjectMaterialized;
            ((IObjectContextAdapter)this).ObjectContext.SavingChanges += ObjectContext_SavingChanges;
        }

        public void ObjectContext_ObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
            if (e.Entity == null)
                return;
            if (e.Entity.GetType() == typeof(IControllerHooks))
            {
                IControllerHooks IControllerHooks = e.Entity as IControllerHooks;
                // There is no problem in using Navigation Properties here
                // If they haven't been materialized yet, it will be done here, but only once.
                // Don't worry, there will be no stack overflow.

                //Teacher teacher = student.Teacher;
                //Course firstCourse = student.Courses.First();

                // Do whatever you want here with your entities.
            }
        }

        public void ObjectContext_SavingChanges(object sender, EventArgs e)
        {
            ObjectContext context = sender as ObjectContext;
            if (context != null)
            {
                // You can use other EntityState constants here
                foreach (ObjectStateEntry entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified))
                {
                    // You can handle multiple Entity Types here. I use Student as an example for the sake of simplicity
                    if (entry.Entity.GetType().GetInterfaces().Contains(typeof(IControllerHooks)))
                    {
                        IControllerHooks ControllerHooks = entry.Entity as IControllerHooks;
                        // Do whatever you want with your entities.
                        // You can throw an exception here if you want to
                        // prevent SaveChanges().
                        //if (student.Teacher == null)
                        //    throw Exception("Something went wrong.");
                        if (entry.State == EntityState.Added)
                        {
                            ControllerHooks.OnCreate();
                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            //foreach(string pname in this.Entry(entry.Entity).CurrentValues.PropertyNames)
                            //{
                            //    System.Diagnostics.Debug.WriteLine(pname + ": " + this.Entry(entry.Entity).Property(pname).CurrentValue);
                            //    //System.Diagnostics.Debug.WriteLine("A");
                            //    if (this.Entry(entry.Entity).Property(pname).CurrentValue == null)
                            //    {
                            //        this.Entry(entry.Entity).Property(pname).CurrentValue = 0;
                            //    }
                            //}
                            //this.Entry(entry.Entity).Property("EntryUserId").IsModified = false;
                            //this.Entry(entry.Entity).Property("EntryDate").IsModified = false;
                            //this.Entry(entry.Entity).Property("IPAddress").IsModified = false;
                            //this.Entry(entry.Entity).Property("PCName").IsModified = false;

                            //if (entry.Entity.GetType().GetProperty("EntryUserId") != null)
                            //    this.Entry(entry.Entity).Property("EntryUserId").CurrentValue = HttpContext.Current.Session["UserId"];
                            //if (entry.Entity.GetType().GetProperty("EntryDate") != null)
                            //    this.Entry(entry.Entity).Property("EntryDate").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                            //if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                            //    this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                            //if (entry.Entity.GetType().GetProperty("PCName") != null)
                            //    this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();

                            ControllerHooks.OnEdit(this.Entry(entry.Entity));
                        }
                    }
                    else if (entry.Entity.GetType().GetInterfaces().Contains(typeof(IFIMSContext)))
                    {
                        IFIMSContext FIMSContext = entry.Entity as IFIMSContext;
                        // Do whatever you want with your entities.
                        // You can throw an exception here if you want to
                        // prevent SaveChanges().
                        //if (student.Teacher == null)
                        //    throw Exception("Something went wrong.");
                        if (entry.State == EntityState.Added)
                        {
                            if (entry.Entity.GetType().GetProperty("EntryUserId") != null)
                                this.Entry(entry.Entity).Property("EntryUserId").CurrentValue = clsMain.getCurrentUser();
                            if (entry.Entity.GetType().GetProperty("EntryDate") != null)
                                this.Entry(entry.Entity).Property("EntryDate").CurrentValue = clsMain.getCurrentTime();
                            if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                                this.Entry(entry.Entity).Property("CreatedBy").CurrentValue = clsMain.getCurrentUser();
                            if (entry.Entity.GetType().GetProperty("CreatedTime") != null)
                                this.Entry(entry.Entity).Property("CreatedTime").CurrentValue = clsMain.getCurrentTime();
                            if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                                this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                            if (entry.Entity.GetType().GetProperty("PCName") != null)
                                this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();
                            if (entry.Entity.GetType().GetProperty("UpdateTime") != null)
                                this.Entry(entry.Entity).Property("UpdateTime").CurrentValue = DateTime.Parse("01-Jan-1900");
                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            //foreach(string pname in this.Entry(entry.Entity).CurrentValues.PropertyNames)
                            //{
                            //    System.Diagnostics.Debug.WriteLine(pname + ": " + this.Entry(entry.Entity).Property(pname).CurrentValue);
                            //    //System.Diagnostics.Debug.WriteLine("A");
                            //    if (this.Entry(entry.Entity).Property(pname).CurrentValue == null)
                            //    {
                            //        this.Entry(entry.Entity).Property(pname).CurrentValue = 0;
                            //    }
                            //}
                            //this.Entry(entry.Entity).Property("EntryUserId").IsModified = false;
                            //this.Entry(entry.Entity).Property("EntryDate").IsModified = false;
                            //this.Entry(entry.Entity).Property("IPAddress").IsModified = false;
                            //this.Entry(entry.Entity).Property("PCName").IsModified = false;

                            //if (entry.Entity.GetType().GetProperty("EntryUserId") != null)
                            //    this.Entry(entry.Entity).Property("EntryUserId").CurrentValue = HttpContext.Current.Session["UserId"];
                            //if (entry.Entity.GetType().GetProperty("EntryDate") != null)
                            //    this.Entry(entry.Entity).Property("EntryDate").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                            //if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                            //    this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                            //if (entry.Entity.GetType().GetProperty("PCName") != null)
                            //    this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();

                            //ControllerHooks.OnEdit(this.Entry(entry.Entity));
                        }
                    }
                    //else
                    //{
                    //    if (entry.State == EntityState.Added)
                    //    {
                    //        if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                    //            this.Entry(entry.Entity).Property("CreatedBy").CurrentValue = HttpContext.Current.Session["UserId"];
                    //        if (entry.Entity.GetType().GetProperty("CreatedTime") != null)
                    //            this.Entry(entry.Entity).Property("CreatedTime").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                    //        if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                    //            this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                    //        if (entry.Entity.GetType().GetProperty("PCName") != null)
                    //            this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();
                    //    }
                    //    else if (entry.State == EntityState.Modified)
                    //    {

                    //        if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                    //            this.Entry(entry.Entity).Property("CreatedBy").IsModified = false;
                    //        if (entry.Entity.GetType().GetProperty("CreatedTime") != null)
                    //            this.Entry(entry.Entity).Property("CreatedTime").IsModified = false;
                    //        if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                    //            this.Entry(entry.Entity).Property("IPAddress").IsModified = false;
                    //        if (entry.Entity.GetType().GetProperty("PCName") != null)
                    //            this.Entry(entry.Entity).Property("PCName").IsModified = false;

                    //        if (entry.Entity.GetType().GetProperty("UpdatedBy") != null)
                    //            this.Entry(entry.Entity).Property("UpdatedBy").CurrentValue = HttpContext.Current.Session["UserId"];
                    //        if (entry.Entity.GetType().GetProperty("UpdateTime") != null)
                    //            this.Entry(entry.Entity).Property("UpdateTime").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                    //    }
                    //}
                }
            }
        }
    }

    public partial class ProductCubeEntities : DbContext
    {
        public IQueryable<Factory> UserFactories
        {
            get
            {
                var facid = clsMain.getEntity().Sys_User_Name.Find(clsMain.getCurrentUser()).FactoryId.Split(new string[] { "," }, StringSplitOptions.None).Where(l => !String.IsNullOrEmpty(l)).Select(c => int.Parse(c)).ToList();
                if (facid.Count > 0)
                    return Factories.Where(l => facid.Contains(l.FactoryId));
                else
                    return Factories.AsQueryable();
            }
        }
    }

    public enum UserStatus { Success = 1, LockedOut = 2, Failure = 0 };
    public partial class UserView
    {
        public UserView() 
        {
        }

        public UserView(Sys_User_Name user) 
        {
            UserId = user.UserId;
            UserName = user.UserName;
            UserPWD = user.UserPWD;
            IsAdmin = user.UserTypeId;
        }

        public short UserId { get; set; }
        public string UserName { get; set; }
        public string UserPWD { get; set; }
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public bool IsRemember { get; set; }
        public bool IsLock { get; set; }
        public short IsAdmin { get; set; }
        public Bond bond { get; set; }
    }

    public partial class Sys_User_Name : IControllerHooks
    {
        //public Sys_User_Name()
        //{
            //SOFDate = DateTime.Now;
            //Remarks = "";
            //EntryDate = DateTime.Now;
            //RevisionDate = DateTime.Parse("01-Jan-1900");
            //PCName = System.Net.Dns.GetHostName();
            //IPAddress = "0.0.0.0";
        //}

        public void OnCreate()
        {
            //GPHEntities db = new GPHEntities();
        }

        public void OnEdit(DbEntityEntry entity)
        {
            if(UserPWD.Length == 0)
                entity.Property("UserPWD").IsModified = false;
        }
    }

    public class clsReport
    {
        [Display(Name = "Factory")]
        public int FactoryId { get; set; }
        [Display(Name = "Supplier")]
        public int SupplierId { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        [Display(Name = "Product")]
        public int ItemId { get; set; }
        [Display(Name = "Material Type")]
        public int MaterialTypeId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime AsOnDate { get; set; }

        public clsReport()
        {
            FromDate = ToDate = AsOnDate = DateTime.Today;
        }
    }

    public partial class PRMain : IControllerHooks
    {
        public void OnCreate()
        {
            
        }

        public void OnEdit(DbEntityEntry entity)
        {
            //entity.Property("CreatedTime").CurrentValue = clsMain.getCurrentTime();
        }
    }
}