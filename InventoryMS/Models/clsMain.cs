﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using InventoryMS.Controllers;
using System.Text.RegularExpressions;
using System.Data.Entity.Infrastructure;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace InventoryMS.Models
{
    [CAuthorize]
    public static class clsMain
    {
        public static bool IsInValidDate(DateTime checkDate)
        {
            InventoryCubeEntities db = new InventoryCubeEntities();
            List<Int16> UserList = new List<Int16> { 2, 7, 12 }; //noman, luna, ilias

            //if (UserList.Contains(Int16.Parse(HttpContext.Current.Session["UserId"].ToString())) || checkDate >= db.CustomParameters.FirstOrDefault().LockDate)
            //    return false;
            //else
                return true;
        }

        public static bool IsPermitted(string Controller, string Action)
        {
            InventoryCubeEntities db = new InventoryCubeEntities();
            List<Int16> UserList = new List<Int16> { 2, 7, 12 }; //noman, luna, ilias

            //if (UserList.Contains(Int16.Parse(HttpContext.Current.Session["UserId"].ToString())) || checkDate >= db.CustomParameters.FirstOrDefault().LockDate)
            //    return false;
            //else
            return true;
        }

        public static bool IsActionEnabled(string Controller, string Action)
        {
            InventoryCubeEntities db = new InventoryCubeEntities();
            List<Int16> UserList = new List<Int16> { 2, 7, 12 }; //noman, luna, ilias

            var m = (from p in db.User_Permission.AsEnumerable()
                     join q in db.User_Role_Details.AsEnumerable() on p.RoleId equals q.RoleId into r
                     from s in r.DefaultIfEmpty()
                     //join t in db.MenuWiseActions.AsEnumerable() on (s == null ? p.ActionId : s.ActionId) equals t.ActionId
                     join u in db.User_Controller.AsEnumerable() on (s == null ? p.ControllerId : s.ControllerId) equals u.ControllerId
                     join v in db.User_Action.AsEnumerable() on (s == null ? p.ActionId : s.ActionId) equals v.ActionId
                     where p.UserId == clsMain.getCurrentUser() && u.ControllerName == Controller && v.ActionName == Action
                     select p).Distinct().ToList();


            if (m == null || m.Count() == 0)
                return false;
            else
                return true;
        }

        public static string Decrypt(string textToDecrypt)
        {
            try
            {
                string ToReturn = "";
                string publickey = "12345678";
                string secretkey = "87654321";
                byte[] privatekeyByte = { };
                privatekeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = new byte[textToDecrypt.Replace(" ", "+").Length];
                inputbyteArray = Convert.FromBase64String(textToDecrypt.Replace(" ", "+"));
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateDecryptor(publickeybyte, privatekeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    Encoding encoding = Encoding.UTF8;
                    ToReturn = encoding.GetString(ms.ToArray());
                }
                return ToReturn;
            }
            catch (Exception ae)
            {
                throw new Exception(ae.Message, ae.InnerException);
            }
        }

        public static string getEmail(string strmail)
        {
            int o;
            var vmails = strmail.Split(new string[]{","}, StringSplitOptions.None).Where(l => int.TryParse(l, out o)).Select(c => short.Parse(c)).ToList();
            var ivmails = strmail.Split(new string[] { "," }, StringSplitOptions.None).Where(l => !int.TryParse(l, out o)).Select(c => c.Replace("@UserId", getCurrentUserName())).ToList();
            var mails = getEntity().Sys_User_Name.Where(l => vmails.Contains(l.UserId)).Select(c => c.eMail).Union(ivmails).ToList().Aggregate((i, j) => i + "; " + j);

            return mails;
        }

        public static IQueryable<Factory> getAccessFactory()
        {
            var facid = getEntity().Sys_User_Name.Find(getCurrentUser()).FactoryId.Split(new string[]{","}, StringSplitOptions.None).Select(c => int.Parse(c)).ToList();
            var factory = (new ProductCubeEntities()).Factories.Where(l => facid.Contains(l.FactoryId));

            return factory;
        }

        public static InventoryCubeEntities getEntity()
        {
            return new InventoryCubeEntities();
        }

        public static DateTime getCurrentTime()
        {
            return getEntity().Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault(); 
        }

        public static short getCurrentUser()
        {
            return Int16.Parse(HttpContext.Current.Session["UserId"] != null ? HttpContext.Current.Session["UserId"].ToString() : "0");
        }

        public static string getCurrentUserName()
        {
            return HttpContext.Current.Session["UserName"].ToString();
        }

        public static string GetUser_IP()
        {
            HttpContext httpContext = HttpContext.Current;
            string VisitorsIPAddr = string.Empty;
            if (httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (httpContext.Request.ServerVariables["REMOTE_ADDR"] != null)
            {
                VisitorsIPAddr = httpContext.Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            else if (httpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = httpContext.Request.UserHostAddress;
            }

            if (VisitorsIPAddr.Equals("::1"))
            {
                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                VisitorsIPAddr = host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault().ToString();
            }
            //IPHostEntry ipEntry = System.Net.Dns.GetHostEntry("Win8N");
            //IPAddress[] addr = ipEntry.AddressList;
            return VisitorsIPAddr;
        }

        public static string GetUser_PCName()
        {
            string CName = "";
            try
            {
                CName = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                CName = "INTERNET";
            }
            catch (Exception ex)
            {
                CName = "=N/A=";
            }

            return CName;
        }

        public static string getExternalIp()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }

        public static void prcLoginReport(ref ReportDocument reportDocument, bool IsLoginRequired)
        {
            clsServerConfig ServerConfig = clsMain.GetReportLogon();

            if (IsLoginRequired)
            {
                ConnectionInfo rptConnection = new ConnectionInfo();
                rptConnection.ServerName = ServerConfig.Server;
                rptConnection.DatabaseName = ServerConfig.Database;
                rptConnection.UserID = ServerConfig.UserId;
                rptConnection.Password = ServerConfig.Password;

                foreach (ReportDocument subRpt in reportDocument.Subreports)
                {
                    foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in subRpt.Database.Tables)
                    {
                        TableLogOnInfo logInfo = crTable.LogOnInfo;
                        logInfo.ConnectionInfo = rptConnection;
                        crTable.ApplyLogOnInfo(logInfo);
                    }
                }

                foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in reportDocument.Database.Tables)
                {
                    TableLogOnInfo loi = crTable.LogOnInfo;
                    loi.ConnectionInfo = rptConnection;
                    crTable.ApplyLogOnInfo(loi);
                }
            }
        }

        internal static clsServerConfig GetReportLogon()
        {
            clsServerConfig ServerConfig = new clsServerConfig();
            ServerConfig.Server = "203.202.249.115,14333";
            ServerConfig.UserId = "sa";
            ServerConfig.Password = "#GPH-S/shark@115sdk";
            ServerConfig.Database = "GPH";

            return ServerConfig;
        }

        public static object getDefaultValue(string TableName, string PropertyName, Type type)
        {
            using (InventoryCubeEntities db = new InventoryCubeEntities())
            {
                object obj = null;
                string DefaultValue = db.viewDefaultLists.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("(getdate())"))
                            obj = (new InventoryMS.Models.Old.LCManagementEntities()).CustomParameters.FirstOrDefault().CurrentDate;
                        else if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                            obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }

                return obj;
            }
        }

        public static object getDefaultValue_Fund(string TableName, string PropertyName, Type type)
        {
            using (FundContext db = new FundContext())
            {
                object obj = null;
                var viewDefaultLists = db.Database.SqlQuery<clsDefaultValue>("Select * From viewDefaultList").ToList();
                string DefaultValue = viewDefaultLists.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                        {
                            var dateValue = db.Database.SqlQuery(typeof(DateTime), "Select Cast(" + DefaultValue.Substring(1, DefaultValue.Length - 2) + " As DateTime)", "").Cast<DateTime>().ToList().FirstOrDefault();
                            obj = dateValue;//DateTime.Parse();
                        }
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }

                return obj;
            }
        }

        public static object getPCDefaultValue(string TableName, string PropertyName, Type type)
        {
            using (ProductCubeEntities db = new ProductCubeEntities())
            {
                object obj = null;
                string DefaultValue = db.viewDefaultLists.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("(getdate())"))
                            obj = (new InventoryMS.Models.Old.LCManagementEntities()).CustomParameters.FirstOrDefault().CurrentDate;
                        else if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                            obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }

                return obj;
            }


        }

        public static object getDefaultValueLC(string TableName, string PropertyName, Type type)
        {
            using (InventoryMS.Models.Old.LCManagementEntities db = new InventoryMS.Models.Old.LCManagementEntities())
            {
                object obj = null;
                string DefaultValue = db.viewDefaultList_LC.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("(getdate())"))
                            obj = db.CustomParameters.FirstOrDefault().CurrentDate;
                        else if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                            obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }

                return obj;
            }
        }

        public static object getDefaultValueEMC(string TableName, string PropertyName, Type type)
        {
            using (EMailCubeEntities db = new EMailCubeEntities())
            {
                object obj = null;
                string DefaultValue = db.viewDefaultList_EMC.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("(getdate())"))
                            obj = (new InventoryMS.Models.Old.LCManagementEntities()).CustomParameters.FirstOrDefault().CurrentDate;
                        else if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                            obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }

                return obj;
            }
        }

        public static object getDescription(string TableName, string PropertyName)
        {
            //using (InventoryCubeEntities db = new InventoryCubeEntities())
            //{
            //    return db.viewDecriptionLists.Where(l => l.TableName == TableName && l.ColumnName == PropertyName).Select(l => "[Diplay(name=\"" + "" + "\")]").FirstOrDefault();
            //}

            return "";
        }
    }

    public class PagedData<T> where T : class
    {
        public IEnumerable<T> Data { get; set; }
        public int NumberOfPages { get; set; }
        public int CurrentPage { get; set; }
    }

    public interface IControllerHooks
    {
        void OnCreate();
        void OnEdit(DbEntityEntry entity);
    }

    public interface IFIMSContext
    {

    }

    public class PersistPropertyOnEdit : Attribute
    {
        public readonly bool PersistPostbackDataFlag;
        public PersistPropertyOnEdit(bool persistPostbackDataFlag)
        {
            this.PersistPostbackDataFlag = persistPostbackDataFlag;
        }
    }

    public class clsServerConfig
    {
        public string Server { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
    }
}