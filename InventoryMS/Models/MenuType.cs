//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(MenuTypeMetaData))]
    public partial class MenuType : IFIMSContext
    {
    	
        public MenuType()
        {
            this.User_Controller = new HashSet<User_Controller>();
            this.User_Role_Details = new HashSet<User_Role_Details>();
        }
    
    	public MenuType(IFIMSContext viewModel)
        {
            this.User_Controller = new HashSet<User_Controller>();
            this.User_Role_Details = new HashSet<User_Role_Details>();
    
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("MenuType", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("MenuType", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	public short MenuTypeId { get; set; }
    	public string MenuTypeName { get; set; }
    	public string MenuTypeKey { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual ICollection<User_Controller> User_Controller { get; set; }
        public virtual ICollection<User_Role_Details> User_Role_Details { get; set; }
    }
    
    //public class MenuTypeMetaData
    //{
    //}
}
