//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(NewAreaMetaData))]
    public partial class NewArea : IFIMSContext
    {
    	
        public NewArea()
        {
        }
    
    	public NewArea(IFIMSContext viewModel)
        {
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("NewArea", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("NewArea", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	public short AreaId { get; set; }
    	public string AreaName { get; set; }
    	public short UserId { get; set; }
    	public short SCId { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    }
    
    //public class NewAreaMetaData
    //{
    //}
}
