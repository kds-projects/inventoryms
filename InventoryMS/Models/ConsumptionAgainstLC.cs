//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(ConsumptionAgainstLCMetaData))]
    public partial class ConsumptionAgainstLC : IFIMSContext
    {
    	
        public ConsumptionAgainstLC()
        {
        }
    
    	public ConsumptionAgainstLC(IFIMSContext viewModel)
        {
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("ConsumptionAgainstLC", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ConsumptionAgainstLC", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	public int ConsumptionLCId { get; set; }
    	public int ConsumptionId { get; set; }
    	public int ConsumptionSubId { get; set; }
    	[Display(Name="ID")]
    	public int GRRSubId { get; set; }
    	[Display(Name="ID")]
    	public int IssueSubId { get; set; }
    	[Display(Name="ID")]
    	public int IssueLCId { get; set; }
    	public int LCFileId { get; set; }
    	public int FSCId { get; set; }
    	public int ProductId { get; set; }
    	public decimal Quantity { get; set; }
    	public short RowNo { get; set; }
    	public string Remarks { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual ConsumptionMain ConsumptionMain { get; set; }
        public virtual ConsumptionSub ConsumptionSub { get; set; }
        public virtual IssueAgainstLC IssueAgainstLC { get; set; }
        public virtual IssueSub IssueSub { get; set; }
    }
    
    //public class ConsumptionAgainstLCMetaData
    //{
    //}
}
