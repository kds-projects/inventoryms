﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.ComponentModel;
using System.Data.Entity;

namespace InventoryMS.Models
{
    public static class clsConverter
    {
        static clsConverter()
        {
            
        }

        public static object Convert(this Object obj, object viewModel)
        {
            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                PropertyInfo pii = viewModel.GetType().GetProperty(pi.Name);

                if (pii != null)
                {
                    if (pii.CanWrite)
                    {
                        pii.SetValue(viewModel, pi.GetValue(obj));
                    }
                }
            }

            return viewModel;
        }
        public static object ConvertNotNull(this Object obj, object viewModel)
        {
            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                PropertyInfo pii = viewModel.GetType().GetProperty(pi.Name);

                if (pii != null && pi.GetValue(obj) != null)
                {
                    if (pii.CanWrite)
                    {
                        pii.SetValue(viewModel, pi.GetValue(obj));
                    }
                }
            }

            return viewModel;
        }
        //public static object UserAccess(this Object obj, object newobj)
        //{
        //    var facid = clsMain.getEntity().Sys_User_Name.Find(clsMain.getCurrentUser()).FactoryId.Split(new string[] { "," }, StringSplitOptions.None).Select(c => int.Parse(c)).ToList();
        //    var factory = ((DbSet)obj).AsQueryable

        //    return viewModel;
        //}
    }

    public partial class clsDefaultValue
    {
        public string Table { get; set; }
        public string Column { get; set; }
        public string DefaultValue { get; set; }
    }

    public class FactoryVariant : IFIMSContext
    {
        public string SLNo { get; set; }
        public string FIDs { get; set; }
        public string SFactory { get; set; }
        public string VIDs { get; set; }
        public string SVariant { get; set; }
    }

    public class Category : IFIMSContext
    {
        public Category()
        {
        }

        [Key]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Name")]
        public string CategoryName { get; set; }
        [Display(Name = "Prefix")]
        public string Prefix { get; set; }
        [Display(Name = "P. ID")]
        public Nullable<short> ParentId { get; set; }
        [Display(Name = "Level")]
        public Nullable<byte> StageLevel { get; set; }
        [Display(Name = "Total Child")]
        public Nullable<short> ChildernCount { get; set; }
        [Display(Name = "Default Factory")]
        public Nullable<short> DefaultFactoryId { get; set; }
        //[Display(Name = "Default Cost Centre")]
        //public Nullable<short> DefaultCCId { get; set; }
        [Display(Name = "Measure Unit")]
        public string[] MUnitList { get; set; }
        [Display(Name = "Quantity Unit")]
        public string[] QUnitList { get; set; }
        [Display(Name = "Default Measure Unit")]
        public Nullable<short> DefaultMUnitId { get; set; }
        [Display(Name = "Default Quantity Unit")]
        public Nullable<short> DefaultQUnitId { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<byte> IsActive { get; set; }
        //public string tableContent { get; set; }
        //[Display(Name = "Factory")]
        //public ICollection<string[]> FactoryList { get; set; }
        //[Display(Name = "Variant")]
        //public ICollection<string[]> VariantList { get; set; }
        public string MeasureUnitList
        {
            get
            {
                return (MUnitList == null ? "" : MUnitList.Aggregate((i, j) => i + ", " + j));
            }
            set
            {
                MUnitList = value.Replace(", ", ",").Split(new string[] { "," }, StringSplitOptions.None);
            }
        }
        public string QuantityUnitList
        {
            get
            {
                return (QUnitList == null ? "" : QUnitList.Aggregate((i, j) => i + ", " + j));
            }
            set
            {
                QUnitList = value.Replace(", ", ",").Split(new string[] { "," }, StringSplitOptions.None);
            }
        }

        public ICollection<FactoryVariant> factoryVariant { get; set; }
    }

    public partial class CategoryVariantView : IFIMSContext
    {
        public CategoryVariantView()
        {
        }

        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Factory")]
        public Nullable<short> FactoryId { get; set; }
        [Display(Name = "Variant")]
        public Nullable<short> VariantId { get; set; }
        //[Display(Name = "Active ?")]
        //public Nullable<byte> IsActive { get; set; }
    }

    public partial class BondView : IFIMSContext
    {
        public BondView()
        {
            IsActive = 0;
        }

        [Display(Name = "ID")]
        public Nullable<short> BondId { get; set; }
        [Display(Name = "Code")]
        public string BondCode { get; set; }
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        [Display(Name = "Company")]
        public Nullable<short> CompanyId { get; set; }
    	[Display(Name="Location")]
        public Nullable<short> LocationId { get; set; }
        [Display(Name = "Location")]
        public string LocationName { get; set; }
    	[Display(Name="License #")]
        public string LicenseNumber { get; set; }
        [Display(Name = "License Date")]
        public Nullable<System.DateTime> LicenseDate { get; set; }
        [Display(Name = "License Renew Date"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> LicenseRenewalDate { get; set; }
    	[Display(Name="License Period")]
        public string LicensePeriod
        {
            get {
                return "";
            }
            set
            {
                _lp = value;
            }
        }
        [Display(Name = "Start Date"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Display(Name = "End Date"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Display(Name = "Warehouse Capacity")]
        public Nullable<decimal> Capacity { get; set; }
        [Display(Name = "Capacity Unit")]
        public Nullable<short> CapacityUnitId { get; set; }
        [Display(Name = "Capacity Unit")]
        public string CapacityUnit { get; set; }
    	[Display(Name="Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<byte> IsActive { get; set; }
        private string _lp;

        public ICollection<BondPeriodView> bondPeriodView { get; set; }
    }

    public partial class BondPeriodView : IFIMSContext
    {
        public BondPeriodView()
        {
            IsActive = 0;
        }

        [Display(Name = "License #")]
        public short BondId { get; set; }
        [Display(Name = "ID")]
        public int BondPeriodId { get; set; }
        [Display(Name = "License Period")]
        public string LicensePeriod
        {
            get
            {
                return StartDate.Value.ToString("dd-MMM-yyyy") + " - " + EndDate.Value.ToString("dd-MMM-yyyy");
            }
            set
            {
                _lp = value;
            }
        }
        [Display(Name = "Start Date")]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Display(Name = "End Date")]
        public Nullable<System.DateTime> EndDate { get; set; }
        public bool BondCapacityCheck { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "SL #")]
        public Nullable<short> RankId { get; set; }
        private string _lp;
        public bool _editStatus;
    }

    public partial class BondCapacityView : IFIMSContext
    {
        [Display(Name = "ID")]
        public Nullable<int> CapacityId { get; set; }
        [Display(Name = "License #")]
        public short BondId { get; set; }
        [Display(Name = "License Period")]
        public int BondPeriodId { get; set; }
        [Display(Name = "License #")]
        public string LicenseNumber { get; set; }
        [Display(Name = "Bond Serial #"), Required]
        public short BondSerialNo { get; set; }
        [Display(Name = "Raw Material"), DataType(DataType.MultilineText)][AllowHtml]
        public string RMTitle { get; set; }
        [Display(Name = "HS Code"), DataType(DataType.MultilineText)]
        public string HSCode { get; set; }
        [Display(Name = "Import Period")]
        public string LicensePeriod
        {
            get
            {
                return StartDate.Value.ToString("dd-MMM-yyyy") + " - " + EndDate.Value.ToString("dd-MMM-yyyy");
            }
            set
            {
                _lp = value;
            }
        }
        [Display(Name = "Start Date"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Display(Name = "End Date"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Display(Name = "Capacity")]
        public Nullable<decimal> Capacity { get; set; }
        [Display(Name = "Capacity Unit")]
        public Nullable<short> CapacityUnitId { get; set; }
        [Display(Name = "Capacity Unit")]
        public string CapacityUnit { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<byte> IsActive { get; set; }
        private string _lp;
    }

    public partial class BondProductView : IFIMSContext
    {
        [Display(Name = "SL #")]
        public Nullable<int> BSPId { get; set; }
        [Display(Name = "Capacity #")]
        public Nullable<int> CapacityId { get; set; }
        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Material")]
        public Nullable<int> ProductId { get; set; }
        [Display(Name = "Material")]
        public string ProductName { get; set; }
    }

    public class LocationView : IFIMSContext
    {
        [Display(Name = "ID")]
        public Nullable<short> LocationId { get; set; }
        [Display(Name = "Name")]
        public string LocationName { get; set; }
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }
        [Display(Name = "Prefix")]
        public string Prefix { get; set; }
        [Display(Name = "Details")]
        public string Details { get; set; }
        [Display(Name = "Country")]
        public Nullable<short> CountryId { get; set; }
        [Display(Name = "Country")]
        public string CountryName { get; set; }

        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<byte> IsActive { get; set; }
    }
    
    public class CountryView : IFIMSContext
    {
        public Nullable<short> CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string ShortName { get; set; }
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
    }

    public class CompanyView : IFIMSContext
    {
        public CompanyView()
        {

        }

        [Display(Name="ID")]
        public Nullable<short> CompanyId { get; set; }
        [Display(Name = "Code")]
        public string CompanyCode { get; set; }
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }
        [Display(Name = "Name")]
        public string CompanyName { get; set; }
        [Display(Name = "Address")]
        public string CompanyAddress { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<byte> IsActive { get; set; }
    }

    public class FactoryView : IFIMSContext
    {
        public FactoryView()
        {
        }

        [Display(Name = "Factory")]
        public Nullable<short> FactoryId { get; set; }
        [Display(Name = "Code")]
        public string FactoryCode { get; set; }
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }
        [Display(Name = "Factory")]
        public string FactoryName { get; set; }
        [Display(Name = "Company")]
        public Nullable<short> CompanyId { get; set; }
        [Display(Name = "Company")]
        public string CompanyName { get; set; }
        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
    }

    public class VariantView : IFIMSContext
    {
        public VariantView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<short> VariantId { get; set; }
        [Display(Name = "Name")]
        public string VariantName { get; set; }
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }
        [Display(Name = "Prefix")]
        public string Prefix { get; set; }
        [Display(Name = "Suffix")]
        public string Suffix { get; set; }
        [Display(Name = "P. Variant")]
        public Nullable<short> PVariantId { get; set; }
        [Display(Name = "P. Variant")]
        public string ParentName { get; set; }
        [Display(Name = "Text Type")]
        public Nullable<short> VariantTextType { get; set; }
        [Display(Name = "Input Type")]
        public Nullable<short> VariantInputType { get; set; }
        [Display(Name = "Active ?")]
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
    }

    public class VariantValueView : IFIMSContext
    {
        public VariantValueView()
        {
        }
        
        [Display(Name = "ID")]
        public Nullable<short> VarValueId { get; set; }
        [Display(Name = "Variant")]
        public Nullable<short> VariantId { get; set; }
        [Display(Name = "Variant")]
        public string VariantName { get; set; }
        [Display(Name = "Value")]
        public string VariantValue { get; set; }
        [Display(Name = "Active ?")]
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
    }

    public class SupplierView : IFIMSContext
    {
        public SupplierView()
        {
            BusinessStartDate = DateTime.Today; LeadTime = 0;
            Manpower = 0; AnnualRevenue = 0; FoundingYear = 2000;
        }

        [Display(Name = "ID")]
        public Nullable<short> SupplierId { get; set; }
        [Display(Name = "Code")]
        public string SupplierCode { get; set; }
        [Display(Name = "Name"), Required, DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SupplierName { get; set; }
        [Display(Name = "Short Name"), Required, DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ShortName { get; set; }
        [Display(Name = "Manuf. Name"), Required, DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ManufacturerName { get; set; }
        [Display(Name = "TIN"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string TINNo { get; set; }
        [Display(Name = "VAT"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string VATNo { get; set; }
        [Display(Name = "BIN"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string BINNo { get; set; }
        [Required, Display(Name = "Inc/Reg No"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string RegistrationNo { get; set; }
        [Display(Name = "Origin")]
        public Nullable<short> OriginCountryId { get; set; }
        [Display(Name = "Found. Year")]
        public Nullable<short> FoundingYear { get; set; }
        [Display(Name = "Business Type"), Required]
        public Nullable<short> BusinessTypeId { get; set; }
        [Display(Name = "Supplier Type"), Required]
        public Nullable<short> PurchaseTypeId { get; set; }
        [Display(Name = "Nationality")]
        public Nullable<short> CitizenshipId { get; set; }
        [Display(Name = "Legal Structure")]
        public Nullable<short> LegalStructureId { get; set; }
        [Display(Name = "House #"), Required, DisplayFormat(ConvertEmptyStringToNull = false)]
        public string HouseNo { get; set; }
        [Display(Name = "City"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string City { get; set; }
        [Display(Name = "State"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string State { get; set; }
        [Display(Name = "ZIP/Post"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ZIPCode { get; set; }
        [Display(Name = "Country"), Required]
        public Nullable<short> AddressCountryId { get; set; }
        [Display(Name = "Phone"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneNo { get; set; }
        [Display(Name = "Mobile"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MobileNo { get; set; }
        [Display(Name = "Fax"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Fax { get; set; }
        [Required, Display(Name = "E-Mail"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string eMailAddress { get; set; }
        [Display(Name = "Web"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string WebAddress { get; set; }
        [Display(Name = "Payment Address"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PaymentAddress { get; set; }
        [Display(Name = "Local Address"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LocalAddress { get; set; }
        [Display(Name = "Manpower")]
        public Nullable<short> Manpower { get; set; }
        [Display(Name = "Annual Revenue")]
        public Nullable<decimal> AnnualRevenue { get; set; }
        [Display(Name = "Score")]
        public decimal RatingScore { get; set; }
        [Display(Name = "Certificates"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CertificatesAvail { get; set; }
        [Display(Name = "Awards"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AwardAvail { get; set; }
        [Display(Name = "Pref. Port")]
        public Nullable<short> PreferredPortId { get; set; }
        [Display(Name = "Auto Mail")]
        public Nullable<byte> IsAutoMailEnabled { get; set; }
        [Display(Name = "Auto Mail")]
        public bool AutoMailEnabled { get { return (IsAutoMailEnabled == 1 ? true : false); } set { IsAutoMailEnabled = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Auto SMS")]
        public Nullable<byte> IsAutoSMSEnabled { get; set; }
        [Display(Name = "Auto SMS")]
        public bool AutoSMSEnabled { get { return (IsAutoSMSEnabled == 1 ? true : false); } set { IsAutoSMSEnabled = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Bs. Start"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> BusinessStartDate { get; set; }
        [Display(Name = "KDS Contact"), Required]
        public Nullable<short> SupervisorUserId { get; set; }
        [Display(Name = "Lead Time"), Required]
        public Nullable<short> LeadTime { get; set; }
        [Display(Name = "Brand"), Required]
        public Nullable<short> BrandId { get; set; }
        [Display(Name = "Referred By"), Required]
        public string SupplierReferredBy { get; set; }
        [Display(Name = "Negotiation By"), Required]
        public string SupplierNegotiationBy { get; set; }
        [Display(Name = "Active")]
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Active")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }

        [Display(Name = "Imp. Item"), Required]
        public string CategoryList { get; set; }

        [Display(Name = "Factory"), Required]
        public string[] FactoryList { get; set; }

        public Nullable<short> ApprovedType { get; set; }
        public Nullable<short> ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedTime { get; set; }

        public Nullable<bool> is_edit_for_approval { get; set; }

        public ICollection<SupplierContactView> supplierContactView { get; set; }
        public ICollection<SupplierProductView> supplerProductView { get; set; }
    }

    public class SupplierContactView : IFIMSContext
    {
        public SupplierContactView()
        {
        }

    	[Display(Name="ID")]
        public Nullable<int> SContactId { get; set; }
    	[Display(Name="Supplier")]
        public Nullable<short> SupplierId { get; set; }
        [Display(Name = "Name"), Required]
        public string Name { get; set; }
        [Display(Name = "Designation"), Required]
        public string Designation { get; set; }
        [Display(Name = "Phone"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneNo { get; set; }
        [Display(Name = "Mobile"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MobileNo { get; set; }
        [Display(Name = "E-Mail"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string eMail { get; set; }
    	[Display(Name="Auto Mail")]
        public Nullable<byte> IsAutoMailReceived { get; set; }
        [Display(Name = "Auto Mail")]
        public bool AutoMailReceived { get { return (IsAutoMailReceived == 1 ? true : false); } set { IsAutoMailReceived = Byte.Parse(value ? "1" : "0"); } }
    	[Display(Name="Active")]
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Type")]
        public Nullable<short> TypeId { get; set; }
        [Display(Name = "Type")]
        public string TypeName { get; set; }
        [Display(Name = "Active")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public Nullable<short> RankId { get; set; }
        public bool _editStatus;
    }

    public class SupplierProductView : IFIMSContext
    {
        public SupplierProductView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<int> SProductId { get; set; }
        [Display(Name = "Supplier")]
        public Nullable<short> SupplierId { get; set; }
        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Default")]
        public Nullable<byte> IsDefault { get; set; }
        [Display(Name = "Active")]
        public Nullable<byte> IsActive { get; set; }
        [Display(Name = "Active")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public bool _editStatus;
    }

    public partial class SupplierFactoryView : IFIMSContext
    {
        public SupplierFactoryView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<int> SupplierFactoryId { get; set; }
        [Display(Name = "Product")]
        public Nullable<short> SupplierId { get; set; }
        [Display(Name = "Factory")]
        public Nullable<short> FactoryId { get; set; }
    }

    public class ItemView : IFIMSContext
    {
        public ItemView()
        {

        }

        [Display(Name = "ID")]
        public Nullable<int> ItemId { get; set; }
        [Display(Name = "Code")]
        public string ItemCode { get; set; }
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }
        [Display(Name = "Name")]
        public string ItemName { get; set; }
        [Display(Name = "Systemic Name")]
        public string SystemName { get; set; }
        [Display(Name = "HS Code")]
        public string HSCode { get; set; }
        [Display(Name = "Bond Name")]
        public string BondName { get; set; }
        [Display(Name = "Prefix")]
        public string Prefix { get; set; }
        [Display(Name = "Variant String")]
        public string VariantString { get; set; }
        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Factory(s)")]
        public string[] FList { get; set; }
        [Display(Name = "Default Factory")]
        public Nullable<short> DefaultFactoryId { get; set; }
        //[Display(Name = "Default Cost Centre")]
        //public Nullable<short> DefaultCCId { get; set; }
        [Display(Name = "Measure Unit")]
        public string[] MUnitList { get; set; }
        [Display(Name = "Quantity Unit")]
        public string[] QUnitList { get; set; }
        [Display(Name = "Def. Measure Unit")]
        public Nullable<short> DefaultMUnitId { get; set; }
        [Display(Name = "Def. Quantity Unit")]
        public Nullable<short> DefaultQUnitId { get; set; }
        [Display(Name = "Re-Order Level")]
        public Nullable<decimal> ReOrderLevel { get; set; }
        [Display(Name = "Re-Order Qty")]
        public Nullable<decimal> ReOrderQuantity { get; set; }
        [Display(Name = "Safety Stock Qty")]
        public Nullable<decimal> SafetyStockQtyStd { get; set; }
        [Display(Name = "Opening Quantity")]
        public Nullable<decimal> OpeningQuantity { get; set; }
        [Display(Name = "Unit")]
        public Nullable<short> OpeningQuantityUnitId { get; set; }
        [Display(Name = "Opening Date")]
        public Nullable<System.DateTime> OpeningDate { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Stockable ?")]
        public bool Stockable { get { return (IsStockable == 1 ? true : false); } set { IsStockable = Byte.Parse(value ? "1" : "0"); } }
        public string MeasureUnitList
        {
            get
            {
                return (MUnitList == null ? "" : MUnitList.Aggregate((i, j) => i + ", " + j));
            }
            set
            {
                if(value == null)
                {
                    MUnitList = new string[] { "" };
                }
                else
                    MUnitList = value.Replace(", ", ",").Split(new string[] { "," }, StringSplitOptions.None);
            }
        }
        public string QuantityUnitList
        {
            get
            {
                return (QUnitList == null ? "" : QUnitList.Aggregate((i, j) => i + ", " + j));
            }
            set
            {
                if (value == null)
                {
                    QUnitList = new string[] { "" };
                }
                else
                    QUnitList = value.Replace(", ", ",").Split(new string[] { "," }, StringSplitOptions.None);
            }
        }
        [Display(Name = "Puchaseable ?")]
        public Nullable<byte> IsPurchaseable { get; set; }
        [Display(Name = "Saleable ?")]
        public Nullable<byte> IsSaleable { get; set; }
        [Display(Name = "Expenseable ?")]
        public Nullable<byte> IsExpense { get; set; }
        [Display(Name = "Fixed Assets ?")]
        public Nullable<byte> IsFixedAsset { get; set; }
        [Display(Name = "Consumable ?")]
        public Nullable<byte> IsConsumable { get; set; }
        [Display(Name = "Stockable ?")]
        public Nullable<byte> IsStockable { get; set; }
        public Nullable<int> IncomeAccId { get; set; }
        public Nullable<int> ExpenseAccId { get; set; }
        [Display(Name = "Active ?")]
        public byte IsActive { get; set; }

        public ICollection<ItemVariantView> itemVariant { get; set; }
        public ICollection<ItemFactoryView> itemFactory { get; set; }
        public ICollection<ItemUnitView> itemUnit { get; set; }
    }

    public partial class ItemVariantView : IFIMSContext
    {
        public ItemVariantView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<int> ItemVariantId { get; set; }
        [Display(Name = "Product")]
        public Nullable<int> ItemId { get; set; }
        [Display(Name = "Variant")]
        public Nullable<short> VariantId { get; set; }
        [Display(Name = "Value")]
        public string VariantValue { get; set; }
        [Display(Name = "Active ?")]
        public Nullable<byte> IsActive { get; set; }
        public bool _editStatus;
    }

    public partial class ItemUnitView : IFIMSContext
    {
        public ItemUnitView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<int> ItemUnitId { get; set; }
        [Display(Name = "Product")]
        public Nullable<int> ItemId { get; set; }
        [Display(Name = "Qty Unit")]
        public Nullable<short> UnitId { get; set; }
        [Display(Name = "Qty Unit")]
        public string UnitName { get; set; }
        [Display(Name = "KG/Unit")]
        public Nullable<decimal> KGProportion { get; set; }
        [Display(Name = "Active ?")]
        public Nullable<byte> IsActive { get; set; }
        public bool _editStatus;
    }

    public partial class ItemFactoryView : IFIMSContext
    {
        public ItemFactoryView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<int> ItemFactoryId { get; set; }
        [Display(Name = "Product")]
        public Nullable<int> ItemId { get; set; }
        [Display(Name = "Factory")]
        public Nullable<short> FactoryId { get; set; }

        [Display(Name = "Factory Name")]
        public string FactoryName { get; set; }
        [Display(Name = "Material TypeId")]
        public Nullable<short> MaterialTypeId { get; set; }
        [Display(Name = "Material Type")]
        public string TypeName { get; set; }

        public bool _editStatus;
    }

    public class Requisition : IFIMSContext
    {
        public Requisition()
        {
            
        }

        [Display(Name = "ID")]
        public Nullable<int> PRId { get; set; }
        [Display(Name = "Factory"), Required]
        public Nullable<short> FactoryId { get; set; }
        [Display(Name = "Factory")]
        public string FactoryName { get; set; }
        [Display(Name = "Requistion #")]
        public string PRNo { get; set; }
        [Required, Display(Name = "Date"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> PRDate { get; set; }
        [Required, Display(Name = "Req. Month"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:MMMM-yyyy}")]
        public Nullable<System.DateTime> PRMonth { get; set; }
        [Display(Name = "Total")]
        public Nullable<decimal> TotalQuantity { get; set; }
        [Display(Name = "Quantity Unit")]
        public Nullable<short> QuantityUnitId { get; set; }
        [Display(Name = "Quantity Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Remarks"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }
        [Display(Name = "Active")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Active")]
        public byte IsActive { get; set; }
        public Nullable<byte> ApprovedType { get; set; }
        public Nullable<short> ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedTime { get; set; }
        public Nullable<short> CreatedBy { get; set; }
        public Nullable<short> RevisedBy { get; set; }
        public Nullable<System.DateTime> RevisedTime { get; set; }

        public ICollection<RequisitionSub> requisitionSub { get; set; }
    }

    public class RequisitionSub : IFIMSContext
    {
        public RequisitionSub()
        {
        }

        [Display(Name = "Requisition #")]
        public Nullable<int> PRId { get; set; }
        [Display(Name = "ID")]
        public Nullable<int> PRSubId { get; set; }
        [Display(Name = "Category"), Required]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Product"), Required]
        public Nullable<int> ProductId { get; set; }
        [Display(Name = "Bond Product"), Required]
        public Nullable<int> BondProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        [Display(Name = "Prf. Supplier"), Required]
        public Nullable<short> PreferredSupplierId { get; set; }
        public string SupplierName { get; set; }
        [Display(Name = "Brand"), Required]
        public Nullable<short> BrandId { get; set; }
        public string BrandName { get; set; }
        [Display(Name = "Curr. Stock")]
        public Nullable<decimal> CurrentStock { get; set; }
        [Display(Name = "Pipeline")]
        public Nullable<decimal> StockInTransit { get; set; }
        [Display(Name = "Pending Req Qty")]
        public Nullable<decimal> PendingRequisitionQuanity { get; set; }
        [Display(Name = "Pending Req Qty")]
        public Nullable<decimal> PendingRequisitionQuanityForItem { get; set; }
        [Display(Name = "Hist. Period")]
        public string HistoricalPeriod { get; set; }
        [Display(Name = "Hist. Avg Consm")]
        public Nullable<decimal> HistoricalConsumption { get; set; }
        [Display(Name = "Avg Consm Per Day")]
        public Nullable<decimal> AverageConsumptionPerDay { get; set; }
        [Display(Name = "Days With T S")]
        public Nullable<decimal> DaysWithTotalStock { get; set; }
        [Display(Name = "N 3 M Avg Consm L Y")]
        public Nullable<decimal> Next3MonthAvgConsumptionLastYear { get; set; }
        [Display(Name = "N 3 M Avg Consm C Y")]
        public Nullable<decimal> Next3MonthAvgConsumptionCurrentYear { get; set; }
        [Display(Name = "Avg L T E O")]
        public Nullable<short> AvgLeadTimeEarlierOrders { get; set; }
        [Display(Name = "Lead Time"), Required]
        public Nullable<short> LeadTime { get; set; }
        [Display(Name = "Org. Factory ETA"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> OriginalETA { get; set; }
        [Display(Name = "Factory ETA"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> FactoryETA { get; set; }
        [Display(Name = "Cal ETA"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> CETA { get; set; }
        [Display(Name = "Bond SL"), Required]
        public Nullable<int> CapacityId { get; set; }
        [Display(Name = "Bond Balance"), Required]
        public Nullable<decimal> BondBalance { get; set; }
        [Display(Name = "Req. Qty"), Required]
        public Nullable<decimal> Quantity { get; set; }
        [Display(Name = "Quantity Unit"), Required]
        public Nullable<short> QuantityUnitId { get; set; }
        [Display(Name = "Quantity Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "KG Per Unit"), Required]
        public Nullable<decimal> KGPerUnit { get; set; }
        [Display(Name = "Req. Qty (KG)"), Required]
        public Nullable<decimal> QuantityInKG { get; set; }
        [Display(Name = "Apprx Price")]
        public Nullable<decimal> UnitPrice { get; set; }
        [Display(Name = "Amount")]
        public Nullable<decimal> Amount { get { return Quantity * UnitPrice; } }
        [Display(Name = "With Duty?"), Required]
        public Nullable<short> IsWithDuty { get; set; }
        [Display(Name = "Local Purchase?")]
        public Nullable<short> IsLocalPurchase { get; set; }
        [Display(Name = "Non Bond Product?")]
        public Nullable<short> IsNonBondProduct { get; set; }
        [Display(Name = "Non Bond Capacity?"), Required]
        public Nullable<short> IsNonBondCapacity { get { return Convert.ToInt16(IsWithDuty + IsLocalPurchase + IsNonBondProduct); } }
        [Display(Name = "Bond Capacity Check?"), Required]
        public Nullable<bool> BondCapacityCheck { get; set; }
        [Display(Name = "Bond Source")]
        public Nullable<short> BondDeductSourceType { get; set; }
        [Display(Name = "SL #")]
        public Nullable<short> RowNo { get; set; }
        [Display(Name = "Status")]
        public byte Status { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        [Display(Name = "Total Stock")]
        public Nullable<decimal> TotalStock { get { return CurrentStock + StockInTransit + PendingRequisitionQuanity; } }
        [Display(Name = "Avg Consm/Month")]
        public Nullable<decimal> AvgConsmPerMonth { get { return AvgConsmPerDayHP.HasValue && AvgConsmPerDayHP == 0 ? 0 : HistoricalConsumption / AvgConsmPerDayHP ; } }
        [Display(Name = "Amount")]
        public Nullable<decimal> AvgConsmPerDayN3M { get { return Next3MonthAvgConsumptionCurrentYear / Convert.ToDecimal("90.00"); } }
        [Display(Name = "Amount")]
        public Nullable<DateTime> StartDate { get { return HistoricalPeriod == null ? DateTime.Today : DateTime.Parse(HistoricalPeriod.Split(new string[] { " - " }, StringSplitOptions.None).FirstOrDefault()); } }
        public Nullable<DateTime> EndDate { get { return HistoricalPeriod == null ? DateTime.Today : DateTime.Parse(HistoricalPeriod.Split(new string[] { " - " }, StringSplitOptions.None).LastOrDefault()); } }
        public Nullable<decimal> AvgConsmPerDayHP { get { return HistoricalConsumption == null || EndDate.Value.Subtract(StartDate.Value).Days == 0 ? 0 : HistoricalConsumption / EndDate.Value.Subtract(StartDate.Value).Days; } }
        public bool _editStatus;
        public string ItemCode { get; set; }
    }

    public class RequisitionView : IFIMSContext
    {
        public RequisitionView()
        {
        }

        [Display(Name = "Requisition #")]
        public Nullable<int> PRId { get; set; }
        [Display(Name = "ID")]
        public Nullable<int> PRSubId { get; set; }
        [Display(Name = "PO")]
        public Nullable<int> POId { get; set; }
        [Display(Name = "PO Sub")]
        public Nullable<int> POSubId { get; set; }
        [Display(Name = "QA")]
        public Nullable<int> QApprovalId { get; set; }
        [Display(Name = "QA Sub")]
        public Nullable<int> QApprovalSubId { get; set; }
        [Display(Name = "Quot ID")]
        public Nullable<int> QuotationId { get; set; }
        [Display(Name = "Quot Sub")]
        public Nullable<int> QuotationSubId { get; set; }
        [Display(Name = "Price ID")]
        public Nullable<int> PApprovalId { get; set; }
        [Display(Name = "Price Sub")]
        public Nullable<int> PApprovalSubId { get; set; }
        [Display(Name = "PI ID")]
        public Nullable<int> PIFileId { get; set; }
        [Display(Name = "PI Sub")]
        public Nullable<int> PIFileSubId { get; set; }
        [Display(Name = "LC ID")]
        public Nullable<int> LCFileId { get; set; }
        [Display(Name = "LC Sub")]
        public Nullable<int> LCFileSubId { get; set; }
        [Display(Name = "PLine ID")]
        public Nullable<int> PLEId { get; set; }
        [Display(Name = "PLine Sub")]
        public Nullable<int> PLESubId { get; set; }
        [Display(Name = "Req No")]
        public string PRNo { get; set; }
        [Display(Name = "Req Date")]
        public DateTime PRDate { get; set; }
        [Display(Name = "PO No")]
        public string PONo { get; set; }
        [Display(Name = "PO Date")]
        public DateTime PODate { get; set; }
        [Display(Name = "Q App No")]
        public string QANo { get; set; }
        [Display(Name = "Q App Date")]
        public DateTime QADate { get; set; }
        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Category")]
        public String CategoryName { get; set; }
        [Display(Name = "Product")]
        public Nullable<int> ProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        [Display(Name = "Prf. Supplier")]
        public Nullable<short> PreferredSupplierId { get; set; }
        public string SupplierName { get; set; }
        [Display(Name = "Brand")]
        public Nullable<short> BrandId { get; set; }
        public string BrandName { get; set; }
        [Display(Name = "Curr. Stock")]
        public Nullable<decimal> CurrentStock { get; set; }
        [Display(Name = "Pipeline")]
        public Nullable<decimal> StockInTransit { get; set; }
        [Display(Name = "Pending Req Qty")]
        public Nullable<decimal> PendingRequisitionQuanity { get; set; }
        [Display(Name = "Hist. Period")]
        public string HistoricalPeriod { get; set; }
        [Display(Name = "Hist. Avg Consm")]
        public Nullable<decimal> HistoricalConsumption { get; set; }
        [Display(Name = "Avg Consm Per Day")]
        public Nullable<decimal> AverageConsumptionPerDay { get; set; }
        [Display(Name = "Days With T S")]
        public Nullable<decimal> DaysWithTotalStock { get; set; }
        [Display(Name = "N 3 M Avg Consm L Y")]
        public Nullable<decimal> Next3MonthAvgConsumptionLastYear { get; set; }
        [Display(Name = "N 3 M Avg Consm C Y")]
        public Nullable<decimal> Next3MonthAvgConsumptionCurrentYear { get; set; }
        [Display(Name = "Avg L T E O")]
        public Nullable<short> AvgLeadTimeEarlierOrders { get; set; }
        [Display(Name = "Lead Time")]
        public Nullable<short> LeadTime { get; set; }
        [Display(Name = "Factory ETA"), DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> FactoryETA { get; set; }
        [Display(Name = "Req. Qty")]
        public Nullable<decimal> Quantity { get; set; }
        [Display(Name = "PO. Qty")]
        public Nullable<decimal> POQuantity { get; set; }
        [Display(Name = "App. Qty")]
        public Nullable<decimal> ApprovedQuantity { get; set; }
        [Display(Name = "PI Qty")]
        public Nullable<decimal> PIQuantity { get; set; }
        [Display(Name = "LC Qty")]
        public Nullable<decimal> LCQuantity { get; set; }
        [Display(Name = "Req. Pend. Qty")]
        public Nullable<decimal> PendingQuantity { get; set; }
        [Display(Name = "Quantity Unit")]
        public Nullable<short> QuantityUnitId { get; set; }
        [Display(Name = "Quantity Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Apprx Price")]
        public Nullable<decimal> UnitPrice { get; set; }
        [Display(Name = "Currency")]
        public Nullable<short> CurrencyId { get; set; }
        [Display(Name = "Currency")]
        public string Currency { get; set; }
        [Display(Name = "Amount")]
        public Nullable<decimal> Amount { get { return Quantity * UnitPrice; } }
        [Display(Name = "Bond SL #")]
        public Nullable<int> BondSL { get; set; }
        [Display(Name = "Bond Balance")]
        public Nullable<decimal> BondBalance { get; set; }
        [Display(Name = "SL #")]
        public Nullable<short> RowNo { get; set; }
        [Display(Name = "Status")]
        public byte Status { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        public bool _editStatus;
    }

    public partial class POView : IFIMSContext
    {
        public POView()
        {
        }

        [Display(Name = "ID")]
        public Nullable<int> POId { get; set; }
        [Display(Name = "PO #")]
        public string PONo { get; set; }
        [Display(Name = "Requsition(s)")]
        public string[] PRList { get; set; }
        [Display(Name = "Date")]
        public Nullable<System.DateTime> PODate { get; set; }
        [Display(Name = "Bond")]
        public Nullable<short> BondId { get; set; }
        [Display(Name = "Bond")]
        public string BondName { get; set; }
        [Display(Name = "Total Qty")]
        public Nullable<decimal> TotalQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public Nullable<short> QuantityUnitId { get; set; }
        [Display(Name = "Remarks"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }
        [Display(Name = "Active")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Active ?")]
        public byte IsActive { get; set; }
        [Display(Name = "SL #")]
        public Nullable<short> RankId { get; set; }
        [Display(Name = "Approval Type")]
        public Nullable<byte> ApprovedType { get; set; }
        [Display(Name = "Approved By")]
        public Nullable<short> ApprovedBy { get; set; }
        [Display(Name = "Approve Time")]
        public Nullable<System.DateTime> ApprovedTime { get; set; }

        public ICollection<POSub> poSub { get; set; }
    }

    public partial class POSub : IFIMSContext
    {
        public POSub()
        {
        }

        [Display(Name = "Purchase #")]
        public Nullable<int> POId { get; set; }
        [Display(Name = "ID")]
        public Nullable<int> POSubId { get; set; }
        [Display(Name = "Requisition #")]
        public Nullable<int> PRId { get; set; }
        [Display(Name = "Requisition #")]
        public string PRNo { get; set; }
        public Nullable<int> PRSubId { get; set; }
        [Display(Name = "Category")]
        public Nullable<short> CategoryId { get; set; }
        [Display(Name = "Product")]
        public Nullable<int> ProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        [Display(Name = "Prf. Supplier")]
        public Nullable<short> PreferredSupplierId { get; set; }
        public string SupplierName { get; set; }
        [Display(Name = "Brand")]
        public Nullable<short> BrandId { get; set; }
        public string BrandName { get; set; }
        [Display(Name = "Current Stock")]
        public Nullable<decimal> CurrentStock { get; set; }
        [Display(Name = "Pipeline")]
        public Nullable<decimal> StockInTransit { get; set; }
        [Display(Name = "L 3 M Avg Consm")]
        public Nullable<decimal> Last3MonthAvgConsumtion { get; set; }
        [Display(Name = "N 3 M Avg Consm L Y")]
        public Nullable<decimal> Next3MonthAvgConsumtionLastYear { get; set; }
        [Display(Name = "N 3 M Avg Consm C Y")]
        public Nullable<decimal> Next3MonthAvgConsumtionCurrentYear { get; set; }
        [Display(Name = "Avg L T E O")]
        public Nullable<decimal> AvgLeadTimeEarlierOrders { get; set; }
        [Display(Name = "Factory ETA")]
        public Nullable<System.DateTime> FactoryETA { get; set; }
        [Display(Name = "Quantity")]
        public Nullable<decimal> Quantity { get; set; }
        [Display(Name = "App. Quantity")]
        public Nullable<decimal> ApprovedQuantity { get; set; }
        [Display(Name = "Quantity Unit")]
        public Nullable<short> QuantityUnitId { get; set; }
        [Display(Name = "Quantity Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "SL #")]
        public Nullable<short> RowNo { get; set; }
        public bool _editStatus;
    }
    public partial class PORequisitionView : IFIMSContext
    {
        public PORequisitionView()
        {
        }

        public Nullable<long> POPRId { get; set; }
        public Nullable<int> POId { get; set; }
        public Nullable<int> PRId { get; set; }
        public bool _editStatus;
    }

    public partial class PurchaseOrder : IFIMSContext
    {
        public ICollection<QuotationView> quotationView { get; set; }
        public ICollection<QuotationSubView> quotationSubView { get; set; }

        public BondPeriod BondPeriod { get { return (new InventoryCubeEntities()).BondPeriods.Find(BondPeriodId); } }
    }

    public partial class QuantityApproval : IFIMSContext
    {
        public QuantityApproval()
        {
            
        }

        [Display(Name = "ID")]
        public int QApprovalId { get; set; }
        [Display(Name = "Approval #")]
        public string QApprovalNo { get; set; }
        [Display(Name = "Date")]
        public System.DateTime QApprovalDate { get; set; }
        [Display(Name = "Approved By")]
        public short ApprovedBy { get; set; }
        [Display(Name = "PO #")]
        public int POId { get; set; }

        public ICollection<QuantityApprovalDetails> quantityApprovalDetails { get; set; }
    }

    public partial class QuantityApprovalDetails : IFIMSContext
    {
        public QuantityApprovalDetails()
        {
        }

        [Display(Name = "Approval #")]
        public int QApprovalId { get; set; }
        [Display(Name = "ID")]
        public int QApprovalSubId { get; set; }
        [Display(Name = "PO #")]
        public int POSubId { get; set; }
        [Display(Name = "Requisition #")]
        public int PRId { get; set; }
        public int PRSubId { get; set; }
        [Display(Name = "Quantity")]
        public decimal ApprovedQuantity { get; set; }
        [Display(Name = "Quantity Unit")]
        public short QuantityUnitId { get; set; }
        [Display(Name = "SL #")]
        public short RowNo { get; set; }
    }

    public partial class QuantityApprovalSub : IFIMSContext
    {
        //public ICollection<PISub> PISubs { get { return (new LCManagementEntities()).PISubs.Where(l => l.QApprovalSubId == QApprovalSubId).ToList(); } }
    }

    public partial class QuotationView : IFIMSContext
    {
        public QuotationView()
        {

        }

        [Key]
        public Nullable<int> QVId { get; set; }
        [Display(Name = "Quotation #")]
        public Nullable<int> QuotationId { get; set; }
        [Display(Name = "PO #")]
        public Nullable<int> POSubId { get; set; }
        [Display(Name = "Q Apv #")]
        public Nullable<int> QApprovalSubId { get; set; }
        [Display(Name = "Req #")]
        public Nullable<int> PRSubId { get; set; }
    }

    public partial class QuotationSubView : IFIMSContext
    {
        public QuotationSubView()
        {
        }

        public Nullable<int> QVId { get; set; }
        [Display(Name = "ID")]
        public int QuotationId { get; set; }
        [Display(Name = "Approval #")]
        public int QuotationSubId { get; set; }
        [Display(Name = "Supplier")]
        public short SupplierId { get; set; }
        [Display(Name = "Tenure Type")]
        public short TenureType { get; set; }
        [Display(Name = "Credit Days")]
        public short TenureDays { get; set; }
        [Display(Name = "Current Quotation Price")]
        public decimal QuotationPrice { get; set; }
        [Display(Name = "Last Order Price")]
        public decimal LastOrderPrice { get; set; }
        [Display(Name = "Last Year Price")]
        public decimal LastYearPrice { get; set; }
        [Display(Name = "P C F L O")]
        public decimal PriceChangeFromLastOrder { get; set; }
        [Display(Name = "P C F L Y")]
        public decimal PriceChangeFromLastYear { get; set; }
        [Display(Name = "Currency")]
        public short CurrencyId { get; set; }

    }
    public class PriceQuotation: IFIMSContext
    {
        public int PriceDetailsId { get; set; }
        public int ApprovalSubId { get; set; }
        public short SupplierId { get; set; }
        public short TermId { get; set; }
        public decimal Sight { get; set; }
        public decimal SupplierCredit { get; set; }
        public decimal UPass { get; set; }
        public short SupplierCreditDays { get; set; }
        public short UPassDays { get; set; }
    }
    public partial class PriceApproval : IFIMSContext
    {
        public PriceApproval()
        {
        }

        [Display(Name = "ID")]
        public int PApprovalId { get; set; }
        [Display(Name = "P Approval #")]
        public string PApprovalNo { get; set; }
        [Display(Name = "Date")]
        public System.DateTime PApprovalDate { get; set; }
        [Display(Name = "Approved By")]
        public short ApprovedBy { get; set; }
        [Display(Name = "Q Approval #")]
        public int QApprovalId { get; set; }
        [Display(Name = "PO #")]
        public int POId { get; set; }
    }

    public partial class PriceApprovalDetails : IFIMSContext
    {
        public PriceApprovalDetails()
        {
        }

        [Display(Name = "Approval #")]
        public int QApprovalId { get; set; }
        [Display(Name = "ID")]
        public int QApprovalSubId { get; set; }
        [Display(Name = "PO #")]
        public int POSubId { get; set; }
        [Display(Name = "PO #")]
        public int QuotationId { get; set; }
        public short RowNo { get; set; }
    }

    public class CustomTypeView : IFIMSContext
    {
        public short TypeId { get; set; }
        public string TypeName { get; set; }
        public string Flag { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false), Display(Name = "Category List")]
        public string CategoryList { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false), Display(Name="Short Name")]
        public string ShortName { get; set; }
        [Display(Name = "Active")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public byte IsActive { get; set; }
        public short RankId { get; set; }
    }

    public class LC
    {
        [Display(Name = "LC #")]
        public int LCFileId { get; set; }
        public string LCFileNo { get; set; }
        public Nullable<System.DateTime> PaymentSDate { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public string LCNo { get; set; }
        public System.DateTime LCDate { get; set; }
        public int PIFileId { get; set; }
        public string PINo { get; set; }
        public System.DateTime PIDate { get; set; }
        public short BeneficiaryId { get; set; }
        public int LocationId { get; set; }
        public short SupplierId { get; set; }
        public short LocalAgentId { get; set; }
        public short OriginCountryId { get; set; }
        public short POLId { get; set; }
        public short IssuerBankId { get; set; }
        public short IssuerBranchId { get; set; }
        public short NegotiatingBankId { get; set; }
        public short BeneficiaryBankId { get; set; }
        public System.DateTime ETD { get; set; }
        public System.DateTime ETA { get; set; }
        public System.DateTime RequisitionDate { get; set; }
        public System.DateTime BookingConfirmDate { get; set; }
        public string LCType { get; set; }
        [Display(Name = "L D S")]
        public System.DateTime ShipmentDate { get; set; }
        [Display(Name = "BL Date")]
        public System.DateTime BLDate { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public short Tenure { get; set; }
        public short BankTenure { get; set; }
        public System.DateTime TenureStartDate { get; set; }
        public string CoverNoteNo { get; set; }
        public System.DateTime CoverNoteDate { get; set; }
        public decimal LCFileAmount { get; set; }
        public string CurrencyType { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
    }

    public partial class PipelineView : IFIMSContext
    {
        public PipelineView()
        {
            
        }

        [Display(Name = "ID")]
        public int? PLEId { get; set; }
        [Display(Name = "File No")]
        public string PLENo { get; set; }
        [Display(Name = "Date"), Required(ErrorMessage = "Date must not be blank.")]
        public System.DateTime? PLEDate { get; set; }
        [Display(Name = "LC #"), Required(ErrorMessage = "LC no must not be blank.")]
        public int? LCFileId { get; set; }
        [Display(Name = "Lot #"), Required(ErrorMessage = "Lot no must not be blank.")]
        public string LotNo { get; set; }
        [Display(Name = "ETD")]
        public System.DateTime? ShipmentDate { get; set; }
        [Display(Name = "BL Date")]
        public System.DateTime? BLDate { get; set; }

        
        [Display(Name = "Trans Arrival")]
        public System.DateTime? TransArrivalDate { get; set; }
        [Display(Name = "Trans Dep (Est)")]
        public System.DateTime? TransDepartureDate { get; set; }
        [Display(Name = "Transshipment Port")]
        public string TransshipmentPort { get; set; }
        [Display(Name = "Trans Dep (Actual)")]
        public System.DateTime? TransDepartureDateActual { get; set; }


        [Display(Name = "ETA (Port)")]
        public System.DateTime? PortArivalDatePrev { get; set; }
        [Display(Name = "ETA (Port)")]
        public System.DateTime? PortArivalDate { get; set; }
        [Display(Name = "ETA (Store)")]
        public System.DateTime? StoreArivalDatePrev { get; set; }
        [Display(Name = "ETA (Store)")]
        public System.DateTime? StoreArivalDate { get; set; }

        [Display(Name = "Doc Arvl At Bank")]
        public System.DateTime? DocArrivalAtBankDate { get; set; }
        [Display(Name = "Factory ETA")]
        public System.DateTime? FactoryETA { get; set; }
        [Display(Name = "Com Invoice Date")]
        public System.DateTime? CommercialInvoiceDate { get; set; }
        [Display(Name = "Com Invoice No")]
        public string CommercialInvoiceNo { get; set; }
        [Display(Name = "Bill of Entry Date")]
        public System.DateTime? BillOfEntryDate { get; set; }
        [Display(Name = "Bill of Entry No")]
        public string BillOfEntryNo { get; set; }
        [Display(Name = "Bill of Entry Value")]
        public Decimal? BillOfEntryValue { get; set; }
        [Display(Name = "Package Count")]
        public decimal? PackageCount { get; set; }
        [Display(Name = "Package Unit")]
        public short? PackageUnitId { get; set; }
        [Display(Name = "Package Unit")]
        public string PackageUnit { get; set; }
        [Display(Name = "Del Date F Port")]
        public System.DateTime? PortDeliveryDate { get; set; }
        [Display(Name = "Received Date")]
        public System.DateTime? LotReceivedDate { get; set; }
        [Display(Name = "Record Date")]
        public System.DateTime? ArrivalRecordDate { get; set; }
        [Display(Name = "Doc Submit To C&F")]
        public System.DateTime? DocumentSubmitToCNFDate { get; set; }
        [Display(Name = "Current Location"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CurrentLocation { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }
        [Display(Name = "Port of Loading")]
        public short? POLId { get; set; }
        [Display(Name = "Last Lot?")]
        public short? IsLastLot { get; set; }
        [Display(Name = "Store Received?")]
        public short? IsLotReceived { get; set; }
        [Display(Name = "Truck Count")]
        public decimal NoOfTruck { get; set; }
        [Display(Name = "Mode of Transport")]
        public string ModeOfTransport { get; set; }
        [Display(Name = "Upload File")]
        public HttpPostedFileBase file_name { get; set; }
        [Display(Name = "Attachments")]
        public string Attachments { get; set; }

        public bool lastLot
        {
            set { IsLastLot = Convert.ToInt16(!value ? 0 : 1); }
            get { return IsLastLot != null && IsLastLot > 0; }
        }
        public bool LotReceived
        {
            set { IsLotReceived = Convert.ToInt16(!value ? 0 : 1); }
            get { return IsLotReceived != null && IsLastLot > 0; }
        }

        public ICollection<PipelineSubView> PipelineSubViews { get; set; }
        public ICollection<PackingListView> PackingListViews { get; set; }
    }

    public partial class PipelineSubView : IFIMSContext
    {
        public PipelineSubView()
        {
        }

        [Display(Name = "ID")]
        public int? PLEId { get; set; }
        [Display(Name = "ID")]
        public int? PLESubId { get; set; }
        [Display(Name = "LC #"), Required]
        public int LCFileSubId { get; set; }
        [Display(Name = "PI #"), Required]
        public int PIFileSubId { get; set; }
        [Display(Name = "Category")]
        public short CategoryId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Item Name"), Required]
        public int ItemId { get; set; }
        [Display(Name = "Item Name")]
        public string ItemName { get; set; }
        [Display(Name = "Factory ETA")]
        public DateTime FactoryETA { get; set; }
        [Display(Name = "LC Quantity")]
        public decimal LCQuantity { get; set; }
        [Display(Name = "Pipeline Quantity"), Required]
        public decimal StockInTransit { get; set; }
        [Display(Name = "Quantity Unit")]
        public short QuantityUnitId { get; set; }
        [Display(Name = "Quantity Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }
        [Display(Name = "Pipeline Value")]
        public decimal PipelineValue { get; set; }
        public short? RowNo { get; set; }
    }

    public partial class PackingListView : IFIMSContext
    {
        [Display(Name = "ID")]
        public int PLEId { get; set; }
        [Display(Name = "ID")]
        public int PLECId { get; set; }
        public short TypeId { get; set; }
        public string Size { get; set; }
        public short Quantity { get; set; }
        public short LCLQuantity { get; set; }
        public short FCLQuantity { get; set; }
    }

    public class ItemList
    {
        public int BSPId { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public bool isChecked { get; set; }
        public string CatName { get; set; }
    }

    public class ItemDetails
    {
        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ProdId { get; set; }
        public short FactoryId { get; set; }
        public decimal PresentStock { get; set; }
        public decimal N3MonthConsmLY { get; set; }
        public decimal P3MonthConsm { get; set; }
        public decimal Pipeline { get; set; }
        public decimal ReqPendingQuantity { get; set; }
        public decimal ReqPendingQuantityForItem { get; set; }
        public int CapacityId { get; set; }
        public decimal BondBalance { get; set; }
        public bool BondCapacityCheck { get; set; }
        public string FDate { get; set; }
        public string TDate { get; set; }
        public decimal TotalStock { get { return PresentStock + Pipeline + ReqPendingQuantityForItem; } }
        public decimal AvgConsmPerDay { get { return ((DateTime.Parse(TDate) - DateTime.Parse(FDate)).TotalDays == 0 ? 0 : Math.Round(P3MonthConsm / Convert.ToDecimal((DateTime.Parse(TDate) - DateTime.Parse(FDate)).TotalDays), 2)); } }
        public decimal AvgConsmPerMonth { get { return (AvgConsmPerDay == 0 ? 0 : Math.Round(30 * AvgConsmPerDay, 0)); } }
        public decimal DaysWithTotalStock { get { return (AvgConsmPerDay == 0 ? 0 : Math.Round(TotalStock / AvgConsmPerDay, 0)); } }
    }

    public class ItemWiseBondBalance
    {
        public int ProductId { get; set; }
        public int CapacityId { get; set; }
        public decimal BalanceQuantity { get; set; }
    }

    public class QuotationPreview
    {
        public ICollection<POSub> poSub { get; set; }
        public ICollection<QuotationView> quotationView { get; set; }
        public ICollection<QuotationSubView> quotationSubView { get; set; }
        public ICollection<RequisitionView> requisitionView { get; set; }
    }

    public partial class GRRView : IFIMSContext
    {
        public GRRView()
        {
        }

        public int GRRId { get; set; }
        [Display(Name = "Factory")]
        public short? FactoryId { get; set; }
        [Display(Name = "Supplier")]
        public short? SupplierId { get; set; }
        [Display(Name = "Supplier")]
        public string SupplierName { get; set; }
        [Display(Name = "GRR #"), Required]
        public string GRRNo { get; set; }
        [Display(Name = "GRR Date")]
        public System.DateTime? GRRDate { get; set; }
        [Display(Name = "Receive Date")]
        public System.DateTime? ReceiveDate { get; set; }
        [Display(Name = "LC #"), Required]
        public int? LCFileId { get; set; }
        [Display(Name = "LC #"), Required]
        public string LCNo { get; set; }
        [Display(Name = "Port Arrival Date")]
        public System.DateTime? LotDate { get; set; }
        [Display(Name = "Lot #"), Required]
        public int? PLEId { get; set; }

        [DisplayName("Material Type")]
        public short? MaterialTypeId { get; set; }

        [Display(Name = "LC Date")]
        public System.DateTime? LCDate { get; set; }
        [Display(Name = "Truck Ch #")]
        public string TruckChallanNo { get; set; }
        [Display(Name = "Truck Ch Date")]
        public System.DateTime? TruckChallanDate { get; set; }
        [Display(Name = "Sup Ch #")]
        public string SupplierChallanNo { get; set; }
        [Display(Name = "Sup Ch Date")]
        public System.DateTime? SupplierChallanDate { get; set; }
        [Display(Name = "Gate Pass #")]
        public string GatePassNo { get; set; }
        [Display(Name = "Gate Pass Date")]
        public System.DateTime? GatePassDate { get; set; }
        [Display(Name = "Vehicle No")]
        public string VehicleNo { get; set; }
        [Display(Name = "Purhcase Type")]
        public short? PurchaseTypeId { get; set; }
        [Display(Name = "Receive Type")]
        public short? ReceiveTypeId { get; set; }
        [Display(Name = "Warehouse")]
        public short? WarehouseId { get; set; }
        [Display(Name = "Requision #")]
        public int? SRRId { get; set; }
        public byte ApprovedType { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }

        public ICollection<GRRSubView> grrSubView { get; set; }
    }

    public partial class GRRSubView : IFIMSContext
    {
        public GRRSubView()
        {
        }

        [Display(Name = "GRR #")]
        public int GRRId { get; set; }
        [Display(Name = "ID")]
        public int GRRSubId { get; set; }
        [Display(Name = "Category")]
        public short? CategoryId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Lot #")]
        public int? PLESubId { get; set; }
        [Display(Name = "PI #")]
        public int? PIFileSubId { get; set; }
        [Display(Name = "Requistion #")]
        public int? SRRSubId { get; set; }
        [Display(Name = "Issue #")]
        public int? IssueSubId { get; set; }
        [Display(Name = "Sales Invoice #")]
        public int? SalesInvoiceSubId { get; set; }
        [Display(Name = "Old MRR #")]
        public int? OldMRRSubId { get; set; }
        [Display(Name = "Product")]
        public int ProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        [Display(Name = "PL Qty")]
        public decimal? PipelineQuantity { get; set; }
        [Display(Name = "GRR Qty")]
        public decimal? GRRQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? QuantityUnitId { get; set; }
        [Display(Name = "Qty Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Import Price")]
        public decimal? ImportPriceFC { get; set; }
        [Display(Name = "Currency")]
        public short? CurrencyId { get; set; }
        [Display(Name = "Currency")]
        public string CurrencyCode { get; set; }
        [Display(Name = "Landed Price (BDT)")]
        public decimal? LandedPriceBDT { get; set; }
        [Display(Name = "Line #")]
        public string Line_No { get; set; }
        [Display(Name = "BIN")]
        public string BIN { get; set; }
        //[Display(Name = "Vehicle #")]
        //public string VehicleNo { get; set; }
        //[Display(Name = "Challan #")]
        //public string ChallanNo { get; set; }
        //[Display(Name = "Challan Date")]
        //public System.DateTime ChallanDate { get; set; }
        //[Display(Name = "IGP #")]
        //public string IGPNo { get; set; }
        //[Display(Name = "IGP Date")]
        //public System.DateTime IGPDate { get; set; }
        [Display(Name = "SL #")]
        public short RowNo { get; set; }
        [Display(Name = "Receive Type")]
        public string ReceiveThrough { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }
        public bool _editStatus { get; set; }
    }

    public partial class SRRMainView : IFIMSContext
    {

        public SRRMainView()
        {
            this.SRRSubViews = new HashSet<SRRSubView>();
            this.SRRAgainstPlanViews = new HashSet<SRRAgainstPlanView>();
        }

        [Key, DisplayName("ID")]
        public int SRRId { get; set; }
        [DisplayName("Req #")]
        public string SRRNo { get; set; }
        [DisplayName("Req Date")]
        public System.DateTime? SRRDate { get; set; }
        [DisplayName("Req Type")]
        public short? SRRTypeId { get; set; }
        [DisplayName("Material Type")]
        public short? MaterialTypeId { get; set; }
        [DisplayName("Factory")]
        public short? FactoryId { get; set; }
        [DisplayName("Ref. No")]
        public short? ReferenceId { get; set; }
        [Display(Name = "Purpose")]
        public string Purpose { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [DisplayName("Department")]
        public short? DepartmentId { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }

        public virtual ICollection<SRRSubView> SRRSubViews { get; set; }
        public virtual ICollection<SRRAgainstPlanView> SRRAgainstPlanViews { get; set; }
    }

    public partial class SRRSubView : IFIMSContext
    {
        public SRRSubView()
        {
        }

        public SRRSubView(bool is_init)
        {
            if (is_init) {
                foreach (PropertyInfo pi in this.GetType().GetProperties())
                {
                    var type = pi.PropertyType;
                    object obj = null;

                    if (type == typeof(int) || type == typeof(int?))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short) || type == typeof(short?))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte) || type == typeof(byte?))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double) || type == typeof(double?))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal) || type == typeof(decimal?))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime) || type == typeof(DateTime?))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }

                    if (pi.CanWrite)
                    {
                        pi.SetValue(this, obj);
                    }
                }
            }
        }

        [DisplayName("Req #")]
        public int SRRId { get; set; }
        [Key, DisplayName("ID")]
        public int SRRSubId { get; set; }
        [Key, DisplayName("Issue #")]
        public int IssueId { get; set; }
        [Key, DisplayName("Issue #")]
        public int IssueSubId { get; set; }
        [DisplayName("Origin Cat Id")]
        public short? OriginCategoryId { get; set; }
        [DisplayName("Origin P Id")]
        public int? OriginProductId { get; set; }
        [DisplayName("Category")]
        public short? CategoryId { get; set; }
        [DisplayName("Category")]
        [NotMapped]
        public string CategoryName { get; set; }
        [DisplayName("Product")]
        public int? ProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        [DisplayName("Current Stock 1"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? CurrentStock1 { get; set; }
        [DisplayName("Current Stock 2"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? CurrentStock2 { get; set; }
        [DisplayName("Floor Stock"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? FloorStock { get; set; }
        //[DisplayName("Floor Stock 2"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        //public decimal? FloorStock2 { get; set; }
        [Display(Name = "Queue Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? QueueQuantity { get; set; }
        [Display(Name = "Req Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? SRRQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? QuantityUnitId { get; set; }
        [Display(Name = "Qty Unit")]
        public string QuantityUnit { get; set; }
        [DisplayName("Required Date")]
        public System.DateTime? RequiredDate { get; set; }
        [DisplayName("Net Req Qty"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? NetQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? UnitId { get; set; }
        [Display(Name = "Brand")]
        public short? BrandId { get; set; }
        [Display(Name = "SL #")]
        public short? RowNo { get; set; }
        [Display(Name = "Status")]
        public short? Status { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [Display(Name = "SRR")]
        public SRRMain SRRMain {
            get {
                using (var db = new InventoryCubeEntities())
                {
                    return db.SRRMains.Find(SRRId);
                }
            }
        }
        public short IssueType { get; set; }
        public short MaterialTypeId { get; set; }
        public bool _IsOnEdit { get; set; }
    }

    public partial class SPSRRSubView : IFIMSContext
    {
        public SPSRRSubView()
        {
        }

        public SPSRRSubView(bool is_init)
        {
            if (is_init)
            {
                foreach (PropertyInfo pi in this.GetType().GetProperties())
                {
                    var type = pi.PropertyType;
                    object obj = null;

                    if (type == typeof(int) || type == typeof(int?))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short) || type == typeof(short?))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte) || type == typeof(byte?))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double) || type == typeof(double?))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal) || type == typeof(decimal?))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime) || type == typeof(DateTime?))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }

                    if (pi.CanWrite)
                    {
                        pi.SetValue(this, obj);
                    }
                }
            }
        }

        [DisplayName("Req #")]
        public int SRRId { get; set; }
        [Key, DisplayName("ID")]
        public int SRRSubId { get; set; }
        [Key, DisplayName("Issue #")]
        public int IssueId { get; set; }
        [Key, DisplayName("Issue #")]
        public int IssueSubId { get; set; }
        [DisplayName("Origin Cat Id")]
        public short? OriginCategoryId { get; set; }
        [DisplayName("Origin P Id")]
        public int? OriginProductId { get; set; }
        [DisplayName("Category")]
        public short? CategoryId { get; set; }
        [DisplayName("Category")]
        [NotMapped]
        public string CategoryName { get; set; }
        [DisplayName("Product")]
        public int? ProductId { get; set; }
        [Display(Name = "Product")]
        public string ProductName { get; set; }
        [DisplayName("Current Stock 1"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? CurrentStock1 { get; set; }
        [DisplayName("Current Stock 2"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? CurrentStock2 { get; set; }
        [DisplayName("Floor Stock"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? FloorStock { get; set; }
        //[DisplayName("Floor Stock 2"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        //public decimal? FloorStock2 { get; set; }
        [Display(Name = "Queue Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? QueueQuantity { get; set; }
        [Display(Name = "Req Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? SRRQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? QuantityUnitId { get; set; }
        [Display(Name = "Qty Unit")]
        public string QuantityUnit { get; set; }
        [DisplayName("Required Date")]
        public System.DateTime? RequiredDate { get; set; }
        [DisplayName("Net Req Qty"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? NetQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? UnitId { get; set; }
        [Display(Name = "Brand")]
        public short? BrandId { get; set; }
        [Display(Name = "SL #")]
        public short? RowNo { get; set; }
        [Display(Name = "Status")]
        public short? Status { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [Display(Name = "SPSRR")]
        public SPSRRMain SPSRRMain
        {
            get
            {
                using (var db = new InventoryCubeEntities())
                {
                    return db.SPSRRMains.Find(SRRId);
                }
            }
        }
        public short IssueType { get; set; }
        public short MaterialTypeId { get; set; }
        public bool _IsOnEdit { get; set; }
    }

    public partial class SRRAgainstPlanView : IFIMSContext
    {

        public SRRAgainstPlanView()
        {
        }

        //public SRRAgainstPlanView(IFIMSContext viewModel)
        //{
        //    foreach (PropertyInfo pi in this.GetType().GetProperties())
        //    {
        //        if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
        //        {
        //            pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
        //        }
        //        else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
        //        {
        //            object obj = clsMain.getDefaultValue("SRRAgainstPlan", pi.Name, pi.PropertyType);
        //            if (obj != null)
        //                pi.SetValue(this, obj);
        //        }
        //    }
        //}

        [Display(Name = "SRR #")]
        public int SRRId { get; set; }
        [Key, Display(Name = "ID")]
        public int SRRPlanId { get; set; }
        [Display(Name = "Plan #")]
        public int? PlanId { get; set; }
        [Display(Name = "Booking #")]
        public int? BookingId { get; set; }
        [Display(Name = "Booking #")]
        public int? BookingSubId { get; set; }
        [Display(Name = "Product #")]
        public int? ProductId { get; set; }
        [Display(Name = "Req Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode=true)]
        public decimal Quantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? UnitId { get; set; }
        [Display(Name = "SL #")]
        public short? RowNo { get; set; }
        [Display(Name = "Active?")]
        public short? Status { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }

        public virtual SRRMain SRRMain { get; set; }
    }

    public partial class IssueMainView : IFIMSContext
    {
        public IssueMainView()
        {
            //this.IssueSubs = new HashSet<IssueSub>();
            //this.IssueAgainstLCs = new HashSet<IssueAgainstLC>();
            //this.ConsumptionAgIssues = new HashSet<ConsumptionAgIssue>();
        }

        [Display(Name = "ID")]
        public int IssueId { get; set; }
        [Display(Name = "SRR #")]
        public int SRRId { get; set; }
        [Display(Name = "Factory")]
        public short FactoryId { get; set; }
        [Display(Name = "Department")]
        public short DepartmentId { get; set; }
        [Display(Name = "Issue #")]
        public string IssueNo { get; set; }
        [Display(Name = "Issue Date")]
        public System.DateTime IssueDate { get; set; }
        [Display(Name = "Issue Type")]
        public short IssueTypeId { get; set; }
        [Display(Name = "Reference")]
        public short ReferenceId { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }
    }

    public partial class IssueSubView : IFIMSContext
    {

        public IssueSubView()
        {
            //this.IssueAgainstLCs = new HashSet<IssueAgainstLC>();
            //this.ConsumptionAgIssues = new HashSet<ConsumptionAgIssue>();
        }

        [Display(Name = "Issue #")]
        public int IssueId { get; set; }
        [Display(Name = "ID")]
        public int IssueSubId { get; set; }
        [Display(Name = "Product")]
        public int ProductId { get; set; }
        [Display(Name = "Issue Quantity")]
        public decimal Quantity { get; set; }
        [Display(Name = "Rate")]
        public decimal Rate { get; set; }
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }
        [Display(Name = "Quantity Unit")]
        public short UnitId { get; set; }
        public short RowNo { get; set; }
        [Display(Name = "SRR #")]
        public int SRRSubId { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }
    }

    public partial class ReturnMainView : IFIMSContext
    {

        public ReturnMainView()
        {
            //this.IssueReturnSubs = new HashSet<IssueReturnSub>();
            //this.IssueReturnAgainstLCs = new HashSet<IssueReturnAgainstLC>();
        }

        public int ReturnId { get; set; }
        public int IssueId { get; set; }
        public short FactoryId { get; set; }
        public short DepartmentId { get; set; }
        public string ReturnNo { get; set; }
        public System.DateTime ReturnDate { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> _IsOnEdit { get; set; }

        //public virtual IssueMain IssueMain { get; set; }
        //public virtual ICollection<IssueReturnSub> IssueReturnSubs { get; set; }
        //public virtual ICollection<IssueReturnAgainstLC> IssueReturnAgainstLCs { get; set; }
    }

    public partial class ReturnSubView : IFIMSContext
    {
        public ReturnSubView()
        {
        }

        public ReturnSubView(bool is_init)
        {
            if (is_init)
            {
                foreach (PropertyInfo pi in this.GetType().GetProperties())
                {
                    var type = pi.PropertyType;
                    object obj = null;

                    if (type == typeof(int) || type == typeof(int?))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short) || type == typeof(short?))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte) || type == typeof(byte?))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double) || type == typeof(double?))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal) || type == typeof(decimal?))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime) || type == typeof(DateTime?))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }

                    pi.SetValue(this, obj);
                }
            }
        }

        [DisplayName("Return #")]
        public int ReturnId { get; set; }
        [Key, DisplayName("ID")]
        public int ReturnSubId { get; set; }
        [DisplayName("Origin Cat Id")]
        public short? OriginCategoryId { get; set; }
        [DisplayName("Origin P Id")]
        public int? OriginProductId { get; set; }
        [DisplayName("Category")]
        public short? CategoryId { get; set; }
        [DisplayName("Category")]
        [NotMapped]
        public string CategoryName { get; set; }
        [DisplayName("Product")]
        public int? ProductId { get; set; }
        [Display(Name = "Return Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? ReturnQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? QuantityUnitId { get; set; }
        [Display(Name = "Qty Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Brand")]
        public short? BrandId { get; set; }
        [Display(Name = "SL #")]
        public short? RowNo { get; set; }
        public bool _IsOnEdit { get; set; }
    }

    public partial class ConsumptionMainView : IFIMSContext
    {
        public ConsumptionMainView()
        {
            //this.ConsumptionAgIssues = new HashSet<ConsumptionAgIssue>();
            //this.ConsumptionSubs = new HashSet<ConsumptionSub>();
        }

        [Display(Name = "ID")]
        public int ConsumptionId { get; set; }
        [Display(Name = "Consm No")]
        public string ConsumptionNo { get; set; }
        [Display(Name = "Date")]
        public System.DateTime ConsumptionDate { get; set; }
        [Display(Name = "Factory")]
        public short FactoryId { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }
    }
    
    public partial class ConsumptionSubView : IFIMSContext
    {
        public ConsumptionSubView()
        {
        }

        public ConsumptionSubView(bool is_init)
        {
            if (is_init)
            {
                foreach (PropertyInfo pi in this.GetType().GetProperties())
                {
                    var type = pi.PropertyType;
                    object obj = null;

                    if (type == typeof(int) || type == typeof(int?))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short) || type == typeof(short?))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte) || type == typeof(byte?))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double) || type == typeof(double?))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal) || type == typeof(decimal?))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime) || type == typeof(DateTime?))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }

                    pi.SetValue(this, obj);
                }
            }
        }

        [DisplayName("Consumption #")]
        public int ConsumptionId { get; set; }
        [Key, DisplayName("ID")]
        public int ConsumptionSubId { get; set; }
        [DisplayName("Origin Cat Id")]
        public short? OriginCategoryId { get; set; }
        [DisplayName("Origin P Id")]
        public int? OriginProductId { get; set; }
        [DisplayName("Category")]
        public short? CategoryId { get; set; }
        [DisplayName("Category")]
        [NotMapped]
        public string CategoryName { get; set; }
        [DisplayName("Product")]
        public int? ProductId { get; set; }
        [Display(Name = "Consumption Quantity"), DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? ConsumptionQuantity { get; set; }
        [Display(Name = "Qty Unit")]
        public short? QuantityUnitId { get; set; }
        [Display(Name = "Qty Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Brand")]
        public short? BrandId { get; set; }
        [Display(Name = "SL #")]
        public short? RowNo { get; set; }
        public bool _IsOnEdit { get; set; }
    }

    public partial class PIView : IFIMSContext
    {
        public PIView()
        {
            PIReceiveDateFromSourcing = clsMain.getCurrentTime();
        }

        [Key]
        public int PIFileId { get; set; }
        [DisplayName("File No")]
        public string PIFileNo { get; set; }
        [DisplayName("PI No"), Required(ErrorMessage = "PI No can't be blank."), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PINo { get; set; }
        [DisplayName("PI Date"), Required(ErrorMessage = "PI Date can't be blank.")]
        public System.DateTime? PIDate { get; set; }
        [DisplayName("Approval Ref. #")]
        public string ApprovalRef { get; set; }
        [DisplayName("Port of Loading"), Required(ErrorMessage = "Port of Loading have to select.")]
        public short? POLId { get; set; }
        [DisplayName("Applicant"), Required(ErrorMessage = "Applicant have to select.")]
        public short? BeneficiaryId { get; set; }
        [HiddenInput(DisplayValue = false), DisplayName("Location"), Required(ErrorMessage = "Location have to select.")]
        public int? LocationId { get; set; }
        [DisplayName("Supplier"), Required(ErrorMessage = "Supplier have to select.")]
        public short? SupplierId { get; set; }
        [DisplayName("Agent"), Required(ErrorMessage = "Agent have to select.")]
        public short? LocalAgentId { get; set; }
        [DisplayName("Country of Orgin"), Required(ErrorMessage = "Country have to select.")]
        public short? OriginCountryId { get; set; }
        [DisplayName("LC Shipment Date")]
        public System.DateTime? ShipmentDate { get; set; }
        [DisplayName("Esst. Despatch Date")]
        public System.DateTime? ETD { get; set; }
        [DisplayName("Esst. Arrival Date")]
        public System.DateTime? ETA { get; set; }
        [Required, Display(Name = "Esst. Ship. Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime? ETS { get; set; }
        [Required, Display(Name = "Esst. Prod. Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime? ETP { get; set; }
        [DisplayName("Requisition Date")]
        public System.DateTime? RequisitionDate { get; set; }
        [DisplayName("Order Confirm Date")]
        public System.DateTime? BookingConfirmDate { get; set; }
        [DisplayName("Receive From Sourcing")]
        public System.DateTime? PIReceiveDateFromSourcing { get; set; }
        [DisplayName("PI Amount"), Required(ErrorMessage = "PI Amount must not be blank.")]
        [DisplayFormat(DataFormatString = "{0:###,###0.00}")]
        public decimal? PIAmount { get; set; }
        [Required]
        public string CurrencyType { get; set; }
        [Required]
        public short CurrencyId { get; set; }
        [DisplayName("Is Forward To Bank")]
        public byte? IsForwardToBank { get; set; }
        [DisplayName("Forward Date")]
        public DateTime? ForwardToBankDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public short? ForwardUserId { get; set; }
        //[HiddenInput(DisplayValue = false)]
        public decimal? USDRate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public decimal? BDTRate { get; set; }
        [HiddenInput(DisplayValue = false), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal? AmountInUSD { get; set; }
        [HiddenInput(DisplayValue = false), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal? AmountInBDT { get; set; }
        [DisplayName("PI Validity Date"), DataType(DataType.DateTime)]
        public System.DateTime? ExpireDate { get; set; }
        [DisplayName("Supplier Tenure"), Required(ErrorMessage = "Supplier tenure days must not be blank.")]
        public short? Tenure { get; set; }
        [DisplayName("Payment Term"), DisplayFormat(ConvertEmptyStringToNull = false), Required(ErrorMessage = "Payment term have to select.")]
        public string PaymentTerms { get; set; }
        [DisplayName("Lead Time"), Required(ErrorMessage = "Lead time must not be blank.")]
        public short? LeadTime { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Remarks"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }

        [Display(Name = "Q. App."), Required]
        public string[] QAList { get; set; }

        [Display(Name = "Q. App."), Required]
        public string[] FactoryList { get; set; }

        public ICollection<RequisitionView> piSub { get; set; }
    }

    public partial class PISubView : IFIMSContext
    {
        public int? PIFileId { get; set; }
        public int? PIFileSubId { get; set; }
        public int? PIFileRelationId { get; set; }
        public int? RelationId { get; set; }
        public int? PApprovalSubId { get; set; }
        public int? QApprovalSubId { get; set; }
        public int? PRSubId { get; set; }
        public int? POSubId { get; set; }
        public short? FactoryId { get; set; }
        public int? ProductId { get; set; }
        public decimal? CurrentStock { get; set; }
        public decimal? StockInTransit { get; set; }
        public decimal? TotalStockArrangement { get; set; }
        public decimal? PDRequirement { get; set; }
        public decimal? TotalStockForDay { get; set; }
        public short? QuantityUnitId { get; set; }
        public decimal? PIQuantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Amount { get; set; }
        public short? CurrencyId { get; set; }
        public decimal? BDTRate { get; set; }
        public decimal? USDRate { get; set; }
        public Nullable<decimal> AmountInUSD { get; set; }
        public Nullable<decimal> AmountInBDT { get; set; }
        public byte IsCapex { get; set; }
        public short? RowNo { get; set; }
        public short? IsPItemId { get; set; }
        public int? LCFileSubId { get; set; }
        public short? RelationType { get; set; }
    }

    public partial class PILC
    {
        public int? PIFileId { get; set; }
        public string PINo { get; set; }
        public System.DateTime? PIDate { get; set; }
        public short? BeneficiaryId { get; set; }
        public int? LocationId { get; set; }
        public short? SupplierId { get; set; }
        public short? LocalAgentId { get; set; }
        public short? OriginCountryId { get; set; }
        public short? POLId { get; set; }
        public System.DateTime? RequisitionDate { get; set; }
        public System.DateTime? BookingConfirmDate { get; set; }
        public System.DateTime? ShipmentDate { get; set; }
        public short? Tenure { get; set; }
        public decimal? PIAmount { get; set; }
        public decimal? LCFileAmount { get { return PIAmount; } set { PIAmount = value; } }
        public string CurrencyType { get; set; }
        public decimal? USDRate { get; set; }
        public decimal? RateInUSD { get; set; }
        public decimal? AmountInTK { get; set; }
        public string TenureType { get; set; }
    }

    public partial class Menu : IFIMSContext
    {
        public Menu()
        {
        }

        [Key]
        public short MenuId { get; set; }
        [Display(Name = "Code")]
        public string MenuCode { get; set; }
        [Display(Name = "Name")]
        public string MenuName { get; set; }
        [Display(Name = "Title")]
        public string MenuTitle { get; set; }
        [Display(Name = "Parent")]
        public short? MenuPId { get; set; }
        [Display(Name = "Parent")]
        public string MenuPCode { get; set; }
        [Display(Name = "Category")]
        public byte? ModCatId { get; set; }
        [Display(Name = "Type")]
        public byte? MenuTypeId { get; set; }
        [Display(Name = "Sub Type")]
        public byte? MenuSubTypeId { get; set; }
        [Display(Name = "Reference")]
        public short? RefClassId { get; set; }
        [Display(Name = "Action")]
        public short? DefaultActionId { get; set; }
        [Display(Name = "Param List")]
        public string DefaultParameter { get; set; }
        [Display(Name = "Param Value List")]
        public string DefaultParameterValue { get; set; }
        public string CSSClass { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        public byte? IsActive { get; set; }
    }

    public partial class EMailCollection: IFIMSContext
    {
        public EMailCollection()
        {
        }

        public EMailCollection(IFIMSContext viewModel)
        {
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValueEMC("EMailCollection", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    }
    
    public partial class EMailCollectionView: IFIMSContext
    {
        public int? EmailId { get; set; }
        public string EmailType { get; set; }
        public string ModuleName { get; set; }
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailBCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailAttachmentLink { get; set; }
        public byte? HasAttachment { get; set; }
        public byte? CreateAttachment { get; set; }
        public string AllatchmentFileName { get; set; }
        public byte[] Attachment { get; set; }
        public string ReportName { get; set; }
        public string ParameterList { get; set; }
        public string ParameterValueList { get; set; }
        public string RelationType { get; set; }
        public int? RelationId { get; set; }
        public int? RelationSubId { get; set; }
        public System.DateTime? EntryDate { get; set; }
        public short? CreatedBy { get; set; }
        public System.DateTime? ProcessAt { get; set; }
        public byte? SendingStatus { get; set; }
        public System.DateTime? SendingDate { get; set; }
        public System.DateTime? LastProcessingDate { get; set; }
        public long? SendingCount { get; set; }
        public short? LastSentBy { get; set; }
        public string FormulaList { get; set; }
        public string FormulaValueList { get; set; }
    }

    public partial class Role : IFIMSContext
    {
        public Role()
        {
        }

        public short? RoleId { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Activity")]
        public byte IsActive { get; set; }
        [Display(Name = "SL #")]
        public short? RankId { get; set; }
    }

    public partial class RemarksDetailView : IFIMSContext
    {

        public RemarksDetailView()
        {
        }

        public int RemarksId { get; set; }
        public int PRId { get; set; }
        public int ReferenceId { get; set; }
        public short TypeId { get; set; }
        public short SupplierId { get; set; }
        public int ProductId { get; set; }
        public string Remarks { get; set; }
        public Nullable<short> EntryUserId { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<short> RevisionUserId { get; set; }
        public Nullable<System.DateTime> RevisionDate { get; set; }
        public string IPAddress { get; set; }
        public string PCName { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }
    }

    public partial class BrandView : IFIMSContext
    {
        [Display(Name = "ID")]
        public short BrandId { get; set; }
        [Display(Name = "Code")]
        public string BrandCode { get; set; }
        [Display(Name = "Name")]
        public string BrandName { get; set; }
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }
        [Display(Name = "Active ?")]
        public bool Activity { get { return (IsActive == 1 ? true : false); } set { IsActive = Byte.Parse(value ? "1" : "0"); } }
        [Display(Name = "Active ?")]
        public byte IsActive { get; set; }
        [Display(Name = "SL #")]
        public short RankId { get; set; }
    }

    public partial class MailSend : IFIMSContext
    {
        [Display(Name = "ID")]
        public int? MailId { get; set; }
        [Display(Name = "Sending Date")]
        public DateTime? SendingDate { get; set; }
        [Display(Name = "Addressee")]
        public string MailAddressee { get; set; }
        [Display(Name = "To")]
        public string MailTo { get; set; }
        [Display(Name = "CC")]
        public string MailCC { get; set; }
        [Display(Name = "BCC")]
        public string MailBCC { get; set; }
        [Display(Name = "Remarks"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [Display(Name = "Upload File")]
        public HttpPostedFileBase file { get; set; }    
    }

    public partial class UserPermission : IFIMSContext
    {
        public UserPermission()
        {
        }

        public short UserId { get; set; }
        public short RoleId { get; set; }
        public short ActionId { get; set; }
    }

    [MetadataType(typeof(MenuWiseActionMetaData))]
    public partial class MenuWiseAction : IFIMSContext
    {

    }

    public class MenuWiseActionMetaData
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ParameterList { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ParameterValueList { get; set; }
    }

    public partial class CompleteStatus : IFIMSContext
    {
        public CompleteStatus()
        {
        }

        public int TranId { get; set; }
        public short status { get; set; }
        public DateTime? FactoryETA { get; set; }
    }

    public partial class PlanDetails: IFIMSContext
    {
        public int BookingId { get; set; }
        public string BookingNo { get; set; }
        public string CustomerName { get; set; }
        public string RepresentativeName { get; set; }
        public int SRRId { get; set; }
        public int SRRPlanId { get; set; }
        public int PlanId { get; set; }
        public DateTime PlanDate { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int BookingSubId { get; set; }
        public string Measurement { get; set; }
        public string PaperCombination { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal PlanQty { get; set; }
        public decimal BalanceQty { get; set; }
        public decimal RequistionQty { get; set; }
        public string QuantityUnit { get; set; }
        public short QuantityUnitId { get; set; }
    }

    public partial class PlanAgRequision : IFIMSContext
    {
        public int PlanId { get; set; }
        public decimal Quantity { get; set; }
    }

    public partial class RequistionProduct : IFIMSContext
    {
        public short CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public short RowNo { get; set; }
        public short IssueType { get; set; }
    }

    public partial class Old2NewQuantityUnit : IFIMSContext
    {
        public string OldUnit { get; set; }
        public string NewUnit { get; set; }
    }

    public partial class TypeConverson : IFIMSContext
    {
        public short SourceTypeId { get; set; }
        public short DeistinationTypeId { get; set; }
    }

    public partial class _SelectListItem : IFIMSContext
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Id2 { get; set; }
        public string Text2 { get; set; }
    }

    public partial class SupplierSelectionMainView : IFIMSContext
    {
        public SupplierSelectionMainView()
        {
            //this.IssueSubs = new HashSet<IssueSub>();
            //this.IssueAgainstLCs = new HashSet<IssueAgainstLC>();
            //this.ConsumptionAgIssues = new HashSet<ConsumptionAgIssue>();
        }

        [Display(Name = "ID")]
        public int SupplierSelectionId { get; set; }
        [Display(Name = "SSE #")]
        public string SupplierSelectionNo { get; set; }
        [Display(Name = "SSE Date")]
        public System.DateTime SSEDate { get; set; }
        [Display(Name = "Factory")]
        public short FactoryId { get; set; }
        [Display(Name = "CategoryId")]
        public short CategoryId { get; set; }
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        [Display(Name = "ItemId")]
        public int ItemId { get; set; }
        [Display(Name = "Iteme Name")]
        public string ItemeName { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }
    }

    public partial class SupplierSelectionSubView : IFIMSContext
    {

        public SupplierSelectionSubView()
        {
            
        }
        public SupplierSelectionSubView(bool is_init)
        {
            if (is_init)
            {
                foreach (PropertyInfo pi in this.GetType().GetProperties())
                {
                    var type = pi.PropertyType;
                    object obj = null;

                    if (type == typeof(int) || type == typeof(int?))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short) || type == typeof(short?))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte) || type == typeof(byte?))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double) || type == typeof(double?))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal) || type == typeof(decimal?))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime) || type == typeof(DateTime?))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }

                    pi.SetValue(this, obj);
                }
            }
        }

        [Display(Name = "SSE ID #")]
        public int SupplierSelectionId { get; set; }
        [Display(Name = "ID")]
        public int SupplierSelectionSubId { get; set; }
        [Display(Name = "Supplier ID")]
        public short SuppliersID { get; set; }
        [Display(Name = "Suppliers Name")]
        public string SuppliersName { get; set; }
        [Display(Name = "Our Requirment")]
        public string SampleOurReq { get; set; }
        [Display(Name = "Sample Result")]
        public string SampleResult { get; set; }
        [Display(Name = "Evaluation Ranking")]
        public bool EvaluationRanking { get; set; }
        [Display(Name = "Remarks"), DisplayFormat(ConvertEmptyStringToNull = false), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [Display(Name = "SL #")]
        public short? RowNo { get; set; }
        [NotMapped]
        public bool _IsOnEdit { get; set; }
    }
}