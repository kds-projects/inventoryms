//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InventoryMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    //[MetadataType(typeof(MenuDetailMetaData))]
    public partial class MenuDetail : IFIMSContext
    {
    	
        public MenuDetail()
        {
            this.MenuWiseActions = new HashSet<MenuWiseAction>();
        }
    
    	public MenuDetail(IFIMSContext viewModel)
        {
            this.MenuWiseActions = new HashSet<MenuWiseAction>();
    
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
    				if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
    				{
    					object obj = clsMain.getDefaultValue("MenuDetail", pi.Name, pi.PropertyType);
    					if (obj != null)
    						pi.SetValue(this, obj);
    				}
    				else
    				{
    					pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
    				}
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("MenuDetail", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }
    
    	public short MenuId { get; set; }
    	public string MenuCode { get; set; }
    	public string MenuName { get; set; }
    	public string MenuTitle { get; set; }
    	public short MenuPId { get; set; }
    	public string MenuPCode { get; set; }
    	public byte ModCatId { get; set; }
    	public byte MenuTypeId { get; set; }
    	public byte MenuSubTypeId { get; set; }
    	public short RefClassId { get; set; }
    	public string CSSClass { get; set; }
    	public short RankId { get; set; }
    	public byte IsActive { get; set; }
    	public short DefaultActionId { get; set; }
    	public string DefaultParameter { get; set; }
    	public string DefaultParameterValue { get; set; }
    	public string Versions { get; set; }
    	[NotMapped]
    	public bool _IsOnEdit { get; set; }
    
        public virtual ICollection<MenuWiseAction> MenuWiseActions { get; set; }
    }
    
    //public class MenuDetailMetaData
    //{
    //}
}
