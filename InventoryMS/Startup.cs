﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InventoryMS.Startup))]
namespace InventoryMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
