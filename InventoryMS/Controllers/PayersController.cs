﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers
{
    public class PayersController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();

        // GET: /Payers/
        public ActionResult Index()
        {
            return View(db.Payers.ToList());
        }

        // GET: /Payers/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payer payer = db.Payers.Find(id);
            if (payer == null)
            {
                return HttpNotFound();
            }
            return View(payer);
        }

        // GET: /Payers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Payers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="PayerId,PayerCode,PayerName,ShortName,PayerTypeId,IsActive,RankId,CreatedBy,CreatedTime,UpdatedBy,UpdateTime,PCName,IPAddress")] Payer payer)
        {
            if (ModelState.IsValid)
            {
                db.Payers.Add(payer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payer);
        }
        // POST: /Warehouse/Save
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(Payer payer)
        {
            try
            {
                //var abc = db.SRRMains.Where(l => l.Year == DateTime.Today.Year && l.FactoryId == 1).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000");
                payer.RankId = Convert.ToInt16(db.Payers.Select(c => c.RankId + 1).DefaultIfEmpty(1).Max());
                db.Payers.Add(new Payer(payer));
                db.SaveChanges();

                return Json(new { status = "success", id = payer.PayerId, name = payer.PayerName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Payers/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payer payer = db.Payers.Find(id);
            if (payer == null)
            {
                return HttpNotFound();
            }
            return View(payer);
        }

        // POST: /Payers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="PayerId,PayerCode,PayerName,ShortName,PayerTypeId,IsActive,RankId,CreatedBy,CreatedTime,UpdatedBy,UpdateTime,PCName,IPAddress")] Payer payer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payer);
        }

        // GET: /Payers/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payer payer = db.Payers.Find(id);
            if (payer == null)
            {
                return HttpNotFound();
            }
            return View(payer);
        }

        // POST: /Payers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            Payer payer = db.Payers.Find(id);
            db.Payers.Remove(payer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
