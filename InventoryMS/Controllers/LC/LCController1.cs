﻿using InventoryMS.Models;
using System;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryMS.Controllers.LC
{
    public class LCController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private InventoryMS.Models.Old.LCManagementEntities dbLC = new InventoryMS.Models.Old.LCManagementEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();

        // GET: LC
        [CAuthorize]
        public ActionResult Pipeline(string id)
        {
            if (id.ToLower().Equals("edit"))
            {
                var LCNos = (from p in dbFund.LCMains.AsEnumerable()
                             //join q in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals q.LCFileId
                             //join r in dbFund.PISubs.AsEnumerable() on q.PIFileSubId equals r.PIFileSubId
                             //join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                             join t in db.PipelineMains.AsEnumerable() on p.LCFileId equals t.LCFileId
                             select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             ).Union(
                             from p in dbLC.LCMains.AsEnumerable()
                             join t in db.PipelineMains.AsEnumerable() on p.LCFileId equals t.LCFileId
                             select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             ).Distinct().ToList();

                ViewBag.LCFile = new SelectList(LCNos, "LCFileId", "LCNo");
                ViewBag.ULotNos = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "PLEId", "LotNo");
                ViewBag.UNLotNos = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "PLEId", "LotNo");
                ViewBag.POL = new SelectList(dbFund.Ports, "PortId", "PortName");

                return View("PipelineUP");
            }
            else
            {
                var plLC = db.PipelineMains.Where(l => l.IsLastLot == 1).Select(c => c.LCFileId).Distinct().ToList();

                var LCNos = (from p in dbFund.LCMains.AsEnumerable()
                             join q in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals q.LCFileId
                             join r in dbFund.PISubs.AsEnumerable() on q.PIFileId equals r.PIFileId
                             join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                             join t in db.PipelineSubs.GroupBy(l => l.PIFileSubId).Select(c => new { PIFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit), IsLastLot = c.Max(l => l.PipelineMain.IsLastLot) }).Where(l => l.IsLastLot == 0).AsEnumerable() on r.PIFileSubId equals t.PIFileSubId into u
                             from v in u.DefaultIfEmpty()
                             where !plLC.Contains(p.LCFileId) && r.PIQuantity - (v == null ? 0 : v.PLQty) > 0
                             select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             ).Union(
                             from p in dbLC.LCMains.AsEnumerable()
                             join q in dbLC.LCSubs.AsEnumerable() on p.LCFileId equals q.LCFileId
                             join r in dbLC.PISubs.AsEnumerable() on q.PIFileSubId equals r.PIFileSubId
                             join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                             join t in db.PipelineSubs.GroupBy(l => l.LCFileSubId).Select(c => new { LCFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit), IsLastLot = c.Max(l => l.PipelineMain.IsLastLot) }).Where(l => l.IsLastLot == 0).AsEnumerable() on q.LCFileSubId equals t.LCFileSubId into u
                             from v in u.DefaultIfEmpty()
                             where !plLC.Contains(p.LCFileId) && r.BookingQuantity - (v == null ? 0 : v.PLQty) > 0
                             select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             ).Distinct().ToList();

                ViewBag.LCFile = new SelectList(LCNos, "LCFileId", "LCNo");
                ViewBag.LotNos = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "LotNo", "LotNo");
                ViewBag.POL = new SelectList(dbFund.Ports, "PortId", "PortName");

                return View("Pipeline");
            }
        }

        // GET: LC
        [CAuthorize]
        public ActionResult Save(PipelineView pipelineView, ICollection<PipelineSubView> pipelineSubView, ICollection<PackingListView> packingListView)
        {
            try
            {
                PipelineMain plmain = new PipelineMain();
                if (pipelineView.PLEId == null || pipelineView.PLEId <= 0)
                {
                    plmain = new PipelineMain(pipelineView);

                    plmain.ArrivalRecordDate = DateTime.Now;
                }
                else
                {
                    plmain = db.PipelineMains.Find(pipelineView.PLEId);
                    if (plmain.ArrivalRecordDate.AddDays(7) < clsMain.getCurrentTime().Date)
                    {
                        plmain.PortArivalDatePrev = plmain.PortArivalDate;
                        plmain.StoreArivalDatePrev = plmain.StoreArivalDate;
                        plmain.ArrivalRecordDate = clsMain.getCurrentTime();
                    }

                    plmain = (PipelineMain)pipelineView.ConvertNotNull(plmain);
                }

                if (pipelineView.PLEId == null || pipelineView.PLEId <= 0)
                {
                    var beneficiaryid = 0;

                    var lc = dbLC.LCMains.Find(pipelineView.LCFileId);
                    if (lc == null)
                    {
                        beneficiaryid = lc.BeneficiaryBankId;
                    }
                    else
                    {
                        beneficiaryid = dbFund.LCMains.Find(pipelineView.LCFileId).BeneficiaryId;
                    }

                    plmain.PLEDate = clsMain.getCurrentTime();
                    plmain.CreatedBy = Int16.Parse(Session["UserId"].ToString());
                    plmain.CreatedTime = plmain.PLEDate;
                    //plmain.PLENo = " + QAMain.PurchaseOrder.BondId.ToString("00") + ";
                    var bond = db.Bonds.Where(l => l.CompanyId == beneficiaryid).FirstOrDefault().BondId.ToString("00");

                    plmain.PLENo = "PL/" + bond + "/" + db.PipelineMains.Where(l => l.PLENo.Substring(0, 5).Equals("PL/" + bond) && l.PLEDate.Year == plmain.PLEDate.Year).Select(c => c.SerialNo + 1).DefaultIfEmpty(1).Max().Value.ToString("00000") + "-" + plmain.PLEDate.ToString("MM/yy").ToUpper();

                    foreach (var sub in pipelineSubView)
                    {
                        PipelineSub plsub = new PipelineSub(sub);
                        plmain.PipelineSubs.Add(plsub);
                    }

                    db.PipelineMains.Add(plmain);
                }
                else
                {
                    foreach (var sub in pipelineSubView)
                    {
                        if (sub.PLESubId == null || sub.PLESubId <= 0)
                        {
                            PipelineSub plsub = new PipelineSub(sub);

                            plmain.PipelineSubs.Add(plsub);
                        }
                        else
                        {
                            PipelineSub plsub = db.PipelineSubs.Find(sub.PLESubId);
                            plsub = (PipelineSub)sub.ConvertNotNull(plsub);
                            db.Entry(plsub).State = EntityState.Modified;
                        }
                    }

                    if (packingListView != null)
                    {
                        foreach (var sub in packingListView)
                        {
                            if (sub.PLECId == null || sub.PLECId <= 0)
                            {
                                PipelinePackingList plpack = new PipelinePackingList(sub);

                                plmain.PipelinePackingLists.Add(plpack);
                            }
                            else
                            {
                                PipelinePackingList plpack = db.PipelinePackingLists.Find(sub.PLECId);
                                plpack = (PipelinePackingList)sub.ConvertNotNull(plpack);
                                db.Entry(plpack).State = EntityState.Modified;
                            }
                        }

                        var unpacksub = db.PipelinePackingLists.Where(l => l.PLEId == plmain.PLEId).ToList();

                        for (int ri = 0; ri < unpacksub.Count; ri++)
                        {
                            if (db.Entry(unpacksub[ri]).State == EntityState.Unchanged)
                                db.Entry(unpacksub[ri]).State = EntityState.Deleted;
                        }
                    }

                    var unsub = db.PipelineSubs.Where(l => l.PLEId == plmain.PLEId).ToList();

                    for (int ri = 0; ri < unsub.Count; ri++)
                    {
                        if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                            db.Entry(unsub[ri]).State = EntityState.Deleted;
                    }

                    plmain.UpdatedBy = Int16.Parse(Session["UserId"].ToString());
                    plmain.UpdateTime = clsMain.getCurrentTime();

                    db.Entry(plmain).State = EntityState.Modified;
                }

                db.SaveChanges();
                return Json(new { status = "Success", plno = plmain.PLENo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
            //return Content("Pipeline status update successfull.");
        }

        public ActionResult RowTemplate(ICollection<PipelineSubView> pipelineSubView)
        {
            return PartialView(pipelineSubView);
        }


        // GET: LC
        public ActionResult LCDetails(int id)
        {
            var lcold = dbLC.LCMains.Find(id);
            var lcnew = dbFund.LCMains.Find(id);
            var lc = lcold == null ? (InventoryMS.Models.LC)lcnew.Convert(new InventoryMS.Models.LC()) : (InventoryMS.Models.LC)lcold.Convert(new InventoryMS.Models.LC());

            if (lc == null && lcold == null)
                throw new Exception("Invalid lc.");

            return Json(new { data = lc }, JsonRequestBehavior.AllowGet);
        }

        // GET: LC
        public ActionResult ItemDetails(int id)
        {
            var lcold = dbLC.LCMains.Find(id);
            var lcnew = dbFund.LCMains.Find(id);
            var lc = lcold == null ? (InventoryMS.Models.LC)lcnew.Convert(new InventoryMS.Models.LC()) : (InventoryMS.Models.LC)lcold.Convert(new InventoryMS.Models.LC());

            if (lc == null)
                throw new Exception("Invalid lc.");

            var items = new List<PipelineSubView>();

            if (lc.LCDate >= DateTime.Parse("01-Jan-2018"))
            {
                items = (
                             from p in dbLC.LCSubs.AsEnumerable()
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                             join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                             join t in db.PipelineSubs.GroupBy(c => c.LCFileSubId).Select(c => new { LCFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit) }).AsEnumerable() on p.LCFileSubId equals t.LCFileSubId into u
                             from s in u.DefaultIfEmpty()
                             join v in dbLC.PISubs.AsEnumerable() on p.PIFileSubId equals v.PIFileSubId
                             join w in db.PRSubs.AsEnumerable() on v.PRSubId equals w.PRSubId
                             where p.LCFileId == id && p.BookingQuantity - (s == null ? 0 : s.PLQty) > 0
                             select new { p, q, r, w.FactoryETA, BalQty = p.BookingQuantity - (s == null ? 0 : s.PLQty) }
                             ).Select(c => new PipelineSubView()
                             {
                                 CategoryId = c.q.CategoryId,
                                 ItemId = c.q.ItemId,
                                 ItemName = c.q.ItemName,
                                 FactoryETA = c.FactoryETA,
                                 LCQuantity = c.BalQty,
                                 QuantityUnit = c.r.QuantityUnit,
                                 QuantityUnitId = c.r.QuantityUnitId,
                                 StockInTransit = c.BalQty,
                                 LCFileSubId = c.p.LCFileSubId,
                                 PIFileSubId = c.p.PIFileSubId,
                                 PLEId = 0,
                                 PLESubId = 0
                             }).ToList();
            }
            else
            {
                items = (
                             from pi in dbFund.LCMains.Find(id).LCWisePIs.AsEnumerable()
                             join p in dbFund.PISubs.AsEnumerable() on pi.PIFileId equals p.PIFileId
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                             join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                             join t in db.PipelineSubs.GroupBy(c => c.PIFileSubId).Select(c => new { PIFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit) }).AsEnumerable() on p.PIFileSubId equals t.PIFileSubId into u
                             from s in u.DefaultIfEmpty()
                             join w in db.PRSubs.AsEnumerable() on p.PRSubId equals w.PRSubId
                             where p.PIQuantity - (s == null ? 0 : s.PLQty) > 0
                             select new { p, q, r, w.FactoryETA, BalQty = p.PIQuantity - (s == null ? 0 : s.PLQty) }
                             ).Select(c => new PipelineSubView()
                             {
                                 CategoryId = c.q.CategoryId,
                                 ItemId = c.q.ItemId,
                                 ItemName = c.q.ItemName,
                                 FactoryETA = c.FactoryETA,
                                 LCQuantity = c.BalQty,
                                 QuantityUnit = c.r.QuantityUnit,
                                 QuantityUnitId = c.r.QuantityUnitId,
                                 StockInTransit = c.BalQty,
                                 LCFileSubId = 0,
                                 PIFileSubId = c.p.PIFileSubId,
                                 PLEId = 0,
                                 PLESubId = 0
                             }).ToList();
            }

            ViewBag.polid = lc.POLId;

            return PartialView("RowTemplate", items);
        }

        // GET: LC
        public ActionResult LotList(int id, string type = "create")
        {
            if (type.ToLower().Equals("edit"))
            {
                ViewBag.LotNos = new SelectList(db.PipelineMains.Where(l => l.LCFileId == id), "PLEId", "LotNo");

                return PartialView();
            }
            else
            {
                ViewBag.UNLotNos = new SelectList((
                                from p in db.Custom_Type.Where(l => l.Flag.Equals("LOTNO")).AsEnumerable()
                                join q in db.PipelineMains.Where(l => l.LCFileId == id).AsEnumerable() on p.TypeName equals q.LotNo into r
                                from s in r.DefaultIfEmpty()
                                where s == null
                                select p
                                ).Distinct().ToList(), "TypeName", "TypeName");

                return PartialView("UNLotList");
            }
        }

        // GET: LC
        public ActionResult LotDetails(int id)
        {
            try
            {
                var pl = db.PipelineMains.Find(id);
                var polid = pl.POLId == 0 ? dbLC.LCMains.Find(pl.LCFileId).POLId : pl.POLId;

                if (pl == null)
                    throw new Exception("Invalid lot no.");

                var lots = (from p in db.Custom_Type.Where(l => l.Flag.Equals("LOTNO")).AsEnumerable()
                            join q in db.PipelineMains.Where(l => l.LCFileId == pl.LCFileId).AsEnumerable() on p.TypeName equals q.LotNo into r
                            from s in r.DefaultIfEmpty()
                            where s == null || p.TypeName.Equals(pl.LotNo)
                            select p
                            ).Distinct().ToList();

                ViewBag.UNLotNos = new SelectList(lots, "TypeName", "TypeName", pl.LotNo);

                var unlot = RenderRazorViewToString(this.ControllerContext, "UNLotList", null);

                return Json(new
                {
                    lotlist = unlot,
                    plno = pl.PLENo,
                    polid = polid,
                    sd = pl.ShipmentDate,
                    bl = pl.BLDate,
                    pe = pl.PortArivalDate,
                    se = pl.StoreArivalDate,
                    lrd = pl.LotReceivedDate,
                    dab = pl.DocArrivalAtBankDate,
                    fe = pl.FactoryETA.ToString("dd-MMM-yyyy"),
                    ll = pl.IsLastLot,
                    sr = pl.IsLotReceived,
                    cl = pl.CurrentLocation,
                    ddfp = pl.PortDeliveryDate,
                    boed = pl.BillOfEntryDate,
                    boeno = pl.BillOfEntryNo,
                    rm = pl.Remarks,
                    ct = clsMain.getCurrentTime()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("Error");
            }
        }

        // GET: LC
        public ActionResult LotItemDetails(int id)
        {
            var items = (from p in dbLC.LCSubs.AsEnumerable()
                         join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                         join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                         join s in db.PipelineSubs.AsEnumerable() on p.LCFileSubId equals s.LCFileSubId
                         join t in dbLC.PISubs.AsEnumerable() on p.PIFileSubId equals t.PIFileSubId
                         join u in db.PRSubs.AsEnumerable() on t.PRSubId equals u.PRSubId
                         where s.PLEId == id
                         select new { p, q, r, s, u.FactoryETA }
                     ).Select(c => new PipelineSubView()
                     {
                         CategoryId = c.q.CategoryId,
                         ItemId = c.q.ItemId,
                         ItemName = c.q.ItemName,
                         FactoryETA = c.FactoryETA,
                         LCQuantity = c.p.BookingQuantity,
                         QuantityUnit = c.r.QuantityUnit,
                         QuantityUnitId = c.r.QuantityUnitId,
                         StockInTransit = c.s.StockInTransit,
                         LCFileSubId = c.p.LCFileSubId,
                         PLEId = c.s.PLEId,
                         PLESubId = c.s.PLESubId
                     }).ToList();

            if (items == null)
                throw new Exception("Invalid lot no.");

            return PartialView("RowTemplate", items);
        }

        public ActionResult PackingTemplate(int? id, short? slNo)
        {
            id = id ?? 0;
            var pack = (from p in db.PipelinePackingLists.AsEnumerable()
                        join q in dbProd.CustomTypes.AsEnumerable() on p.TypeId equals q.TypeId
                        where p.PLEId == id
                        select new { p, q.TypeName }
                       ).Select(c => new PackingListView()
                       {
                           PLEId = c.p.PLEId,
                           PLECId = c.p.PLECId,
                           TypeId = c.p.TypeId,
                           Size = c.TypeName,
                           Quantity = c.p.Quantity,
                           LCLQuantity = c.p.LCLQuantity,
                           FCLQuantity = c.p.FCLQuantity
                       });

            if (slNo == null || slNo == 0)
                return PartialView(pack);
            else
            {
                ViewBag.SLNo = slNo.Value.ToString("000");
                ViewBag.Size = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CONTAINERSIZE")), "TypeId", "TypeName");

                return PartialView("PackingTemplate_Blank", new PackingListView());
            }
        }

        public ActionResult MailTemplate(int? id)
        {
            var emaillist = new MailSend();

            if (id != null)
            {
                ViewBag.MailList = new MultiSelectList(db.Sys_User_Name.Select(c => new { Mail = c.eMail }).Distinct(), "Mail", "Mail");
                var lcsid = db.PipelineMains.Find(id).PipelineSubs.FirstOrDefault().LCFileSubId;
                var factoryid = dbLC.LCSubs.Find(lcsid).FactoryId;
                emaillist = db.EmailFrameworks.Where(l => l.EmailTypeId == 3 && l.FactoryId == factoryid).Select(c => new MailSend()
                {
                    MailTo = c.EmailGroupTo,
                    MailCC = c.EmailGroupCC,
                    MailBCC = c.EmailGroupBCC
                }).FirstOrDefault();
            }

            return PartialView(emaillist);
        }

        public static string RenderRazorViewToString(ControllerContext controllerContext, string viewName, object model)
        {
            controllerContext.Controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                ViewResult.View.Render(ViewContext, sw);
                ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }


        [HttpPost]
        [CAuthorize]
        public ActionResult MailSend() //MailSend mailSend, 
        {
            try
            {
                MailMessage mail = new MailMessage();
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        if (file != null)
                        {
                            Attachment attachment = new Attachment(file.InputStream, file.FileName);

                            mail.Attachments.Add(attachment);
                        }
                    }
                }

                var pleid = 0;
                if (!String.IsNullOrEmpty(Request.Params["PLEId"]))
                    pleid = int.Parse(Request.Params["PLEId"]);

                var email = db.EmailLibraries.Find(3);
                var pldetails = (from p in db.PipelineMains.AsEnumerable()
                                 join q in dbLC.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                                 join r in dbLC.LCSubs.AsEnumerable() on q.LCFileId equals r.LCFileId
                                 join s in dbProd.Factories.AsEnumerable() on r.FactoryId equals s.FactoryId
                                 join t in db.Suppliers.AsEnumerable() on q.SupplierId equals t.SupplierId
                                 join u in dbLC.PISubs.AsEnumerable() on r.PIFileSubId equals u.PIFileSubId
                                 join v in db.PRSubs.AsEnumerable() on u.PRSubId equals v.PRSubId
                                 where p.PLEId == pleid
                                 select new
                                 {
                                     p,
                                     q.LCNo,
                                     q.LCDate,
                                     s.FactoryId,
                                     s.FactoryName,
                                     t.SupplierName,
                                     PRNo = v.PRMain.PRNo,
                                     LotNo = p.LotNo,
                                     PRDate = v.PRMain.PRDate,
                                     BrandName = v.Brand.BrandName
                                 });

                var items = (from p in db.PipelineSubs.Where(l => l.PLEId == pleid).AsEnumerable()
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ItemId equals q.ItemId
                             join qc in dbProd.ItemCategories.AsEnumerable() on q.CategoryId equals qc.CategoryId
                             join lc in dbLC.LCSubs.AsEnumerable() on p.LCFileSubId equals lc.LCFileSubId
                             join u in dbProd.viewQuantityUnits.AsEnumerable() on lc.QuantityUnitId equals u.QuantityUnitId
                             join r in dbProd.ItemVariants.AsEnumerable() on q.ItemId equals r.ItemId into s
                             from t in s.DefaultIfEmpty()
                             select new { ItemName = t == null ? q.ItemName : lc.FactoryId == 2 || lc.FactoryId == 16 ? q.ItemName : qc.CategoryName, p.StockInTransit, u.QuantityUnit, p.PLESubId });

                //var ic = items.Count(); var tq = items.Select(c => c.StockInTransit).Sum();
                var pipeline = pldetails.FirstOrDefault();
                var itemlist = items.Select(c => c.ItemName).Distinct().Aggregate((i, j) => i + "," + j);
                var qty = items.Select(c => new { c.PLESubId, c.QuantityUnit, c.StockInTransit }).Distinct().GroupBy(c => c.QuantityUnit).Select(c => new { Unit = c.Key, StockInTransit = c.Sum(l => l.StockInTransit) }).Select(c => c.StockInTransit.ToString("##,##0.00") + " " + c.Unit).Aggregate((i, j) => i + "," + j);
                var factory = pldetails.Select(c => c.FactoryName).Distinct().Aggregate((i, j) => i + "," + j);
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                var mailaddressinfo = db.EmailFrameworks.Where(l => l.EmailTypeId == 3 && l.FactoryId == pipeline.FactoryId).FirstOrDefault();

                var user = db.Sys_User_Name.Find(clsMain.getCurrentUser());

                mail.From = new MailAddress(user.eMail);//mailaddressinfo.EmailGroupFrom);

                foreach (var to in mailaddressinfo.EmailGroupTo.Split(';').Where(c => !String.IsNullOrEmpty(c)).OrderBy(c => c))
                    if (!mail.To.Contains(new MailAddress(to)))
                        mail.To.Add(to);

                foreach (var cc in mailaddressinfo.EmailGroupCC.Split(';').Where(c => !String.IsNullOrEmpty(c)).OrderBy(c => c))
                    if (!mail.To.Contains(new MailAddress(cc)) && !mail.CC.Contains(new MailAddress(cc)))
                        mail.CC.Add(cc);

                foreach (var bcc in mailaddressinfo.EmailGroupBCC.Split(';').Where(c => !String.IsNullOrEmpty(c)).OrderBy(c => c))
                    if (!mail.To.Contains(new MailAddress(bcc)) && !mail.CC.Contains(new MailAddress(bcc)) && !mail.Bcc.Contains(new MailAddress(bcc)))
                        mail.Bcc.Add(bcc);

                if (!mail.To.Contains(new MailAddress(user.eMail)) && !mail.CC.Contains(new MailAddress(user.eMail)) && !mail.Bcc.Contains(new MailAddress(user.eMail)))
                    mail.Bcc.Add(user.eMail);

                mail.Subject = email.Subject.Replace("@LCNo", pipeline.LCNo);
                mail.Body = email.EmailBody.Replace("@Addressee", Request.Params["MailAddressee"])
                    .Replace("@PortDeliveryDate", pipeline.p.PortDeliveryDate.ToString("dd-MMM-yyyy"))
                    .Replace("@PRNo", pipeline.PRNo)
                    .Replace("@PRDate", pipeline.PRDate.ToString("dd-MMM-yyyy"))
                    .Replace("@LCNo", pipeline.LCNo)
                    .Replace("@LotNo", pipeline.LotNo)
                    .Replace("@LCDate", pipeline.LCDate.ToString("dd-MMM-yyyy"))
                    .Replace("@ItemList", itemlist)
                    .Replace("@TotalQty", qty)
                    .Replace("@Brand", pipeline.BrandName)
                    .Replace("@Factory", factory)
                    .Replace("@Supplier", pipeline.SupplierName)
                    .Replace("@DeliveryOfficer", textInfo.ToTitleCase(user.ShortName.ToLower()));

                mail.IsBodyHtml = true;
                //factory
                SmtpClient SmtpServer = new SmtpClient("203.82.207.6");
                SmtpServer.Credentials = new System.Net.NetworkCredential("mohammad.noman@kdsgroup.net", "bakalam");
                SmtpServer.Port = 25;
                SmtpServer.EnableSsl = false;
                //SmtpServer.Send
                SmtpServer.Send(mail);

                SmtpServer = null;
                return Json(new { status = "Success", message = "Mail send successfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = "Error", message = "Error while mail sending." }, JsonRequestBehavior.AllowGet);
            }
            //return Json("Mail send failed.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadFiles() //MailSend mailSend, 
        {
            //HttpPostedFileBase file

            if (Request.Files.Count > 0)
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        //Method 1 Get file details from current request    
                        //Uncomment following code if you wants to use method 1    
                        //if (Request.Files.Count > 0)    
                        // {    
                        //     var Inputfile = Request.Files[0];    

                        //     if (Inputfile != null && Inputfile.ContentLength > 0)    
                        //     {    
                        //         var filename = Path.GetFileName(Inputfile.FileName);    
                        //       var path = Path.Combine(Server.MapPath("~/uploadedfile/"), filename);    
                        //        Inputfile.SaveAs(path);    
                        //    }    
                        // }    

                        //Method 2 Get file details from HttpPostedFileBase class    

                        //string path = Path.Combine(Server.MapPath("~"), Path.GetFileName(file.FileName));
                        //file.SaveAs(path);

                        //if (!mailFrom.Contains("<"))
                        //    mailFrom = "CUBE | SDMS (Sales & Distribution Management System)<" + mailFrom + ">;";

                        if (file != null)
                        {
                            Attachment attachment = new Attachment(file.InputStream, file.FileName);

                            mail.Attachments.Add(attachment);

                        }
                    }

                    mail.From = new MailAddress("ipms.cube@kdsgroup.net");

                    mail.To.Add("mohammad.noman@kdsgroup.net");

                    //mail.Bcc.Add("mohammad.noman@kdsgroup.net");

                    mail.Subject = "Testing mail";
                    mail.Body = "fyi";
                    mail.IsBodyHtml = true;

                    SmtpClient SmtpServer = new SmtpClient("203.82.207.6");
                    SmtpServer.Credentials = new System.Net.NetworkCredential("mohammad.noman@kdsgroup.net", "bakalam");
                    SmtpServer.Port = 25;
                    SmtpServer.EnableSsl = false;
                    //SmtpServer.Send
                    SmtpServer.Send(mail);

                    SmtpServer = null;
                    return Json("File uploaded successfully.", JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json("Error while file uploading.", JsonRequestBehavior.AllowGet);
                }

            }
            return Json("model state failed.", JsonRequestBehavior.AllowGet);
        }
    }
}