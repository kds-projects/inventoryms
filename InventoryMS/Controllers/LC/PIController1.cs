﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers
{
    public class PIController1 : Controller
    {
        private InventoryMS.Models.Old.LCManagementEntities db = new InventoryMS.Models.Old.LCManagementEntities();
        private InventoryCubeEntities dbInv = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();

        // GET: /PI/
        public ActionResult Index()
        {
            //var pimains = db.PIMains.Include(l => l.Company).Include(l => l.LocalAgent).Include(l => l.Port).Include(l => l.SupplierInformation).Include(l => l.Country);
            return View(db.PIMains);
        }

        // GET: /PI/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PIMain pIMain = db.PIMains.Find(id);
            if (pIMain == null)
            {
                return HttpNotFound();
            }
            return View(pIMain);
        }

        // GET: /PI/Pending
        public ActionResult PendingRequsition(int id, short supid)
        {
            ViewBag.QAList = new SelectList((from p in dbInv.QuantityApprovalMains.AsEnumerable()
                      join q in dbInv.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                      join r in dbInv.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals r.QApprovalSubId
                      join u in db.PISubs.GroupBy(c => c.PApprovalSubId).Select(c => new { PApprovalSubId = c.Key, PIQty = c.Sum(l => l.BookingQuantity) }).AsEnumerable() on r.PApprovalSubId equals u.PApprovalSubId into v
                      from s in v.DefaultIfEmpty()
                      where p.PurchaseOrder.Bond.CompanyId == id && r.SupplierId == supid && q.ApprovedQuantity - (s == null ? 0 : s.PIQty) > 0
                      //where x.SupplierId == supid && qaid.Contains(p.QApprovalId)
                      select p).Distinct().ToList(), "QApprovalId", "QApprovalNo");

            return PartialView();
        }

        // GET: /PI/Pending
        public ActionResult RequisitionInfo(string[] id)
        {
            var QAList = (from p in dbInv.QuantityApprovalMains.AsEnumerable()
                          join q in dbInv.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                          join r in dbInv.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals r.QApprovalSubId

                          join u in db.PISubs.AsEnumerable() on r.PApprovalSubId equals u.PApprovalSubId into v
                          from s in v.DefaultIfEmpty()
                          where s == null && id.Select(c => int.Parse(c)).Contains(p.QApprovalId)
                          select new { p, q, r }).Select(c => new PIView()
                          {
                              BeneficiaryId = c.p.PurchaseOrder.Bond.CompanyId,
                              ETA = c.q.PRSub.FactoryETA,
                              LeadTime = c.q.PRSub.LeadTime,
                              SupplierId = c.r.SupplierId,
                              Tenure = c.r.TenureDays,
                              RequisitionDate = c.q.PRMain.PRDate
                          })
                              ;

            return PartialView();
        }

        public ActionResult RowTemplate(string[] id, short supid, int? pid, int? lid)
        {
            ICollection<RequisitionView> reqSubs = GetPIDetails(id, supid, pid, lid);

            return PartialView(reqSubs);
        }

        public ActionResult RowTemplatePI(ICollection<RequisitionView> reqSubs)
        {
            return PartialView("RowTemplate", reqSubs);
        }

        public ICollection<RequisitionView> GetPIDetails(string[] id, short supid, int? pid, int? lid)
        {
            ICollection<RequisitionView> reqSubs = new List<RequisitionView>();
            List<int> qaid = id.Select(c => int.Parse(c.Trim())).ToList();

            if (pid == null)
            {
                reqSubs = (from po in dbInv.QuantityApprovalMains.AsEnumerable()
                           join o in dbInv.QuantityApprovalSubs.AsEnumerable() on po.QApprovalId equals o.QApprovalId
                           join p in dbInv.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                           join r in dbInv.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                           join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                           join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                           join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                           join x in dbInv.PriceApprovalSubs.AsEnumerable() on o.QApprovalSubId equals x.QApprovalSubId
                           join q in dbInv.Suppliers.AsEnumerable() on x.SupplierId equals q.SupplierId
                           join y in dbProd.CustomTypes.AsEnumerable() on x.CurrencyId equals y.TypeId
                           join za in db.PISubs.GroupBy(c => c.PApprovalSubId).Select(c => new { PApprovalSubId = c.Key, PIQty = c.Sum(l => l.BookingQuantity) }).AsEnumerable() on x.PApprovalSubId equals za.PApprovalSubId into zb
                           from z in zb.DefaultIfEmpty()
                           where x.SupplierId == supid && qaid.Contains(o.QApprovalId) && o.ApprovedQuantity - (z == null ? 0 : z.PIQty) > 0
                           select new { po, p, x, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o, y.TypeName, BalQty = o.ApprovedQuantity - (z == null ? 0 : z.PIQty) }
                        ).Select(c => new RequisitionView()
                        {
                            POId = c.po.PurchaseOrder.POId,
                            POSubId = c.o.PurchaseOrderSub.POSubId,
                            QApprovalId = c.o.QApprovalId,
                            QApprovalSubId = c.o.QApprovalSubId,
                            QuotationId = c.x.QuotationId,
                            PApprovalId = c.x.PApprovalId,
                            PApprovalSubId = c.x.PApprovalSubId,
                            PRId = c.p.PRId,
                            PRSubId = c.p.PRSubId,
                            PRNo = c.p.PRMain.PRNo,
                            PRDate = c.p.PRMain.PRDate,
                            PONo = c.po.PurchaseOrder.PONo,
                            PODate = c.po.PurchaseOrder.PODate,
                            QANo = c.o.QuantityApprovalMain.QApprovalNo,
                            QADate = c.o.QuantityApprovalMain.QApprovalDate,
                            ProductId = c.p.ProductId,
                            CurrentStock = c.p.CurrentStock,
                            StockInTransit = c.p.StockInTransit,
                            PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                            HistoricalConsumption = c.p.HistoricalConsumption,
                            HistoricalPeriod = c.p.HistoricalPeriod,
                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                            LeadTime = c.p.LeadTime,
                            FactoryETA = c.p.FactoryETA,
                            BrandId = c.p.BrandId,
                            BrandName = c.BrandName,
                            PreferredSupplierId = c.p.PreferredSupplierId,
                            SupplierName = c.SupplierName,
                            Quantity = c.p.Quantity,
                            QuantityUnit = c.QuantityUnit,
                            QuantityUnitId = c.p.QuantityUnitId,
                            POQuantity = c.o.PurchaseOrderSub.Quantity,
                            ApprovedQuantity = c.BalQty,
                            UnitPrice = c.x.ApprovedPrice,
                            CurrencyId = c.x.CurrencyId,
                            Currency = c.TypeName,
                            CategoryId = c.CategoryId,
                            ProductName = c.ItemName,
                            RowNo = c.p.RowNo,
                            Remarks = c.p.Remarks
                        }).ToList();
            }
            else
            {
                reqSubs = (from po in dbInv.QuantityApprovalMains.AsEnumerable()
                            join o in dbInv.QuantityApprovalSubs.AsEnumerable() on po.QApprovalId equals o.QApprovalId
                            join p in dbInv.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                            join r in dbInv.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                            join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                            join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                            join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                            join x in dbInv.PriceApprovalSubs.AsEnumerable() on o.QApprovalSubId equals x.QApprovalSubId
                            join q in dbInv.Suppliers.AsEnumerable() on x.SupplierId equals q.SupplierId
                            join y in dbProd.CustomTypes.AsEnumerable() on x.CurrencyId equals y.TypeId
                            join ya in db.PIMains.Find(pid).PISubs.AsEnumerable() on x.PApprovalSubId equals ya.PApprovalSubId into yb
                            from z in yb.DefaultIfEmpty()
                            join za in db.PISubs.GroupBy(c => c.PApprovalSubId).Select(c => new { PApprovalSubId = c.Key, PIQty = c.Sum(l => l.BookingQuantity) }).AsEnumerable() on x.PApprovalSubId equals za.PApprovalSubId into zb
                            from zc in zb.DefaultIfEmpty()
                            join xa in db.LCSubs.AsEnumerable() on (z == null ? -1 : z.PIFileSubId) equals xa.PIFileSubId into xb
                            from xc in xb.DefaultIfEmpty()
                            join xd in dbInv.PipelineSubs.AsEnumerable() on (xc == null ? -1 : xc.LCFileSubId) equals xd.LCFileSubId into xe
                            from xf in xe.DefaultIfEmpty()
                            where x.SupplierId == supid && qaid.Contains(o.QApprovalId) && (o.ApprovedQuantity - (zc == null ? 0 : zc.PIQty) > 0 || z != null)
                            select new { po, p, x, z, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o, y.TypeName, zc, xc, xf }
                            ).Select(c => new RequisitionView()
                            {
                                POId = c.po.PurchaseOrder.POId,
                                POSubId = c.o.PurchaseOrderSub.POSubId,
                                QApprovalId = c.o.QApprovalId,
                                QApprovalSubId = c.o.QApprovalSubId,
                                QuotationId = c.x.QuotationId,
                                PApprovalId = c.x.PApprovalId,
                                PApprovalSubId = c.x.PApprovalSubId,
                                PIFileId = (c.z == null ? 0 : c.z.PIFileId),
                                PIFileSubId = (c.z == null ? 0 : c.z.PIFileSubId),
                                LCFileId = (c.xc == null ? 0 : c.xc.LCFileId),
                                LCFileSubId = (c.xc == null ? 0 : c.xc.LCFileSubId),
                                PLEId = (c.xf == null ? 0 : c.xf.PLEId),
                                PLESubId = (c.xf == null ? 0 : c.xf.PLESubId),
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                PONo = c.po.PurchaseOrder.PONo,
                                PODate = c.po.PurchaseOrder.PODate,
                                QANo = c.o.QuantityApprovalMain.QApprovalNo,
                                QADate = c.o.QuantityApprovalMain.QApprovalDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                DaysWithTotalStock = c.p.DaysWithTotalStock,
                                AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.o.PurchaseOrderSub.Quantity,
                                ApprovedQuantity = c.o.ApprovedQuantity + (c.z == null ? 0 : c.z.BookingQuantity) - (c.zc == null ? 0 : c.zc.PIQty),
                                PIQuantity = (c.z == null ? 0 : c.z.BookingQuantity),
                                UnitPrice = c.x.ApprovedPrice,
                                CurrencyId = c.x.CurrencyId,
                                Currency = c.TypeName,
                                CategoryId = c.CategoryId,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.p.Remarks
                            }).ToList();
            }

            return reqSubs;
        }

        // GET: /PI/Create
        public ActionResult Create()
        {
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.Supplier = new SelectList(dbInv.Suppliers, "SupplierId", "ShortName");
            ViewBag.QAList = new MultiSelectList(new List<string> { }.Select(c => new { QApprovalId = c, QApprovalNo = c }), "QApprovalId", "QApprovalNo");

            //ViewBag.BeneficiaryId = new SelectList(db.Companies, "CompanyId", "CompanyName");
            ViewBag.LocalAgent = new SelectList(db.LocalAgents, "AgentId", "AgentName");
            ViewBag.POL = new SelectList(dbInv.viewPorts, "PortId", "PortName");
            //ViewBag.SupplierId = new SelectList(db.SupplierInformations, "SupplierId", "SupplierName");
            //ViewBag.QuantityUnitId = db.viewQuantityUnits.ToList();
            ViewBag.OriginCountry = new SelectList(dbInv.Countries, "CountryId", "CountryName");
            ViewBag.CurrencyType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CURRCODE")), "TypeName", "TypeName");
            ViewBag.TenureTerms = new SelectList(db.viewTenureTypes, "TenureType", "TenureType");

            return View();
        }

        // POST: /PI/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [CAuthorize]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(PIView piview, ICollection<PISub> pisub)
        {
            try
            {
                PIMain pimain = new PIMain(piview);
                pimain.ETA = db.CustomParameters.First().CurrentDate; //pimain.PIReceiveDateFromSourcing = pimain.PIDate;
                pimain.ETD = pimain.ETA; pimain.BookingConfirmDate = pimain.ETA; pimain.RequisitionDate = pimain.ETA;
                pimain.PIFileId = db.PIMains.Max(o => (int?)o.PIFileId + 1).GetValueOrDefault(1);
                var pgci = db.prcGetConversionId(piview.PIDate.Value).ToList();
                List<int> cr = pgci.Where(c => c.HasValue).Select(c => c.Value).ToList<int>();
                pimain.RateInUSD = (from p in db.CurrencyConversions.AsEnumerable()
                                    join q in dbProd.CustomTypes.AsEnumerable() on p.CurrencyId equals q.TypeId
                                    where q.TypeName.Equals(pimain.CurrencyType) && cr.Contains(p.ConversionId)
                                    select p.RateInUSD).FirstOrDefault();
                pimain.PIFileNo = db.Database.SqlQuery(typeof(string), "Select dbo.fncGetMaxNo ('PI', '" + piview.PIDate.Value.Year + "', '" + piview.PIDate.Value.ToString("dd-MMM-yyyy") + "') As MaxNo", "").Cast<String>().ToList().FirstOrDefault();

                int msid = db.PISubs.Max(o => (int?)o.PIFileSubId + 1).GetValueOrDefault(1);
                foreach (var sub in pisub)
                {
                    var prsub = dbInv.PRSubs.Find(sub.PRSubId);

                    sub.RateInUSD = pimain.RateInUSD;
                    sub.PIFileRelationId = msid;
                    sub.PIFileSubId = msid++;
                    sub.ProductId = prsub.ProductId;
                    sub.FactoryId = prsub.PRMain.FactoryId;

                    var prod = dbProd.ItemInfoes.Find(sub.ProductId);

                    if (prod.ItemCode.StartsWith("01"))
                        sub.IsCapex = 0;
                    else
                        sub.IsCapex = 1;

                    pimain.PISubs.Add(sub);
                }

                //if (ModelState.IsValid)
                //{
                using (var con = new LCManagementEntities())
                {
                    con.PIMains.Add(pimain);
                    con.SaveChanges();
                }
                    //return RedirectToAction("Details", new { id = pimain.PIFileId });
                //}

                //return View(pimain);
                return Json(new { status = "Success", pino = pimain.PIFileNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /PI/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PIMain pimain = db.PIMains.Find(id);
            //pimain.PISubs = db.PISubs.Where(l => l.PIFileId == pimain.PIFileId).ToList();
            if (pimain == null)
            {
                return HttpNotFound();
            }
            
            PIView piview = new PIView();
            piview = (PIView)pimain.Convert(piview);

            piview.piSub = (from po in dbInv.QuantityApprovalMains.AsEnumerable()
                            join o in dbInv.QuantityApprovalSubs.AsEnumerable() on po.QApprovalId equals o.QApprovalId
                            join p in dbInv.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                            join r in dbInv.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                            join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                            join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                            join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                            join x in dbInv.PriceApprovalSubs.AsEnumerable() on o.QApprovalSubId equals x.QApprovalSubId
                            join q in dbInv.Suppliers.AsEnumerable() on x.SupplierId equals q.SupplierId
                            join y in dbProd.CustomTypes.AsEnumerable() on x.CurrencyId equals y.TypeId
                            join z in pimain.PISubs.AsEnumerable() on x.PApprovalSubId equals z.PApprovalSubId
                            join za in db.PISubs.GroupBy(c => c.PApprovalSubId).Select(c => new { PApprovalSubId = c.Key, PIQty = c.Sum(l => l.BookingQuantity) }).AsEnumerable() on x.PApprovalSubId equals za.PApprovalSubId into zb
                            from zc in zb.DefaultIfEmpty()
                            join xa in db.LCSubs.AsEnumerable() on z.PIFileSubId equals xa.PIFileSubId into xb
                            from xc in xb.DefaultIfEmpty()
                            join xd in dbInv.PipelineSubs.AsEnumerable() on (xc == null ? -1 : xc.LCFileSubId) equals xd.LCFileSubId into xe
                            from xf in xe.DefaultIfEmpty()
                            where x.SupplierId == piview.SupplierId //&& qaid.Contains(o.QApprovalId) && o.ApprovedQuantity - (z == null ? 0 : z.PIQty) > 0
                            select new { po, p, x, z, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o, y.TypeName, zc, xc, xf }
                            ).Select(c => new RequisitionView()
                            {
                                POId = c.po.PurchaseOrder.POId,
                                POSubId = c.o.PurchaseOrderSub.POSubId,
                                QApprovalId = c.o.QApprovalId,
                                QApprovalSubId = c.o.QApprovalSubId,
                                QuotationId = c.x.QuotationId,
                                PApprovalId = c.x.PApprovalId,
                                PApprovalSubId = c.x.PApprovalSubId,
                                PIFileId = c.z.PIFileId,
                                PIFileSubId = c.z.PIFileSubId,
                                LCFileId = (c.xc == null ? 0 : c.xc.LCFileId),
                                LCFileSubId = (c.xc == null ? 0 : c.xc.LCFileSubId),
                                PLEId = (c.xf == null ? 0 : c.xf.PLEId),
                                PLESubId = (c.xf == null ? 0 : c.xf.PLESubId),
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                PONo = c.po.PurchaseOrder.PONo,
                                PODate = c.po.PurchaseOrder.PODate,
                                QANo = c.o.QuantityApprovalMain.QApprovalNo,
                                QADate = c.o.QuantityApprovalMain.QApprovalDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                DaysWithTotalStock = c.p.DaysWithTotalStock,
                                AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.o.PurchaseOrderSub.Quantity,
                                ApprovedQuantity = c.o.ApprovedQuantity + c.z.BookingQuantity - (c.zc == null ? 0 : c.zc.PIQty),
                                PIQuantity = c.z.BookingQuantity,
                                UnitPrice = c.x.ApprovedPrice,
                                CurrencyId = c.x.CurrencyId,
                                Currency = c.TypeName,
                                CategoryId = c.CategoryId,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.p.Remarks
                            }).ToList();

            var qaid = piview.piSub.Select(c => c.QApprovalId).Distinct();
            ViewBag.QAList = new MultiSelectList((from p in dbInv.QuantityApprovalMains.AsEnumerable()
                                             join q in dbInv.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                                             join r in dbInv.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals r.QApprovalSubId
                                             join u in db.PISubs.GroupBy(c => c.PApprovalSubId).Select(c => new { PApprovalSubId = c.Key, PIQty = c.Sum(l => l.BookingQuantity) }).AsEnumerable() on r.PApprovalSubId equals u.PApprovalSubId into v
                                             from s in v.DefaultIfEmpty()
                                             where p.PurchaseOrder.Bond.CompanyId == piview.BeneficiaryId && r.SupplierId == piview.SupplierId 
                                                && (q.ApprovedQuantity - (s == null ? 0 : s.PIQty) > 0 || qaid.Contains(p.QApprovalId))
                                             //where x.SupplierId == supid && qaid.Contains(p.QApprovalId)
                                             select p).Distinct().ToList(), "QApprovalId", "QApprovalNo", qaid);

            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", pimain.BeneficiaryId);
            ViewBag.Supplier = new SelectList(dbInv.Suppliers, "SupplierId", "ShortName", pimain.SupplierId);
            //ViewBag.QAList = new MultiSelectList(new List<string> { }.Select(c => new { QApprovalId = c, QApprovalNo = c }), "QApprovalId", "QApprovalNo");

            //ViewBag.BeneficiaryId = new SelectList(db.Companies, "CompanyId", "CompanyName");
            ViewBag.LocalAgent = new SelectList(db.LocalAgents, "AgentId", "AgentName", pimain.LocalAgentId);
            ViewBag.POL = new SelectList(dbInv.viewPorts, "PortId", "PortName", pimain.POLId);
            //ViewBag.SupplierId = new SelectList(db.SupplierInformations, "SupplierId", "SupplierName");
            //ViewBag.QuantityUnitId = db.viewQuantityUnits.ToList();
            ViewBag.OriginCountry = new SelectList(dbInv.Countries, "CountryId", "CountryName", pimain.OriginCountryId);
            ViewBag.CurrencyType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CURRCODE")), "TypeName", "TypeName", pimain.CurrencyType);
            ViewBag.TenureTerms = new SelectList(db.viewTenureTypes, "TenureType", "TenureType", pimain.PaymentTerms);

            return View(piview);
        }

        // POST: /PI/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Update(PIView piview, List<PISubView> piSubView)
        {
            try
            {
                PIMain pimain = db.PIMains.Find(piview.PIFileId);
                pimain = (PIMain)piview.ConvertNotNull(pimain);
                pimain.RevisionUserId = clsMain.getCurrentUser();
                pimain.RevisionDate = clsMain.getCurrentTime();

                LCMain lcmain = pimain.LCMains.FirstOrDefault();
                int msid = db.PISubs.Max(o => (int?)o.PIFileSubId + 1).GetValueOrDefault(1);
                int lmsid = db.LCSubs.Max(o => (int?)o.LCFileSubId + 1).GetValueOrDefault(1);

                if (lcmain != null)
                {
                    PILC pilc = (PILC)pimain.Convert(new PILC());
                    lcmain = (LCMain)pilc.ConvertNotNull(lcmain);
                }

                if (piSubView != null)
                {
                    foreach (var sub in piSubView)
                    {
                        if (sub.PIFileSubId > 0)
                        {
                            PISub pisub = db.PISubs.Find(sub.PIFileSubId); 
                            
                            var prsub = dbInv.PRSubs.Find(sub.PRSubId);

                            sub.ProductId = prsub.ProductId;
                            sub.FactoryId = prsub.PRMain.FactoryId;

                            var prod = dbProd.ItemInfoes.Find(sub.ProductId);

                            if (prod.ItemCode.StartsWith("01"))
                                sub.IsCapex = 0;
                            else
                                sub.IsCapex = 1;

                            pisub = (PISub)sub.ConvertNotNull(pisub);

                            db.Entry(pisub).State = EntityState.Modified;

                            if (lcmain != null)
                            {
                                LCSub lcsub = db.LCSubs.Find(sub.LCFileSubId);
                                lcsub = (LCSub)sub.ConvertNotNull(lcsub);

                                db.Entry(lcsub).State = EntityState.Modified;
                            }
                        }
                        else
                        {
                            var prsub = dbInv.PRSubs.Find(sub.PRSubId);

                            sub.RateInUSD = pimain.RateInUSD;
                            sub.PIFileRelationId = msid;
                            sub.PIFileSubId = msid++;
                            sub.ProductId = prsub.ProductId;
                            sub.FactoryId = prsub.PRMain.FactoryId;

                            var prod = dbProd.ItemInfoes.Find(sub.ProductId);

                            if (prod.ItemCode.StartsWith("01"))
                                sub.IsCapex = 0;
                            else
                                sub.IsCapex = 1;

                            PISub pisub = (PISub)sub.Convert(new PISub());
                            pimain.PISubs.Add(pisub);

                            if(lcmain != null)
                            {
                                LCSub lcsub = (LCSub)sub.Convert(new LCSub());
                                lcsub.LCFileId = lcmain.LCFileId;
                                lcsub.LCFileRelationId = lmsid;
                                lcsub.LCFileSubId = lmsid++;
                                lcsub.RateInUSD = lcmain.RateInUSD;

                                lcmain.LCSubs.Add(lcsub);
                            }
                        }
                    }

                    var unsub = db.PISubs.Where(l => l.PIFileId == pimain.PIFileId).ToList();

                    for (int ri = 0; ri < unsub.Count; ri++)
                    {
                        if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                            db.Entry(unsub[ri]).State = EntityState.Deleted;
                    }

                    if (lcmain != null)
                    {
                        var unlsub = db.LCSubs.Where(l => l.LCFileId == lcmain.LCFileId).ToList();

                        for (int ri = 0; ri < unlsub.Count; ri++)
                        {
                            if (db.Entry(unlsub[ri]).State == EntityState.Unchanged)
                                db.Entry(unlsub[ri]).State = EntityState.Deleted;
                        }

                        db.Entry(lcmain).State = EntityState.Modified;
                    }

                    db.Entry(pimain).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Json(new { status = "Update successful.", pino = pimain.PINo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //// GET: /PI/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    PIMain pIMain = db.PIMains.Find(id);
        //    if (pIMain == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pIMain);
        //}

        //// POST: /PI/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    PIMain pIMain = db.PIMains.Find(id);
        //    db.PIMains.Remove(pIMain);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //public ActionResult ProductDetails()
        //{
        //    PISub pisub = new PISub();

        //    pisub.QuantityUnitId = 13;

        //    ViewBag.FactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName");
        //    //ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName");
        //    ViewBag.QuantityUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", 13);
        //    //ViewBag.FactoryId = db.Products.ToList();
        //    return PartialView(pisub);
        //}

        ////[HttpPost]
        //public ActionResult AddProduct(PISub pisub)
        //{
        //    pisub.Product = db.Products.Where(l => l.ProductId == pisub.ProductId).FirstOrDefault();
        //    ViewBag.QtyUnit = db.viewQuantityUnits.Where(l => l.QuantityUnitId == pisub.QuantityUnitId).FirstOrDefault().QuantityUnit;
        //    //if(pisub == null)
        //    //    return Content("NULL");
        //    //else
        //    //    return Content(pisub.UnitPrice.ToString());
        //    return PartialView(pisub);
        //}

        //public JsonResult ProductList()
        //{
        //    // IList<Product> prodlist = db.Products.ToList();
        //    //Product p = new Product(); p.ProductName = "";
        //    //prodlist.Insert(0, p);
        //    var list = from x in db.Products
        //               select new { id = x.ProductId, text = x.ProductName };  // ...hmm..like this?

        //    return Json(list, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult PIInHand()
        //{
        //    //var pimains = db.PIMains.Where(l => l.IsForwardToBank == 0).ToList();
        //    return View(db.PIMains.Where(l => l.IsForwardToBank == 0));
        //}

        //public ActionResult ForwardToBank(int id)
        //{
        //    PIMain pimain = db.PIMains.Find(id);
        //    pimain.ForwardToBankDate = db.CustomParameters.FirstOrDefault().CurrentDate;

        //    if (pimain == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    //ViewBag.errormessage = "This PI has already forwarded to bank.";

        //    ViewBag.ForwardBankId = new SelectList(db.Banks, "BankId", "BankName");
        //    ViewBag.ForwardBranchId = new SelectList(db.BankBranches, "BranchId", "BranchName");
        //    return View(pimain);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ForwardToBank(int id, DateTime ForwardToBankDate, short ForwardBankId, short ForwardBranchId)
        //{
        //    PIMain pimain = db.PIMains.Find(id);
        //    if (pimain == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    try
        //    {
        //        pimain.ForwardToBankDate = ForwardToBankDate;
        //        pimain.IsForwardToBank = 1;
        //        pimain.ForwardUserId = 1;
        //        pimain.ForwardBankId = ForwardBankId;
        //        pimain.ForwardBranchId = ForwardBranchId;

        //        db.Entry(pimain).State = EntityState.Modified;
        //        db.SaveChanges();

        //        return RedirectToAction("PIInHand");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.errormessage = ex.Message;

        //        ViewBag.ForwardBankId = new SelectList(db.Banks, "BankId", "BankName");
        //        ViewBag.ForwardBranchId = new SelectList(db.BankBranches, "BranchId", "BranchName");
        //        return View(pimain);
        //    }
        //}
    }
}
