﻿using InventoryMS.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using static Stimulsoft.Report.Func;

namespace InventoryMS.Controllers.LC
{
    public class LCController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private EMailCubeEntities dbEmail = new EMailCubeEntities();

        // GET: LC
        [CAuthorize]
        public ActionResult Pipeline(string id)
        {
            if (id.ToLower().Equals("edit"))
            {
                var LCNos = (from p in dbFund.LCMains.AsEnumerable()
                                 //join q in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals q.LCFileId
                                 //join r in dbFund.PISubs.AsEnumerable() on q.PIFileSubId equals r.PIFileSubId
                                 //join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                             join t in db.PipelineMains.AsEnumerable() on p.LCFileId equals t.LCFileId
                             select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             //).Union(
                             //from p in dbLC.LCMains.AsEnumerable()
                             //join t in db.PipelineMains.AsEnumerable() on p.LCFileId equals t.LCFileId
                             //select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             ).Distinct().ToList();

                ViewBag.LCFile = new SelectList(LCNos, "LCFileId", "LCNo");
                ViewBag.ULotNos = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "PLEId", "LotNo");
                ViewBag.UNLotNos = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "PLEId", "LotNo");
                ViewBag.POL = new SelectList(dbFund.Ports, "PortId", "PortName");
                ViewBag.PackageUnit = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("PACKAGEUNIT")), "TypeId", "TypeName");

                return View("PipelineUP");
            }
            else
            {
                var plLC = db.PipelineMains.Where(l => l.IsLastLot == 1).Select(c => c.LCFileId).Distinct().ToList();

                var LCNos = (from p in dbFund.LCMains.AsEnumerable()
                             join q in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals q.LCFileId
                             join r in dbFund.PISubs.AsEnumerable() on q.PIFileId equals r.PIFileId
                             join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                             join t in db.PipelineSubs.GroupBy(l => l.PIFileSubId).Select(c => new { PIFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit), IsLastLot = c.Max(l => l.PipelineMain.IsLastLot) }).Where(l => l.IsLastLot == 0).AsEnumerable() on r.PIFileSubId equals t.PIFileSubId into u
                             from v in u.DefaultIfEmpty()
                             where !plLC.Contains(p.LCFileId) && r.PIQuantity - (v == null ? 0 : v.PLQty) > 0
                             select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             //).Union(
                             //from p in dbLC.LCMains.AsEnumerable()
                             //join q in dbLC.LCSubs.AsEnumerable() on p.LCFileId equals q.LCFileId
                             //join r in dbLC.PISubs.AsEnumerable() on q.PIFileSubId equals r.PIFileSubId
                             //join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                             //join t in db.PipelineSubs.GroupBy(l => l.LCFileSubId).Select(c => new { LCFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit), IsLastLot = c.Max(l => l.PipelineMain.IsLastLot) }).Where(l => l.IsLastLot == 0).AsEnumerable() on q.LCFileSubId equals t.LCFileSubId into u
                             //from v in u.DefaultIfEmpty()
                             //where !plLC.Contains(p.LCFileId) && r.BookingQuantity - (v == null ? 0 : v.PLQty) > 0
                             //select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                             )
                             .Distinct().ToList();

                ViewBag.LCFile = new SelectList(LCNos, "LCFileId", "LCNo");
                ViewBag.LotNos = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "LotNo", "LotNo");
                ViewBag.POL = new SelectList(dbFund.Ports, "PortId", "PortName");
                ViewBag.PackageUnit = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("PACKAGEUNIT")), "TypeId", "TypeName");

                return View("Pipeline");
            }
        }

        // GET: LC
        [CAuthorize]
        public ActionResult Save(PipelineView pipelineView, ICollection<PipelineSubView> pipelineSubView, ICollection<PackingListView> packingListView)
        {
            try
            {
                PipelineMain plmain = new PipelineMain();
                if (pipelineView.PLEId == null || pipelineView.PLEId <= 0)
                {
                    plmain = new PipelineMain(pipelineView);

                    plmain.ArrivalRecordDate = DateTime.Now;
                }
                else
                {
                    plmain = db.PipelineMains.Find(pipelineView.PLEId);
                    if (plmain.ArrivalRecordDate.AddDays(7) < clsMain.getCurrentTime().Date)
                    {
                        plmain.PortArivalDatePrev = plmain.PortArivalDate;
                        plmain.StoreArivalDatePrev = plmain.StoreArivalDate;
                        plmain.ArrivalRecordDate = clsMain.getCurrentTime();
                    }
                    if (plmain.IsMailSentForBankRetirement == 1 && pipelineView.PortArivalDate.HasValue && (pipelineView.PortArivalDate.Value.Date - plmain.PortArivalDate.Date).Days >= 10)
                    {
                        plmain.IsMailSentForBankRetirement = 0;
                        plmain.EmailSentForBankRetirementDate = DateTime.Parse("01-Jan-1900");
                    }

                    plmain = (PipelineMain)pipelineView.ConvertNotNull(plmain);
                }

                if (pipelineView.PLEId == null || pipelineView.PLEId <= 0)
                {
                    var beneficiaryid = dbFund.LCMains.Find(pipelineView.LCFileId).BeneficiaryId;

                    plmain.PLEDate = clsMain.getCurrentTime();
                    plmain.CreatedBy = Int16.Parse(Session["UserId"].ToString());
                    plmain.CreatedTime = plmain.PLEDate;
                    //plmain.PLENo = " + QAMain.PurchaseOrder.BondId.ToString("00") + ";
                    var bond = db.Bonds.Where(l => l.CompanyId == beneficiaryid).FirstOrDefault().BondId.ToString("00");

                    plmain.PLENo = "PL/" + bond + "/" + db.PipelineMains.Where(l => l.PLENo.Substring(0, 5).Equals("PL/" + bond) && l.PLEDate.Year == plmain.PLEDate.Year).Select(c => c.SerialNo + 1).DefaultIfEmpty(1).Max().Value.ToString("00000") + "-" + plmain.PLEDate.ToString("MM/yy").ToUpper();

                    foreach (var sub in pipelineSubView)
                    {
                        PipelineSub plsub = new PipelineSub(sub);
                        plmain.PipelineSubs.Add(plsub);
                    }

                    db.PipelineMains.Add(plmain);
                }
                else
                {
                    foreach (var sub in pipelineSubView)
                    {
                        if (sub.PLESubId == null || sub.PLESubId <= 0)
                        {
                            PipelineSub plsub = new PipelineSub(sub);

                            plmain.PipelineSubs.Add(plsub);
                        }
                        else
                        {
                            PipelineSub plsub = db.PipelineSubs.Find(sub.PLESubId);
                            plsub = (PipelineSub)sub.ConvertNotNull(plsub);
                            db.Entry(plsub).State = EntityState.Modified;
                        }
                    }

                    if (packingListView != null)
                    {
                        foreach (var sub in packingListView)
                        {
                            if (sub.PLECId == null || sub.PLECId <= 0)
                            {
                                PipelinePackingList plpack = new PipelinePackingList(sub);

                                plmain.PipelinePackingLists.Add(plpack);
                            }
                            else
                            {
                                PipelinePackingList plpack = db.PipelinePackingLists.Find(sub.PLECId);
                                plpack = (PipelinePackingList)sub.ConvertNotNull(plpack);
                                db.Entry(plpack).State = EntityState.Modified;
                            }
                        }

                        var unpacksub = db.PipelinePackingLists.Where(l => l.PLEId == plmain.PLEId).ToList();

                        for (int ri = 0; ri < unpacksub.Count; ri++)
                        {
                            if (db.Entry(unpacksub[ri]).State == EntityState.Unchanged)
                                db.Entry(unpacksub[ri]).State = EntityState.Deleted;
                        }
                    }

                    var unsub = db.PipelineSubs.Where(l => l.PLEId == plmain.PLEId).ToList();

                    for (int ri = 0; ri < unsub.Count; ri++)
                    {
                        if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                            db.Entry(unsub[ri]).State = EntityState.Deleted;
                    }

                    plmain.UpdatedBy = Int16.Parse(Session["UserId"].ToString());
                    plmain.UpdateTime = clsMain.getCurrentTime();

                    db.Entry(plmain).State = EntityState.Modified;
                }

                db.SaveChanges();

                //if (plmain.IsEmailSent == 0 && plmain.EmailSentDate < clsMain.getCurrentTime().AddDays(11))
                //{
                //    var row = (from p in db.EmailLibraries.AsEnumerable()
                //               join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                //               where p.EmailTypeId == 2 //&& q.FactoryId == prmain.FactoryId
                //               select new
                //               {
                //                   p.TypeName,
                //                   p.EmailBody,
                //                   q.EmailGroupFrom,
                //                   q.EmailGroupTo,
                //                   q.EmailGroupCC,
                //                   q.EmailGroupBCC,
                //                   p.Subject
                //               }).FirstOrDefault();

                //    EMailCollectionView MailCollection = new EMailCollectionView();
                //    MailCollection.EmailType = row.TypeName;
                //    MailCollection.ModuleName = "IPMS";
                //    MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                //    MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                //    MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                //    MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                //    MailCollection.EmailSubject = row.Subject.Replace("@RequisitionNo", prmain.PRNo);
                //    MailCollection.EmailBody = row.EmailBody.Replace("@RequisitionNo", prmain.PRNo)
                //        .Replace("@Factory", dbProd.Factories.Find(prmain.FactoryId).ShortName)
                //        .Replace("@Date", prmain.PRQDate.ToString("dd-MMM-yyyy"))
                //        .Replace("@ApprovalLink", @"http://192.168.0.47/ipms/report/mrr?__RequestVerificationToken=sKyQrtf_PxmyNAbQnwxvfNQZzuyx9vk6CPB19h8BO_5WlWJ6gPMsrL1ZoyR7xZEeXro5w4ZWVaXYvikdxR_U0qpOcaJnDuNmP9DPDr2A8sg1&fn=rptRequisitionProfile&FactoryId=" + prmain.FactoryId + "&Year=" + prmain.Year + "&PRId=" + prmain.PRId);
                //    MailCollection.EmailAttachmentLink = "";
                //    MailCollection.CreateAttachment = 0;
                //    MailCollection.CreatedBy = clsMain.getCurrentUser();

                //    EMailCollection mail = new EMailCollection(MailCollection);

                //    dbEM.EMailCollections.Add(mail);

                //    dbEM.SaveChanges();
                //}

                return Json(new { status = "Success", plno = plmain.PLENo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
            //return Content("Pipeline status update successfull.");
        }

        public ActionResult RowTemplate(ICollection<PipelineSubView> pipelineSubView)
        {
            return PartialView(pipelineSubView);
        }

        // GET: LC
        public ActionResult LCList(int id, string type = "create")
        {
            var lclist = (from p in db.PipelineMains.AsEnumerable()
                          join q in dbFund.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                          where p.IsLotReceived == 0
                          select new { q.LCFileId, q.LCNo }
                         ).Distinct();
            //if (type.ToLower().Equals("edit"))
            //{
            //    ViewBag.LotNos = new SelectList(db.PipelineMains.Where(l => l.LCFileId == id), "PLEId", "LotNo");

            //    return PartialView();
            //}
            //else
            //{
            //    ViewBag.UNLotNos = new SelectList((
            //                    from p in db.Custom_Type.Where(l => l.Flag.Equals("LOTNO")).AsEnumerable()
            //                    join q in db.PipelineMains.Where(l => l.LCFileId == id).AsEnumerable() on p.TypeName equals q.LotNo into r
            //                    from s in r.DefaultIfEmpty()
            //                    where s == null
            //                    select p
            //                    ).Distinct().ToList(), "TypeName", "TypeName");

            //    return PartialView("UNLotList");
            //}

            ViewBag.LCFileId = new SelectList(lclist, "LCFileId", "LCNo");

            return PartialView();
        }

        // GET: LC
        public ActionResult LCDetails(int id)
        {
            var lc = dbFund.LCMains.Find(id);

            if (lc == null)
                throw new Exception("Invalid lc.");

            return Json(new { data = lc }, JsonRequestBehavior.AllowGet);
        }

        // GET: LC
        public ActionResult ItemDetails(int id)
        {
            var lc = dbFund.LCMains.Find(id);

            if (lc == null)
                throw new Exception("Invalid lc.");

            var items = new List<PipelineSubView>();

            items = (from pi in dbFund.LCMains.Find(id).LCWisePIs.AsEnumerable()
                     join p in dbFund.PISubs.AsEnumerable() on pi.PIFileId equals p.PIFileId
                     join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                     join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                     join t in db.PipelineSubs.GroupBy(c => c.PIFileSubId).Select(c => new { PIFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit) }).AsEnumerable() on p.PIFileSubId equals t.PIFileSubId into u
                     from s in u.DefaultIfEmpty()
                     join w in db.PRSubs.AsEnumerable() on p.PRSubId equals w.PRSubId
                     where p.PIQuantity - (s == null ? 0 : s.PLQty) > 0
                     select new { p, q, r, w.FactoryETA, BalQty = p.PIQuantity - (s == null ? 0 : s.PLQty) }
                    ).Select(c => new PipelineSubView()
                    {
                        CategoryId = c.q.CategoryId,
                        ItemId = c.q.ItemId,
                        ItemName = c.q.ItemName,
                        FactoryETA = c.FactoryETA,
                        LCQuantity = c.BalQty,
                        QuantityUnit = c.r.QuantityUnit,
                        QuantityUnitId = c.r.QuantityUnitId,
                        StockInTransit = c.BalQty,
                        UnitPrice = c.p.UnitPrice,
                        PipelineValue = c.BalQty * c.p.UnitPrice,
                        LCFileSubId = 0,
                        PIFileSubId = c.p.PIFileSubId,
                        PLEId = 0,
                        PLESubId = 0
                    }).ToList();

            var requisition_list = (from p in items.AsEnumerable()
                                    join q in dbFund.PISubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId
                                    join r in db.PRSubs.AsEnumerable() on q.PRSubId equals r.PRSubId
                                    join s in db.PRMains.AsEnumerable() on r.PRId equals s.PRId
                                    select s.PRNo
                                   ).Distinct().ToList().Aggregate((i, j) => i + ", " + j);

            ViewBag.polid = lc.POLId;
            ViewBag.requisition_list = requisition_list;

            return PartialView("RowTemplate", items);
        }

        // GET: LC
        public ActionResult LotList(int id, string type = "create")
        {
            if (type.ToLower().Equals("edit"))
            {
                ViewBag.LotNos = new SelectList(db.PipelineMains.Where(l => l.LCFileId == id), "PLEId", "LotNo");

                return PartialView();
            }
            else
            {
                ViewBag.UNLotNos = new SelectList((
                                from p in db.Custom_Type.Where(l => l.Flag.Equals("LOTNO")).AsEnumerable()
                                join q in db.PipelineMains.Where(l => l.LCFileId == id).AsEnumerable() on p.TypeName equals q.LotNo into r
                                from s in r.DefaultIfEmpty()
                                where s == null
                                select p
                                ).Distinct().ToList(), "TypeName", "TypeName");

                return PartialView("UNLotList");
            }
        }

        // GET: LC
        public ActionResult LotDetails(int id)
        {
            try
            {
                var pl = db.PipelineMains.Find(id);
                var accept = dbFund.LCAcceptances.Where(c => c.PLEId == pl.PLEId).FirstOrDefault();

                var polid = pl.POLId == 0 ? dbFund.LCMains.Find(pl.LCFileId).POLId : pl.POLId;

                var requisition_list = (from p in pl.PipelineSubs.AsEnumerable()
                                        join q in dbFund.PISubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId
                                        join r in db.PRSubs.AsEnumerable() on q.PRSubId equals r.PRSubId
                                        join s in db.PRMains.AsEnumerable() on r.PRId equals s.PRId
                                        select s.PRNo
                                       ).Distinct().ToList().Aggregate((i, j) => i + ", " + j);

                if (pl == null)
                    throw new Exception("Invalid lot no.");

                var lots = (from p in db.Custom_Type.Where(l => l.Flag.Equals("LOTNO")).AsEnumerable()
                            join q in db.PipelineMains.Where(l => l.LCFileId == pl.LCFileId).AsEnumerable() on p.TypeName equals q.LotNo into r
                            from s in r.DefaultIfEmpty()
                            where s == null || p.TypeName.Equals(pl.LotNo)
                            select p
                            ).Distinct().ToList();

                ViewBag.UNLotNos = new SelectList(lots, "TypeName", "TypeName", pl.LotNo);

                var unlot = RenderRazorViewToString(this.ControllerContext, "UNLotList", null);
                var attachment_base_url = Request.Url.Scheme + "://" + Request.Url.Authority + "/" + Request.Url.Segments[1] + "attachments/"; // ConfigurationManager.AppSettings["attachment_base_url"] == null ? Server.MapPath("~/Attachments/") : ConfigurationManager.AppSettings["attachment_base_url"];

                return Json(new
                {
                    lotlist = unlot,
                    plno = pl.PLENo,
                    polid = polid,
                    sd = pl.ShipmentDate,
                    bl = pl.BLDate,
                    ta = pl.TransArrivalDate,
                    tdd = pl.TransDepartureDate,
                    TransshipmentPort = pl.TransshipmentPort,
                    tdda = pl.TransDepartureDateActual,
                    pe = pl.PortArivalDate,
                    se = pl.StoreArivalDate,
                    lrd = pl.LotReceivedDate,
                    dab = accept == null ? DateTime.Parse("01-Jan-1900") : accept.BankReceiveDate,
                    fe = pl.FactoryETA.ToString("dd-MMM-yyyy"),
                    ll = pl.IsLastLot,
                    sr = pl.IsLotReceived,
                    cl = pl.CurrentLocation,
                    ddfp = pl.PortDeliveryDate,
                    cid = pl.CommercialInvoiceDate,
                    cino = pl.CommercialInvoiceNo,
                    boed = pl.BillOfEntryDate,
                    boeno = pl.BillOfEntryNo,
                    boevalue = pl.BillOfEntryValue.ToString("##,##0.00"),
                    packageqty = pl.PackageCount.ToString("##,##0"),
                    packageunitid = pl.PackageUnitId,
                    docstocnfdate = accept == null ? DateTime.Parse("01-Jan-1900") : accept.HandoverToCNFDate,
                    rm = pl.Remarks,
                    ct = clsMain.getCurrentTime(),
                    nooftruck = pl.NoOfTruck,
                    modeoftransport = pl.ModeOfTransport,
                    requisition_list = requisition_list,
                    attachments = pl.Attachments,
                    attachment_base_url = attachment_base_url
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("Error");
            }
        }

        // GET: LC
        public ActionResult LotItemDetails(int id)

        {
            var items = (from p in dbFund.PISubs.AsEnumerable()
                         join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                         join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                         join s in db.PipelineSubs.AsEnumerable() on p.PIFileSubId equals s.PIFileSubId
                         join u in db.PRSubs.AsEnumerable() on p.PRSubId equals u.PRSubId
                         where s.PLEId == id
                         select new { p, q, r, s, u.FactoryETA }
                     ).Select(c => new PipelineSubView()
                     {
                         CategoryId = c.q.CategoryId,
                         ItemId = c.q.ItemId,
                         ItemName = c.q.ItemName,
                         FactoryETA = c.FactoryETA,
                         LCQuantity = c.p.PIQuantity,
                         QuantityUnit = c.r.QuantityUnit,
                         QuantityUnitId = c.r.QuantityUnitId,
                         StockInTransit = c.s.StockInTransit,
                         UnitPrice = c.p.UnitPrice,
                         PipelineValue = c.s.StockInTransit * c.p.UnitPrice,
                         LCFileSubId = 0,
                         PIFileSubId = c.p.PIFileSubId,
                         PLEId = c.s.PLEId,
                         PLESubId = c.s.PLESubId
                     }).ToList();

            if (items == null)
                throw new Exception("Invalid lot no.");

            return PartialView("RowTemplate", items);
        }

        public ActionResult PackingTemplate(int? id, short? slNo)
        {
            id = id ?? 0;
            var pack = (from p in db.PipelinePackingLists.AsEnumerable()
                        join q in dbProd.CustomTypes.AsEnumerable() on p.TypeId equals q.TypeId
                        where p.PLEId == id
                        select new { p, q.TypeName }
                       ).Select(c => new PackingListView()
                       {
                           PLEId = c.p.PLEId,
                           PLECId = c.p.PLECId,
                           TypeId = c.p.TypeId,
                           Size = c.TypeName,
                           Quantity = c.p.Quantity,
                           LCLQuantity = c.p.LCLQuantity,
                           FCLQuantity = c.p.FCLQuantity
                       });

            if (slNo == null || slNo == 0)
                return PartialView(pack);
            else
            {
                ViewBag.SLNo = slNo.Value.ToString("000");
                ViewBag.Size = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CONTAINERSIZE")), "TypeId", "TypeName");

                return PartialView("PackingTemplate_Blank", new PackingListView());
            }
        }


        public ActionResult WharfRent()

        {
            var wharfRentlist = dbFund.Wharfrents.Include(x => x.LCMain).Select(x => new WhrafRentViewModel
            {

                LcNo = x.LCMain.LCNo,
                LcDate = x.LCMain.LCDate,
                LotNo = x.LotNo,
                BillDate = x.BillDate,
                BreathingDate = x.BrethingDate,
                PortLandingDate = x.PortLandingDate,
                DeliveryDate = x.DeliveryDate,
                FromDate = x.WhrafRentFromDate,
                ToDate = x.WhrafRentToDate,
                WhrafRentAmount = x.WhrafRentAmount,
                DetentionCharge = x.ContainerDetentionCharge,
                Remarks = x.Remarks,
                LcFileId = x.LcFileId,
                WharfrentId = x.WharfrentId,
                DetentionFromDate = x.DetentionFromDate,
                DetentionToDate = x.DetentionToDate,
                FilePath = x.FilePath


            }).ToList();


            return View(wharfRentlist);
        }

        public ActionResult WharfRentEdit(int WharfrentId)
        {


            var WfrafRent = dbFund.Wharfrents.Where(x=>x.WharfrentId== WharfrentId).ToList().Select
                (
                   x=> new WharfRentAttatchModel
                   { 
                      WharfrentId=x.WharfrentId,
                      DeliveryDate=x.DeliveryDate,
                      LotNo = x.LotNo,
                      BillDate=x.BillDate,
                      BrethingDate=x.BrethingDate,
                      ContainerDetentionCharge=x.ContainerDetentionCharge,
                      ContainerReturnDate=x.ContainerReturnDate,
                      DetentionFromDate=x.DetentionFromDate,
                      LcFileId=x.LcFileId,
                      PortLandingDate=x.PortLandingDate,
                      DetentionToDate=x.DetentionToDate,
                      WhrafRentAmount=x.WhrafRentAmount,
                      WhrafRentFromDate=x.WhrafRentFromDate,
                      WhrafRentToDate=x.WhrafRentToDate,
                      Remarks=x.Remarks,
                      FilePath = x.FilePath

                   }
                ).FirstOrDefault();

            var LcList = dbFund.LCMains.ToList();
            ViewBag.LcList = new SelectList(LcList, "LCFileId", "LCNo");
            ViewBag.FileName = WfrafRent.FilePath;
            return View(WfrafRent);
        }

        [HttpPost]
        public ActionResult WharfRentEdit(WharfRentAttatchModel wharfrent)
        {
            try
            {
                var wharfent = dbFund.Wharfrents.Find(wharfrent.WharfrentId);
                wharfent.Remarks = wharfrent.Remarks;

                dbFund.Entry(wharfent).State = EntityState.Modified;
                dbFund.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadLotNo(int lcno)
        {
            var lotNo = db.PipelineMains.Where(x => x.LCFileId == lcno).Select(x => new
            {
                Text = x.LotNo,
                Value = x.LotNo

            }).ToList();

            ViewBag.lotNo = new SelectList(lotNo, "Value", "Text");

            return PartialView("_GetLotNo");

        }

        public ActionResult MailTemplate(int? id)
        {
            var emaillist = new MailSend();

            if (id != null)
            {
                ViewBag.MailList = new MultiSelectList(db.Sys_User_Name.Select(c => new { Mail = c.eMail }).Distinct(), "Mail", "Mail");
                var pisid = db.PipelineMains.Find(id).PipelineSubs.FirstOrDefault().PIFileSubId;
                var factoryid = dbFund.PISubs.Find(pisid).FactoryId;
                emaillist = db.EmailFrameworks.Where(l => l.EmailTypeId == 3 && l.FactoryId == factoryid).Select(c => new MailSend()
                {
                    MailTo = c.EmailGroupTo,
                    MailCC = c.EmailGroupCC,
                    MailBCC = c.EmailGroupBCC
                }).FirstOrDefault();
            }

            return PartialView(emaillist);
        }

        public static string RenderRazorViewToString(ControllerContext controllerContext, string viewName, object model)
        {
            controllerContext.Controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                ViewResult.View.Render(ViewContext, sw);
                ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost, CAuthorize]
        public ActionResult MailSend() //MailSend mailSend, 
        {
            try
            {
                MailMessage mail = new MailMessage();
                //if (Request.Files.Count > 0)
                //{
                //    HttpFileCollectionBase files = Request.Files;
                //    for (int i = 0; i < files.Count; i++)
                //    {
                //        HttpPostedFileBase file = files[i];

                //        if (file != null)
                //        {
                //            Attachment attachment = new Attachment(file.InputStream, file.FileName);

                //            mail.Attachments.Add(attachment);
                //        }
                //    }
                //}

                var pleid = 0; var remarks = "";
                if (!String.IsNullOrEmpty(Request.Params["PLEId"]))
                    pleid = int.Parse(Request.Params["PLEId"]);
                if (!String.IsNullOrEmpty(Request.Params["Remarks"]))
                    remarks = Request.Params["Remarks"];

                var email = db.EmailLibraries.Find(3);
                var pldetails = (from p in db.PipelineMains.AsEnumerable()
                                 join q in dbFund.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                                 join r in dbFund.LCWisePIs.AsEnumerable() on q.LCFileId equals r.LCFileId
                                 join u in dbFund.PISubs.AsEnumerable() on r.PIFileId equals u.PIFileId
                                 join s in dbProd.Factories.AsEnumerable() on u.FactoryId equals s.FactoryId
                                 join t in db.Suppliers.AsEnumerable() on q.SupplierId equals t.SupplierId
                                 join v in db.PRSubs.AsEnumerable() on u.PRSubId equals v.PRSubId
                                 join w in db.Custom_Type.AsEnumerable() on p.PackageUnitId equals w.TypeId into x
                                 from y in x.DefaultIfEmpty()
                                 where p.PLEId == pleid
                                 select new
                                 {
                                     p,
                                     q.LCNo,
                                     q.LCDate,
                                     s.FactoryId,
                                     s.FactoryName,
                                     t.SupplierName,
                                     PRNo = v.PRMain.PRNo,
                                     LotNo = p.LotNo,
                                     PRDate = v.PRMain.PRDate,
                                     BrandName = v.Brand.BrandName,
                                     PackUnit = (y == null ? "" : y.TypeName)
                                 });

                var items = (from p in db.PipelineSubs.Where(l => l.PLEId == pleid).AsEnumerable()
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ItemId equals q.ItemId
                             join qc in dbProd.ItemCategories.AsEnumerable() on q.CategoryId equals qc.CategoryId
                             join pi in dbFund.PISubs.AsEnumerable() on p.PIFileSubId equals pi.PIFileSubId
                             join u in dbProd.viewQuantityUnits.AsEnumerable() on pi.QuantityUnitId equals u.QuantityUnitId
                             join r in dbProd.ItemVariants.AsEnumerable() on q.ItemId equals r.ItemId into s
                             from t in s.DefaultIfEmpty()
                             select new { ItemName = t == null ? q.ItemName : pi.FactoryId == 2 || pi.FactoryId == 16 ? q.ItemName : qc.CategoryName, p.StockInTransit, u.QuantityUnit, p.PLESubId });

                //var ic = items.Count(); var tq = items.Select(c => c.StockInTransit).Sum();

                var pipeline = pldetails.FirstOrDefault();
               
                // multiple prNo start
                var distinctPrnNos = pldetails
                        .GroupBy(pl => pl.PRNo)
                        .Select(group => group.Key)
                        .Distinct();
                var distinctPrnNosString = string.Join(", ", distinctPrnNos);
                // multiple prNo end

                var attachments = JObject.Parse(pipeline.p.Attachments);

                string _file_name = null;
                foreach (JProperty property in attachments.Properties())
                {
                    if (property.Value.ToString() == "PackingList")
                    {
                        _file_name = property.Name;
                    }
                }
                if (_file_name == null)
                {
                    return Json(new { status = "Error", message = "No packing list available." }, JsonRequestBehavior.AllowGet);
                }

                var attachment_base_url = ConfigurationManager.AppSettings["attachment_base_url"] == null ? Server.MapPath("~/Attachments/") : ConfigurationManager.AppSettings["attachment_base_url"];
                string _path = Path.Combine(attachment_base_url, _file_name);
                if (System.IO.File.Exists(_path))
                {
                    Attachment _attachment = new Attachment(System.IO.File.Open(_path, FileMode.Open), _file_name.Split('_')[1]);
                    mail.Attachments.Add(_attachment);
                }


                if (pipeline.p.PackageCount <= 0 || pipeline.PackUnit == "")
                {
                    return Json(new { status = "Error", message = "Pack quantity or unit must not be blank or 0." }, JsonRequestBehavior.AllowGet);
                }
                else if (pipeline.p.ModeOfTransport == "" || pipeline.p.ModeOfTransport == "-")
                {
                    return Json(new { status = "Error", message = "Mode of transport must not be blank." }, JsonRequestBehavior.AllowGet);
                }
                else if (pipeline.p.NoOfTruck <= 0)
                {
                    return Json(new { status = "Error", message = "No of container/truck must be gretter than 0." }, JsonRequestBehavior.AllowGet);
                }

                var itemlist = items.Select(c => c.ItemName).Distinct().Aggregate((i, j) => i + "," + j);
                var qty = items.Select(c => new { c.PLESubId, c.QuantityUnit, c.StockInTransit }).Distinct().GroupBy(c => c.QuantityUnit).Select(c => new { Unit = c.Key, StockInTransit = c.Sum(l => l.StockInTransit) }).Select(c => c.StockInTransit.ToString("##,##0.00") + " " + c.Unit).Aggregate((i, j) => i + "," + j);
                var factory = pldetails.Select(c => c.FactoryName).Distinct().Aggregate((i, j) => i + "," + j);
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                var mailaddressinfo = db.EmailFrameworks.Where(l => l.EmailTypeId == 3 && l.FactoryId == pipeline.FactoryId).FirstOrDefault();

                var user = db.Sys_User_Name.Find(clsMain.getCurrentUser());
                //mail.From = new MailAddress(mailaddressinfo.EmailGroupFrom);
                //mail.To.Add("mohammad.noman@kdsgroup.net");

                mail.From = new MailAddress(user.eMail);//mailaddressinfo.EmailGroupFrom);

                foreach (var to in mailaddressinfo.EmailGroupTo.Split(';').Where(c => !String.IsNullOrEmpty(c.Trim())).OrderBy(c => c))
                    if (!mail.To.Contains(new MailAddress(to)))
                        mail.To.Add(to);

                foreach (var cc in mailaddressinfo.EmailGroupCC.Split(';').Where(c => !String.IsNullOrEmpty(c.Trim())).OrderBy(c => c))
                    if (!mail.To.Contains(new MailAddress(cc)) && !mail.CC.Contains(new MailAddress(cc)))
                        mail.CC.Add(cc);

                foreach (var bcc in mailaddressinfo.EmailGroupBCC.Split(';').Where(c => !String.IsNullOrEmpty(c.Trim())).OrderBy(c => c))
                    if (!mail.To.Contains(new MailAddress(bcc)) && !mail.CC.Contains(new MailAddress(bcc)) && !mail.Bcc.Contains(new MailAddress(bcc)))
                        mail.Bcc.Add(bcc);

                if (!mail.To.Contains(new MailAddress(user.eMail)) && !mail.CC.Contains(new MailAddress(user.eMail)) && !mail.Bcc.Contains(new MailAddress(user.eMail)))
                    mail.Bcc.Add(user.eMail);

                mail.Subject = email.Subject.Replace("@LCNo", pipeline.LCNo);
                mail.Body = email.EmailBody.Replace("@Addressee", Request.Params["MailAddressee"])
                    .Replace("@PortDeliveryDate", pipeline.p.PortDeliveryDate.ToString("dd-MMM-yyyy"))
                    //.Replace("@PRNo", pipeline.PRNo)
                    .Replace("@PRNo", distinctPrnNosString)
                    .Replace("@PRDate", pipeline.PRDate.ToString("dd-MMM-yyyy"))
                    .Replace("@LCNo", pipeline.LCNo)
                    .Replace("@LotNo", pipeline.LotNo)
                    .Replace("@LCDate", pipeline.LCDate.ToString("dd-MMM-yyyy"))
                    .Replace("@ItemList", itemlist)
                    .Replace("@TotalQty", qty)
                    .Replace("@Brand", pipeline.BrandName)
                    .Replace("@Factory", factory)
                    .Replace("@Supplier", pipeline.SupplierName)
                    .Replace("@PackQty", pipeline.p.PackageCount.ToString() + " " + pipeline.PackUnit)
                    .Replace("@PackUnit", textInfo.ToTitleCase(pipeline.PackUnit.ToString().ToLower()))
                    .Replace("@NoOfTruck", pipeline.p.NoOfTruck.ToString())
                    .Replace("@ModeOfTransport", pipeline.p.ModeOfTransport)
                    .Replace("@DeliveryOfficer", textInfo.ToTitleCase(user.ShortName.ToLower()))
                    .Replace("@Remarks", remarks);

                mail.IsBodyHtml = true;
                //factory

                try
                {
                    var mail_credential = dbEmail.EmailAddressDetails.Where(c => c.EmailAddress.ToLower().Trim() == mail.From.Address.ToLower().Trim()).FirstOrDefault();
                    var mail_config = dbEmail.EmailConfigs.Where(c => c.IsDefault == 1).FirstOrDefault();

                    if (mail_credential != null && mail_config != null)
                    {
                        SmtpClient SmtpServer = new SmtpClient(mail_config.Host);
                        string credential_password = clsMain.Decrypt(mail_credential.Password);
                        SmtpServer.Credentials = new System.Net.NetworkCredential(mail_credential.EmailAddress, credential_password);
                        SmtpServer.Port = mail_config.Port;
                        SmtpServer.EnableSsl = mail_config.SSLEnabled;
                        //SmtpServer.Send
                        SmtpServer.Send(mail);

                        SmtpServer = null;

                        var plmain = db.PipelineMains.Find(pleid);
                        plmain.IsEmailSent += 1;
                        plmain.EmailSentDate = clsMain.getCurrentTime();
                        db.Entry(plmain).State = EntityState.Modified;
                        db.SaveChanges();

                        return Json(new { status = "Success", message = "Mail send successfully." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = "Error", message = "Error while mail sending. Sender email address or credential missing" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = "Error while mail sending. Sender email address or credential missing" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = "Error while mail sending." }, JsonRequestBehavior.AllowGet);
            }
            //return Json("Mail send failed.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadFile()
        {
            //try
            //{ 
            //    string path = @"\\192.168.0.47\Attachments\abc.txt";
            //    if (!System.IO.File.Exists(path))
            //    {
            //        // Create a file to write to.
            //        using (StreamWriter sw = System.IO.File.CreateText(path))
            //        {
            //            sw.WriteLine("Hello");
            //            sw.WriteLine("And");
            //            sw.WriteLine("Welcome");
            //        }
            //    }

            //    // Open the file to read from.
            //    using (StreamReader sr = System.IO.File.OpenText(path))
            //    {
            //        string s = "";
            //        while ((s = sr.ReadLine()) != null)
            //        {
            //            Console.WriteLine(s);
            //        }
            //    }
            //}
            //catch(Exception ex)
            //{
            //    return Json(new { status = "Error", message = "Error while file saving." }, JsonRequestBehavior.AllowGet);
            //}

            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                    if (file != null)
                    {
                        try
                        {
                            string _FileName = "";
                            if (file.ContentLength > 0)
                            {
                                var attachment_base_url = ConfigurationManager.AppSettings["attachment_base_url"] == null ? Server.MapPath("~/Attachments/") : ConfigurationManager.AppSettings["attachment_base_url"];
                                _FileName = files.Keys[i]; // Path.GetFileName(file.FileName);
                                string _path = Path.Combine(attachment_base_url, _FileName);
                                file.SaveAs(_path);
                                var file_name_woe = Path.GetFileNameWithoutExtension(_path);
                                var ple_id = int.Parse(file_name_woe.Split('_')[0]);
                                var pl = db.PipelineMains.Find(ple_id);
                                var attachments = JObject.Parse(pl.Attachments);
                                attachments[_FileName] = file_name_woe.Split('_')[1];
                                pl.Attachments = Newtonsoft.Json.JsonConvert.SerializeObject(attachments);
                                //string a = "";
                                db.Entry(pl).State = EntityState.Modified;
                                db.SaveChanges();

                                return Json(
                                    new
                                    {
                                        status = "Success",
                                        message = "File uploaded successfully.",
                                        file_name = _FileName,
                                        attachment_base_url = Request.Url.Scheme + "://" + Request.Url.Authority + "/" + Request.Url.Segments[1] + "attachments/"
                                    }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch
                        {
                            return Json(new { status = "Error", message = "Error while file uploading." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }

            return Json(new { status = "Error", message = "Nothing to upload." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveFile(string file_name)
        {
            if (!String.IsNullOrEmpty(file_name))
            {
                try
                {
                    var attachment_base_url = ConfigurationManager.AppSettings["attachment_base_url"] == null ? Server.MapPath("~/Attachments/") : ConfigurationManager.AppSettings["attachment_base_url"];

                    string _path = Path.Combine(attachment_base_url, file_name);
                    if (System.IO.File.Exists(_path))
                    {
                        System.IO.File.Delete(_path);
                        var file_name_woe = Path.GetFileNameWithoutExtension(_path);
                        var ple_id = int.Parse(file_name_woe.Split('_')[0]);
                        var pl = db.PipelineMains.Find(ple_id);
                        var attachments = JObject.Parse(pl.Attachments);
                        attachments.Remove(file_name);
                        pl.Attachments = Newtonsoft.Json.JsonConvert.SerializeObject(attachments);
                        //string a = "";
                        db.Entry(pl).State = EntityState.Modified;
                        db.SaveChanges();
                        Console.WriteLine("File deleted.");
                    }
                    return Json(new { status = "Success", message = "File delete successfully." }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { status = "Error", message = "Error while file uploading." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = "Error", message = "Nothing to upload." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadWF()
        {

            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;
                
                    HttpPostedFileBase file = files[0];

                    if (file != null)
                    {
                        try
                        {
                            string _FileName = "";
                            if (file.ContentLength > 0)
                            {
                                var attachment_base_url = ConfigurationManager.AppSettings["attachment_base_url"] == null ? Server.MapPath("~/Attachments/") : ConfigurationManager.AppSettings["attachment_base_url"];
                                _FileName =  files.Keys[0]; // Path.GetFileName(file.FileName);
                                string _path = Path.Combine(attachment_base_url, _FileName);
                                file.SaveAs(_path);

                                //var file_name_woe = Path.GetFileNameWithoutExtension(_path);
                            var file_name_woe = Path.GetFileName(_path);
                            var whrafrentId = int.Parse(file_name_woe.Split('_')[0]);
                            var pl = dbFund.Wharfrents.Find(whrafrentId);

                            //var attachments = JObject.Parse(pl.FilePath);
                            //attachments[_FileName] = file_name_woe.Split('_')[1];
                            pl.FilePath = file_name_woe;


                            //pl.FilePath = Newtonsoft.Json.JsonConvert.SerializeObject(file_name_woe.Split('_')[1]);

                            dbFund.Entry(pl).State = EntityState.Modified;
                            dbFund.SaveChanges();

                            return Json(
                                    new
                                    {
                                        status = "Success",
                                        message = "File uploaded successfully.",
                                        file_name = _FileName,
                                        attachment_base_url = Request.Url.Scheme + "://" + Request.Url.Authority + "/" + Request.Url.Segments[1] + "attachments/"
                                    }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch
                        {
                            return Json(new { status = "Error", message = "Error while file uploading." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                
            }

            return Json(new { status = "Error", message = "Nothing to upload." }, JsonRequestBehavior.AllowGet);

            //if (file != null && file.ContentLength > 0)
            //    try
            //    {  //Server.MapPath takes the absolte path of folder 'Uploads'
            //        string path = Path.Combine(Server.MapPath("~/App_Data/File"),
            //                                   Path.GetFileName(file.FileName));
            //        //Save file using Path+fileName take from above string
            //        file.SaveAs(path);
            //        ViewBag.Message = "File uploaded successfully";
            //    }
            //    catch (Exception ex)
            //    {
            //        ViewBag.Message = "ERROR:" + ex.Message.ToString();
            //    }
            //else
            //{
            //    ViewBag.Message = "You have not specified a file.";
            //}
            //return View();
        }


        [HttpPost]
        public ActionResult UploadFiles() //MailSend mailSend, 
        {
            //HttpPostedFileBase file

            if (Request.Files.Count > 0)
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        //Method 1 Get file details from current request    
                        //Uncomment following code if you wants to use method 1    
                        //if (Request.Files.Count > 0)    
                        // {    
                        //     var Inputfile = Request.Files[0];    

                        //     if (Inputfile != null && Inputfile.ContentLength > 0)    
                        //     {    
                        //         var filename = Path.GetFileName(Inputfile.FileName);    
                        //       var path = Path.Combine(Server.MapPath("~/uploadedfile/"), filename);    
                        //        Inputfile.SaveAs(path);    
                        //    }    
                        // }    

                        //Method 2 Get file details from HttpPostedFileBase class    

                        //string path = Path.Combine(Server.MapPath("~"), Path.GetFileName(file.FileName));
                        //file.SaveAs(path);

                        //if (!mailFrom.Contains("<"))
                        //    mailFrom = "CUBE | SDMS (Sales & Distribution Management System)<" + mailFrom + ">;";

                        if (file != null)
                        {
                            Attachment attachment = new Attachment(file.InputStream, file.FileName);

                            mail.Attachments.Add(attachment);

                        }
                    }

                    mail.From = new MailAddress("ipms.cube@kdsgroup.net");

                    mail.To.Add("mohammad.noman@kdsgroup.net");

                    //mail.Bcc.Add("mohammad.noman@kdsgroup.net");

                    mail.Subject = "Testing mail";
                    mail.Body = "fyi";
                    mail.IsBodyHtml = true;

                    SmtpClient SmtpServer = new SmtpClient("203.82.207.6");
                    SmtpServer.Credentials = new System.Net.NetworkCredential("mohammad.noman@kdsgroup.net", "bakalam");
                    SmtpServer.Port = 25;
                    SmtpServer.EnableSsl = false;
                    //SmtpServer.Send
                    SmtpServer.Send(mail);

                    SmtpServer = null;
                    return Json("File uploaded successfully.", JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json("Error while file uploading.", JsonRequestBehavior.AllowGet);
                }

            }
            return Json("model state failed.", JsonRequestBehavior.AllowGet);
        }
    }
}