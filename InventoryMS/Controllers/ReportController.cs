﻿//using CrystalDecisions.CrystalReports.Engine;
using InventoryMS.Models;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stimulsoft.Base;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;

namespace InventoryMS.Controllers
{
    [CAuthorize]
    public class ReportController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();

        public ActionResult RequisitionQueue()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        public ActionResult PendingRequisition()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        public ActionResult BondProfile()
        {
            var bond_list = (from p in db.Bonds.AsEnumerable()
                             join q in dbProd.Companies.AsEnumerable() on p.CompanyId equals q.CompanyId
                             select new { p.BondId, LicenseNumber = p.LicenseNumber + " - " + q.CompanyName }
                            ).Distinct().ToList();
            ViewBag.BondId = new SelectList(bond_list, "BondId", "LicenseNumber");
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");
            ViewBag.CapacityId = new SelectList(db.BondCapacities.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");
            return View();
        }

        public ActionResult BondWiseItem()
        {
            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber");
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");

            return View();
        }

        public ActionResult GetCompanyByBondId(int bondId)
        {
            short comId = db.Bonds.FirstOrDefault(x => x.BondId == bondId).CompanyId;
            string result = db.viewCompanies.Where(x => x.CompanyId == comId).FirstOrDefault().CompanyName;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequisitionProfile()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Year = new SelectList(db.PRMains.Select(c => new { Year = c.ApprovedTime.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.PRId = new SelectList(db.PRMains.Where(l => l.FactoryId == 0), "PRId", "PRNo");

            return View();
        }

        public ActionResult MaterialReceive()
        {

            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.ReceiveType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("GRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.GRRMains.Select(c => new { Year = c.ApprovedTime.Year }).Where(l => l.Year != 1900).OrderByDescending(c => c.Year).Distinct(), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.GRRId = new SelectList(db.GRRMains.Where(l => l.FactoryId == 0).OrderByDescending(l => l.GRRId), "GRRId", "GRRNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");
           
            return View();
        }

        public ActionResult MaterialReceiveStore()
        {

            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.ReceiveType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("GRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.SPGRRMains.Select(c => new { Year = c.ApprovedTime.Year }).Where(l => l.Year != 1900).OrderByDescending(c => c.Year).Distinct(), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.GRRId = new SelectList(db.SPGRRMains.Where(l => l.FactoryId == 0).OrderByDescending(l => l.GRRId), "GRRId", "GRRNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }


        public ActionResult SRR()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.SRRType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.SRRMains.Select(c => new { Year = c.SRRDate.Year }).Where(l => l.Year != 1900).OrderByDescending(c => c.Year).Distinct(), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.SRRId = new SelectList(db.SRRMains.Where(l => l.FactoryId == 0), "SRRId", "SRRNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult SRRStore()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.SRRType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.SPSRRMains.Select(c => new { Year = c.SRRDate.Year }).Where(l => l.Year != 1900).OrderByDescending(c => c.Year).Distinct(), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.SRRId = new SelectList(db.SPSRRMains.Where(l => l.FactoryId == 0), "SRRId", "SRRNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }


        public ActionResult Issue()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.IssueType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("ISSUETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.IssueMains.Select(c => new { Year = c.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.IssueId = new SelectList(db.IssueMains.Where(l => l.FactoryId == 0), "IssueId", "IssueNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult Issue_WV()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.IssueType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("ISSUETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.IssueMains.Select(c => new { Year = c.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.IssueId = new SelectList(db.IssueMains.Where(l => l.FactoryId == 0), "IssueId", "IssueNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }


        public ActionResult IssueStore()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.IssueType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("ISSUETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.SPIssueMains.Select(c => new { Year = c.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.IssueId = new SelectList(db.SPIssueMains.Where(l => l.FactoryId == 0), "IssueId", "IssueNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }


        public ActionResult IssueReturn()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.IssueTypeId = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("ISSUETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Year = new SelectList(db.IssueReturnMains.Select(c => new { Year = c.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.ReturnId = new SelectList(db.IssueReturnMains.Where(l => l.ReturnId == 0), "ReturnId", "ReturnNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");
            
            return View();
        }

        public ActionResult IssueReturnRegister()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult Consumption()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.Year = new SelectList(db.ConsumptionMains.Select(c => new { Year = c.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.ConsumptionId = new SelectList(db.ConsumptionMains.Where(l => l.FactoryId == 0), "ConsumptionId", "ConsumptionNo");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult Upcoming()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult ItemLedger()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");

            return View();
        }

        public ActionResult ItemLedger_WV()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");

            return View();
        }

        public ActionResult ItemPosition()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");
            //var MaterialType = dbProd.CustomTypes.Where(x => x.Flag.Equals("MATERIALTYPE")).ToList();
            //MaterialType.Add(new CustomType { TypeId = 868788, TypeName = "All -Except Spare Parts" });
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");
            //ViewBag.MaterialType = new SelectList(MaterialType, "TypeId", "TypeName");

            return View();
        }

        public ActionResult ItemPosition_ACT()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");
            var select = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");
            List<SelectListItem> materialType = new List<SelectListItem>();
            materialType.Add(new SelectListItem() { Text = "ALL (WITHOUT STORE & SPARES)", Value = "868788" });
            materialType.AddRange(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")).Select( x=> new SelectListItem() { Text = x.TypeName, Value = x.TypeId.ToString() }));
            ViewBag.MaterialType = new SelectList(materialType, "Value", "Text"); ;

            return View();
        }

        public ActionResult ItemPosition_ACT_Floor()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");
            var select = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")), "TypeId", "TypeName");
            List<SelectListItem> materialType = new List<SelectListItem>();
            materialType.Add(new SelectListItem() { Text = "ALL (WITHOUT STORE & SPARES)", Value = "868788" });
            materialType.AddRange(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE")).Select(x => new SelectListItem() { Text = x.TypeName, Value = x.TypeId.ToString() }));
            ViewBag.MaterialType = new SelectList(materialType, "Value", "Text"); ;

            return View();
        }

        public ActionResult RequisitionSummary()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.SupplierId = new SelectList(db.Suppliers.Where(c => c.SupplierId > 0).OrderBy(c => c.SupplierName), "SupplierId", "SupplierName");
            ViewBag.PRId = new SelectList(db.PRMains.Where(l => l.FactoryId == 0), "PRId", "PRNo");

            return View();
        }

        public ActionResult RequisitionRegister()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.PRId = new SelectList(db.PRMains.Where(l => l.FactoryId == 0), "PRId", "PRNo");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult POForQuantityApproval()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.POId = new SelectList(db.PurchaseOrders.Where(l => l.POId == 0), "POId", "PONo");

            return View();
        }

        public ActionResult PO()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.POId = new SelectList(db.PurchaseOrders.Where(l => l.POId == 0), "POId", "PONo");

            return View();
        }

        public ActionResult QuantityApproval()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.POId = new SelectList(db.PurchaseOrders.Where(l => l.POId == 0), "POId", "PONo");

            return View();
        }

        public ActionResult PendingQuantityApproval()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        public ActionResult PendingPriceApproval()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.QApprovalId = new SelectList(db.QuantityApprovalMains.Where(l => l.QApprovalId == 0), "QApprovalId", "QApprovalNo");
            ViewBag.Title = "Price Approval Pending Register";

            return View();
        }

        public ActionResult PriceApproval()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.QApprovalId = new SelectList(db.QuantityApprovalMains.Where(l => l.QApprovalId == 0), "QApprovalId", "QApprovalNo");
            ViewBag.Title = "Price Approval Register";

            return View();
        }

        public ActionResult QAButLCNotOpen()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.Title = "Price Approval Register";

            return View();
        }

        public ActionResult AllAButLCNotOpen()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.Title = "Price Approval Register";
            //System.DateTime.Today.AddDays(10)
            return View();
        }

        public ActionResult PipelineStatus()
        {
            ViewBag.FactoryId = new MultiSelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.SupplierId = new SelectList(db.Suppliers.Select(c => new { SupplierId = c.SupplierId, SupplierName = c.SupplierName + "/" + c.ShortName }).OrderBy(c => c.SupplierName), "SupplierId", "SupplierName");
            ViewBag.PRList = new MultiSelectList(db.PRMains.Where(c => c.PRId == 0), "PRId", "PRNo");

            var pendingtype = new List<String>(){"Pending For Qty Approval", "Pending For Price Approval", 
                "Forward For LC Opening", "Pending For L/C", "L/C Open But Not Received", 
                "Pipeline Pending Over Factory ETA", "Pipeline Arrival Earlier Than Factory ETA", 
                "Pipeline arriving after 15 days of bond expiry", 
                "Consignments already in port but not yet cleared", 
                "At Port & Port Arrival", "Pending Over ETP"}.Select(c => new { PendingType = c });
            ViewBag.PendingType = new SelectList(pendingtype, "PendingType", "PendingType");

            return View();
        }

        public ActionResult MaterialArrival()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");

            return View();
        }
        
        public ActionResult MaterialArrivalOM()
        {
            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber");
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");

            //ViewBag.BondPeriodId = new SelectList(db.BondPeriods.ToList(), "BondPeriodId", "LicensePeriod");
            return View();
        }
        
        public ActionResult GetLicencePeriodOM(int bondId)
        {
            var result = new SelectList(db.BondPeriods.Where(l => l.BondId == bondId), "BondPeriodId", "LicensePeriod");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult NonUsageMaterial()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        public ActionResult ImportSummary()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories, "CategoryId", "CategoryName");
            ViewBag.SupplierId = new SelectList(db.Suppliers.Where(l => l.SupplierId == 0), "SupplierId", "SupplierName");

            return View();
        }

        public ActionResult PriceCampare()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult Item()
        {
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult StockPosition()
        {
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");

            return View();
        }

        public ActionResult BondBalance()
        {
            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber");
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");

            return View();
        }

        public ActionResult Category()
        {
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        public ActionResult Supplier()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            var supid = db.SupplierFactories.Where(l => l.FactoryId == factoryid).Select(c => c.SupplierId);
            ViewBag.Factory = new MultiSelectList(dbProd.UserFactories, "FactoryId", "FactoryName", new []{ factoryid });
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(l => supid.Contains(l.SupplierId)), "SupplierId", "SupplierName");

            return View();
        }

        public ActionResult SupplierEvaluations()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            var supid = db.SupplierFactories.Where(l => l.FactoryId == factoryid).Select(c => c.SupplierId);
            ViewBag.Factory = new MultiSelectList(dbProd.UserFactories, "FactoryId", "FactoryName", new[] { factoryid });
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(l => supid.Contains(l.SupplierId)), "SupplierId", "SupplierName");

            return View();
        }

        public ActionResult SupplierSelection()
        {
            ViewBag.FactoryId = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Year = new SelectList(db.SupplierSelectionMains.Select(c => new { Year = c.Year }).Where(l => l.Year != 1900).Distinct().OrderByDescending(c => c.Year), "Year", "Year", clsMain.getCurrentTime().Year);
            ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName");
            ViewBag.CategoryId = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.ItemId = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.SupplierSelectionId = new SelectList(db.SupplierSelectionMains.Where(l => l.FactoryId == 0), "SupplierSelectionId", "SupplierSelectionNo");


            return View();
        }



        public ActionResult GoodsReceive()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            var supid = db.SupplierFactories.Where(l => l.FactoryId == factoryid).Select(c => c.SupplierId);
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(l => supid.Contains(l.SupplierId)), "SupplierId", "SupplierName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult StockPipeline()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            ViewBag.Factory = new MultiSelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult DocumentRetirement()
        {
            ViewBag.Factory = new MultiSelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult CategoryWiseStockCompare()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new MultiSelectList(dbProd.ItemCategories.Where(l => l.StageLevel == 4 && l.CategoryCode.StartsWith("0101")).OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult MRRAnalysis()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            var supid = db.SupplierFactories.Where(l => l.FactoryId == factoryid).Select(c => c.SupplierId);
            ViewBag.Factory = new SelectList(dbProd.UserFactories.Where(c => c.FactoryId == 1 || c.FactoryId == 5), "FactoryId", "FactoryName");
            ViewBag.Category = new MultiSelectList(dbProd.ItemCategories.Where(l => l.StageLevel == 4 && l.CategoryCode.StartsWith("0101")).OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(l => supid.Contains(l.SupplierId)), "SupplierId", "SupplierName");
            ViewBag.Brand = new SelectList(db.Brands, "BrandId", "BrandName");

            return View();
        }

        public ActionResult StockPosition_Inv()
        {
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(l => l.CategoryId == 0), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult UniformLabel()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.RefNo = new SelectList(dbFund.LCMains.Where(c => c.LCFileId == 0), "LCNo", "LCNo");

            return View();
        }

        [ActionName("ShowReport")]
        public ActionResult ShowReport()
        {
            if (Request.QueryString["title"] != null && Request.QueryString["title"].ToString().Length > 1)
                ViewBag.Title = Request.QueryString["title"].ToString();

            return PartialView("MRR");
        }

        public ActionResult MRR()
        {
            if (Request.QueryString["title"] != null && Request.QueryString["title"].ToString().Length > 1)
                ViewBag.Title = Request.QueryString["title"].ToString();

            return PartialView();
        }

        public ActionResult GetReportSnapshot()
        {
            DateTime? AsOnDate = null; string CreatedBy = "", rptFileName = "", Caption = "";
            if (AsOnDate == null)
                AsOnDate = DateTime.Today.AddMonths(-1);
            if (CreatedBy == "" && Session["UserName"] != null)
                CreatedBy = Session["UserName"].ToString();
            else if (CreatedBy == null)
                CreatedBy = "";

            //if (Request.QueryString["rptFileName"] != null)
            //    rptFileName = Request.QueryString["rptFileName"];
            //else
                //rptFileName = "rptCategoryList";

            rptFileName = Request.QueryString["fn"].ToString();

            // Create the report object
            StiReport report = new StiReport();

            string reportPath = Server.MapPath("~/Reports/" + rptFileName + ".mrt");

            if (!System.IO.File.Exists(reportPath))
            {
                Response.Write("The specified report does not exist\n" + reportPath + " \n");
            }

            report.Load(reportPath);

            //report.Dictionary.Variables["_ItemId"].Value = "0";
            //report.Dictionary.Variables["_FDate"].Value = "01-Jan-2018";
            //report.Dictionary.Variables["_TDate"].Value = "01-Jan-2019";
            //report.Dictionary.Variables["_FactoryId"].Value = "";
            //report.Dictionary.Variables["_CategoryId"].Value = "0";
            //report.Dictionary.Variables["_UserId"].Value = CreatedBy.ToString();

            List<string> qs = Request.QueryString.Keys.Cast<string>().ToList();
            foreach (StiVariable pf in report.Dictionary.Variables)
            {
                if (qs.Contains(pf.Name.Replace("_", "")))
                {
                    if (!String.IsNullOrEmpty(Request.QueryString[pf.Name.Replace("_", "")].ToString()))
                    {
                        if (pf.Type == typeof(string))
                        {
                            report.Dictionary.Variables[pf.Name].Value = "" + Request.QueryString[pf.Name.Replace("_", "")].ToString() + "";
                        }
                        else if (pf.Type.BaseType == typeof(System.ValueType))
                        {
                            if (pf.Type == typeof(DateTime))
                            {
                                report.Dictionary.Variables[pf.Name].Value = "" + DateTime.Parse(Request.QueryString[pf.Name.Replace("_", "")].ToString()).ToString("yyyy-MM-dd") + "";
                            }
                            else
                            {
                                report.Dictionary.Variables[pf.Name].Value = Request.QueryString[pf.Name.Replace("_", "")].ToString();
                            }
                        }
                    }
                    else
                    {
                        if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                        {
                            report.Dictionary.Variables[pf.Name].Value = "0";
                        }
                        else if (pf.Type == typeof(string))
                        {
                            report.Dictionary.Variables[pf.Name].Value = "";
                        }
                    }
                }
                else
                {
                    if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                    {
                        report.Dictionary.Variables[pf.Name].Value = "0";
                    }
                    else if (pf.Type == typeof(string))
                    {
                        report.Dictionary.Variables[pf.Name].Value = "";
                    }
                }
            }

            if(report.Dictionary.Variables["_CreditUserId"] != null)
                report.Dictionary.Variables["_CreditUserId"].Value = Session["UserId"].ToString();

            if (report.Dictionary.Variables["_UserId"] != null)
                report.Dictionary.Variables["_UserId"].Value = Session["UserName"].ToString();

            StiSqlDatabase db = (StiSqlDatabase)report.Dictionary.Databases["IMPS"];

            if (db == null)
                db = (StiSqlDatabase)report.Dictionary.Databases["IPMS"];

            if (db == null)
            {
                db = (StiSqlDatabase)report.Dictionary.Databases["IPMS_Prod"];

                db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=ProductCube;User Id=sa;Password=dbsrv170@sdk;Connect Timeout=500";
                db.PromptUserNameAndPassword = false;
            }
            else
            {
                db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=InventoryCube;User Id=sa;Password=dbsrv170@sdk;Persist Security Info=True;Integrated Security=False;Connect Timeout=500";
                //((StiSqlSource)report.Dictionary.DataSources["IPMS"]).CommandTimeout = 6000;
                db.PromptUserNameAndPassword = false;
            }

            return StiMvcViewer.GetReportSnapshotResult(report);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        public ActionResult PrintReport()
        {
            StiReport report = StiMvcViewer.GetReportObject();

            return StiMvcViewer.PrintReportResult();
        }

        public ActionResult ExportReport()
        {

            var settings = new Stimulsoft.Report.Export.StiPdfExportSettings();
            var service = new Stimulsoft.Report.Export.StiPdfExportService();

            return StiMvcViewer.ExportReportResult();
        }

        public ActionResult SupplierList(short? id)
        {
            if (id != null && id > 0)
            {
                var supid = db.SupplierFactories.Where(l => l.FactoryId == id).Select(c => c.SupplierId);
                ViewBag.Supplier = new SelectList(db.Suppliers.Where(l => supid.Contains(l.SupplierId)), "SupplierId", "SupplierName");
            }
            else
            {
                ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "SupplierName");
            }

            return PartialView();
        }

        public ActionResult CategoryList(short? id, byte? stagelevel)
        {
            if (id != null && id > 0)
            {
                //ViewBag.Category = new SelectList(dbProd.ItemCategories.Join(dbProd.ItemInfoes.Join(dbProd.ItemFactories, i => i.ItemId, f => f.ItemId, (i, f) => new { Item = i, Fact = f }).Where(c => c.Fact.FactoryId == id), p => p.CategoryId, q => q.Item.CategoryId, (p, q) => new { cat = p, item = q.Item }).Where(l => l.cat.StageLevel >= (stagelevel ?? 4)).Select(c => new { CategoryId = c.cat.CategoryId, CategoryName = c.cat.CategoryName }).Distinct().OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
                //ViewBag.Category = new SelectList((from p in dbProd.ItemInfoes.AsEnumerable()
                //                                   join r in dbProd.ItemFactories.AsEnumerable() on p.ItemId equals r.ItemId
                //                                   from q in dbProd.ItemCategories.AsEnumerable()
                //                                   where r.FactoryId == id && p.ItemCode.StartsWith(q.CategoryCode)
                //                                   select q
                //                       ).Distinct(), "CategoryId", "CategoryName");
                ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(l => l.StageLevel >= (stagelevel ?? 4)).OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            }
            else
            {
                ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(l => l.StageLevel >= (stagelevel ?? 4)).OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
            }

            return PartialView();
        }

        public ActionResult FactoryCategoryList(short facid)
        {
            var catList = (from p in dbProd.ItemCategories.AsEnumerable()
                           join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                           join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                           join s in dbProd.Factories.AsEnumerable() on r.FactoryId equals s.FactoryId
                           where s.FactoryId == facid && p.StageLevel == 4
                           select p
                          ).Distinct().ToList();

            ViewBag.Category = new SelectList(catList, "CategoryId", "CategoryName");

            return PartialView();
        }

        public ActionResult ProductList(short? id)
        {
            if (id != null && id > 0)
            {
                //ViewBag.Product = new SelectList(dbProd.ItemInfoes.Join(dbProd.ItemFactories, i => i.ItemId, f => f.ItemId, (i, f) => new { Item = i, Fact = f }).Where(c => c.Fact.FactoryId == id)).Select(c => new { ProductId = I, CategoryName = c.cat.CategoryName }).Distinct().OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
                ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == id).Select(c => new { c.ItemId, c.ItemName }).Distinct().OrderBy(c => c.ItemName.Length).ThenBy(c => c.ItemName), "ItemId", "ItemName");
            }
            else
            {
                ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");
            }

            return PartialView();
        }

        public ActionResult FactoryCategoryProductList(short facid, short catid)
        {
            ViewBag.Product = new SelectList((from p in dbProd.ItemInfoes.AsEnumerable()
                                              join q in dbProd.ItemFactories.AsEnumerable() on p.ItemId equals q.ItemId
                                              where q.FactoryId == facid && (p.CategoryId == catid || catid == 0)
                                              select p), "ItemId", "ItemName");
            
            return PartialView();
        }

        public ActionResult QApprovalList(short id, string type = "pap")
        {
            var qaid = (from p in db.QuantityApprovalMains.AsEnumerable()
                        join q in db.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                        join r in db.PRSubs.AsEnumerable() on q.PRSubId equals r.PRSubId
                        join s in db.PRMains.AsEnumerable() on r.PRId equals s.PRId
                        join t in db.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals t.QApprovalSubId into u
                        from v in u.DefaultIfEmpty()
                        where s.FactoryId == id && (type.Equals("pap") ? v == null : v != null)
                        select p).Distinct().ToList();
            ViewBag.QApproval = new SelectList(qaid, "QApprovalId", "QApprovalNo");

            return PartialView();
        }

        public ActionResult POList(short id, string type = "pap")
        {
            var PO = (from p in db.PurchaseOrders.AsEnumerable()
                        join q in db.PurchaseOrderSubs.AsEnumerable() on p.POId equals q.POId
                        join r in db.PRSubs.AsEnumerable() on q.PRSubId equals r.PRSubId
                        join s in db.PRMains.AsEnumerable() on r.PRId equals s.PRId
                        join t in db.QuantityApprovalSubs.AsEnumerable() on q.POSubId equals t.POSubId into u
                        from v in u.DefaultIfEmpty()
                        where s.FactoryId == id && (q.Quantity - (v == null ? 0 : v.ApprovedQuantity)) > 0
                        select p).Distinct().ToList();
            ViewBag.POId = new SelectList(PO, "POId", "PONo");

            return PartialView();
        }

        public ActionResult CapacityList(int id)
        {
            ViewBag.CapacityId = new SelectList(db.BondCapacities.Where(l => l.BondPeriodId == id), "CapacityId", "BondSerialNo");

            return PartialView();
        }

        public ActionResult PipelineRequisitionList(ICollection<short> facid, short? catid)
        {
            if (catid != null && catid > 0)
            {
                var catcode = dbProd.ItemCategories.Where(c => c.CategoryId == catid).FirstOrDefault().CategoryCode;
                var catlist = dbProd.ItemCategories.Where(c => c.CategoryCode.StartsWith(catcode)).Select(c => c.CategoryId).ToList();
            }

            var prlist0 = (from p in db.PRSubs.AsEnumerable()
                           join r in db.QuantityApprovalSubs.GroupBy(c => c.PRSubId).Select(c => new { PRSubId = c.Key, ApprovedQty = c.Sum(h => h.ApprovedQuantity) }).AsEnumerable() on p.PRSubId equals r.PRSubId into s
                           from q in s.DefaultIfEmpty()
                           where p.PRMain.ApprovedType == 1 && p.IsPOComplete == 0 && (p.Quantity - (q == null ? 0 : q.ApprovedQty)) > 0 && facid.Contains(p.PRMain.FactoryId)
                           select new { PRId = p.PRMain.PRId, PRNo = p.PRMain.PRNo }
                          ).Distinct();

            var prlist1 = (from p in db.QuantityApprovalSubs.AsEnumerable()
                           join r in dbFund.PISubs.GroupBy(c => c.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(h => h.PIQuantity) }).AsEnumerable() on p.QApprovalSubId equals r.QApprovalSubId into s
                           from q in s.DefaultIfEmpty()
                           where p.IsPIComplete == 0 && (p.ApprovedQuantity - (q == null ? 0 : q.PIQty)) > 0 && facid.Contains(p.PRMain.FactoryId)
                           select new { PRId = p.PRMain.PRId, PRNo = p.PRMain.PRNo }
                          ).Distinct();

            var prlist2 = (from p in dbFund.PISubs.AsEnumerable()
                           join l in db.PRSubs.AsEnumerable() on p.PRSubId equals l.PRSubId
                           join r in dbFund.LCWisePIs.AsEnumerable() on p.PIFileId equals r.PIFileId into s
                           from q in s.DefaultIfEmpty()
                           where q == null && facid.Contains(l.PRMain.FactoryId)
                           select new { PRId = l.PRMain.PRId, PRNo = l.PRMain.PRNo }
                          ).Distinct();

            var llot = db.PipelineMains.Where(c => c.IsLastLot == 1).Select(c => c.LCFileId).ToList();
            var prlist3 = (from p in dbFund.PISubs.AsEnumerable()
                           join l in db.PRSubs.AsEnumerable() on p.PRSubId equals l.PRSubId
                           join lc in dbFund.LCWisePIs.AsEnumerable() on p.PIFileId equals lc.PIFileId
                           join r in db.PipelineSubs.GroupBy(c => c.PIFileSubId).Select(c => new { PIFileSubId = c.Key, PLQty = c.Sum(h => h.StockInTransit) }).AsEnumerable() on p.PIFileSubId equals r.PIFileSubId into s
                           from q in s.DefaultIfEmpty()
                           where (p.PIQuantity - (q == null ? 0 : q.PLQty)) > 0 && !llot.Contains(lc.LCFileId) && facid.Contains(l.PRMain.FactoryId)
                           select new { PRId = l.PRMain.PRId, PRNo = l.PRMain.PRNo }
                          ).Distinct();

            var prlist4 = (from p in dbFund.PISubs.AsEnumerable()
                           join l in db.PRSubs.AsEnumerable() on p.PRSubId equals l.PRSubId
                           join lc in dbFund.LCWisePIs.AsEnumerable() on p.PIFileId equals lc.PIFileId
                           join q in db.PipelineSubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId
                           where q.PipelineMain.IsLotReceived == 0 && llot.Contains(lc.LCFileId) && facid.Contains(l.PRMain.FactoryId)
                           select new { PRId = l.PRMain.PRId, PRNo = l.PRMain.PRNo }
                          ).Distinct();

            var prlist = prlist0.Union(prlist1).Union(prlist2).Union(prlist3).Union(prlist4).Distinct();
            ViewBag.PRList = new MultiSelectList(prlist.OrderBy(c => c.PRNo), "PRId", "PRNo");

            return PartialView();
        }

        public ActionResult GRRList(short id, short year)
        {
            ViewBag.GRRId = new SelectList(db.GRRMains.Where(l => l.FactoryId == id && l.Year == year).OrderByDescending(l=>l.GRRId), "GRRId", "GRRNo");

            return PartialView();
        }

        public ActionResult GRRListStore(short id, short year)
        {
            ViewBag.GRRId = new SelectList(db.SPGRRMains.Where(l => l.FactoryId == id && l.Year == year).OrderByDescending(l => l.GRRId), "GRRId", "GRRNo");

            return PartialView();
        }


        public ActionResult SRRList(short id, short year)
        {
            ViewBag.SRRId = new SelectList(db.SRRMains.Where(l => l.FactoryId == id && l.Year == year), "SRRId", "SRRNo");

            return PartialView();
        }


        public ActionResult SRRListStore(short id, short year)
        {
            ViewBag.SRRId = new SelectList(db.SPSRRMains.Where(l => l.FactoryId == id && l.Year == year), "SRRId", "SRRNo");

            return PartialView();
        }


        public ActionResult IssueList(short id, short year)
        {
            ViewBag.IssueId = new SelectList(db.IssueMains.Where(l => l.FactoryId == id && l.Year == year), "IssueId", "IssueNo");

            return PartialView();
        }

        public ActionResult IssueListStore(short id, short year)
        {
            ViewBag.IssueId = new SelectList(db.SPIssueMains.Where(l => l.FactoryId == id && l.Year == year), "IssueId", "IssueNo");

            return PartialView();
        }


        public ActionResult IssueReturnList(short id, short year)
        {
            ViewBag.ReturnId = new SelectList(db.IssueReturnMains.Where(l => l.FactoryId == id && l.Year == year), "ReturnId", "ReturnNo");

            return PartialView();
        }

        public ActionResult SupplierSelectionList(short id, short year)
        {
            ViewBag.SupplierSelectionId = new SelectList(db.SupplierSelectionMains.Where(l => l.FactoryId == id && l.Year == year), "SupplierSelectionId", "SupplierSelectionNo");

            return PartialView();
        }

        public ActionResult ConsumptionList(short id, short year)
        {
            ViewBag.ConsumptionId = new SelectList(db.ConsumptionMains.Where(l => l.FactoryId == id && l.Year == year), "ConsumptionId", "ConsumptionNo");

            return PartialView();
        }

        public ActionResult InventoryAging()
        {
            var factoryid = dbProd.UserFactories.FirstOrDefault().FactoryId;
            ViewBag.Factory = new MultiSelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Category = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

            return View();
        }

        public ActionResult ReferenceList(short fac_id, string ref_type)
        {
            if (ref_type == "RQ")
            {
                ViewBag.multiple = true;
                ViewBag.RefIds = new MultiSelectList(db.PRMains.Where(l => l.FactoryId == fac_id).Distinct().OrderByDescending(c => c.PRId), "PRNo", "PRNo");
            }
            else if (ref_type == "PO")
            {
                ViewBag.multiple = true;
                ViewBag.RefIds = new MultiSelectList(db.PurchaseOrderSubs.Where(l => l.PRSub.PRMain.FactoryId == fac_id).Select(c => new { PRId = c.PurchaseOrder.POId, PRNo = c.PurchaseOrder.PONo }).Distinct().OrderByDescending(c => c.PRId), "PRNo", "PRNo");
            }
            else if (ref_type == "QA")
            {
                ViewBag.multiple = true;
                ViewBag.RefIds = new MultiSelectList(db.QuantityApprovalSubs.Where(l => l.PRSub.PRMain.FactoryId == fac_id).Select(c => new { PRId = c.QuantityApprovalMain.QApprovalId, PRNo = c.QuantityApprovalMain.QApprovalNo }).Distinct().OrderByDescending(c => c.PRId), "PRNo", "PRNo");
            }
            else if (ref_type == "PA")
            {
                ViewBag.multiple = true;
                ViewBag.RefIds = new MultiSelectList(db.PriceApprovalSubs.Where(l => l.PRSub.PRMain.FactoryId == fac_id).Select(c => new { PRId = c.PriceApprovalMain.PApprovalId, PRNo = c.PriceApprovalMain.PApprovalNo }).Distinct().OrderByDescending(c => c.PRId), "PRNo", "PRNo");
            }
            else if (ref_type == "LC")
            {
                ViewBag.multiple = false;
                ViewBag.RefIds = new SelectList(
                                            (from p in dbFund.LCMains.AsEnumerable()
                                             join q in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals q.LCFileId
                                             join r in dbFund.PISubs.AsEnumerable() on q.PIFileId equals r.PIFileId
                                             where r.FactoryId == fac_id
                                             select p
                                            ).Select(c => new { PRId = c.LCFileId, PRNo = c.LCNo }).Distinct().OrderByDescending(c => c.PRId), "PRNo", "PRNo");
            }

            return PartialView();
        }
    }
}