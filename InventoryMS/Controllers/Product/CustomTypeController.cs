﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Product
{
    public class CustomTypeController : Controller
    {
        private ProductCubeEntities db = new ProductCubeEntities();

        // GET: /CustomType/
        public ActionResult Index(string id)
        {
            List<CustomType> ct = new List<CustomType>();

            ct = db.CustomTypes.Where(l => l.Flag == id.ToUpper()).ToList();

            if (id.ToUpper() == "SUPCRFCTS")
                ViewBag.EntityName = "Supplier Certificate";
            else if (id.ToUpper() == "SUPAWRDS")
                ViewBag.EntityName = "Supplier Awards";

            ViewBag.id = id;

            return View(ct);
        }

        // GET: /CustomType/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomType customType = db.CustomTypes.Find(id);
            if (customType == null)
            {
                return HttpNotFound();
            }
            return View(customType);
        }

        // GET: /CustomType/Create
        public ActionResult Manage(string id, short? tid)
        {
            if (id.ToUpper() == "SUPCRFCTS")
                ViewBag.EntityName = "Supplier Certificate";
            else if (id.ToUpper() == "SUPAWRDS")
                ViewBag.EntityName = "Supplier Awards";

            if(ViewBag.EntityName == null)
            {
                return HttpNotFound();
            }

            CustomTypeView ctv = new CustomTypeView();

            if(tid != null && tid > 0)
            {
                ctv = (CustomTypeView)db.CustomTypes.Find(tid).Convert(new CustomTypeView());
            }
            ctv.Flag = id.ToUpper();
            ViewBag.id = id;

            return View(ctv);
        }

        // POST: /CustomType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save([Bind(Include="TypeId,TypeName,ShortName,Flag,CategoryList,Activity")] CustomTypeView customTypeView)
        {
            if (ModelState.IsValid)
            {
                if (customTypeView.TypeId == 0)
                {
                    CustomType customType = new CustomType(customTypeView);

                    customType.TypeId = db.CustomTypes.Select(c => c.TypeId).DefaultIfEmpty<short>(0).Max();
                    ++customType.TypeId;

                    db.CustomTypes.Add(customType);
                }
                else
                {
                    var customType = db.CustomTypes.Find(customTypeView.TypeId);
                    customType = (CustomType)customTypeView.ConvertNotNull(customType);

                    db.Entry(customType).State = EntityState.Modified;
                }

                db.SaveChanges();
                return Content("Saved.");
            }

            throw new Exception("Error");
        }

        // GET: /CustomType/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomType customType = db.CustomTypes.Find(id);
            if (customType == null)
            {
                return HttpNotFound();
            }
            return View(customType);
        }

        // POST: /CustomType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="TypeId,TypeName,ShortName,Flag,CategoryList,Activity")] CustomTypeView customTypeView)
        {
            CustomType customType = new CustomType(customTypeView);
            if (ModelState.IsValid)
            {
                db.Entry(customType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customType);
        }

        // GET: /CustomType/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomType customType = db.CustomTypes.Find(id);
            if (customType == null)
            {
                return HttpNotFound();
            }
            return View(customType);
        }

        // POST: /CustomType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            CustomType customType = db.CustomTypes.Find(id);
            db.CustomTypes.Remove(customType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
