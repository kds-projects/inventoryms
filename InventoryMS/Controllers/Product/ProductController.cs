﻿using InventoryMS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace InventoryMS.Controllers.Product
{
    public class ProductController : Controller
    {
        private ProductCubeEntities db = new ProductCubeEntities();
        private InventoryCubeEntities dbIMS = new InventoryCubeEntities();

        // GET: /Item/
        public ActionResult Index()
        {
            var items = db.ItemInfoes.Where(c => c.ItemId == 0).Select(c => new ItemView()
            {
                ItemId = c.ItemId,
                ItemName = c.ItemName,
                SystemName = c.SystemName,
                ShortName = c.ShortName,
                IsActive = c.IsActive,
                BondName = c.BondName,
                CategoryName = c.ItemCategory.CategoryName,
                ItemCode = c.ItemCode,
                CategoryId = c.CategoryId,
                HSCode = c.HSCode
            }).ToList();

            ViewBag.FactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName", 0);
            ViewBag.CategoryId = new SelectList(db.ItemCategories, "CategoryCode", "CategoryName", 0);
            return View(items);
        }

        public ActionResult ItemList(int? id, string catid)
        {
            var items = (from p in db.ItemInfoes.AsEnumerable()
                         join q in db.ItemFactories.AsEnumerable() on p.ItemId equals q.ItemId
                         join r in db.Factories.AsEnumerable() on q.FactoryId equals r.FactoryId
                         join s in db.ItemCategories.AsEnumerable() on p.CategoryId equals s.CategoryId
                         where q.FactoryId == (id == null ? q.FactoryId : id)
                         && p.ItemCode.StartsWith(catid == null ? s.CategoryCode : catid)
                         select new { p }
                        ).GroupBy(c => new
                        {
                            c.p.ItemId,
                            c.p.ItemCode,
                            c.p.ItemName,
                            c.p.SystemName,
                            c.p.ShortName,
                            c.p.IsActive,
                            c.p.BondName,
                            c.p.CategoryId,
                            c.p.ItemCategory.CategoryName,
                            c.p.HSCode
                        }).Select(c => new ItemView()
                        {
                            ItemId = c.Key.ItemId,
                            ItemName = c.Key.ItemName,
                            SystemName = c.Key.SystemName,
                            ShortName = c.Key.ShortName,
                            IsActive = c.Key.IsActive,
                            BondName = c.Key.BondName,
                            CategoryName = c.Key.CategoryName,
                            ItemCode = c.Key.ItemCode,
                            CategoryId = c.Key.CategoryId,
                            HSCode = c.Key.HSCode
                        }).ToList().Distinct();

            return PartialView(items);
        }

        // GET: /Item/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemInfo itemInfo = db.ItemInfoes.Find(id);

            if (itemInfo == null)
            {
                return HttpNotFound();
            }

            var item = (from p in db.ItemInfoes.AsEnumerable()
                        join q in db.ItemFactories.AsEnumerable() on p.ItemId equals q.ItemId
                        join r in db.Factories.AsEnumerable() on q.FactoryId equals r.FactoryId
                        join s in db.ItemCategories.AsEnumerable() on p.CategoryId equals s.CategoryId
                        join t in db.ItemUnits.AsEnumerable() on p.DefaultQUnitId equals t.UnitId
                        join u in db.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals u.QuantityUnitId
                        where q.ItemId == id
                        select new { p, r, s, t, u }
                        ).Select(c => new ItemView()
                        {
                            ItemId = c.p.ItemId,
                            ItemName = c.p.ItemName,
                            SystemName = c.p.SystemName,
                            ShortName = c.p.ShortName,
                            IsActive = c.p.IsActive,
                            BondName = c.p.BondName,
                            CategoryName = c.s.CategoryName,
                            ItemCode = c.p.ItemCode,
                            CategoryId = c.p.CategoryId,
                            HSCode = c.p.HSCode,
                            QuantityUnitList = c.u.QuantityUnit + " => KGProportion (" + c.t.KGProportion.ToString("##,##0.00####") + ")"
                        }).FirstOrDefault();

            var fids = itemInfo.ItemFactories.Select(c => c.FactoryId).ToList();
            ViewBag.factory = db.Factories.Where(c => fids.Contains(c.FactoryId)).Select(c => c.ShortName).ToList().Aggregate((i, j) => i + "\n" + j);

            return View(item);
        }

        // GET: /Item/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit");
            ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName");
            ViewBag.FList = new MultiSelectList(db.Factories, "FactoryId", "FactoryName");
            ViewBag.fl = "";
            ViewBag.CategoryId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel == 4), "CategoryId", "CategoryName");
            ViewBag.items = new SelectList(db.ItemInfoes.OrderBy(c => c.ItemName.Length).ThenBy(c => c.ItemName), "ItemId", "ItemName");

            ViewBag.MaterialType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");

            return View(new ItemView());
        }

        // POST: /Item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [CAuthorize]
        public ActionResult Save([Bind(Include = "SystemName,ItemName,ShortName,BondName,CategoryId,HSCode,DefaultFactoryId,FList,QUnitList,DefaultQUnitId,Activity")] ItemView itemView, ICollection<ItemVariantView> itemVariantView, ICollection<ItemFactoryView> itemFactoryView, ICollection<ItemUnitView> itemUnitView)
        {
            if (db.ItemInfoes.Where(c => c.ItemName == itemView.ItemName).Count() == 0)
            {
                ViewBag.errormessage = "";

                ItemInfo itemInfo = new ItemInfo(itemView);
                ItemCategory pcat = db.ItemCategories.Find(itemView.CategoryId);

                string pCode = (pcat == null || pcat.CategoryCode == null ? "" : pcat.CategoryCode);

                int serialNo = db.ItemInfoes.Where(l => l.CategoryId == itemView.CategoryId).Select(c => c.SerialNo).Max().GetValueOrDefault(1000) + 1;
                itemInfo.ItemCode = pCode + serialNo.ToString();

                if (itemVariantView != null)
                    foreach (ItemVariantView ivv in itemVariantView)
                    {
                        ItemVariant iv = new ItemVariant(ivv);
                        itemInfo.ItemVariants.Add(iv);
                    }
                if (itemFactoryView != null)
                    foreach (ItemFactoryView ifv in itemFactoryView)
                    {
                        ItemFactory ift = new ItemFactory(ifv);
                        itemInfo.ItemFactories.Add(ift);
                    }
                if (itemUnitView != null)
                {
                    foreach (ItemUnitView iuv in itemUnitView)
                    {
                        ItemUnit iu = new ItemUnit(iuv);
                        itemInfo.ItemUnits.Add(iu);
                    }

                    itemInfo.QuantityUnitList = itemUnitView.Select(c => c.UnitId.ToString()).Aggregate((i, j) => i + ", " + j);
                }

                itemInfo.CreatedBy = clsMain.getCurrentUser();
                itemInfo.CreatedTime = clsMain.getCurrentTime();
                itemInfo.PCName = clsMain.GetUser_PCName();
                itemInfo.IPAddress = clsMain.GetUser_IP();

                db.ItemInfoes.Add(itemInfo);
                try
                {
                    db.SaveChanges();
                    return Json(new { status = "Save successful.", itemid = itemInfo.ItemId, itemCode = itemInfo.ItemCode }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = "Error", message = "Duplicate item. Save failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Item/Multiple
        [CAuthorize]
        public ActionResult Multiple()
        {
            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit");
            ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName");
            ViewBag.FList = new MultiSelectList(db.Factories, "FactoryId", "FactoryName");
            ViewBag.fl = "";
            ViewBag.CategoryId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel == 4 && l.CategoryCode.StartsWith("0101")), "CategoryId", "CategoryName");
            //ViewBag.items = new SelectList(db.ItemInfoes.OrderBy(c => c.ItemName.Length).ThenBy(c => c.ItemName), "ItemId", "ItemName");
            ViewBag.BondId = new SelectList(dbIMS.Bonds.Select(c => new { c.BondId, LicenseNumber = c.LicenseNumber }), "BondId", "LicenseNumber");
            ViewBag.BondPeriodId = new SelectList(dbIMS.BondPeriods.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");
            ViewBag.CapacityId = new SelectList(dbIMS.BondCapacities.Where(l => l.BondId == 0), "CapacityId", "BondSerialNo");

            return View(new ItemView());
        }

        [CAuthorize]
        public ActionResult Save_Multiple(ItemView _itemView, ICollection<ItemVariantView> itemVariantView, ICollection<ItemFactoryView> itemFactoryView, ICollection<ItemUnitView> itemUnitView, ICollection<BondProductView> bondProductView)
        {
            ItemVariant common_variant = new ItemVariant();
            if (itemVariantView != null && itemVariantView.Count > 0)
            {
                common_variant = new ItemVariant(itemVariantView.Where(c => c.VariantId == 8).FirstOrDefault());
                common_variant.CreatedBy = clsMain.getCurrentUser();
                common_variant.CreatedTime = clsMain.getCurrentTime();
                common_variant.PCName = clsMain.GetUser_PCName();
                common_variant.IPAddress = clsMain.GetUser_IP();
                ItemCategory pcat = db.ItemCategories.Find(_itemView.CategoryId);
                int serialNo = db.ItemInfoes.Where(l => l.CategoryId == pcat.CategoryId).Select(c => c.SerialNo).Max().GetValueOrDefault(1000) + 1;
                string pCode = (pcat == null || pcat.CategoryCode == null ? "" : pcat.CategoryCode);
                var item_list = new List<string>();

                foreach (var variant in itemVariantView.Where(c => c.VariantId != 8).OrderBy(c => Convert.ToInt16(c.VariantValue)))
                {
                    var itemView = _itemView;
                    itemView.ItemName = pcat.CategoryName + " " + variant.VariantValue + " " + common_variant.VariantValue;
                    itemView.SystemName = itemView.ItemName;
                    itemView.BondName = itemView.ItemName;
                    itemView.ShortName = pcat.CategoryName.Substring(0, 3) + "-" + pcat.CategoryName.Replace(" GSM", "").Split(new Char[] { ' ' }).LastOrDefault() + "/" + variant.VariantValue;

                    if (db.ItemInfoes.Where(c => c.ItemName == itemView.ItemName).Count() == 0)
                    {
                        ViewBag.errormessage = "";

                        ItemInfo itemInfo = new ItemInfo(itemView);
                        itemInfo.ItemCode = pCode + (serialNo++).ToString();

                        ItemVariant iv = new ItemVariant(variant);

                        iv.CreatedBy = common_variant.CreatedBy;
                        iv.CreatedTime = common_variant.CreatedTime;
                        iv.PCName = common_variant.PCName;
                        iv.IPAddress = common_variant.IPAddress;

                        itemInfo.ItemVariants.Add(iv);
                        itemInfo.ItemVariants.Add(new ItemVariant(common_variant));
                        //if (itemVariantView != null)
                        //    foreach (ItemVariantView ivv in itemVariantView)
                        //    {
                        //        ItemVariant iv = new ItemVariant(ivv);
                        //        itemInfo.ItemVariants.Add(iv);
                        //    }
                        if (itemFactoryView != null)
                        {
                            foreach (ItemFactoryView ifv in itemFactoryView)
                            {
                                ItemFactory ift = new ItemFactory(ifv);
                                itemInfo.ItemFactories.Add(ift);
                            }
                        }
                        if (itemUnitView != null)
                        {
                            foreach (ItemUnitView iuv in itemUnitView)
                            {
                                ItemUnit iu = new ItemUnit(iuv);
                                itemInfo.ItemUnits.Add(iu);
                            }

                            itemInfo.QuantityUnitList = itemUnitView.Select(c => c.UnitId.ToString()).Aggregate((i, j) => i + ", " + j);
                        }

                        itemInfo.CreatedBy = common_variant.CreatedBy;
                        itemInfo.CreatedTime = common_variant.CreatedTime;
                        itemInfo.PCName = common_variant.PCName;
                        itemInfo.IPAddress = common_variant.IPAddress;

                        item_list.Add(itemInfo.ItemName);

                        db.ItemInfoes.Add(itemInfo);
                    }
                    //else
                    //{
                    //    return Json(new { status = "Error", message = "Duplicate item. Save failed." }, JsonRequestBehavior.AllowGet);
                    //}
                }
                try
                {
                    db.SaveChanges();

                    try
                    {
                        if (bondProductView != null)
                        {
                            foreach (var item in item_list)
                            {
                                int prod_id = db.ItemInfoes.Where(c => c.ItemName == item).FirstOrDefault().ItemId;

                                if (prod_id > 0)
                                {
                                    foreach (BondProductView bpv in bondProductView)
                                    {
                                        BondProduct bp = new BondProduct(bpv);
                                        bp.ProductId = prod_id;
                                        dbIMS.BondProducts.Add(bp);
                                    }
                                }
                            }

                            dbIMS.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        //return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { status = "Save successful.", itemid = "ALL" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = "Error", message = "No item. Save failed." }, JsonRequestBehavior.AllowGet);

        }


        // GET: /Item/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemInfo itemInfo = db.ItemInfoes.Find(id);

            if (itemInfo == null)
            {
                return HttpNotFound();
            }

            ItemView itemView = (ItemView)itemInfo.Convert(new ItemView());
            itemView.itemVariant = db.ItemVariants.Where(l => l.ItemId == itemInfo.ItemId).Select(
                c => new ItemVariantView()
                {
                    ItemVariantId = c.ItemVariantId,
                    ItemId = c.ItemId,
                    VariantId = c.VariantId,
                    VariantValue = c.VariantValue,
                    IsActive = c.IsActive
                }).ToList();

            itemView.itemFactory = db.ItemFactories.Where(l => l.ItemId == itemInfo.ItemId).Select(
                c => new ItemFactoryView()
                {
                    ItemFactoryId = c.ItemFactoryId,
                    ItemId = c.ItemId,
                    FactoryId = c.FactoryId,
                    FactoryName = db.Factories.Where(x=>x.FactoryId==c.FactoryId).Select(x=> x.FactoryName).FirstOrDefault(),
                    MaterialTypeId = c.MaterialTypeId,
                    TypeName = db.CustomTypes.Where(x => x.TypeId == c.MaterialTypeId).Select(x => x.TypeName).FirstOrDefault(),
                }).ToList();




            itemView.itemUnit = db.ItemUnits.Where(l => l.ItemId == itemInfo.ItemId).Select(
                c => new ItemUnitView()
                {
                    ItemUnitId = c.ItemUnitId,
                    ItemId = c.ItemId,
                    UnitId = c.UnitId,
                    UnitName = c.Variant_Value.VariantValue,
                    KGProportion = c.KGProportion
                }).ToList();

            if ((itemView.itemUnit == null || itemView.itemUnit.Count == 0) && itemView.QUnitList.Count() > 0)
            {
                itemView.itemUnit = db.viewQuantityUnits.Where(l => itemView.QUnitList.Contains(l.QuantityUnitId.ToString())).Select(
                    c => new ItemUnitView()
                    {
                        ItemUnitId = 0,
                        ItemId = itemView.ItemId,
                        UnitId = c.QuantityUnitId,
                        UnitName = c.QuantityUnit,
                        KGProportion = 0
                    }).ToList();
            }

            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit", itemView.DefaultMUnitId);
            ViewBag.QUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");
            ViewBag.DefaultQUnitId = new SelectList(itemView.itemUnit, "UnitId", "UnitName", itemView.DefaultQUnitId);
            ViewBag.FList = new MultiSelectList(db.Factories, "FactoryId", "FactoryName", itemView.FList);
            ViewBag.fl = itemView.itemFactory.Select(c => c.FactoryId.ToString()).Aggregate((i, j) => i + "," + j);
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName", itemView.DefaultFactoryId);
            ViewBag.CategoryId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel == 4), "CategoryId", "CategoryName", itemView.CategoryId);

            ViewBag.MaterialType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            return View(itemView);
        }

        //// POST: /Item/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Update([Bind(Include = "ItemId,SystemName,ItemName,ShortName,BondName,CategoryId,HSCode,DefaultFactoryId,FList,QUnitList,DefaultQUnitId,Activity")] ItemView itemView, ICollection<ItemVariantView> itemVariantView, ICollection<ItemFactoryView> itemFactoryView, ICollection<ItemUnitView> itemUnitView)
        {
            ViewBag.errormessage = "";
            if (ModelState.IsValid)
            {
                ItemInfo itemInfo = db.ItemInfoes.Find(itemView.ItemId);

                if (itemInfo.CategoryId != itemView.CategoryId)
                {
                    ItemCategory pcat = db.ItemCategories.Find(itemView.CategoryId);
                    int serialNo = db.ItemInfoes.Where(l => l.CategoryId == itemView.CategoryId).Select(c => c.SerialNo).Max().GetValueOrDefault(1000) + 1;
                    itemInfo.ItemCode = pcat.CategoryCode + serialNo.ToString();
                    //itemInfo.ItemCode = pcat.CategoryCode + db.ItemInfoes.Where(l => l.CategoryId == itemView.CategoryId).Select(c => c.SerialNo).Max().GetValueOrDefault(Convert.ToInt16("1001")).ToString("0000");
                }

                int rIndex = 0;//, cIndex = itemInfo.ItemFactories.Count > itemFactoryView.Count ? itemInfo.ItemFactories.Count : itemFactoryView.Count;
                //var pfv = itemFactoryView.Where(l => l.ItemFactoryId > 0).ToList();
                //var nfv = itemFactoryView.Where(l => l.ItemFactoryId <= 0).ToList();

                //for (rIndex = 0; rIndex < cIndex; rIndex++)
                //{
                //    if(rIndex < pfv.Count)
                //    {
                //        var ifv = pfv[rIndex];
                //        ItemFactory ift = db.ItemFactories.Find(ifv.ItemFactoryId);
                //        ift = (ItemFactory)ifv.ConvertNotNull(ift);
                //        db.Entry(ift).State = EntityState.Modified;
                //    }
                //    else
                //    {
                //        var ifv = nfv[rIndex];
                //        ItemFactory ift = db.ItemFactories.Find(ifv.ItemFactoryId);
                //        ift = (ItemFactory)ifv.ConvertNotNull(ift);
                //        db.Entry(ift).State = EntityState.Modified;
                //    }

                //    if (ifv.ItemFactoryId > 0)
                //        db.Entry(ifv).State = EntityState.Modified;
                //    else
                //}

                if (itemVariantView == null)
                    itemVariantView = new List<ItemVariantView>();
                if (itemFactoryView == null)
                    itemFactoryView = new List<ItemFactoryView>();

                var itemFactories = db.ItemFactories.Where(l => l.ItemId == itemInfo.ItemId).ToList();
                var itemVariants = db.ItemVariants.Where(l => l.ItemId == itemInfo.ItemId).ToList();

                if (itemFactories.Count >= itemFactoryView.Count)
                {
                    for (rIndex = 0; rIndex < itemFactoryView.Count; rIndex++)
                    {
                        var ifv = itemFactoryView.ToList()[rIndex];
                        ItemFactory ift = new ItemFactory();
                        if (ifv.ItemFactoryId > 0)
                            ift = db.ItemFactories.Find(ifv.ItemFactoryId);
                        else
                            ift = itemInfo.ItemFactories.Where(l => db.Entry(l).State == EntityState.Unchanged).ToList()[0];
                        ift = (ItemFactory)ifv.ConvertNotNull(ift);
                        db.Entry(ift).State = EntityState.Modified;
                    }

                    for (rIndex = 0; rIndex < itemFactories.Count; rIndex++)
                    {
                        var ifv = itemFactories.ToList()[rIndex];
                        if (db.Entry(ifv).State == EntityState.Unchanged)
                            db.ItemFactories.Remove(ifv);//.Entry(scv).State = EntityState.Deleted;
                    }
                }
                else
                {
                    for (rIndex = 0; rIndex < itemFactories.Count; rIndex++)
                    {
                        var ifv = itemFactoryView.ToList()[rIndex];
                        ItemFactory ift = new ItemFactory();
                        if (ifv.ItemFactoryId > 0)
                            ift = db.ItemFactories.Find(ifv.ItemFactoryId);
                        else
                            ift = itemFactories.Where(l => db.Entry(l).State == EntityState.Unchanged).ToList()[0];

                        ift = (ItemFactory)ifv.ConvertNotNull(ift);
                        db.Entry(ift).State = EntityState.Modified;
                        ifv._editStatus = true;
                    }

                    foreach (var ifv in itemFactoryView.Where(l => l._editStatus == false))
                    {
                        var ift = new ItemFactory(ifv);
                        itemInfo.ItemFactories.Add(ift);
                    }
                }

                if (itemVariants.Count >= itemVariantView.Count)
                {
                    for (rIndex = 0; rIndex < itemVariantView.Count; rIndex++)
                    {
                        var ifv = itemVariantView.ToList()[rIndex];
                        ItemVariant ift = new ItemVariant();
                        if (ifv.ItemVariantId > 0)
                            ift = db.ItemVariants.Find(ifv.ItemVariantId);
                        else
                            ift = itemVariants.Where(l => db.Entry(l).State == EntityState.Unchanged).ToList()[0];
                        ift = (ItemVariant)ifv.ConvertNotNull(ift);
                        db.Entry(ift).State = EntityState.Modified;
                    }

                    for (rIndex = 0; rIndex < itemVariants.Count; rIndex++)
                    {
                        var ifv = itemVariants.ToList()[rIndex];
                        if (db.Entry(ifv).State == EntityState.Unchanged)
                            db.Entry(ifv).State = EntityState.Deleted;
                    }
                }
                else
                {
                    for (rIndex = 0; rIndex < itemVariants.Count; rIndex++)
                    {
                        var ifv = itemVariantView.ToList()[rIndex];
                        ItemVariant ift = new ItemVariant();
                        if (ifv.ItemVariantId > 0)
                            ift = db.ItemVariants.Find(ifv.ItemVariantId);
                        else
                            ift = itemVariants.Where(l => db.Entry(l).State == EntityState.Unchanged).ToList()[0];

                        ift = (ItemVariant)ifv.ConvertNotNull(ift);
                        db.Entry(ift).State = EntityState.Modified;
                        ifv._editStatus = true;
                    }

                    foreach (var ifv in itemVariantView.Where(l => l._editStatus == false))
                    {
                        var ift = new ItemVariant(ifv);
                        itemVariants.Add(ift);
                    }
                }

                if (itemUnitView != null)
                {
                    foreach (var iuv in itemUnitView)
                    {
                        if (iuv.ItemUnitId > 0)
                        {
                            ItemUnit iu = db.ItemUnits.Find(iuv.ItemUnitId);
                            iu = (ItemUnit)iuv.ConvertNotNull(iu);

                            db.Entry(iu).State = EntityState.Modified;
                        }
                        else
                        {
                            ItemUnit iu = new ItemUnit(iuv);
                            itemInfo.ItemUnits.Add(iu);
                        }
                    }

                    var unsub = db.ItemUnits.Where(l => l.ItemId == itemInfo.ItemId).ToList();

                    for (int ri = 0; ri < unsub.Count; ri++)
                    {
                        if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                            db.Entry(unsub[ri]).State = EntityState.Deleted;
                    }

                    itemView.QuantityUnitList = itemUnitView.Select(c => c.UnitId.ToString()).Aggregate((i, j) => i + ", " + j);
                }

                itemInfo = (ItemInfo)itemView.ConvertNotNull(itemInfo);
                itemInfo.UpdatedBy = clsMain.getCurrentUser();
                itemInfo.UpdateTime = clsMain.getCurrentTime();
                db.Entry(itemInfo).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    return Content(itemInfo.ItemId.ToString());
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            //itemView.itemVariant = itemVariantView;
            //itemView.itemFactory = itemFactoryView;

            //ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit", itemView.DefaultMUnitId);
            //ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", itemView.DefaultQUnitId);
            //ViewBag.FList = new MultiSelectList(db.Factories, "FactoryId", "FactoryName", itemView.FList);
            //ViewBag.fl = itemView.FList.Aggregate((i, j) => i + "," + j);
            //ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName", itemView.DefaultFactoryId);
            //ViewBag.CategoryId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel == 4), "CategoryId", "CategoryName", itemView.CategoryId);

            //return View(itemView);
            throw new Exception(ViewBag.errormessage);
        }

        // GET: /Item/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemInfo itemInfo = db.ItemInfoes.Find(id);
            if (itemInfo == null)
            {
                return HttpNotFound();
            }
            return View(itemInfo);
        }

        // POST: /Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            ItemInfo itemInfo = db.ItemInfoes.Find(id);
            db.ItemInfoes.Remove(itemInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult VariantList(int? id, short? factoryId)
        {
            return PartialView(new int[] { id.Value, factoryId.Value });//Content(id.Value.ToString());
        }

        public ActionResult VariantListInit(ICollection<ItemVariantView> itemVariant)
        {
            return PartialView(itemVariant);
        }

        [HttpPost]
        public ActionResult FactoryVariantDetails(ICollection<ItemVariantView> itemVariant, ICollection<ItemFactoryView> itemFactory)
        {
            ItemView itemview = new ItemView();
            itemview.itemVariant = itemVariant;
            itemview.itemFactory = itemFactory;

            return PartialView(itemview);
        }

        [HttpPost]
        public ActionResult VariantString(int? id, List<string> varidlist, List<string> varvaluelist)
        {
            string itemName = "";

            itemName += db.ItemCategories.Find(id).Prefix;

            for (int i = 0; i < varidlist.Count; i++)
            {
                string prifix = "", suffix = "", val = "", cross = "";
                if (!String.IsNullOrEmpty(varidlist[i]))
                {
                    var v = db.Variants.Find(Int16.Parse(varidlist[i].Split('_').ToList()[0]));
                    if (varvaluelist[i].StartsWith(" X "))
                    {
                        cross = " X ";
                        val = varvaluelist[i].Substring(3);
                    }
                    else
                    {
                        cross = " ";
                        val = varvaluelist[i];
                    }
                    prifix = (String.IsNullOrEmpty(v.Prefix) ? " " : cross + v.Prefix.Trim() + ":");
                    suffix = (String.IsNullOrEmpty(v.Suffix) ? "" : " " + v.Suffix.Trim());

                    itemName += prifix + val + suffix;
                }
            }

            return Content(itemName);
        }

        [HttpPost]
        public ActionResult QUList(int? id)
        {
            var cat = db.ItemCategories.Find(id);

            return Json(new { qlist = cat.QuantityUnitList, qunit = cat.DefaultQUnitId }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CWItemList(int id)
        {
            ViewBag.items = new SelectList(db.ItemInfoes.Where(l => l.CategoryId == id).OrderBy(c => c.ItemName.Length).ThenBy(c => c.ItemName), "ItemId", "ItemName");

            return PartialView();
        }

        // GET: /Item/Details/5
        public ActionResult ItemDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemInfo itemInfo = db.ItemInfoes.Find(id);

            if (itemInfo == null)
            {
                return HttpNotFound();
            }

            var item = (from p in db.ItemInfoes.AsEnumerable()
                        join q in db.ItemFactories.AsEnumerable() on p.ItemId equals q.ItemId
                        join r in db.Factories.AsEnumerable() on q.FactoryId equals r.FactoryId
                        join s in db.ItemCategories.AsEnumerable() on p.CategoryId equals s.CategoryId
                        join t in db.ItemUnits.AsEnumerable() on p.DefaultQUnitId equals t.UnitId
                        join u in db.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals u.QuantityUnitId
                        where q.ItemId == id
                        select new { p, r, s, t, u }
                        ).Select(c => new ItemView()
                        {
                            ItemId = c.p.ItemId,
                            ItemName = c.p.ItemName,
                            SystemName = c.p.SystemName,
                            ShortName = c.p.ShortName,
                            IsActive = c.p.IsActive,
                            BondName = c.p.BondName,
                            CategoryName = c.s.CategoryName,
                            ItemCode = c.p.ItemCode,
                            CategoryId = c.p.CategoryId,
                            HSCode = c.p.HSCode,
                            QuantityUnitList = c.u.QuantityUnit + " => KGProportion (" + c.t.KGProportion.ToString("##,##0.00####") + ")"
                        }).FirstOrDefault();

            var fids = itemInfo.ItemFactories.Select(c => c.FactoryId).ToList();
            ViewBag.factory = db.Factories.Where(c => fids.Contains(c.FactoryId)).Select(c => c.ShortName).ToList().Aggregate((i, j) => i + ", " + j);

            return PartialView(item);
        }

        public ActionResult CapacityList(int id)
        {
            ViewBag.CapacityId = new SelectList(dbIMS.BondCapacities.Where(l => l.BondPeriodId == id).Select(c => new { CapacityId = c.CapacityId, BondSerialNo = c.BondSerialNo.ToString() + " => " + c.RMTitle }), "CapacityId", "BondSerialNo");

            return PartialView();
        }
    }
}
