﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Product
{
    public class VariantController : Controller
    {
        private ProductCubeEntities db = new ProductCubeEntities();

        // GET: /Variant/
        public ActionResult Index()
        {
            return View(db.Variants.Select(c => new VariantView() { VariantId = c.VariantId, VariantName = c.VariantName, DisplayName  = c.DisplayName, IsActive = c.IsActive }).ToList());
        }

        // GET: /Variant/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Variant variant = db.Variants.Find(id);
            if (variant == null)
            {
                return HttpNotFound();
            }
            return View(variant);
        }

        // GET: /Variant/Create
        public ActionResult Create()
        {
            ViewBag.VariantTextType = new SelectList(db.CustomTypes.Where(l => l.Flag == "TEXTTYPE"), "TypeId", "TypeName");
            ViewBag.VariantInputType = new SelectList(db.CustomTypes.Where(l => l.Flag == "INPUTTYPE"), "TypeId", "TypeName");
            return View(new VariantView());
        }

        // POST: /Variant/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VariantName,DisplayName,Prefix,Suffix,VariantTextType,VariantInputType,Activity")] VariantView variantView)
        {
            if (ModelState.IsValid)
            {
                Variant variant = new Variant(variantView);
                db.Variants.Add(variant);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VariantTextType = new SelectList(db.CustomTypes.Where(l => l.Flag == "TEXTTYPE"), "TypeId", "TypeName", variantView.VariantTextType);
            ViewBag.VariantInputType = new SelectList(db.CustomTypes.Where(l => l.Flag == "INPUTTYPE"), "TypeId", "TypeName", variantView.VariantInputType);
            return View(variantView);
        }

        // GET: /Variant/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Variant variant = db.Variants.Find(id);
            VariantView variantView = (VariantView)variant.Convert(new VariantView());

            if (variant == null)
            {
                return HttpNotFound();
            }

            ViewBag.VariantTextType = new SelectList(db.CustomTypes.Where(l => l.Flag == "TEXTTYPE"), "TypeId", "TypeName", variantView.VariantTextType);
            ViewBag.VariantInputType = new SelectList(db.CustomTypes.Where(l => l.Flag == "INPUTTYPE"), "TypeId", "TypeName", variantView.VariantInputType);
            return View(variantView);
        }

        // POST: /Variant/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VariantId,VariantName,DisplayName,Prefix,Suffix,VariantTextType,VariantInputType,Activity")] VariantView variantView)
        {
            if (ModelState.IsValid)
            {
                Variant variant = db.Variants.Find(variantView.VariantId);
                variant = (Variant)variantView.ConvertNotNull(variant);

                db.Entry(variant).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            ViewBag.VariantTextType = new SelectList(db.CustomTypes.Where(l => l.Flag == "TEXTTYPE"), "TypeId", "TypeName", variantView.VariantTextType);
            ViewBag.VariantInputType = new SelectList(db.CustomTypes.Where(l => l.Flag == "INPUTTYPE"), "TypeId", "TypeName", variantView.VariantInputType);
            return View(variantView);
        }

        // GET: /Variant/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Variant variant = db.Variants.Find(id);
            if (variant == null)
            {
                return HttpNotFound();
            }
            return View(variant);
        }

        // POST: /Variant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            Variant variant = db.Variants.Find(id);
            db.Variants.Remove(variant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
