﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Product
{
    public class CategoryController : Controller
    {
        private ProductCubeEntities db = new ProductCubeEntities();
        private InventoryCubeEntities dbInv = new InventoryCubeEntities();

        // GET: /Category/
        public ActionResult Index()
        {
            return View(db.ItemCategories.ToList());
        }

        // GET: /Category/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCategory itemCategory = db.ItemCategories.Find(id);
            if (itemCategory == null)
            {
                return HttpNotFound();
            }
            return View(itemCategory);
        }

        // GET: /Category/Create
        public ActionResult Create()
        {
            Category category = new Category();

            //var pCategory = (from p in db.ItemCategories.Where(l => l.StageLevel <= 3).AsEnumerable()
            //                 join q in db.ItemCategories.AsEnumerable() on p.CategoryId equals q.ParentId
            //                 select new {p.CategoryId, CategoryName = p.CategoryName}
            //                     )

            ViewBag.ParentId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel <= 3), "CategoryId", "CategoryName");
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName");
            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit");
            ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");

            return View(category);
        }

        // POST: /Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryName,Prefix,ParentId,DefaultFactoryId,MUnitList,QUnitList,DefaultMUnitId,DefaultQUnitId,Activity")] Category category, ICollection<CategoryVariantView> categoryVariant, ICollection<CategoryFactory> categoryFactory)
        {
            if (ModelState.IsValid)
            {
                ItemCategory itemCategory = new ItemCategory(category);
                ItemCategory pcat = db.ItemCategories.Find(itemCategory.ParentId);
                string pCode = ""; 
                if(pcat == null)
                {
                    itemCategory.StageLevel = 1;
                }
                else
                {
                    pCode = pcat.CategoryCode;
                    itemCategory.StageLevel = pcat.StageLevel;
                    ++itemCategory.StageLevel;
                }

                itemCategory.CategoryCode = pCode + db.ItemCategories.Where(l => l.ParentId == itemCategory.ParentId).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("00");

                if(categoryVariant != null)
                    foreach(CategoryVariantView cv in categoryVariant)
                    {
                        CategoryVariant cvt = new CategoryVariant(cv);
                        itemCategory.CategoryVariants.Add(cvt);
                    }

                if (categoryFactory != null)
                    foreach (CategoryFactory cf in categoryFactory)
                    {
                        itemCategory.CategoryFactories.Add(new CategoryFactory(cf));
                    }

                db.ItemCategories.Add(itemCategory);
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            category.factoryVariant = getFVDetaills(categoryVariant);

            ViewBag.ParentId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel <= 3), "CategoryId", "CategoryName", category.CategoryId);
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName", category.DefaultFactoryId);
            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit", category.DefaultMUnitId);
            ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", category.DefaultQUnitId);
            
            return View(category);
        }

        // GET: /Category/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ItemCategory ic = db.ItemCategories.Find(id);
            Category category = (Category)ic.Convert(new Category());

            category.factoryVariant = getFVDetaills(db.CategoryVariants.Where(l => l.CategoryId == id).Select(c => new CategoryVariantView() { CategoryId = c.CategoryId, FactoryId = c.FactoryId, VariantId = c.VariantId }));
            //ItemCategory itemCategory = db.ItemCategories.Where(l => l.CategoryId == id).;
            if (category == null)
            {
                return HttpNotFound();
            }

            ViewBag.ParentId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel <= 3), "CategoryId", "CategoryName", category.ParentId);
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName", category.DefaultFactoryId);
            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit", category.DefaultMUnitId);
            ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", category.DefaultQUnitId);

            return View(category);
        }

        // POST: /Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryId,CategoryName,Prefix,ParentId,DefaultFactoryId,MUnitList,QUnitList,DefaultMUnitId,DefaultQUnitId,Activity")] Category category, ICollection<CategoryVariantView> categoryVariant, ICollection<CategoryFactory> categoryFactory, string tableContent)
        {
            if (ModelState.IsValid)
            {
                ItemCategory itemCategory = db.ItemCategories.Find(category.CategoryId);

                foreach (CategoryFactory cf in db.CategoryFactories.Where(l => l.CategoryId  == itemCategory.CategoryId))
                {
                    db.Entry(cf).State = EntityState.Deleted;
                }

                foreach (CategoryVariant cv in db.CategoryVariants.Where(l => l.CategoryId == itemCategory.CategoryId))
                {
                    db.Entry(cv).State = EntityState.Deleted;
                }

                if (categoryVariant != null)
                    foreach (CategoryVariantView cv in categoryVariant)
                    {
                        CategoryVariant cvt = new CategoryVariant(cv);
                        cvt.CategoryId = itemCategory.CategoryId;
                        itemCategory.CategoryVariants.Add(cvt);
                    }

                if (categoryFactory != null)
                    foreach (CategoryFactory cf in categoryFactory)
                    {
                        cf.CategoryId = itemCategory.CategoryId;
                        itemCategory.CategoryFactories.Add(new CategoryFactory(cf));
                    }

                itemCategory = (ItemCategory)category.ConvertNotNull(itemCategory);
                db.Entry(itemCategory).State = EntityState.Modified;

                //db.ItemCategories.Add(itemCategory);
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            category.factoryVariant = getFVDetaills(categoryVariant);

            ViewBag.ParentId = new SelectList(db.viewItemCategories.Where(l => l.StageLevel <= 3), "CategoryId", "CategoryName", category.CategoryId);
            ViewBag.DefaultFactoryId = new SelectList(db.Factories, "FactoryId", "FactoryName", category.DefaultFactoryId);
            ViewBag.DefaultMUnitId = new SelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit", category.DefaultMUnitId);
            ViewBag.DefaultQUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", category.DefaultQUnitId);

            return View(category);
        }

        // GET: /Category/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCategory itemCategory = db.ItemCategories.Find(id);
            if (itemCategory == null)
            {
                return HttpNotFound();
            }
            return View(itemCategory);
        }

        // POST: /Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            ItemCategory itemCategory = db.ItemCategories.Find(id);
            db.ItemCategories.Remove(itemCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult FactoryWiseVariant()//short? id, string[] SFactory, string[] SVariant)
        {
            //if (ModelState.IsValid)
            //{
            //    object itemCategory = db.ItemCategories.Find(category.CategoryId);
            //    itemCategory = (ItemCategory)category.Convert(itemCategory);

            //    db.Entry(itemCategory).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            ViewBag.FactoryList = new SelectList(db.Factories, "FactoryId", "FactoryName");//, SFactory);
            ViewBag.VariantList = new MultiSelectList(db.Variants, "VariantId", "VariantName");//, SVariant);

            return PartialView();
        }

        public ActionResult UnitList(short? id, string[] MUnitList, string[] QUnitList)
        {
            //if (ModelState.IsValid)
            //{
            //    object itemCategory = db.ItemCategories.Find(category.CategoryId);
            //    itemCategory = (ItemCategory)category.Convert(itemCategory);

            //    db.Entry(itemCategory).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            ViewBag.MUList = new MultiSelectList(db.viewMeasureUnits, "MeasureUnitId", "MeasureUnit", MUnitList);
            ViewBag.QUList = new MultiSelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", QUnitList);

            return PartialView();
        }

        public ActionResult RowTemplate(short id, string FIDs, string SFactory, string[] VIDs, string SVariant)
        {
            string[] rowData = new string[] { id.ToString(), FIDs, SFactory, VIDs.Aggregate((i, j) => i + "," + j), SVariant };

            return PartialView(rowData);
        }

        [HttpPost]
        public ActionResult FactoryVariantDetails(ICollection<FactoryVariant> factoryVariant)
        {
            return PartialView(factoryVariant);
        }

        public ICollection<FactoryVariant> getFVDetaills(IEnumerable<CategoryVariantView> CategoryVariants)
        {
            ICollection<FactoryVariant> factoryVariant = new List<FactoryVariant>();
            
            if(CategoryVariants != null)
                factoryVariant = (from l in CategoryVariants
                                  join m in db.Factories.AsEnumerable() on l.FactoryId equals m.FactoryId
                                  join n in db.Variants.AsEnumerable() on l.VariantId equals n.VariantId
                                  group n by new
                                  {
                                      CategoryId = l.CategoryId,
                                      FactoryId = l.FactoryId,
                                      FactoryName = m.ShortName
                                  } into p
                                  select new FactoryVariant()
                                  {
                                      FIDs = p.Key.FactoryId.ToString(),
                                      SFactory = p.Key.FactoryName,
                                      VIDs = p.Select(i => i.VariantId.ToString()).Aggregate((j, k) => j + ", " + k),
                                      SVariant = p.Select(i => i.VariantName).Aggregate((j, k) => j + ", " + k),
                                  }).ToList();

            return factoryVariant;
        }
    }
}
