﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Product
{
    public class VariantValueController : Controller
    {
        private ProductCubeEntities db = new ProductCubeEntities();

        // GET: /VariantValue/
        public ActionResult Index()
        {
            var variant_value = (from p in db.Variant_Value.AsEnumerable()
                                 join q in db.Variants.AsEnumerable() on p.VariantId equals q.VariantId
                                 select new { p, q.VariantName }).Select(c => new VariantValueView()
                                 {
                                     VarValueId = c.p.VarValueId,
                                     VariantId = c.p.VariantId,
                                     VariantValue = c.p.VariantValue,
                                     VariantName = c.VariantName,
                                     IsActive = c.p.IsActive
                                 });

            return View(variant_value.ToList());
        }

        // GET: /VariantValue/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Variant_Value variant_Value = db.Variant_Value.Find(id);
            if (variant_Value == null)
            {
                return HttpNotFound();
            }
            return View(variant_Value);
        }

        // GET: /VariantValue/Create
        public ActionResult Create()
        {
            ViewBag.VariantId = new SelectList(db.Variants, "VariantId", "VariantName");
            return View(new VariantValueView());
        }

        // POST: /VariantValue/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="VariantId,VariantValue,Activity")] VariantValueView variantValueView)
        {
            if (ModelState.IsValid)
            {
                Variant_Value variantValue = new Variant_Value(variantValueView);

                db.Variant_Value.Add(variantValue);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VariantId = new SelectList(db.Variants, "VariantId", "VariantName", variantValueView.VariantId);
            return View(variantValueView);
        }

        // GET: /VariantValue/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Variant_Value variant_Value = db.Variant_Value.Find(id);
            VariantValueView variantValueView = (VariantValueView)variant_Value.Convert(new VariantValueView());

            if (variant_Value == null)
            {
                return HttpNotFound();
            }
            ViewBag.VariantId = new SelectList(db.Variants, "VariantId", "VariantName", variant_Value.VariantId);
            return View(variantValueView);
        }

        // POST: /VariantValue/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VarValueId,VariantId,VariantValue,Activity")] VariantValueView variantValueView)
        {
            if (ModelState.IsValid)
            {
                Variant_Value variant_Value = db.Variant_Value.Find(variantValueView.VarValueId);
                variant_Value = (Variant_Value)variantValueView.ConvertNotNull(variant_Value);

                db.Entry(variant_Value).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VariantId = new SelectList(db.Variants, "VariantId", "VariantName", variantValueView.VariantId);
            return View(variantValueView);
        }

        // GET: /VariantValue/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Variant_Value variant_Value = db.Variant_Value.Find(id);
            if (variant_Value == null)
            {
                return HttpNotFound();
            }
            return View(variant_Value);
        }

        // POST: /VariantValue/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            Variant_Value variant_Value = db.Variant_Value.Find(id);
            db.Variant_Value.Remove(variant_Value);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
