﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Product
{
    public class ItemAliasController : Controller
    {
        private ProductCubeEntities db = new ProductCubeEntities();

        // GET: /ItemAlias/
        public ActionResult Index()
        {
            return View(db.ItemAlias.ToList());
        }

        // GET: /ItemAlias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemAlia itemAlia = db.ItemAlias.Find(id);
            if (itemAlia == null)
            {
                return HttpNotFound();
            }
            return View(itemAlia);
        }

        // GET: /ItemAlias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ItemAlias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="AliasId,ItemId,AliasName,IsActive,RankId,CreatedBy,CreatedTime,UpdatedBy,UpdateTime,PCName,IPAddress")] ItemAlia itemAlia)
        {
            if (ModelState.IsValid)
            {
                db.ItemAlias.Add(itemAlia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(itemAlia);
        }

        // GET: /ItemAlias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemAlia itemAlia = db.ItemAlias.Find(id);
            if (itemAlia == null)
            {
                return HttpNotFound();
            }
            return View(itemAlia);
        }

        // POST: /ItemAlias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="AliasId,ItemId,AliasName,IsActive,RankId,CreatedBy,CreatedTime,UpdatedBy,UpdateTime,PCName,IPAddress")] ItemAlia itemAlia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(itemAlia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemAlia);
        }

        // GET: /ItemAlias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemAlia itemAlia = db.ItemAlias.Find(id);
            if (itemAlia == null)
            {
                return HttpNotFound();
            }
            return View(itemAlia);
        }

        // POST: /ItemAlias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemAlia itemAlia = db.ItemAlias.Find(id);
            db.ItemAlias.Remove(itemAlia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
