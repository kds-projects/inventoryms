﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using System.Data.SqlClient;

namespace InventoryMS.Controllers.Inventory
{
    public class ConsumptionController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private SDMSEntities dbSDMS = new SDMSEntities();

        // GET: /Consumption/
        public ActionResult Index()
        {
            return View(db.ConsumptionMains.ToList());
        }

        // GET: /Consumption/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptionMain consumptionMain = db.ConsumptionMains.Find(id);
            if (consumptionMain == null)
            {
                return HttpNotFound();
            }
            return View(consumptionMain);
        }

        // GET: /Consumption/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.ConsumptionType = new SelectList(db.Custom_Type.Where(l => l.Flag.Equals("CONSUMEFOR") && l.IsActive == 1), "TypeId", "TypeName");
            ViewBag.BookingNo = new SelectList((new[] { new { id = 1, type = "" } }).Where(c => c.id == 0).ToList(), "id", "type");
            ViewBag.QuantityUnit = new SelectList(dbProd.viewQuantityUnits.Where(l => l.IsActive == 1), "QuantityUnitId", "QuantityUnit");
            ViewBag.Measurement = new SelectList((new[] { new { id = 1, type = "" } }).Where(c => c.id == 0).ToList(), "id", "type");
            return View();
        }

        // POST: /Consumption/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(ConsumptionMainView mainView, ICollection<ConsumptionSubView> subViews)
        {
            try
            {
                //if (db.SRRMains.Where(l => l.SRRNo.Equals(srrView.SRRNo) && l.FactoryId == srrView.FactoryId).Count() > 0)
                //    return Content("Duplicate SRR No.");

                ConsumptionMain main = new ConsumptionMain(mainView);

                main.ConsumptionNo = mainView.ConsumptionNo.Substring(0, 3) + "/" + dbProd.Factories.Find(mainView.FactoryId).Prefix + "/" + db.IssueMains.Where(l => l.Year == mainView.ConsumptionDate.Year && l.FactoryId == mainView.FactoryId).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + mainView.ConsumptionDate.ToString("MM/yy").ToUpperInvariant();

                if (subViews != null)
                {
                    foreach (ConsumptionSubView _sub in subViews)
                    {
                        ConsumptionSub sub = new ConsumptionSub(_sub);

                        var issue_list = (from p in db.IssueMains.AsEnumerable()
                                          join q in db.IssueSubs.AsEnumerable() on p.IssueId equals q.IssueId
                                          join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                                          join s in db.IssueReturnAgainstLCs.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into rs
                                          from t in rs.DefaultIfEmpty()
                                          join u in db.ConsumptionAgainstLCs.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on r.IssueSubId equals u.IssueSubId into ru
                                          from v in ru.DefaultIfEmpty()
                                          where q.ProductId == sub.ProductId && r.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                                          select new { p.IssueDate, p.IssueId, q.IssueSubId, r.LCFileId, r.IssueLCId, NetQuantity = r.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) }
                                         ).Distinct().OrderBy(c => c.IssueDate);

                        var consume_qty = sub.ConsumeQuantity;
                        short row_no = 1;

                        foreach (var item in issue_list)
                        {
                            ConsumptionAgainstLC lc = new ConsumptionAgainstLC();
                            lc.IssueSubId = item.IssueSubId;
                            lc.IssueLCId = item.IssueLCId;
                            lc.ProductId = sub.ProductId;
                            lc.LCFileId = item.LCFileId;
                            lc.FSCId = 0;
                            lc.RowNo = row_no++;
                            lc.Remarks = "";

                            if (consume_qty > item.NetQuantity)
                            {
                                lc.Quantity = item.NetQuantity;
                                consume_qty -= item.NetQuantity;
                            }
                            else
                            {
                                lc.Quantity = consume_qty;
                                sub.ConsumptionAgainstLCs.Add(lc);
                                break;
                            }

                            sub.ConsumptionAgainstLCs.Add(lc);
                        }

                        main.ConsumptionSubs.Add(new ConsumptionSub(sub));
                    }
                }

                main.ApprovedType = 1;
                main.ApprovedBy = Convert.ToInt16(main.FactoryId == 3 || main.FactoryId == 4 || main.FactoryId == 15 ? 17 : main.FactoryId == 2 ? 15 : main.FactoryId == 16 ? 48 : 4);
                main.ApprovedTime = clsMain.getCurrentTime();

                db.ConsumptionMains.Add(main);
                db.SaveChanges();

                return Json(new { status = "Save successful.", consumeno = main.ConsumptionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }
        
        // GET: /Consumption/Daily
        [CAuthorize]
        public ActionResult Daily()
        {
            ConsumptionMain consmMain = new ConsumptionMain();
            consmMain.ConsumptionDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            //ViewBag.ConsumptionType = new SelectList(db.Custom_Type.Where(l => l.Flag.Equals("CONSUMEFOR") && l.IsActive == 1), "TypeId", "TypeName");
            //ViewBag.BookingNo = new SelectList((new[] { new { id = 1, type = "" } }).Where(c => c.id == 0).ToList(), "id", "type");
            //ViewBag.QuantityUnit = new SelectList(dbProd.viewQuantityUnits.Where(l => l.IsActive == 1), "QuantityUnitId", "QuantityUnit");
            //ViewBag.Measurement = new SelectList((new[] { new { id = 1, type = "" } }).Where(c => c.id == 0).ToList(), "id", "type");
            ViewBag.ConsumptionNo = "##/####/0000-" + DateTime.Now.ToString("MM/yy");
            ViewBag.MaterialType = dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId).ToList();

            return View(consmMain);
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult Save_Daily(ConsumptionMain main, ICollection<ConsumptionSub> subs, string prefix = "CS")
        {
            try
            {
                ConsumptionMain cmain = new ConsumptionMain(main);

                    //cmain.ConsumptionNo = "CM/" + dbProd.Factories.Find(cmain.FactoryId).Prefix + "/" + db.ConsumptionMains.Where(l => l.Year == cmain.ConsumptionDate.Year && l.FactoryId == cmain.FactoryId).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + cmain.ConsumptionDate.ToString("MM/yy").ToUpperInvariant();


                cmain.ConsumptionNo = prefix
               + "/" + dbProd.Factories.Find(cmain.FactoryId).Prefix
               + "/" + db.ConsumptionMains.Where(l => l.Year == cmain.ConsumptionDate.Year && l.FactoryId == cmain.FactoryId && l.ConsumptionNo.StartsWith(prefix)).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000")
               + "-" + cmain.ConsumptionDate.ToString("MM/yy").ToUpperInvariant();

                cmain.CreatedBy = clsMain.getCurrentUser();
                cmain.CreatedTime = clsMain.getCurrentTime();

                foreach (var _sub in subs)
                {
                    ConsumptionSub sub = new ConsumptionSub(_sub);
                    sub.CategoryId = dbProd.ItemInfoes.Find(sub.ProductId).CategoryId;

                    var issue_list = (from p in db.IssueMains.AsEnumerable()
                                      join q in db.IssueSubs.AsEnumerable() on p.IssueId equals q.IssueId
                                      join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                                      join s in db.IssueReturnAgainstLCs.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into rs
                                      from t in rs.DefaultIfEmpty()
                                      join u in db.ConsumptionAgainstLCs.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on r.IssueSubId equals u.IssueSubId into ru
                                      from v in ru.DefaultIfEmpty()
                                      where q.ProductId == sub.ProductId && r.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                                      select new { p.IssueDate, p.IssueId, q.IssueSubId, r.GRRSubId, r.LCFileId, r.IssueLCId, NetQuantity = r.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) }
                                     ).Distinct().OrderByDescending(c => c.IssueDate);

                    var consume_qty = sub.ConsumeQuantity;
                    short row_no = 1;

                    foreach (var item in issue_list)
                    {
                        ConsumptionAgainstLC lc = new ConsumptionAgainstLC();
                        lc.IssueSubId = item.IssueSubId;
                        lc.IssueLCId = item.IssueLCId;
                        lc.ProductId = sub.ProductId;
                        lc.LCFileId = item.LCFileId;
                        lc.GRRSubId = item.GRRSubId;
                        lc.FSCId = 0;
                        lc.RowNo = row_no++;
                        lc.Remarks = "";

                        if (consume_qty > item.NetQuantity)
                        {
                            lc.Quantity = item.NetQuantity;
                            consume_qty -= item.NetQuantity;
                        }
                        else
                        {
                            lc.Quantity = consume_qty;
                            sub.ConsumptionAgainstLCs.Add(lc);
                            break;
                        }

                        sub.ConsumptionAgainstLCs.Add(lc);
                    }

                    cmain.ConsumptionSubs.Add(sub);
                }

                cmain.ApprovedType = 1;
                cmain.ApprovedBy = Convert.ToInt16(cmain.FactoryId == 3 || cmain.FactoryId == 4 || cmain.FactoryId == 15 ? 17 : cmain.FactoryId == 2 ? 15 : cmain.FactoryId == 16 ? 48 : 4);
                cmain.ApprovedTime = clsMain.getCurrentTime();

                db.ConsumptionMains.Add(cmain);

                try
                {
                    db.SaveChanges();

                    //var row = (from p in db.EmailLibraries.AsEnumerable()
                    //           join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                    //           where p.EmailTypeId == 1 && q.FactoryId == cmain.FactoryId
                    //           select new
                    //           {
                    //               p.TypeName,
                    //               p.EmailBody,
                    //               q.EmailGroupFrom,
                    //               q.EmailGroupTo,
                    //               q.EmailGroupCC,
                    //               q.EmailGroupBCC,
                    //               p.Subject
                    //           }).FirstOrDefault();

                    //EMailCollectionView MailCollection = new EMailCollectionView();
                    //MailCollection.EmailType = row.TypeName;
                    //MailCollection.ModuleName = "IPMS";
                    //MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                    //MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                    //MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                    //MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                    //MailCollection.EmailSubject = row.Subject.Replace("@RequisitionNo", cmain.ConsumptionNo);
                    //MailCollection.EmailBody = row.EmailBody.Replace("@RequisitionNo", cmain.ConsumptionNo)
                    //    .Replace("@Factory", dbProd.Factories.Find(cmain.FactoryId).ShortName)
                    //    .Replace("@Date", cmain.ConsumptionDate.ToString("dd-MMM-yyyy"))
                    //    .Replace("@ApprovalLink", @"http://192.168.0.47/ipms/requisition/details/" + cmain.ConsumptionId);
                    //MailCollection.EmailAttachmentLink = "";
                    //MailCollection.CreateAttachment = 0;
                    //MailCollection.CreatedBy = clsMain.getCurrentUser();

                    //EMailCollection mail = new EMailCollection(MailCollection);

                    //dbEM.EMailCollections.Add(mail);

                    //dbEM.SaveChanges();

                    return Json(new { status = "Save successful.", cmno = cmain.ConsumptionNo }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Consumption/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptionMain consumptionMain = db.ConsumptionMains.Find(id);
            if (consumptionMain == null)
            {
                return HttpNotFound();
            }
            return View(consumptionMain);
        }

        // POST: /Consumption/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ConsumptionId,ConsumptionNo,ConsumptionDate,BookingId,BookingSubId,PlanId,ComplainId,ComplainSubId,OtherRefId,BookingQuantity,ExcessQuantity,QuantityUnitId,Remarks,CreatedBy,CreatedTime,UpdatedBy,UpdateTime,PCName,IPAddress")] ConsumptionMain consumptionMain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consumptionMain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(consumptionMain);
        }

        // GET: /Consumption/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptionMain consumptionMain = db.ConsumptionMains.Find(id);
            if (consumptionMain == null)
            {
                return HttpNotFound();
            }
            return View(consumptionMain);
        }

        // POST: /Consumption/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ConsumptionMain consumptionMain = db.ConsumptionMains.Find(id);
            db.ConsumptionMains.Remove(consumptionMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ConsumptionDetails(short rowno = 1, short factory_id = 0)
        {
            try
            {
                //var cid_list = (from p in dbProd.ItemCategories.AsEnumerable()
                //                join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                //                join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                //                where p.StageLevel == 4 && r.FactoryId == factory_id && p.CategoryCode.Substring(0, 2) == "01"
                //                select p.CategoryId
                //               ).Distinct();

                //var item_list = (from p in db.IssueMains.AsEnumerable()
                //                join q in db.IssueSubs.AsEnumerable() on p.IssueId equals q.IssueId
                //                join i in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals i.ItemId
                //                join ic in dbProd.ItemCategories.AsEnumerable() on i.CategoryId equals ic.CategoryId
                //                //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                //                join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                //                from t in rs.DefaultIfEmpty()
                //                join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                //                from v in ru.DefaultIfEmpty()
                //                where cid_list.Contains(i.CategoryId) && p.FactoryId == factory_id && q.Quantity - (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                //                select i
                //               ).Distinct();
                var item_list = db.Database.SqlQuery<_SelectListItem>("Exec prcGetSelectListItem " + factory_id.ToString() + ", 'CONSUMPTION'").Distinct();


                //var cat_list = dbProd.ItemCategories.Where(c => item_list.Select(l => l.Id).Contains(c.CategoryId).Select(c => new { c.ItemCategory.CategoryId, c.ItemCategory.CategoryName }).Distinct();

                var cat_list = item_list.Select(c => new { CategoryId = c.Id2, CategoryName = c.Text2 }).Distinct();

                ViewBag.CategoryList = cat_list.OrderBy(c => c.CategoryName);
                ViewBag.Category = new SelectList(cat_list.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
                ViewBag.Product = new SelectList(item_list.Select(c => new { c.Id, c.Text }).Distinct().OrderBy(c => c.Text).ThenBy(c => c.Text.Length), "Id", "Text");
                ViewBag.Brand = new SelectList(db.Brands.Where(c => c.IsActive == 1), "BrandId", "BrandName", 0);

                ConsumptionSubView sub = new ConsumptionSubView(true);
                sub.RowNo = rowno;
                sub.CategoryName = "";

                return PartialView(new List<ConsumptionSubView>() { sub });
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '6'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult RMList(int id = 0)
        {
            try
            {
                ViewBag.Category = new SelectList(dbProd.ItemCategories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
                ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 0), "ItemId", "ItemName");
                ViewBag.Brand = new SelectList(db.Brands.Where(c => c.IsActive == 1), "BrandId", "BrandName", 0);

                DataTable dt = new DataTable();
                dt.Columns.Add("PlanId", typeof(int));
                dt.Columns.Add("Quantity", typeof(Decimal));

                var sqlp = new SqlParameter("@PlanDetails", dt);
                sqlp.SqlDbType = SqlDbType.Structured;
                sqlp.TypeName = "dbo.PlanList";

                var list = dbSDMS.Database.SqlQuery<ConsumptionSubView>("Exec dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", new SqlParameter[] { new SqlParameter("@FactoryId", Convert.ToInt32(0)), new SqlParameter("@Flag", "RMLIST".ToString()), new SqlParameter("@Id", Convert.ToInt32(id)), sqlp }).ToList();
                //var list = db.Database.SqlQuery<PlanDetails>("Exec SalesCube.dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", FId, flag, id, sqlp);

                return PartialView("ConsumptionDetails", list);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '6'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult ProductList(int id, short facid, short rowno)
        {
            try
            {
                //var product = (from p in dbProd.ItemInfoes.AsEnumerable()
                //               join q in dbProd.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals q.QuantityUnitId
                //               where p.ItemId == id
                //               select new { q.QuantityUnitId, q.QuantityUnit }
                //              ).FirstOrDefault();
                var product = (from i in dbProd.ItemInfoes.AsEnumerable()
                               join qu in dbProd.viewQuantityUnits.AsEnumerable() on i.DefaultQUnitId equals qu.QuantityUnitId
                               join q in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid && c.IssueMain.IssueTypeId == 38).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, Quantity = c.Sum(l => l.Quantity) }).AsEnumerable() on i.ItemId equals q.ProductId
                               //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                               //join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                               //from t in rs.DefaultIfEmpty()
                               //join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                               //from v in ru.DefaultIfEmpty()
                               join s in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on i.ItemId equals s.ProductId into rs
                               from t in rs.DefaultIfEmpty()
                               join u in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on i.ItemId equals u.ProductId into ru
                               from v in ru.DefaultIfEmpty()
                               where ((id > 0 && i.CategoryId == id) || id == 0) && q.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                               select i
                              ).Distinct();

                ViewBag.Product = new SelectList(product, "ItemId", "ItemName");
                ViewBag.rowno = rowno.ToString("00");

                return PartialView();
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult ProductDetails(int id, short facid)
        {
            try
            {
                //var product = (from p in dbProd.ItemInfoes.AsEnumerable()
                //               join q in dbProd.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals q.QuantityUnitId
                //               where p.ItemId == id
                //               select new { q.QuantityUnitId, q.QuantityUnit }
                //              ).FirstOrDefault();
                var product = (from i in dbProd.ItemInfoes.AsEnumerable()
                               join qu in dbProd.viewQuantityUnits.AsEnumerable() on i.DefaultQUnitId equals qu.QuantityUnitId
                               join q in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid && c.IssueMain.IssueTypeId == 38).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, Quantity = c.Sum(l => l.Quantity) }).AsEnumerable() on i.ItemId equals q.ProductId
                               //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                               //join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                               //from t in rs.DefaultIfEmpty()
                               //join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                               //from v in ru.DefaultIfEmpty()
                               join s in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on i.ItemId equals s.ProductId into rs
                               from t in rs.DefaultIfEmpty()
                               join u in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on i.ItemId equals u.ProductId into ru
                               from v in ru.DefaultIfEmpty()
                               where i.ItemId == id && q.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                               select new { qu.QuantityUnitId, qu.QuantityUnit, balqty = q.Quantity - (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) }
                              ).GroupBy(c => new { c.QuantityUnit, c.QuantityUnitId }).Select(c => new { QuantityUnit = c.Key.QuantityUnit, QuantityUnitId = c.Key.QuantityUnitId, balqty = c.Sum(l => l.balqty) }).FirstOrDefault();

                return Json(new { unitid = product.QuantityUnitId, unit = product.QuantityUnit, balqty = product.balqty.ToString("##,##0.0000") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        //public ActionResult ReferenceList(int id, byte factory_id = 0)
        //{
        //    //var list = dbSDMS.Database.SqlQuery<SRRSubView>("Exec dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", new SqlParameter[] { new SqlParameter("@FactoryId", Convert.ToInt32(0)), new SqlParameter("@Flag", "REQUISITIONPRODUCT".ToString()), new SqlParameter("@Id", Convert.ToInt32(0)), sqlp }).ToList();

        //    ViewBag.ReferenceId = new SelectList(dbSDMS.BookingMains.Where(c => c.CategoryId == 1).OrderByDescending(c => c.BookingDate).ThenByDescending(c => c.BookingNo).Take(10), "BookingId", "BookingNo");

        //    return PartialView();
        //}
        [HttpPost]
        public JsonResult ReferenceList(DateTime? consumptiondate, int id = 0, byte factory_id = 0, string type = "##")
        {

            /*var lst= db.ConsumptionMains.Where(l => l.Year == consumptiondate.Value.Year && l.FactoryId == id && l.ConsumptionNo.StartsWith(type)).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000");
 */
            var serialNumber = db.ConsumptionMains.Where(l => l.Year == consumptiondate.Value.Year && l.FactoryId == factory_id && l.ConsumptionNo.StartsWith(type))
                 .Select(c => c.SerialNo + 1)
                 .Max()
                 .GetValueOrDefault(Convert.ToInt16("1"))
                 .ToString("0000");
            string consumptionno = type
                + "/"
                + dbProd.Factories.Find(factory_id).Prefix
                + "/"
                + serialNumber
                + "-" + consumptiondate.Value.ToString("MM/yy").ToUpperInvariant();


            return Json(consumptionno, JsonRequestBehavior.AllowGet);
        }

            public ActionResult SubReferenceList(int id, byte factory_id = 0, int booking_id = 0)
        {
            //var list = dbSDMS.Database.SqlQuery<SRRSubView>("Exec dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", new SqlParameter[] { new SqlParameter("@FactoryId", Convert.ToInt32(0)), new SqlParameter("@Flag", "REQUISITIONPRODUCT".ToString()), new SqlParameter("@Id", Convert.ToInt32(0)), sqlp }).ToList();

            ViewBag.BookingSubId = new SelectList(dbSDMS.BookingSubs.Where(c => c.BookingId == booking_id && c.RelationType == 1).Select(c => new { BookingSubId = c.BookingSubId, Measurement = c.Measurement + " / " + c.PaperCombination, RowNo = c.RowNo }).OrderBy(c => c.RowNo), "BookingSubId", "Measurement");

            return PartialView();
        }
    }
}
