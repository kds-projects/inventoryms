﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    [CAuthorize]
    public class PurchaseOrderController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();

        // GET: /PurchaseOrder/
        public ActionResult Index()
        {
            return View(db.PurchaseOrders.ToList());
        }

        // GET: /PurchaseOrder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        // GET: /PurchaseOrder/Create
        public ActionResult Create()
        {
            //var prlist = (from p in db.PRMains.AsEnumerable()
            //              join q in db.PRSubs.GroupBy(c => c.PRId).Select(c => new { PRId = c.Key, Quantity = c.Sum(l => l.Quantity) }).AsEnumerable() on p.PRId equals q.PRId
            //              join r in db.PurchaseOrderSubs.GroupBy(c => c.PRId).Select(c => new { PRId = c.Key, Quantity = c.Sum(l => l.Quantity) }).AsEnumerable() on p.PRId equals r.PRId into s
            //              from t in s.DefaultIfEmpty()
            //              where p.ApprovedType == 1 && (q.Quantity - (t == null ? 0 : t.Quantity)) > 0
            //              select new { PRId = p.PRId, PRNo = p.PRNo/*, RQty = q.Quantity, AQty = r.Quantity*/ }).ToList();

            var id = dbProd.UserFactories.Select(c => c.CompanyId).ToList();
            ViewBag.BondId = new SelectList(db.Bonds.Where(l => id.Contains(l.CompanyId)), "BondId", "LicenseNumber");
            ViewBag.PRList = new MultiSelectList(new List<string>{}.Select(c => new { PRId = c, PRNo = c }), "PRId", "PRNo");

            return View(new POView());
        }

        // POST: /PurchaseOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save([Bind(Include = "PODate,BondId,Remarks")] POView poView, ICollection<PORequisitionView> poRequisitionView, ICollection<POSub> poSub)
        {
            if (ModelState.IsValid)
            {
                PurchaseOrder purchaseOrder = new PurchaseOrder(poView);

                purchaseOrder.PONo = "PO/" + purchaseOrder.BondId.ToString("00") + "/" + db.PurchaseOrders.Where(l => l.Year == purchaseOrder.PODate.Year && l.BondId == poView.BondId).Select(c => c.SerialNo+1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + purchaseOrder.PODate.ToString("MM/yy").ToUpperInvariant();
                purchaseOrder.BondPeriodId = db.BondPeriods.Where(l => l.BondId == poView.BondId && l.StartDate <= poView.PODate && l.EndDate >= poView.PODate).Select(c => c.BondPeriodId).DefaultIfEmpty(0).FirstOrDefault();

                if(purchaseOrder.BondPeriodId == 0)
                {
                    foreach(var _sub in poSub)
                    {
                        var _item = dbProd.ItemInfoes.Find(_sub.ProductId);
                        if (_item.ItemCode.StartsWith("01"))
                        {
                            return Json(new { status = "Expired", pono = purchaseOrder.PONo }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                if (poSub != null)
                {
                    foreach (var posub in poSub)
                    {
                        PurchaseOrderSub sub = new PurchaseOrderSub(posub);
                        purchaseOrder.PurchaseOrderSubs.Add(sub);
                    }
                }

                if (poRequisitionView != null)
                {
                    foreach (var poreq in poRequisitionView)
                    {
                        PORequisition req = new PORequisition(poreq);
                        purchaseOrder.PORequisitions.Add(req);
                    }
                }

                db.PurchaseOrders.Add(purchaseOrder);

                try
                {
                    db.SaveChanges();
                    return Json(new { status = "Save successful.", pono = purchaseOrder.PONo }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            //poView.poSub = poSub;

            //ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber", poView.BondId);
            //ViewBag.PRList = new MultiSelectList(db.PRMains, "PRId", "PRNo", poView.PRList);

            //return View(poView);
            throw new Exception(ViewBag.errormessage);
        }

        // GET: /PurchaseOrder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            POView poView = (POView)purchaseOrder.Convert(new POView());

            poView.PRList = db.PORequisitions.Where(l => l.POId == id).Select(c => c.PRId.ToString()).ToArray();

            if (poView == null)
            {
                return HttpNotFound();
            }

            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber", poView.BondId);
            ViewBag.PRList = new MultiSelectList(db.PRMains, "PRId", "PRNo", poView.PRList);
            return View(poView);
        }

        // POST: /PurchaseOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "POId,PODate,BondId,Remarks,PRList")] POView poView, ICollection<PORequisitionView> poRequisitionView, ICollection<POSub> poSub)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(purchaseOrder).State = EntityState.Modified;
                //db.SaveChanges();
                //return RedirectToAction("Index");
            }

            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber", poView.BondId);
            ViewBag.PRList = new MultiSelectList(db.PRMains, "PRId", "PRNo", poView.PRList);
            return View(poView);
        }

        // GET: /PurchaseOrder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        // POST: /PurchaseOrder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            db.PurchaseOrders.Remove(purchaseOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult PRList(short id, DateTime? podate)
        {
            var prlist = (from p in db.PRMains.AsEnumerable()
                          join q in dbProd.UserFactories.AsEnumerable() on p.FactoryId equals q.FactoryId
                          join r in db.Bonds.AsEnumerable() on q.CompanyId equals r.CompanyId
                          join t in db.PRSubs.GroupBy(c => new { c.PRId, c.PRSubId, c.IsPOComplete }).Select(c => new { PRId = c.Key.PRId, PRSubId = c.Key.PRSubId, IsPOComplete = c.Key.IsPOComplete, PRQty = c.Sum(l => l.Quantity) }).AsEnumerable() on p.PRId equals t.PRId
                          join v in db.PurchaseOrderSubs.GroupBy(c => new { c.PRId, c.PRSubId }).Select(c => new { PRId = c.Key.PRId, PRSubId = c.Key.PRSubId, POQty = c.Sum(l => l.Quantity) }).AsEnumerable() on t.PRSubId equals v.PRSubId into w
                          from x in w.DefaultIfEmpty()
                          where r.BondId == id && p.ApprovedType == 1 && t.PRQty - (x == null ? 0 : x.POQty) > 0 && t.IsPOComplete == 0
                          select p).Distinct().ToList();
            ViewBag.pono = "PO/" + id.ToString("00") + "/" + db.PurchaseOrders.Where(l => l.Year == podate.Value.Year && l.BondId == id).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + podate.Value.ToString("MM/yy").ToUpperInvariant();
            ViewBag.PRList = new MultiSelectList(prlist, "PRId", "PRNo");
            return PartialView();
        }

        //[HttpPost]
        public ActionResult RowTemplate(ICollection<int> reqList, int? id)
        {
            ICollection<RequisitionView> reqSubs;

            if (id == null || id == 0)
            {
                reqSubs = (from p in db.PRSubs.Where(l => reqList.Contains(l.PRId)).AsEnumerable()
                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                          join w in db.PurchaseOrderSubs.GroupBy(c => c.PRSubId).Select(c => new { PRSubId = c.Key, POQty = c.Sum(l => l.Quantity) }).AsEnumerable() on p.PRSubId equals w.PRSubId into x
                          from y in x.DefaultIfEmpty()
                          where p.Quantity - (y == null ? 0 : y.POQty) > 0 && p.IsPOComplete == 0
                          select new { p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, BalanceQty = p.Quantity - (y == null ? 0 : y.POQty) }
                            ).Select(c => new RequisitionView()
                            {
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                DaysWithTotalStock = c.p.DaysWithTotalStock,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.p.Quantity,
                                PendingQuantity = c.BalanceQty,
                                CategoryId = c.CategoryId,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.p.Remarks
                            }).ToList();
            }
            else
            {
                reqSubs = (from o in db.PurchaseOrders.Find(id).PurchaseOrderSubs.AsEnumerable()
                          join p in db.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                          where o.IsQAComplete == 0
                          select new { o, p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit }
                            ).Select(c => new RequisitionView()
                            {
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                POId = c.o.POId,
                                POSubId = c.o.POSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                DaysWithTotalStock = c.p.DaysWithTotalStock,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.o.Quantity,
                                CategoryId = c.CategoryId,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.o.Remarks
                            }).ToList();
            }

            //if (reqSubs.Count == 0)
            //    reqSubs.Add(new RequisitionView());

            return PartialView(reqSubs);
        }

        [HttpPost]
        public ActionResult POList(short id, DateTime? podate)
        {
            var polist = (from p in db.PurchaseOrders.AsEnumerable()
                          join q in db.PurchaseOrderSubs.AsEnumerable() on p.POId equals q.POId 
                          join r in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals r.FactoryId
                          where r.FactoryId == id
                          select p).Distinct().ToList();
            
            ViewBag.POId = new SelectList(polist, "POId", "PONo");

            return PartialView();
        }

        public ActionResult PODetails(ICollection<int> reqList, ICollection<POSub> poSub)
        {
            POView po = new POView();
            po.poSub = poSub;
            po.PRList = reqList.Select(c => c.ToString()).ToArray();

            return PartialView(po);
        }
    }
}
