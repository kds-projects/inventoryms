﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class RevisionController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();

        // GET: Revision
        public ActionResult Index()
        {
            return View();
        }

        // GET: Revision/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Revision/StatusUpdate
        [CAuthorize]
        public ActionResult StatusUpdate()
        {
            //var type = new object[] { new { TypeId = 1, TypeName = "Requisiton" }, new { TypeId = 2, TypeName = "Purchase Order" }, new { TypeId = 3, TypeName = "Quantity Approval" } };
            var type = new object[] { new { TypeId = 1, TypeName = "Requisiton" }, new { TypeId = 3, TypeName = "Quantity Approval" } };
            ViewBag.type = new SelectList(type, "TypeId", "TypeName");
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        // POST: Revision/StatusUpdate
        [HttpPost, CAuthorize]
        public ActionResult StatusUpdate(ICollection<CompleteStatus> completeStatus, short type)
        {
            try
            {
                foreach (var sub in completeStatus)
                {
                    if (type == 1)
                    {
                        var item = db.PRSubs.Find(sub.TranId);
                        item.IsPOComplete = sub.status;
                        item.CompleteDate = db.PurchaseOrderSubs.Where(l => l.PRSubId == sub.TranId).Select(c => c.PRMain.CreatedTime).DefaultIfEmpty(clsMain.getCurrentTime()).Max();

                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                    else if (type == 2)
                    {
                        var item = db.PurchaseOrderSubs.Find(sub.TranId);
                        item.IsQAComplete = sub.status;
                        item.CompleteDate = db.QuantityApprovalSubs.Where(l => l.POSubId == sub.TranId).Select(c => c.QuantityApprovalMain.CreatedTime).DefaultIfEmpty(clsMain.getCurrentTime()).Max();

                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                    else if (type == 3)
                    {
                        var item = db.QuantityApprovalSubs.Find(sub.TranId);
                        item.IsPIComplete = sub.status;
                        item.CompleteDate = dbFund.PISubs.Where(l => l.QApprovalSubId == sub.TranId).Select(c => c.PIMain.CreatedTime).DefaultIfEmpty(clsMain.getCurrentTime()).Max();

                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                db.SaveChanges();

                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Revision/RemarksUpdate
        [HttpGet, CAuthorize]
        public ActionResult RemarksUpdate()
        {
            //var type = new object[] { new { TypeId = 1, TypeName = "Requisiton" }, new { TypeId = 2, TypeName = "Purchase Order" }, new { TypeId = 3, TypeName = "Quantity Approval" } };
            var type = new object[] { 
                new { TypeId = 1, TypeName = "Pending for Quantity Approval" }, 
                new { TypeId = 2, TypeName = "Pending for Price Approval" }, 
                new { TypeId = 3, TypeName = "Forward for LC" }, 
                new { TypeId = 4, TypeName = "Pending For LC Open" }, 
                new { TypeId = 5, TypeName = "LC Opened But Goods Not Received Yet" }
                //new { TypeId = 6, TypeName = "Pipeline" }
            };
            ViewBag.type = new SelectList(type, "TypeId", "TypeName");
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.PRList = new SelectList(db.PRMains.OrderBy(c => c.FactoryId).OrderByDescending(c => c.PRDate), "PRId", "PRNo");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(c => c.IsActive == 1 && c.SupplierId > 0 && c.ApprovedType == 1), "SupplierId", "SupplierName");

            return View();
        }

        // POST: Revision/RemarksUpdate
        [HttpPost, CAuthorize]
        public ActionResult RemarksUpdate(RemarksDetailView remarksdetail)
        {
            try
            {
                //var remarks_details = db.RemarksDetails.Where(c => c.PRId == remarksdetail.PRId && c.TypeId == remarksdetail.TypeId && c.SupplierId == remarksdetail.SupplierId && c.ProductId == remarksdetail.ProductId).FirstOrDefault();
                var remarks_details = db.RemarksDetails.Where(c => c.RemarksId == remarksdetail.RemarksId).FirstOrDefault();

                if (remarks_details == null)
                {
                    remarks_details = new RemarksDetail(remarksdetail);
                    db.RemarksDetails.Add(remarks_details);
                }
                else
                {
                    remarks_details.Remarks = remarksdetail.Remarks;
                    remarks_details.SupplierId = remarksdetail.SupplierId;
                    remarks_details.ProductId = remarksdetail.ProductId;
                    remarks_details.RevisionDate = clsMain.getCurrentTime();
                    remarks_details.RevisionUserId = clsMain.getCurrentUser();
                    db.Entry(remarks_details).State = System.Data.Entity.EntityState.Modified;
                }
                
                db.SaveChanges();

                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Revision/RemarksUpdate
        [HttpGet, CAuthorize]
        public ActionResult BondEffectUpdate()
        {
            ////var type = new object[] { new { TypeId = 1, TypeName = "Requisiton" }, new { TypeId = 2, TypeName = "Purchase Order" }, new { TypeId = 3, TypeName = "Quantity Approval" } };
            //var type = new object[] { 
            //    new { TypeId = 1, TypeName = "Pending for Quantity Approval" }, 
            //    new { TypeId = 2, TypeName = "Pending for Price Approval" }, 
            //    new { TypeId = 3, TypeName = "Forward for LC" }, 
            //    new { TypeId = 4, TypeName = "Pending For LC Open" }, 
            //    new { TypeId = 5, TypeName = "LC Opened But Goods Not Received Yet" }
            //    //new { TypeId = 6, TypeName = "Pipeline" }
            //};
            //ViewBag.type = new SelectList(type, "TypeId", "TypeName");
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.PRList = new SelectList(db.PRMains.OrderBy(c => c.FactoryId).OrderByDescending(c => c.PRDate), "PRId", "PRNo");
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.ItemId == 0), "ItemId", "ItemName");
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(c => c.IsActive == 1 && c.SupplierId > 0 && c.ApprovedType == 1), "SupplierId", "SupplierName");

            return View();
        }

        // GET: Revision/StatusUpdate
        [CAuthorize]
        public ActionResult ETAUpdate()
        {
            //var type = new object[] { new { TypeId = 1, TypeName = "Requisiton" }, new { TypeId = 2, TypeName = "Purchase Order" }, new { TypeId = 3, TypeName = "Quantity Approval" } };
            var type = new object[] { 
                new { TypeId = 1, TypeName = "Pending For Qty Approval" }, 
                new { TypeId = 2, TypeName = "Pending For Price Approval" }, 
                new { TypeId = 3, TypeName = "Forward For LC Opening" }, 
                new { TypeId = 4, TypeName = "Pending For L/C" }, 
                new { TypeId = 5, TypeName = "L/C Open But Not Received" }
            };
            ViewBag.type = new SelectList(type, "TypeName", "TypeName", 1);
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");

            return View();
        }

        // POST: Revision/StatusUpdate
        [HttpPost, CAuthorize]
        public ActionResult ETAUpdate(ICollection<CompleteStatus> completeStatus)
        {
            try
            {
                foreach (var sub in completeStatus)
                {
                    var item = db.PRSubs.Find(sub.TranId);
                    if (item.FactoryETA != sub.FactoryETA.Value)
                    {
                        item.FactoryETA = sub.FactoryETA.Value;

                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                db.SaveChanges();

                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Revision/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Revision/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Revision/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Revision/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ItemList(int id, short facid, string update_type = "CANCEL", string req_status = "")
        {
            if (update_type == "CANCEL")
            {
                List<short> factorylist = new List<short>();

                if (facid == 0)
                    factorylist = dbProd.UserFactories.Select(c => c.FactoryId).ToList();
                else
                    factorylist.Add(facid);

                if (id == 1)
                {
                    ViewBag.Caption = "Requisition #";
                    var itemlist = db.PRSubs.Where(l => factorylist.Contains(l.PRMain.FactoryId) && l.IsPOComplete == 0 && (l.Quantity - l.PurchaseOrderSubs.Select(c => c.Quantity).DefaultIfEmpty(0).Sum()) > 0).Select(c => new { PRId = c.PRId, PRNo = c.PRMain.PRNo }).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else if (id == 2)
                {
                    ViewBag.Caption = "PO #";
                    ViewBag.itemList = new SelectList(db.PurchaseOrderSubs.Where(l => factorylist.Contains(l.PRSub.PRMain.FactoryId) && l.IsQAComplete == 0 && (l.Quantity - l.QuantityApprovalSubs.Select(c => c.ApprovedQuantity).DefaultIfEmpty(0).Sum()) > 0).Select(c => new { PRId = c.POId, PRNo = c.PurchaseOrder.PONo }).Distinct(), "PRId", "PRNo");
                }
                else if (id == 3)
                {
                    ViewBag.Caption = "Qty. Approval #";
                    var itemlist = (from p in db.QuantityApprovalSubs.AsEnumerable()
                                    //join q in dbFund.PISubs.Join(dbFund.LCSubs, i => i.PIFileSubId, j => j.PIFileSubId, (i, j) => new { PI = i, LC = j }).GroupBy(c => c.PI.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(l => l.LC.BookingQuantity) }).AsEnumerable() on p.QApprovalSubId equals q.QApprovalSubId into r
                                    join q in dbFund.PISubs.GroupBy(c => c.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(l => l.PIQuantity) }).AsEnumerable() on p.QApprovalSubId equals q.QApprovalSubId into r
                                    from s in r.DefaultIfEmpty()
                                    where factorylist.Contains(p.PRSub.PRMain.FactoryId) && p.IsPIComplete == 0 && p.ApprovedQuantity - (s == null ? 0 : s.PIQty) > 0
                                    select new { PRId = p.QApprovalId, PRNo = p.QuantityApprovalMain.QApprovalNo }
                                   ).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else if (id == 5)
                {
                    ViewBag.Caption = "Qty. Approval #";
                    var itemlist = db.PRSubs.Where(l => factorylist.Contains(l.PRMain.FactoryId) && l.IsPOComplete == 0 && (l.Quantity - l.PurchaseOrderSubs.Select(c => c.Quantity).DefaultIfEmpty(0).Sum()) > 0).Select(c => new { PRId = c.PRId, PRNo = c.PRMain.PRNo }).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else if (id == 6)
                {
                    ViewBag.Caption = "Price Approval #";
                    var itemlist = (from p in db.QuantityApprovalSubs.AsEnumerable()
                                    join q in db.PriceApprovalSubs.AsEnumerable() on p.QApprovalSubId equals q.QApprovalSubId into r
                                    from s in r.DefaultIfEmpty()
                                    where factorylist.Contains(p.PRSub.PRMain.FactoryId) && p.IsPIComplete == 0 && s == null
                                    select new { PRId = p.PRSub.PRId, PRNo = p.PRSub.PRMain.PRNo }
                                   ).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else if (id == 7)
                {
                    ViewBag.Caption = "PI";
                    var itemlist = (from p in db.PriceApprovalSubs.AsEnumerable()
                                    join q in db.QuantityApprovalSubs.AsEnumerable() on p.QApprovalSubId equals q.QApprovalSubId
                                    join t in dbFund.PISubs.GroupBy(c => c.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(l => l.PIQuantity) }).AsEnumerable() on q.QApprovalSubId equals t.QApprovalSubId into r
                                    from s in r.DefaultIfEmpty()
                                    where factorylist.Contains(q.PRSub.PRMain.FactoryId) && q.IsPIComplete == 0 && q.ApprovedQuantity - (s == null ? 0 : s.PIQty) > 0
                                    select new { PRId = p.PRSub.PRId, PRNo = p.PRSub.PRMain.PRNo }
                                   ).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else if (id == 8)
                {
                    ViewBag.Caption = "LC";
                    var itemlist = (from p in dbFund.PISubs.AsEnumerable()
                                    join t in db.PRSubs.AsEnumerable() on p.PRSubId equals t.PRSubId
                                    join q in dbFund.LCWisePIs.AsEnumerable() on p.PIFileId equals q.PIFileId into r
                                    from s in r.DefaultIfEmpty()
                                    where factorylist.Contains(t.PRMain.FactoryId) && s == null
                                    select new { PRId = t.PRId, PRNo = t.PRMain.PRNo }
                                   ).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else if (id == 9)
                {
                    ViewBag.Caption = "LOT";
                    var itemlist = (from p in dbFund.PISubs.AsEnumerable()
                                    join t in db.PRSubs.AsEnumerable() on p.PRSubId equals t.PRSubId
                                    join q in dbFund.LCWisePIs.AsEnumerable() on p.PIFileId equals q.PIFileId
                                    join u in db.PipelineSubs.GroupBy(c => c.PIFileSubId).Select(c => new { PIFileSubId = c.Key, RecQty = c.Sum(l => l.StockInTransit) }).AsEnumerable() on p.PIFileSubId equals u.PIFileSubId into r
                                    from s in r.DefaultIfEmpty()
                                    where factorylist.Contains(t.PRMain.FactoryId) && p.PIQuantity - (s == null ? 0 : s.RecQty) > 0
                                    select new { PRId = t.PRId, PRNo = t.PRMain.PRNo }
                                   ).Distinct();
                    ViewBag.itemList = new SelectList(itemlist, "PRId", "PRNo");
                }
                else
                {
                    ViewBag.Caption = "Requisition #";
                    //var include_list = (from p in db.PRSubs.AsEnumerable()
                    //                    join q in db.PurchaseOrderSubs.AsEnumerable() on p.PRSubId equals q.PRSubId into r
                    //                    from s in r.DefaultIfEmpty()
                    //                    where s != null || p.IsPOComplete == 0
                    //                    select p.PRId
                    //                   ).Distinct();
                    //var exclude_list = db.PRMains.Where(c => !include_list.Contains(c.PRId)).Select(c => c.PRId);
                    //var lc_list = db.PipelineMains.Where(c => c.IsLastLot == 1).Select(c => c.LCFileId).ToArray();
                    //var pi_list = dbFund.LCWisePIs.Where(c => lc_list.Contains(c.LCFileId)).Select(c => c.PIFileId).Distinct().ToArray();
                    //var exclude_list = (from p in db.PRSubs.Where(c => c.IsPOComplete == 1).AsEnumerable()
                    //                    join q in dbFund.PISubs.AsEnumerable() on p.PRSubId equals q.PRSubId
                    //                    where pi_list.Contains(q.PIFileId)
                    //                    select p.PRId
                    //                   ).Distinct().ToArray();
                    //var itemlist = (from p in db.PRSubs.Where(c => c.IsPOComplete == 0).AsEnumerable()
                    //                join pi in dbFund.PISubs.AsEnumerable() on p.PRSubId equals pi.PRSubId into pq //.GroupBy(c => c.PRSubId).Select(c => new { PRSubId = c.Key, PIQuantity = c.Sum(l => l.PIQuantity) }).AsEnumerable() on p.PRSubId equals pi.PRSubId into pq
                    //                from q in pq.DefaultIfEmpty()
                    //                join u in db.PipelineSubs.GroupBy(c => c.PIFileSubId).Select(c => new { PIFileSubId = c.Key, RecQty = c.Sum(l => l.StockInTransit) }).AsEnumerable() on q.PIFileSubId equals u.PIFileSubId into r
                    //                from s in r.DefaultIfEmpty()
                    //                where factorylist.Contains(p.PRMain.FactoryId) && ((q == null ? 10 : q.PIQuantity) - (s == null ? 0 : s.RecQty) > 0) && !pi_list.Contains((q == null ? -1 : q.PIMain.PIFileId))
                    //                select new { PRId = p.PRId, PRNo = p.PRMain.PRNo }
                    //               ).Distinct();
                    var itemlist = db.PRMains.Where(c => factorylist.Contains(c.FactoryId)); //&& !exclude_list.Contains(c.PRId));
                    ViewBag.itemList = new SelectList(itemlist.OrderByDescending(c => c.PRDate), "PRId", "PRNo");
                }
            } 
            else if (update_type == "ETA")
            {
                var items = db.Database.SqlQuery<RequisitionView>("Exec prcETAUpdateDetails '" + (facid == 0 ? "" : facid.ToString()) + "', '" + req_status + "', 0, " + clsMain.getCurrentUser().ToString()).ToList();

                ViewBag.itemList = new SelectList(items.Select(c => new { c.PRId, c.PRNo }).Distinct(), "PRId", "PRNo");
                ViewBag.Caption = "Requisition #";
            }

            return PartialView();
        }

        public ActionResult ItemDetails(int id, int type, string update_type = "CANCEL", string req_status = "")
        {
            ICollection<RequisitionView> reqSubs;

            if (update_type == "CANCEL")
            {
                ViewBag.type = type;

                if (type == 1)
                {
                    ViewBag.Caption = "Requisiton No";
                    reqSubs = (from p in db.PRSubs.AsEnumerable()
                               join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                               join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                               join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                               join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                               join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                               join v in db.PRMains.AsEnumerable() on p.PRId equals v.PRId
                               join uf in dbProd.UserFactories.AsEnumerable() on v.FactoryId equals uf.FactoryId
                               join w in db.PurchaseOrderSubs.GroupBy(c => c.PRSubId).Select(c => new { PRSubId = c.Key, POQty = c.Sum(l => l.Quantity) }).AsEnumerable() on p.PRSubId equals w.PRSubId into x
                               from y in x.DefaultIfEmpty()
                               where p.PRId == id && p.Quantity - (y == null ? 0 : y.POQty) > 0
                               select new { p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, v.PRNo, POQty = (y == null ? 0 : y.POQty) }
                                ).Select(c => new RequisitionView()
                                {
                                    PRId = c.p.PRId,
                                    PRSubId = c.p.PRSubId,
                                    PRNo = c.p.PRMain.PRNo,
                                    PRDate = c.p.PRMain.PRDate,
                                    ProductId = c.p.ProductId,
                                    CurrentStock = c.p.CurrentStock,
                                    StockInTransit = c.p.StockInTransit,
                                    PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                    HistoricalConsumption = c.p.HistoricalConsumption,
                                    HistoricalPeriod = c.p.HistoricalPeriod,
                                    Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                    Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                    AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                    LeadTime = c.p.LeadTime,
                                    FactoryETA = c.p.FactoryETA,
                                    BrandId = c.p.BrandId,
                                    BrandName = c.BrandName,
                                    PreferredSupplierId = c.p.PreferredSupplierId,
                                    SupplierName = c.SupplierName,
                                    Quantity = c.p.Quantity,
                                    QuantityUnit = c.QuantityUnit,
                                    QuantityUnitId = c.p.QuantityUnitId,
                                    POQuantity = c.POQty,
                                    CategoryId = c.CategoryId,
                                    CategoryName = c.CategoryName,
                                    ProductName = c.ItemName,
                                    RowNo = c.p.RowNo,
                                    Remarks = c.p.Remarks
                                }).ToList();
                }
                else if (type == 2)
                {
                    ViewBag.Caption = "PO No";
                    reqSubs = (from por in db.PurchaseOrders.AsEnumerable()
                               join po in db.PurchaseOrderSubs.AsEnumerable() on por.POId equals po.POId
                               join p in db.PRSubs.AsEnumerable() on po.PRSubId equals p.PRSubId
                               join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                               join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                               join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                               join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                               join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                               join v in db.PRMains.AsEnumerable() on p.PRId equals v.PRId
                               join uf in dbProd.UserFactories.AsEnumerable() on v.FactoryId equals uf.FactoryId
                               join w in db.QuantityApprovalSubs.GroupBy(c => c.POSubId).Select(c => new { POSubId = c.Key, AppQty = c.Sum(l => l.ApprovedQuantity) }).AsEnumerable() on po.POSubId equals w.POSubId into x
                               from y in x.DefaultIfEmpty()
                               where por.POId == id && po.Quantity - (y == null ? 0 : y.AppQty) > 0
                               select new { po, p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, v.PRNo, AppQty = (y == null ? 0 : y.AppQty) }
                                ).Select(c => new RequisitionView()
                                {
                                    POId = c.po.POId,
                                    POSubId = c.po.POSubId,
                                    PRId = c.p.PRId,
                                    PRSubId = c.p.PRSubId,
                                    PRNo = c.p.PRMain.PRNo,
                                    PRDate = c.p.PRMain.PRDate,
                                    PONo = c.po.PurchaseOrder.PONo,
                                    PODate = c.po.PurchaseOrder.PODate,
                                    ProductId = c.p.ProductId,
                                    CurrentStock = c.p.CurrentStock,
                                    StockInTransit = c.p.StockInTransit,
                                    PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                    HistoricalConsumption = c.p.HistoricalConsumption,
                                    HistoricalPeriod = c.p.HistoricalPeriod,
                                    Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                    Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                    AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                    LeadTime = c.p.LeadTime,
                                    FactoryETA = c.p.FactoryETA,
                                    BrandId = c.p.BrandId,
                                    BrandName = c.BrandName,
                                    PreferredSupplierId = c.p.PreferredSupplierId,
                                    SupplierName = c.SupplierName,
                                    Quantity = c.p.Quantity,
                                    QuantityUnit = c.QuantityUnit,
                                    QuantityUnitId = c.p.QuantityUnitId,
                                    POQuantity = c.po.Quantity,
                                    ApprovedQuantity = c.AppQty,
                                    CategoryId = c.CategoryId,
                                    CategoryName = c.CategoryName,
                                    ProductName = c.ItemName,
                                    RowNo = c.p.RowNo,
                                    Remarks = c.p.Remarks
                                }).ToList();
                }
                else if (type == 3)
                {
                    ViewBag.Caption = "Qty. Approval No";
                    var itemlist = (from p in db.QuantityApprovalSubs.AsEnumerable()
                                    join q in dbFund.PISubs.GroupBy(c => c.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(l => l.PIQuantity) }).AsEnumerable() on p.QApprovalSubId equals q.QApprovalSubId into r
                                    from s in r.DefaultIfEmpty()
                                    where p.IsPIComplete == 0 && p.ApprovedQuantity - (s == null ? 0 : s.PIQty) > 0
                                    select new { PRId = p.QApprovalId, PRNo = p.QuantityApprovalMain.QApprovalNo }
                                   ).Distinct();
                    reqSubs = (from o in db.QuantityApprovalMains.Find(id).QuantityApprovalSubs.AsEnumerable()
                               join po in db.PurchaseOrderSubs.AsEnumerable() on o.POSubId equals po.POSubId
                               join p in db.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                               join uf in dbProd.UserFactories.AsEnumerable() on p.PRMain.FactoryId equals uf.FactoryId
                               join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                               join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                               join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                               join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                               join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                               //join xa in dbFund.PISubs.Join(dbFund.LCSubs, i => i.PIFileSubId, j => j.PIFileSubId, (i, j) => new { PI = i, LC = j }).GroupBy(c => c.PI.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(l => l.LC.BookingQuantity) }).AsEnumerable() on o.QApprovalSubId equals xa.QApprovalSubId into xb
                               join xa in dbFund.PISubs.GroupBy(c => c.QApprovalSubId).Select(c => new { QApprovalSubId = c.Key, PIQty = c.Sum(l => l.PIQuantity) }).AsEnumerable() on o.QApprovalSubId equals xa.QApprovalSubId into xb
                               //join xa in dbFund.PISubs.AsEnumerable() on o.QApprovalSubId equals xa.QApprovalSubId into xb
                               from y in xb.DefaultIfEmpty()
                               where o.IsPIComplete == 0 && o.ApprovedQuantity - (y == null ? 0 : y.PIQty) > 0
                               select new { po, p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o, PIQty = (y == null ? 0 : y.PIQty) }
                                ).Select(c => new RequisitionView()
                                {
                                    POId = c.po.POId,
                                    POSubId = c.po.POSubId,
                                    QApprovalId = c.o.QApprovalId,
                                    QApprovalSubId = c.o.QApprovalSubId,
                                    PRId = c.p.PRId,
                                    PRSubId = c.p.PRSubId,
                                    PRNo = c.p.PRMain.PRNo,
                                    PRDate = c.p.PRMain.PRDate,
                                    PONo = c.po.PurchaseOrder.PONo,
                                    PODate = c.po.PurchaseOrder.PODate,
                                    QANo = c.o.QuantityApprovalMain.QApprovalNo,
                                    QADate = c.o.QuantityApprovalMain.QApprovalDate,
                                    ProductId = c.p.ProductId,
                                    CurrentStock = c.p.CurrentStock,
                                    StockInTransit = c.p.StockInTransit,
                                    PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                    HistoricalConsumption = c.p.HistoricalConsumption,
                                    HistoricalPeriod = c.p.HistoricalPeriod,
                                    Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                    Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                    AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                    LeadTime = c.p.LeadTime,
                                    FactoryETA = c.p.FactoryETA,
                                    BrandId = c.p.BrandId,
                                    BrandName = c.BrandName,
                                    PreferredSupplierId = c.p.PreferredSupplierId,
                                    SupplierName = c.SupplierName,
                                    Quantity = c.p.Quantity,
                                    QuantityUnit = c.QuantityUnit,
                                    QuantityUnitId = c.p.QuantityUnitId,
                                    POQuantity = c.po.Quantity,
                                    ApprovedQuantity = c.o.ApprovedQuantity,
                                    PIQuantity = c.PIQty,
                                    CategoryId = c.CategoryId,
                                    CategoryName = c.CategoryName,
                                    ProductName = c.ItemName,
                                    RowNo = c.p.RowNo,
                                    Remarks = c.p.Remarks
                                }).ToList();
                }
                else
                    reqSubs = null;
            }
            else if (update_type == "ETA")
            {
                reqSubs = db.Database.SqlQuery<RequisitionView>("Exec prcETAUpdateDetails '', '" + req_status + "', " + id.ToString()).ToList();

                return PartialView("ItemDetails_ETA", reqSubs);
            }
            else
                reqSubs = null;

            return PartialView(reqSubs);
        }

        // POST: Revision/RemarksUpdate
        [HttpPost]
        public ActionResult GetRemarks(int id, int type, short prod_id = 0, short sup_id = 0)
        {
            try
            {
                string remarks = ""; int remarks_id = 0;
                var remarks_details = db.RemarksDetails.Where(c => c.PRId == id && c.TypeId == type && c.ProductId == prod_id && c.SupplierId == sup_id).FirstOrDefault();

                if (remarks_details != null)
                {
                    remarks_id = remarks_details.RemarksId;
                    remarks = remarks_details.Remarks;
                }

                return Json(new { id = remarks_id, remarks = remarks }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: Revision/RemarksUpdate
        [HttpPost]
        public ActionResult ProductDetails(int id)
        {
            try
            {
                var sub_ids = db.PRMains.Find(id).PRSubs.Select(c => c.ProductId).Distinct();
                ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => sub_ids.Contains(c.ItemId)), "ItemId", "ItemName");
                
                return PartialView();
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
