﻿using InventoryMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryMS.Controllers.Inventory
{
    public class MenuController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();

        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }

        // GET: Menu/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Menu/Create
        public ActionResult Create()
        {
            ViewBag.MenuPId = new SelectList(db.MenuDetails.Select(c => new { c.MenuId, MenuTitle = c.MenuCode + " >> " + c.MenuTitle }), "MenuId", "MenuTitle");
            ViewBag.MenuTypeId = new SelectList(db.MenuTypes, "MenuTypeId", "MenuTypeName");
            ViewBag.AControllerId = new SelectList(db.User_Controller, "ControllerId", "ControllerName");
            ViewBag.DefaultActionId = new SelectList(db.User_Action.Where(l => l.ActionId == 0), "ActionId", "ActionName");
            ViewBag.Parameter = new SelectList(db.User_Action.Where(l => l.ActionId == 0), "ActionName", "ActionName");
            ViewBag.Role = new MultiSelectList(db.User_Role, "RoleId", "RoleName");

            return View();
        }

        // POST: Menu/Save
        [HttpPost]
        public ActionResult Save(ICollection<MenuDetail> menuDetail)
        {
            try
            {
                db.MenuDetails.AddRange(menuDetail);

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Menu/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Menu/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Menu/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Menu/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult MenuTracking()
        {
            var mlist = db.MenuDetails;

            return View(mlist);
        }

        public ActionResult MenuTrackingSave(ICollection<MenuWiseAction> menuWiseAction)
        {
            try
            {
                var urds = db.MenuWiseActions.ToList();

                foreach (var action in menuWiseAction)
                {
                    if (urds.Where(l => l.ActionId == action.ActionId && l.MenuId == action.MenuId).Count() > 0)
                    {
                        var urd = urds.Where(l => l.ActionId == action.ActionId && l.MenuId == action.MenuId).First();
                        db.Entry(urd).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        MenuWiseAction urd = new MenuWiseAction(action);
                        db.MenuWiseActions.Add(urd);
                    }
                }

                for (int ri = 0; ri < urds.Count; ri++)
                {
                    if (db.Entry(urds[ri]).State == EntityState.Unchanged)
                        db.Entry(urds[ri]).State = EntityState.Deleted;
                }

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MenuDefaultAction()
        {
            var mlist = db.MenuDetails;

            return View(mlist);
        }

        public ActionResult MenuDefaultActionSave(ICollection<MenuDetail> menuDetail)
        {
            try
            {
                foreach (var md in menuDetail)
                {
                    var menu = db.MenuDetails.Find(md.MenuId);
                    menu.DefaultActionId = md.DefaultActionId;
                    menu.DefaultParameter = (md.DefaultParameter == null ? "" : md.DefaultParameter);
                    menu.DefaultParameterValue = (md.DefaultParameterValue == null ? "" : md.DefaultParameterValue);

                    db.Entry(menu).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ParamList(short id, short mid)
        {
            try
            {
                var plist = db.User_Action.Find(id).ParameterList;

                string pvlist = "";
                if (db.MenuDetails.Find(mid).DefaultActionId == id)
                {
                    pvlist = db.MenuDetails.Find(mid).DefaultParameterValue;
                }

                return Json(new { status = "Success", plist = plist, pvlist = pvlist }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActionList(short id)
        {
            ViewBag.DefaultActionId = new SelectList(db.User_Action.Where(l => l.ControllerId == id), "ActionName", "ActionName");

            return PartialView();
        }

        public ActionResult ActionWiseParam(short id)
        {
            var plist = db.User_Action.Find(id).ParameterList.Replace(", ", ",").Split(',');

            if (plist != null)
                ViewBag.ParamList = new SelectList(plist.Select(c => new { name = c }), "name", "name");
            else
                ViewBag.Parameter = new SelectList(db.User_Action.Where(l => l.ActionId == 0), "ActionName", "ActionName");

            return PartialView();
        }
    }
}
