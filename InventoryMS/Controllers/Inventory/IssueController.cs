﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class IssueController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private SDMSEntities dbSDMS = new SDMSEntities();

        // GET: /Issue/
        public ActionResult Index()
        {
            return View(db.IssueMains.ToList());
        }

        // GET: /Issue/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueMain issueMain = db.IssueMains.Find(id);
            if (issueMain == null)
            {
                return HttpNotFound();
            }
            return View(issueMain);
        }

        // GET: /Issue/Create
        [CAuthorize]
        public ActionResult Create(string id)
        {
            IssueMain issueMain = new IssueMain();
            issueMain.IssueDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            //ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName");
            ViewBag.LC = new SelectList(dbFund.LCMains.Where(l => l.LCFileId == 0), "LCFileId", "LCNo");
            ViewBag.Store = new SelectList(db.Warehouses.Where(c => c.IsActive == 1).ToList(), "WarehouseId", "WarehouseName");
            ViewBag.IssueTo = new SelectList(db.Warehouses.Where(c => c.WarehouseId == 0).ToList(), "WarehouseId", "WarehouseName");
            ViewBag.SourceType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SOURCETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName", 46);
            ViewBag.IssueType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("ISSUETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.SRRId = new SelectList(db.SRRMains.Where(c => c.FactoryId == 0).ToList(), "SRRId", "SRRNo");
            ViewBag.IssueNo = "###/####/0000-" + DateTime.Now.ToString("MM/yy");

            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");

            if (id != "paper")
            {
                return View(issueMain);
            }
            else
            {
                return View("Create_Paper", issueMain);
            }
        }

        // GET: /Issue/Create
        [CAuthorize]
        public ActionResult Paper(string id)
        {
            IssueMain issueMain = new IssueMain();
            issueMain.IssueDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories.Where(c => c.FactoryId == 1 || c.FactoryId == 5), "FactoryId", "FactoryName");
            //ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName");
            //ViewBag.LC = new SelectList(dbFund.LCMains.Where(l => l.LCFileId == 0), "LCFileId", "LCNo");
            ViewBag.Store = new SelectList(db.Warehouses.Where(c => c.IsActive == 1).ToList(), "WarehouseId", "WarehouseName");
            ViewBag.IssueTo = new SelectList(db.Warehouses.Where(c => c.WarehouseId == 0).ToList(), "WarehouseId", "WarehouseName");
            ViewBag.SourceType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SOURCETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName", 46);
            ViewBag.IssueType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("ISSUETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.SRRId = new SelectList(db.SRRMains.Where(c => c.FactoryId == 0).ToList(), "SRRId", "SRRNo");
            ViewBag.IssueNo = "###/####/0000-" + DateTime.Now.ToString("MM/yy");

            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            return View(issueMain);
        }

        // POST: /Issue/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(IssueMainView mainView, ICollection<IssueSubView> subViews,string type= "RMI")
        {
            try
            {
                //if (db.SRRMains.Where(l => l.SRRNo.Equals(srrView.SRRNo) && l.FactoryId == srrView.FactoryId).Count() > 0)
                //    return Content("Duplicate SRR No.");

                IssueMain main = new IssueMain(mainView);
                main.IssueDate = DateTime.Parse(main.IssueDate.ToString("dd-MMM-yyyy") + " " + clsMain.getCurrentTime().ToString("HH:mm:ss"));
                var product_list = subViews.Select(c => c.ProductId.ToString()).Aggregate((i, j) => i + ", " + j);

                main.ApprovedType = 1;
                main.ApprovedTime = main.IssueDate;
                main.ApprovedBy = 6;

               /* string srrno = type + "/" + dbProd.Factories.Find(id).Prefix + "/" + db.SRRMains.Where(l => l.Year == srrdate.Value.Year && l.FactoryId == id && l.SRRNo.StartsWith(type)).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + srrdate.Value.ToString("MM/yy").ToUpperInvariant();

                return Json(new { srrno = srrno }, JsonRequestBehavior.AllowGet);*/

              /*  main.IssueNo = mainView.IssueNo.Substring(0, 3) + "/" + dbProd.Factories.Find(mainView.FactoryId).Prefix + "/" + db.IssueMains.Where(l => l.Year == mainView.IssueDate.Year && l.FactoryId == mainView.FactoryId && l.IssueNo.StartsWith(mainView.IssueNo.Substring(0, 3))).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + mainView.IssueDate.ToString("MM/yy").ToUpperInvariant();
*/
                main.IssueNo = type + "/" + dbProd.Factories.Find(mainView.FactoryId).Prefix + "/" + db.IssueMains.Where(l => l.Year == mainView.IssueDate.Year && l.FactoryId == mainView.FactoryId && l.IssueNo.StartsWith(type)).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + mainView.IssueDate.ToString("MM/yy").ToUpperInvariant();




                if (subViews != null)
                {
                    foreach (IssueSubView _sub in subViews)
                    {
                        var srr_sub = db.SRRSubs.Find(_sub.SRRSubId);
                        IssueSub sub = new IssueSub(_sub);
                        sub.SRRProductId = srr_sub.ProductId;

                        var _rate = db.Database.SqlQuery<Decimal>("Select dbo.fncGetItemAveragePrice(" + _sub.ProductId.ToString() + ", " + mainView.FactoryId.ToString() + ", getdate())").FirstOrDefault();
                        sub.Rate = _rate;
                        if (main.IssueTypeId == 39 || main.IssueTypeId == 41 || main.IssueTypeId == 55)
                        {
                            sub.Status = 1;
                        }
                        else
                        {
                            sub.Status = 0;
                        }

                        var grr_list = (from p in db.GRRMains.AsEnumerable()
                                        join q in db.GRRSubs.AsEnumerable() on p.GRRId equals q.GRRId
                                        join r in db.IssueAgainstLCs.GroupBy(c => c.GRRSubId).Select(c => new { GRRSubId = c.Key, IssuedQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on q.GRRSubId equals r.GRRSubId into qr
                                        from s in qr.DefaultIfEmpty()
                                        join t in db.IssueReturnAgainstLCs.GroupBy(c => c.GRRSubId).Select(c => new { GRRSubId = c.Key, ReturnQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on q.GRRSubId equals t.GRRSubId into qt
                                        from u in qt.DefaultIfEmpty()
                                        where q.ProductId == sub.ProductId && p.LCFileId > 0 && q.GRRQuantity + (u == null ? 0 : u.ReturnQuantity) - (s == null ? 0 : s.IssuedQuantity) > 0
                                        select new { p.GRRDate, p.LCFileId, q.GRRSubId, NetQuantity = q.GRRQuantity + (u == null ? 0 : u.ReturnQuantity) - (s == null ? 0 : s.IssuedQuantity) }
                                       ).Distinct().OrderBy(c => c.GRRDate);
                        
                        var issue_qty = sub.Quantity;
                        short row_no = 1;

                        foreach (var item in grr_list)
                        {
                            IssueAgainstLC lc = new IssueAgainstLC();
                            lc.GRRSubId = item.GRRSubId;
                            lc.ProductId = sub.ProductId;
                            lc.LCFileId = item.LCFileId;
                            lc.FSCId = 0;
                            lc.RowNo = row_no++;
                            lc.Remarks = "";
                            
                            if (issue_qty > item.NetQuantity)
                            {
                                lc.Quantity = item.NetQuantity;
                                issue_qty -= item.NetQuantity;
                            }
                            else
                            {
                                lc.Quantity = issue_qty;
                                sub.IssueAgainstLCs.Add(lc);
                                break;
                            }

                            sub.IssueAgainstLCs.Add(lc);
                        }

                        main.IssueSubs.Add(new IssueSub(sub));
                    }
                }

                db.IssueMains.Add(main);
                db.SaveChanges();

                db.Database.ExecuteSqlCommand("Exec prcUpdateOnwardsRate 0, '" + main.IssueDate.ToString() + "', '" + product_list + "'");

                return Json(new { status = "Save successful.", issueno = main.IssueNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Issue/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueMain issueMain = db.IssueMains.Find(id);
            if (issueMain == null)
            {
                return HttpNotFound();
            }
            return View(issueMain);
        }

        // POST: /Issue/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IssueId,DepartmentId,IssueNo,IssueDate,Month,Year,SerialNo,Remarks,EntryUserId,EntryDate,RevisionUserId,RevisionDate,IPAddress,PCName")] IssueMain issueMain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issueMain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(issueMain);
        }

        // GET: /Issue/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueMain issueMain = db.IssueMains.Find(id);
            if (issueMain == null)
            {
                return HttpNotFound();
            }
            return View(issueMain);
        }

        // POST: /Issue/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IssueMain issueMain = db.IssueMains.Find(id);
            db.IssueMains.Remove(issueMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult SRRList(int id = 0, short type_id = 0, DateTime? issue_date = null)
        {
            var exception_list = new List<short>() { 39, 57 };
            var return_list = new List<short>() { 56, 57 };
            var srrlist = new List<SRRMain>();

            var issue_type_wise_srr_type = new List<TypeConverson>();
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 38, DeistinationTypeId = 59 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 39, DeistinationTypeId = 65 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 40, DeistinationTypeId = 63 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 41, DeistinationTypeId = 61 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 42, DeistinationTypeId = 64 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 55, DeistinationTypeId = 62 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 56, DeistinationTypeId = 60 });
            issue_type_wise_srr_type.Add(new TypeConverson() { SourceTypeId = 57, DeistinationTypeId = 62 });

            srrlist = (from p in db.SRRMains.AsEnumerable()
                       join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                       join iws in issue_type_wise_srr_type.AsEnumerable() on p.SRRTypeId equals iws.DeistinationTypeId
                       join r in db.IssueSubs.GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, IssuedQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on q.SRRSubId equals r.SRRSubId into qr
                       from s in qr.DefaultIfEmpty()
                       join t in db.GRRSubs.Where(c => c.SRRSubId > 0).GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, ReceivedQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on q.SRRSubId equals t.SRRSubId into tu
                       from u in tu.DefaultIfEmpty()
                       where (exception_list.Contains(type_id) ? p.ReferenceId : p.FactoryId) == id 
                                && p.ApprovedType == 1 && iws.SourceTypeId == type_id 
                                && p.SRRSDate <= issue_date
                                && (return_list.Contains(type_id) ? (u == null ? 0 : u.ReceivedQuantity) : q.SRRQuantity) - (s == null ? 0 : s.IssuedQuantity) > 0
                       select p
                       ).Distinct().ToList();
            ViewBag.SRRId = new SelectList(srrlist.OrderBy(c => c.SRRNo), "SRRId", "SRRNo");

            return PartialView();
        }

        // GET: LC
        public ActionResult SRRDetails(List<int> id, short type_id = 0)
        {
            var ref_ids = id; //.Select(c => int.Parse(c)).ToList();
            try
            {
                var ref_list = db.SRRMains.Where(c => ref_ids.Contains(c.SRRId)).Select(c => c.ReferenceId).ToList();
                List<string> issue_to = null;
                
                if (type_id == 38)
                {
                    issue_to = db.Warehouses.Where(c => ref_list.Contains(c.WarehouseId)).Select(c => c.WarehouseName).ToList();
                }
                else if (type_id == 39 || type_id == 55)
                {
                    ref_list = db.SRRMains.Where(c => ref_ids.Contains(c.SRRId)).Select(c => c.FactoryId).ToList();
                    issue_to = dbProd.Factories.Where(c => ref_list.Contains(c.FactoryId)).Select(c => c.FactoryName).ToList();
                }
                else if (type_id == 40)
                {
                    issue_to = db.Payers.Where(c => ref_list.Contains(c.PayerId)).Select(c => c.PayerName).ToList();
                }
                else if (type_id == 41)
                {
                    issue_to = db.Payers.Where(c => ref_list.Contains(c.PayerId)).Select(c => c.PayerName).ToList();
                }
                else if (type_id == 42)
                {
                    issue_to = dbSDMS.CustomerGenInfoes.Where(c => ref_list.Contains(c.CustomerId)).Select(c => c.CustomerName).ToList();
                }
                else if (type_id == 33)
                {
                    issue_to = db.Payers.Where(c => ref_list.Contains(c.PayerId)).Select(c => c.PayerName).ToList();
                }

                if (ref_list == null && issue_to == null)
                    throw new Exception("Invalid requisiton no.");



                return Json(new { issue_to = issue_to.FirstOrDefault() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("Error -> ", ex);
            }
        }

        // GET: LC
        public ActionResult SRRProductDetails(int id, short issue_type_id, string issue_for)
        {
            try
            {
                var return_list = new List<short>() { 56, 57 };
                var srr = db.SRRMains.Find(id);
                var facid = srr.FactoryId;
                if (srr.SRRTypeId == 65 || srr.SRRTypeId == 62)
                {
                    facid = srr.ReferenceId;
                }

                var stock1 = (from p in dbProd.ItemInfoes.AsEnumerable()
                              join ss in db.SRRSubs.Where(c => c.SRRId == id).AsEnumerable() on p.ItemId equals ss.ProductId
                              join q in db.GRRSubs.Where(c => c.GRRMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, PurchaseQty = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on p.ItemId equals q.ProductId into r
                              from s in r.DefaultIfEmpty()
                              join t in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, IssueQty = c.Sum(l => l.Quantity) }).AsEnumerable() on p.ItemId equals t.ProductId into u
                              from v in u.DefaultIfEmpty()
                              join w in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQty = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on p.ItemId equals w.ProductId into x
                              from y in x.DefaultIfEmpty()
                              join xa in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQty = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on p.ItemId equals xa.ProductId into xb
                              from z in xb.DefaultIfEmpty()
                              //where p.ItemId == id
                              select new
                              {
                                  ProductId = p.ItemId,
                                  StockQty = (s == null ? 0 : s.PurchaseQty) - (v == null ? 0 : v.IssueQty) + (y == null ? 0 : y.ReturnQty),
                                  FloorStock = (v == null ? 0 : v.IssueQty) - (z == null ? 0 : z.ConsumeQty) - (y == null ? 0 : y.ReturnQty)
                              }
                             ).Distinct();

                var items = (from p in db.SRRSubs.AsEnumerable()
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                             join r in dbProd.viewQuantityUnits.AsEnumerable() on p.UnitId equals r.QuantityUnitId
                             join s in db.IssueSubs.GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, IssueQuantity = c.Sum(l => l.Quantity) }).AsEnumerable() on p.SRRSubId equals s.SRRSubId into xz
                             from t in xz.DefaultIfEmpty()
                             join u in stock1.AsEnumerable() on p.ProductId equals u.ProductId into v
                             from x in v.DefaultIfEmpty()
                             join gt in db.GRRSubs.Where(c => c.SRRSubId > 0).GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, ReceivedQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on p.SRRSubId equals gt.SRRSubId into gtu
                             from gu in gtu.DefaultIfEmpty()
                             where p.SRRId == id //&& x.FactoryId == facid
                             select new { p, q, r, t, x, gu }
                            ).Select(c => new SRRSubView()
                            {
                                 CategoryId = c.q.CategoryId,
                                 ProductId = c.q.ItemId,
                                 ProductName = c.q.ItemName,
                                 SRRQuantity = c.p.SRRQuantity,
                                 QuantityUnit = c.r.QuantityUnit,
                                 QuantityUnitId = c.r.QuantityUnitId,
                                 NetQuantity = (return_list.Contains(issue_type_id) ? (c.gu == null ? 0 : c.gu.ReceivedQuantity) : c.p.SRRQuantity) - (c.t == null ? 0 : c.t.IssueQuantity),
                                 CurrentStock1 = (c.x == null ? 0 : c.x.StockQty),
                                 FloorStock = (c.x == null ? 0 : c.x.FloorStock),
                                 SRRId = c.p.SRRId,
                                 SRRSubId = c.p.SRRSubId,
                                 RowNo = c.p.RowNo
                            }).Where(c => c.NetQuantity > 0).ToList();

                //var _subs = db.Database.SqlQuery<GRRSubView>("Exec prcGetSelectListItemDetails " + id + ", 'ITEMPRICE'").Distinct();
                //ViewBag.lotdate = db.PipelineMains.Find(id).PortArivalDate.ToString("dd-MMM-yyyy");
                ViewBag.issue_for = issue_for;

                var item_list = (from p in dbProd.ItemCategories.AsEnumerable()
                                 join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                                 join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                                 where p.StageLevel == 4 && r.FactoryId == srr.FactoryId && q.ItemCode.Substring(0, 2) == "01"
                                 select q
                                ).Distinct();

                if (srr.SRRTypeId == 65)
                {
                    item_list = item_list.Intersect(
                        (from p in dbProd.ItemCategories.AsEnumerable()
                         join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                         join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                         where p.StageLevel == 4 && r.FactoryId == srr.ReferenceId && q.ItemCode.Substring(0, 2) == "01"
                         select q
                        ).Distinct()
                    ).Distinct();
                }

                var cat_list = item_list.Select(c => new { c.ItemCategory.CategoryId, c.ItemCategory.CategoryName }).Distinct();

                ViewBag.Category = new SelectList(cat_list, "CategoryId", "CategoryName");
                ViewBag.item_list = item_list.OrderBy(c => c.ItemName).ThenBy(c => c.ItemName.Length);

                if (items == null)
                    throw new Exception("Invalid requisiton no.");

                return PartialView("RowTemplate", items);
            }
            catch (Exception ex)
            {
                throw new Exception("Error -> ", ex);
            }
        }

        public ActionResult ProductDetails(int id, short facid, short? type, short? facid2)
        {
            try
            {
                var product = (from p in dbProd.ItemInfoes.AsEnumerable()
                               join q in dbProd.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals q.QuantityUnitId
                               where p.ItemId == id
                               select new { q.QuantityUnitId, q.QuantityUnit }
                              ).FirstOrDefault();

                var stock1 = (from p in dbProd.ItemInfoes.AsEnumerable()
                              join q in db.GRRSubs.Where(c => c.GRRMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, PurchaseQty = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on p.ItemId equals q.ProductId into r
                              from s in r.DefaultIfEmpty()
                              join t in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, IssueQty = c.Sum(l => l.Quantity), FloorIssueQty = c.Sum(l => l.IssueMain.IssueTypeId == 38 ? l.Quantity : 0) }).AsEnumerable() on p.ItemId equals t.ProductId into u
                              from v in u.DefaultIfEmpty()
                              join w in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQty = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on p.ItemId equals w.ProductId into x
                              from y in x.DefaultIfEmpty()
                              join xa in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQty = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on p.ItemId equals xa.ProductId into xb
                              from z in xb.DefaultIfEmpty()
                              where p.ItemId == id
                              select new
                              {
                                  StockQty = (s == null ? 0 : s.PurchaseQty) - (v == null ? 0 : v.IssueQty) + (y == null ? 0 : y.ReturnQty),
                                  FloorStock = (v == null ? 0 : v.FloorIssueQty) - (z == null ? 0 : z.ConsumeQty) - (y == null ? 0 : y.ReturnQty)
                              }
                             ).Distinct().FirstOrDefault();

                return Json(new { unitid = product.QuantityUnitId, unit = product.QuantityUnit, stock1_qty = (stock1.StockQty).ToString("n2"), floor_stock1 = stock1.FloorStock.ToString("n2") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult RowTemplate(int? id)
        {
            ICollection<GRRSubView> grrSubs = new List<GRRSubView>();

            if (id != null)
            {
                grrSubs = (from p in db.GRRSubs.Where(l => l.GRRId == id).AsEnumerable()
                           join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                           join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                           join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                           join w in dbProd.CustomTypes.AsEnumerable() on p.CurrencyId equals w.TypeId
                           select new { p, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, CurrencyCode = w.TypeName }
                          ).Select(c => new GRRSubView()
                          {
                            CategoryId = c.CategoryId,
                            ProductId = c.p.ProductId,
                            ProductName = c.ItemName,
                            PipelineQuantity = c.p.PipelineQuantity,
                            QuantityUnit = c.QuantityUnit,
                            QuantityUnitId = c.p.QuantityUnitId,
                            GRRQuantity = c.p.GRRQuantity,
                            //ImportPriceFC = c.p.ImportPriceFC,
                            //CurrencyId = c.p.CurrencyId,
                            //CurrencyCode = c.CurrencyCode,
                            //LandedPriceBDT = c.p.LandedPriceBDT,
                            RowNo = c.p.RowNo,
                            Remarks = c.p.Remarks
                          }).ToList();
            }

            return PartialView(grrSubs);
        }
    }
}
