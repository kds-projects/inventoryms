﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers
{
    public class SupplierEvaluationsController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private EMailCubeEntities dbEmail = new EMailCubeEntities();

        // GET: /SupplierEvaluations/
        public ActionResult Index()
        {
            return View(db.SupplierEvaluations.ToList());
        }

        // GET: /SupplierEvaluations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupplierEvaluation supplierEvaluation = db.SupplierEvaluations.Find(id);
            if (supplierEvaluation == null)
            {
                return HttpNotFound();
            }
            return View(supplierEvaluation);
        }

        // GET: /SupplierEvaluations/Create
        public ActionResult Create()
        {
            
            var LCNos = (from p in dbFund.LCMains.AsEnumerable()
                         join q in db.SupplierEvaluations on p.LCFileId equals q.LCFileId into ab
                         from c in ab.DefaultIfEmpty()
                         where c == null
                         select new { LCFileId = p.LCFileId, LCNo = p.LCNo }

                     ).Distinct().ToList();



            //var exceptBanned = from c in ccList
            //                   join b in bannedCCList
            //                     on c.Field<string>("Country") equals b.Field<string>("Country") into j
            //                   from x in j.DefaultIfEmpty()
            //                   where x == null
            //                   select c;


            ViewBag.LCFile = new SelectList(LCNos, "LCFileId", "LCNo");
            return View();



        }

        public JsonResult GetLcInfo(string LCFileId)
        {
          

            var LCinfo = (from p in dbFund.LCMains.AsEnumerable()
                                  join q in db.Suppliers.AsEnumerable() on p.SupplierId equals q.SupplierId
                                  join r in dbProd.Companies.AsEnumerable() on p.BeneficiaryId equals r.CompanyId
                                  join c in db.Countries.AsEnumerable() on q.OriginCountryId equals c.CountryId
                                  join m in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals m.LCFileId
                                  //join j in dbFund.PISubs.AsEnumerable() on j.PIFileSubId equals m.PIFileSubId
                          //join s in db.PriceApprovalSubs.AsEnumerable() on r.PApprovalSubId equals s.PApprovalSubId
                          //join t in db.PipelineMains.AsEnumerable() on p.LCFileId equals t.LCFileId
                          where p.LCFileId == Convert.ToInt32(LCFileId)
                                  select new { CountryName= c.CountryName, SupplierName = q.SupplierName, LCNo = p.LCNo, LCDate = p.LCDate, CompanyName = r.CompanyName }
                     //).Union(
                     //from p in dbLC.LCMains.AsEnumerable()
                     //join t in db.PipelineMains.AsEnumerable() on p.LCFileId equals t.LCFileId
                     //select new { LCFileId = p.LCFileId, LCNo = p.LCNo }
                     ).Distinct().ToList();


            return Json(new { success = true, LCinfo }, JsonRequestBehavior.AllowGet);


        }




        // POST: /SupplierEvaluations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="SupplierRatingId,LCFileId,RatingId")] SupplierEvaluation supplierEvaluation)
        {
            if (ModelState.IsValid)
            {
                db.SupplierEvaluations.Add(supplierEvaluation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(supplierEvaluation);
        }


        public ActionResult Save( ICollection<SupplierEvaluation> supEval)
        {



            try
            {
                List<SupplierEvaluation> m = db.SupplierEvaluations.Where(c => c.LCFileId == 1).ToList();
                if (m.Count > 0)
                {
                    return Json(new { status = "Error", message = "Supplier Evulation Already exist" }, JsonRequestBehavior.AllowGet);
                }

            

               
                    if (supEval != null)
                    {
                    foreach (SupplierEvaluation _sub in supEval)
                    {
                        if (_sub.LCFileId != 0)
                        {
                            SupplierEvaluation sub = new SupplierEvaluation(_sub);
                            sub.LCFileId = _sub.LCFileId;
                            sub.RatingId = _sub.RatingId;
                            db.SupplierEvaluations.Add(sub);

                        }

                    }                   
                    
                }




                db.SaveChanges();

                return Json(new { status = "Success", name = "Supplier Rating Save Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }

        }




        // GET: /SupplierEvaluations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupplierEvaluation supplierEvaluation = db.SupplierEvaluations.Find(id);
            if (supplierEvaluation == null)
            {
                return HttpNotFound();
            }
            return View(supplierEvaluation);
        }

        // POST: /SupplierEvaluations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="SupplierRatingId,LCFileId,RatingId")] SupplierEvaluation supplierEvaluation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(supplierEvaluation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(supplierEvaluation);
        }

        // GET: /SupplierEvaluations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupplierEvaluation supplierEvaluation = db.SupplierEvaluations.Find(id);
            if (supplierEvaluation == null)
            {
                return HttpNotFound();
            }
            return View(supplierEvaluation);
        }

        // POST: /SupplierEvaluations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SupplierEvaluation supplierEvaluation = db.SupplierEvaluations.Find(id);
            db.SupplierEvaluations.Remove(supplierEvaluation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult DetailsTemplateAppData()
        {
            List<string> foo = new List<string>() { "Quality", "Price", "Delivery", "Performance" };
            var survay = db.Custom_Type.Where(v => foo.Contains(v.Flag));

            //var survay = (from p in db.Custom_Type
            //              where (v => foo.Contains(v.Status))

            //              //Where(v => foo.Contains(v.Status))

            //              select p

            //         ).Distinct().ToList();


            return PartialView(survay.ToList());


        }



    }
}
