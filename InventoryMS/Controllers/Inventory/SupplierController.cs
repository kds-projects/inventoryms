﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class SupplierController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private EMailCubeEntities dbEM = new EMailCubeEntities();

        // GET: /Supplier/
        [CAuthorize]
        public ActionResult Index()
        {
            var suppliers = (from p in db.Suppliers.Where(l => l.SupplierId > 0).AsEnumerable()
                         select p).Select(c => new SupplierView()
                         {
                             SupplierId = c.SupplierId,
                             SupplierCode = c.SupplierCode,
                             SupplierName = c.SupplierName,
                             ShortName = c.ShortName,
                             TINNo = c.TINNo,
                             VATNo = c.VATNo,
                             BINNo = c.BINNo,
                             IsActive = c.IsActive,
                             BusinessStartDate = c.BusinessStartDate
                         }).ToList();
            return View(suppliers);
        }

        // GET: /Supplier/Details/5
        [CAuthorize]
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // GET: /Supplier/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.Brand = new SelectList(db.Brands, "BrandId", "BrandName");
            ViewBag.OriginCountry = new SelectList(db.Countries, "CountryId", "CountryName");
            ViewBag.BusinessType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("BSTYPE")), "TypeId", "TypeName", 34);
            ViewBag.PurchaseType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("SUPTYPE")), "TypeId", "TypeName", 39);
            ViewBag.Citizenship = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CTZENSHIP")), "TypeId", "TypeName", 41);
            ViewBag.LegalStructure = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("LEGSTRUCT")), "TypeId", "TypeName", 45);
            ViewBag.PreferredPort = new SelectList(db.viewPorts, "PortId", "PortName");
            ViewBag.AddressCountry = new SelectList(db.Countries, "CountryId", "CountryName");
            //ViewBag.Category = new MultiSelectList(dbProd.ItemCategories.Where(l => l.StageLevel > 1), "CategoryId", "CategoryName");
            ViewBag.Factory = new MultiSelectList(dbProd.Factories.Where(l => l.IsActive == 1), "FactoryId", "FactoryName");
            ViewBag.User = new SelectList(db.Sys_User_Name.Where(l => l.DepartmentName.Equals("SOURCING")), "UserId", "UserTitle");

            return View(new SupplierView());
        }

        // POST: /Supplier/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [CAuthorize]
        public ActionResult Save(SupplierView supplierView, ICollection<SupplierContactView> supplierContactView, ICollection<SupplierProductView> supplierProductView, ICollection<SupplierFactory> supplierFactory)
        {
            try
            {
                Supplier supplier = new Supplier(supplierView);
                supplier.CreatedBy = clsMain.getCurrentUser();
                supplier.CreatedTime = clsMain.getCurrentTime();
                supplier.IPAddress = clsMain.GetUser_IP();
                supplier.PCName = clsMain.GetUser_PCName();
                supplier.eMailAddress = supplier.eMailAddress.ToLower();
                supplier.WebAddress = supplier.WebAddress.ToLower();

                if (supplierContactView != null)
                {
                    foreach (SupplierContactView scv in supplierContactView)
                    {
                        supplier.SupplierContacts.Add(new SupplierContact(scv));
                    }
                }

                if (supplierProductView != null)
                {
                    foreach (SupplierProductView spv in supplierProductView)
                    {
                        supplier.SupplierProducts.Add(new SupplierProduct(spv));
                    }
                }

                if (supplierFactory != null)
                {
                    foreach (SupplierFactory sf in supplierFactory)
                    {
                        supplier.SupplierFactories.Add(sf);
                    }
                }

                db.Suppliers.Add(supplier);

                string entry_by = db.Sys_User_Name.Find(supplier.CreatedBy).UserTitle;

                try
                {
                    db.SaveChanges();

                    var row = (from p in db.EmailLibraries.AsEnumerable()
                               join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                               where p.EmailTypeId == 7
                               select new
                               {
                                   p.TypeName,
                                   p.EmailBody,
                                   q.EmailGroupFrom,
                                   q.EmailGroupTo,
                                   q.EmailGroupCC,
                                   q.EmailGroupBCC,
                                   p.Subject
                               }).FirstOrDefault();

                    EMailCollectionView MailCollection = new EMailCollectionView();
                    MailCollection.EmailType = row.TypeName;
                    MailCollection.ModuleName = "IPMS";
                    MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                    MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                    MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                    MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                    MailCollection.EmailSubject = row.Subject.Replace("@SupplierName", supplier.SupplierName);
                    MailCollection.EmailBody = row.EmailBody.Replace("@SupplierName", supplier.SupplierName)
                        .Replace("@BrandName", db.Brands.Find(supplier.BrandId).BrandName)
                        .Replace("@VAT", supplier.VATNo)
                        .Replace("@BIN", supplier.BINNo)
                        .Replace("@RegNo", supplier.RegistrationNo)
                        .Replace("@Score", supplier.RatingScore.ToString())
                        .Replace("@Email", supplier.eMailAddress.ToLower())
                        .Replace("@EntryBy", entry_by)
                        .Replace("@ReferredBy", supplier.SupplierReferredBy.ToString())
                        .Replace("@NegotiationBy", supplier.SupplierNegotiationBy.ToString())
                        .Replace("@ApprovalLink", @"http://192.168.0.47/ipms/supplier/approval/" + supplier.SupplierId);
                    MailCollection.EmailAttachmentLink = "";
                    MailCollection.CreateAttachment = 0;
                    MailCollection.CreatedBy = clsMain.getCurrentUser();

                    EMailCollection mail = new EMailCollection(MailCollection);

                    dbEM.EMailCollections.Add(mail);

                    dbEM.SaveChanges();

                    return Json(new { status = "Save successful.", sno = supplier.SupplierCode }, JsonRequestBehavior.AllowGet);
                }
                catch(Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Supplier/Details/5
        [CAuthorize]
        public ActionResult ApprovalPending()
        {
            var suppliers = (from p in db.Suppliers.Where(l => l.SupplierId > 0 && l.ApprovedType == 0).AsEnumerable()
                             select p
                            ).Select(c => new SupplierView()
                            {
                                 SupplierId = c.SupplierId,
                                 SupplierCode = c.SupplierCode,
                                 SupplierName = c.SupplierName,
                                 ShortName = c.ShortName,
                                 TINNo = c.TINNo,
                                 VATNo = c.VATNo,
                                 BINNo = c.BINNo,
                                 IsActive = c.IsActive,
                                 BusinessStartDate = c.BusinessStartDate
                            }).ToList();
            
            ViewBag.IsPending = true;
            return View("Index", suppliers);
        }

        // GET: /Supplier/Edit/5
        [CAuthorize]
        public ActionResult Approval(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Supplier supplier = db.Suppliers.Find(id);

            if (supplier == null || supplier.ApprovedType != 0)
            {
                return HttpNotFound();
            }

            SupplierView supplierView = (SupplierView)supplier.Convert(new SupplierView());
            supplierView.is_edit_for_approval = true;

            supplierView.supplierContactView = (from p in db.SupplierContacts.Where(l => l.SupplierId == supplier.SupplierId).AsEnumerable()
                                                join q in dbProd.CustomTypes.AsEnumerable() on p.TypeId equals q.TypeId
                                                select new { p, q }
                                                ).Select(c => new SupplierContactView()
                                                {
                                                    SContactId = c.p.SContactId,
                                                    Name = c.p.Name,
                                                    Designation = c.p.Designation,
                                                    PhoneNo = c.p.PhoneNo,
                                                    TypeId = c.p.TypeId,
                                                    TypeName = c.q.TypeName,
                                                    MobileNo = c.p.MobileNo,
                                                    eMail = c.p.eMail,
                                                    IsAutoMailReceived = c.p.IsAutoMailReceived,
                                                    IsActive = c.p.IsActive
                                                }).ToList();

            //supplierView.CategoryList = db.SupplierProducts.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.CategoryId.ToString()).ToArray<string>();
            //supplierView.FactoryList = db.SupplierFactories.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.FactoryId.ToString()).ToArray<string>();

            var cat = db.SupplierProducts.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.CategoryId);
            var fac = db.SupplierFactories.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.FactoryId);

            ViewBag.Brand = new SelectList(db.Brands, "BrandId", "BrandName");
            ViewBag.OriginCountry = new SelectList(db.Countries, "CountryId", "CountryName");
            ViewBag.BusinessType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("BSTYPE")), "TypeId", "TypeName", 34);
            ViewBag.PurchaseType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("SUPTYPE")), "TypeId", "TypeName", 39);
            ViewBag.Citizenship = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CTZENSHIP")), "TypeId", "TypeName", 41);
            ViewBag.LegalStructure = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("LEGSTRUCT")), "TypeId", "TypeName", 45);
            ViewBag.PreferredPort = new SelectList(db.viewPorts, "PortId", "PortName");
            ViewBag.AddressCountry = new SelectList(db.Countries, "CountryId", "CountryName");
            //ViewBag.Category = new MultiSelectList(dbProd.ItemCategories.Where(l => l.StageLevel > 1), "CategoryId", "CategoryName", cat);
            ViewBag.Factory = new MultiSelectList(dbProd.Factories.Where(l => l.IsActive == 1), "FactoryId", "FactoryName", fac);
            ViewBag.User = new SelectList(db.Sys_User_Name.Where(l => l.DepartmentName.Equals("SOURCING")), "UserId", "UserTitle");

            return View("Edit", supplierView);
        }

        // GET: /Supplier/Edit/5
        [CAuthorize]
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Supplier supplier = db.Suppliers.Find(id);
            SupplierView supplierView = (SupplierView)supplier.Convert(new SupplierView());

            if (supplierView == null)
            {
                return HttpNotFound();
            }

            supplierView.supplierContactView = (from p in db.SupplierContacts.Where(l => l.SupplierId == supplier.SupplierId).AsEnumerable()
                                                join q in dbProd.CustomTypes.AsEnumerable() on p.TypeId equals q.TypeId
                                                select new {p, q}
                                                ).Select(c => new SupplierContactView() 
                                                { 
                                                    SContactId = c.p.SContactId, Name = c.p.Name, Designation = c.p.Designation, 
                                                    PhoneNo = c.p.PhoneNo, TypeId = c.p.TypeId, TypeName = c.q.TypeName, MobileNo = c.p.MobileNo, 
                                                    eMail = c.p.eMail, IsAutoMailReceived = c.p.IsAutoMailReceived, IsActive = c.p.IsActive 
                                                }).ToList();
            
            //supplierView.CategoryList = db.SupplierProducts.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.CategoryId.ToString()).ToArray<string>();
            //supplierView.FactoryList = db.SupplierFactories.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.FactoryId.ToString()).ToArray<string>();

            var cat = db.SupplierProducts.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.CategoryId);
            var fac = db.SupplierFactories.Where(l => l.SupplierId == supplier.SupplierId).Select(c => c.FactoryId);

            ViewBag.Brand = new SelectList(db.Brands, "BrandId", "BrandName");
            ViewBag.OriginCountry = new SelectList(db.Countries, "CountryId", "CountryName");
            ViewBag.BusinessType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("BSTYPE")), "TypeId", "TypeName", 34);
            ViewBag.PurchaseType = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("SUPTYPE")), "TypeId", "TypeName", 39);
            ViewBag.Citizenship = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CTZENSHIP")), "TypeId", "TypeName", 41);
            ViewBag.LegalStructure = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("LEGSTRUCT")), "TypeId", "TypeName", 45);
            ViewBag.PreferredPort = new SelectList(db.viewPorts, "PortId", "PortName");
            ViewBag.AddressCountry = new SelectList(db.Countries, "CountryId", "CountryName");
            //ViewBag.Category = new MultiSelectList(dbProd.ItemCategories.Where(l => l.StageLevel > 1), "CategoryId", "CategoryName", cat);
            ViewBag.Factory = new MultiSelectList(dbProd.Factories.Where(l => l.IsActive == 1), "FactoryId", "FactoryName", fac);
            ViewBag.User = new SelectList(db.Sys_User_Name.Where(l => l.DepartmentName.Equals("SOURCING")), "UserId", "UserTitle");

            return View(supplierView);
        }

        // POST: /Supplier/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [CAuthorize]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Update(SupplierView supplierView, ICollection<SupplierContactView> supplierContactView, ICollection<SupplierProductView> supplierProductView, ICollection<SupplierFactoryView> supplierFactoryView, short ApprovedType = 0)
        {
            try
            {
                Supplier supplier = db.Suppliers.Find(supplierView.SupplierId);
                supplier = (Supplier)supplierView.ConvertNotNull(supplier);
                if (ApprovedType > 0)
                {
                    supplier.ApprovedType = ApprovedType;
                    supplier.ApprovedBy = clsMain.getCurrentUser();
                    supplier.ApprovedTime = clsMain.getCurrentTime();
                }
                else
                {
                    supplier.UpdatedBy = clsMain.getCurrentUser();
                    supplier.UpdateTime = clsMain.getCurrentTime();
                }

                if (supplierContactView != null)
                {
                    foreach (SupplierContactView scv in supplierContactView)
                    {
                        if (scv.SContactId == null || scv.SContactId == 0)
                            supplier.SupplierContacts.Add(new SupplierContact(scv));
                        else
                        {
                            SupplierContact sc = db.SupplierContacts.Find(scv.SContactId);
                            sc = (SupplierContact)scv.ConvertNotNull(sc);
                            db.Entry(sc).State = EntityState.Modified;
                        }
                    }
                }

                var unsub = db.SupplierContacts.Where(l => l.SupplierId == supplier.SupplierId).ToList();

                for (int ri = 0; ri < unsub.Count; ri++)
                {
                    if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                        db.Entry(unsub[ri]).State = EntityState.Deleted;
                }

                if (supplierProductView != null)
                {
                    foreach (SupplierProductView spv in supplierProductView)
                    {
                        var sp = db.SupplierProducts.Where(l => l.CategoryId == spv.CategoryId && l.SupplierId == spv.SupplierId).FirstOrDefault();
                        if (sp == null)
                            supplier.SupplierProducts.Add(new SupplierProduct(spv));
                        else
                        {
                            sp = (SupplierProduct)spv.ConvertNotNull(sp);
                            db.Entry(sp).State = EntityState.Modified;
                        }
                    }
                }

                var unsub1 = db.SupplierProducts.Where(l => l.SupplierId == supplier.SupplierId).ToList();

                for (int ri = 0; ri < unsub1.Count; ri++)
                {
                    if (db.Entry(unsub1[ri]).State == EntityState.Unchanged)
                        db.Entry(unsub1[ri]).State = EntityState.Deleted;
                }

                if (supplierFactoryView != null)
                {
                    foreach (SupplierFactoryView sfv in supplierFactoryView)
                    {
                        var sf = db.SupplierFactories.Where(l => l.FactoryId == sfv.FactoryId && l.SupplierId == sfv.SupplierId).FirstOrDefault();
                        if (sf == null)
                            supplier.SupplierFactories.Add(new SupplierFactory(sfv));
                        else
                        {
                            sf = (SupplierFactory)sfv.ConvertNotNull(sf);
                            db.Entry(sf).State = EntityState.Modified;
                        }
                    }
                }

                var unsub2 = db.SupplierFactories.Where(l => l.SupplierId == supplier.SupplierId).ToList();

                for (int ri = 0; ri < unsub2.Count; ri++)
                {
                    if (db.Entry(unsub2[ri]).State == EntityState.Unchanged)
                        db.Entry(unsub2[ri]).State = EntityState.Deleted;
                }

                db.Entry(supplier).State = EntityState.Modified;

                db.SaveChanges();

                if (ApprovedType > 0)
                {
                    string entry_by = db.Sys_User_Name.Find(supplier.CreatedBy).UserTitle;

                    try
                    {
                        var row = (from p in db.EmailLibraries.AsEnumerable()
                                   join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                                   where p.EmailTypeId == 8
                                   select new
                                   {
                                       p.TypeName,
                                       p.EmailBody,
                                       q.EmailGroupFrom,
                                       q.EmailGroupTo,
                                       q.EmailGroupCC,
                                       q.EmailGroupBCC,
                                       p.Subject
                                   }).FirstOrDefault();

                        if (row != null || String.IsNullOrEmpty(row.TypeName))
                        {
                            EMailCollectionView MailCollection = new EMailCollectionView();
                            MailCollection.EmailType = row.TypeName;
                            MailCollection.ModuleName = "IPMS";
                            MailCollection.EmailFrom = clsMain.getEmail(clsMain.getCurrentUser().ToString()); //clsMain.getEmail(row.EmailGroupFrom);
                            MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                            MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                            MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                            MailCollection.EmailSubject = row.Subject.Replace("@SupplierName", supplier.SupplierName);
                            MailCollection.EmailBody = row.EmailBody.Replace("@SupplierName", supplier.SupplierName)
                                .Replace("@BrandName", db.Brands.Find(supplier.BrandId).BrandName)
                                .Replace("@VAT", supplier.VATNo)
                                .Replace("@BIN", supplier.BINNo)
                                .Replace("@RegNo", supplier.RegistrationNo)
                                .Replace("@Score", supplier.RatingScore.ToString())
                                .Replace("@ApproveType", ApprovedType == 1 ? "Approved" : "Disapproved")
                                .Replace("@Email", supplier.eMailAddress)
                                .Replace("@EntryBy", entry_by)
                                .Replace("@ReferredBy", supplier.SupplierReferredBy.ToString())
                                .Replace("@NegotiationBy", supplier.SupplierNegotiationBy.ToString());
                            MailCollection.EmailAttachmentLink = "";
                            MailCollection.CreateAttachment = 0;
                            MailCollection.CreatedBy = clsMain.getCurrentUser();

                            EMailCollection mail = new EMailCollection(MailCollection);

                            dbEM.EMailCollections.Add(mail);

                            dbEM.SaveChanges();
                        }

                        return Json(new { status = (ApprovedType == 1 ? "Approval" : "Disapproval") + " successful.", sno = supplier.SupplierCode }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { status = (ApprovedType == 1 ? "Approval" : "Disapproval") + " successful but confirmation mail sending.", sno = supplier.SupplierCode }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { status = "Update successful.", sno = supplier.SupplierCode }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Supplier/Delete/5
        [CAuthorize]
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // POST: /Supplier/Delete/5
        [CAuthorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            Supplier supplier = db.Suppliers.Find(id);
            db.Suppliers.Remove(supplier);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Certificates(short? id)
        {
            List<Int16> cList = new List<short>();

            if(id != null && id > 0)
            {
                short cc;
                cList = db.Suppliers.Find(id).CertificatesAvail.Replace(", ", ",").Split(',').Select(c => Int16.TryParse(c, out cc) ? cc : short.Parse("0")).ToList();
            }

            return PartialView(cList);
        }

        public ActionResult Awards(short? id)
        {
            List<Int16> cList = new List<short>();

            if (id != null && id > 0)
            {
                short cc;
                cList = db.Suppliers.Find(id).AwardAvail.Replace(", ", ",").Split(',').Select(c => Int16.TryParse(c, out cc) ? cc : short.Parse("0")).ToList();
            }

            return PartialView(cList);
        }

        public ActionResult ContactTemplate()
        {
            ViewBag.Type = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("SUPTYPE")), "TypeId", "TypeName", 39);
            
            return PartialView();
        }

        [HttpPost]
        public ActionResult RowTemplate(SupplierContactView supplierView)
        {
            //SupplerContactView supplierView = (SupplerContactView)obj.Convert(new SupplerContactView());
            return PartialView(supplierView);
        }

        [HttpPost]
        public ActionResult ContactProductDetails(ICollection<SupplierContactView> supplierContactView, ICollection<SupplierProductView> supplierProductView)
        {
            SupplierView supplier = new SupplierView();
            supplier.supplierContactView = supplierContactView;
            supplier.supplerProductView = supplierProductView;

            return PartialView(supplier);
        }

        public ActionResult BrandCreate()
        {
            return PartialView();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[CAuthorize]
        public ActionResult BrandSave(BrandView brandView)
        {
            try
            {
                Brand brand = new Brand(brandView);
                brand.CreatedBy = clsMain.getCurrentUser();
                brand.CreatedTime = clsMain.getCurrentTime();
                brand.IPAddress = clsMain.GetUser_IP();
                brand.PCName = clsMain.GetUser_PCName();

                db.Brands.Add(brand);

                db.SaveChanges();

                return Json(new { status = "Save successful.", brid = brand.BrandId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
