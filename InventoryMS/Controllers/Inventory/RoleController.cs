﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using System.Data.Entity;
using System.Reflection;

namespace InventoryMS.Controllers
{
    public class RoleController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();

        // GET: Role
        [CAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Role/Details/5
        [CAuthorize]
        public ActionResult Details(short id)
        {
            var rd = db.User_Role_Details.Where(l => l.RoleId == id);
            return Json(new { data = rd.Select(l => new { aid = l.ActionId }) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Role/Create
        [CAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Role/Create
        [CAuthorize, HttpPost]
        public ActionResult Create(Role role)
        {
            try
            {
                User_Role urole = new User_Role(role);

                db.User_Role.Add(urole);
                db.SaveChanges();

                return RedirectToAction("Create");
            }
            catch
            {
                return View(role);
            }
        }

        // GET: Role/Edit/5
        [CAuthorize]
        public ActionResult Edit(int id)
        {
            //User_Role urole = db.User_Role.Find(id);
            Role role = (Role)db.User_Role.Find(id).Convert(new Role());

            return View(role);
        }

        // POST: Role/Edit/5
        [CAuthorize, HttpPost]
        public ActionResult Edit(Role role)
        {
            try
            {
                User_Role urole = (User_Role)role.ConvertNotNull(db.User_Role.Find(role.RoleId));
                urole.UpdatedBy = clsMain.getCurrentUser();
                urole.UpdateTime = clsMain.getCurrentTime();

                db.Entry(urole).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Create");
            }
            catch
            {
                return View(role);
            }
        }

        // GET: Role/Create
        [CAuthorize]
        public ActionResult RoleAccess(short? id)
        {
            ViewBag.RoleId = new SelectList(db.User_Role, "RoleId", "RoleName");
            var mtlist = db.MenuTypes.ToList();

            return View(mtlist);
        }

        // GET: Role/Create
        [HttpPost, CAuthorize]
        public ActionResult RoleAccessSave(short id, List<short> actionlist)
        {
            try
            {
                var urds = db.User_Role_Details.Where(l => l.RoleId == id).ToList();

                foreach(var action in actionlist)
                {
                    if(urds.Where(l => l.ActionId == action).Count() > 0)
                    {
                        var urd = urds.Where(l => l.ActionId == action).First();
                        db.Entry(urd).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        User_Role_Details urd = new User_Role_Details(db.User_Action.Find(action));
                        urd.RoleId = id; urd.MenuTypeId = db.User_Controller.Find(urd.ControllerId).MenuTypeId;
                        db.User_Role_Details.Add(urd);
                    }
                }

                for (int ri = 0; ri < urds.Count; ri++)
                {
                    if (db.Entry(urds[ri]).State == EntityState.Unchanged)
                        db.Entry(urds[ri]).State = EntityState.Deleted;
                } 
                
                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Role/Delete/5
        [CAuthorize]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Role/Delete/5
        [CAuthorize, HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult MenuInsert(ICollection<MenuDetail> menuDetail)
        {
            try
            {
                db.MenuDetails.AddRange(menuDetail);

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MenuTracking()
        {
            var mlist = db.MenuDetails;

            return View(mlist);
        }

        public ActionResult MenuTrackingSave(ICollection<MenuWiseAction> menuWiseAction)
        {
            try
            {
                var urds = db.MenuWiseActions.ToList();

                foreach (var action in menuWiseAction)
                {
                    if (urds.Where(l => l.ActionId == action.ActionId && l.MenuId == action.MenuId).Count() > 0)
                    {
                        var urd = urds.Where(l => l.ActionId == action.ActionId && l.MenuId == action.MenuId).First();
                        db.Entry(urd).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        MenuWiseAction urd = new MenuWiseAction(action);
                        db.MenuWiseActions.Add(urd);
                    }
                }

                for (int ri = 0; ri < urds.Count; ri++)
                {
                    if (db.Entry(urds[ri]).State == EntityState.Unchanged)
                        db.Entry(urds[ri]).State = EntityState.Deleted;
                }

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MenuDefaultAction()
        {
            var mlist = db.MenuDetails;

            return View(mlist);
        }

        public ActionResult MenuDefaultActionSave(ICollection<MenuDetail> menuDetail)
        {
            try
            {
                foreach (var md in menuDetail)
                {
                    var menu = db.MenuDetails.Find(md.MenuId);
                    menu.DefaultActionId = md.DefaultActionId;
                    menu.DefaultParameter = (md.DefaultParameter == null ? "" : md.DefaultParameter);
                    menu.DefaultParameterValue = (md.DefaultParameterValue == null ? "": md.DefaultParameterValue);

                    db.Entry(menu).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ParamList(short id, short mid)
        {
            try
            {
                var plist = db.User_Action.Find(id).ParameterList;

                string pvlist = "";
                if (db.MenuDetails.Find(mid).DefaultActionId == id)
                {
                    pvlist = db.MenuDetails.Find(mid).DefaultParameterValue;
                }

                return Json(new { status = "Success", plist = plist, pvlist = pvlist }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Role/Create
        [CAuthorize]
        public ActionResult UserAccess(short? id)
        {
            ViewBag.UserId = new SelectList(db.Sys_User_Name.Select(c => new { UserId = c.UserId, UserName = c.UserName + " (" + c.UserTitle + ")" }), "UserId", "UserName");
            ViewBag.type = new SelectList((new []{new { tid = "Role" }, new {tid = "User"}}).ToList(), "tid", "tid", "Role");
            ViewBag.RoleId = new MultiSelectList(db.User_Role.Select(c => new { c.RoleId, c.RoleName }), "RoleId", "RoleName");
            var mtlist = db.MenuTypes.ToList();

            return View(mtlist);
        }

        // GET: Role/Create
        [HttpPost, CAuthorize]
        public ActionResult UserAccessSave(string type, ICollection<UserPermission> userPermission)
        {
            try
            {
                var userid = userPermission.First().UserId;
                var ups = db.User_Permission.Where(l => l.UserId == userid).ToList();

                foreach (var up in userPermission)
                {
                    if (type.Equals("User"))
                    {
                        if (ups.Where(l => l.ActionId == up.ActionId).Count() > 0)
                        {
                            var urd = ups.Where(l => l.ActionId == up.ActionId).First();
                            db.Entry(urd).State = EntityState.Modified;
                        }
                        else
                        {
                            User_Permission urd = new User_Permission(db.User_Action.Find(up.ActionId));
                            db.User_Permission.Add(urd);
                        }
                    }
                    else
                    {
                        if (ups.Where(l => l.RoleId == up.RoleId).Count() > 0)
                        {
                            var urd = ups.Where(l => l.RoleId == up.RoleId).First();
                            db.Entry(urd).State = EntityState.Modified;
                        }
                        else
                        {
                            User_Permission urd = new User_Permission(up);
                            urd.IPAddress = clsMain.GetUser_IP();
                            urd.PCName = clsMain.GetUser_PCName();
                            db.User_Permission.Add(urd);
                        }
                    }
                }

                for (int ri = 0; ri < ups.Count; ri++)
                {
                    if (db.Entry(ups[ri]).State == EntityState.Unchanged)
                        db.Entry(ups[ri]).State = EntityState.Deleted;
                }

                db.SaveChanges();

                return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        //// GET: Role/Details/5
        //[CAuthorize]
        //public ActionResult UserPermissionDetails(short id)
        //{
        //    var rd = db.User_Role_Details.Where(l => l.RoleId == id);
        //    return Json(new { data = rd.Select(l => new { aid = l.ActionId }) }, JsonRequestBehavior.AllowGet);
        //}
    }
}
