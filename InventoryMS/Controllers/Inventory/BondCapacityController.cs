﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class BondCapacityController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();

        // GET: /BondCapacity/
        [CAuthorize]
        public ActionResult Index()
        {
            var bondcapacities = (from p in db.BondCapacities.AsEnumerable()
                                  join q in dbProd.viewQuantityUnits.AsEnumerable() on p.CapacityUnitId equals q.QuantityUnitId
                                  select new { p, q.QuantityUnit }).Select(c => new BondCapacityView()
                                {
                                    CapacityId = c.p.CapacityId,
                                    BondId = c.p.BondId,
                                    BondPeriodId = c.p.BondPeriodId,
                                    BondSerialNo = c.p.BondSerialNo,
                                    RMTitle = c.p.RMTitle,
                                    HSCode = c.p.HSCode,
                                    StartDate = c.p.StartDate,
                                    EndDate = c.p.EndDate,
                                    Capacity = c.p.Capacity,
                                    CapacityUnitId = c.p.CapacityUnitId,
                                    CapacityUnit = c.QuantityUnit,
                                    IsActive = c.p.IsActive,
                                    LicenseNumber = c.p.Bond.LicenseNumber
                                }).ToList();
            return View(bondcapacities.ToList());
        }

        // GET: /BondCapacity/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BondCapacity bondCapacity = db.BondCapacities.Find(id);
            if (bondCapacity == null)
            {
                return HttpNotFound();
            }
            return View(bondCapacity);
        }

        // GET: /BondCapacity/Create
        [CAuthorize]
        public ActionResult Manage(int? bondid, int? bondperiodid)
        {
            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber", bondid);
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondPeriodId == bondperiodid), "BondPeriodId", "LicensePeriod", bondperiodid);
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");
            ViewBag.bpid = bondperiodid;

            return View();
        }

        // POST: /BondCapacity/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [CAuthorize]
        public ActionResult Save(int BondId, int BondPeriodId, ICollection<BondCapacityView> bondCapacityView)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int rIndex = 0, cIndex = 0;
                    //if (cIndex >= bondCapacityView.Count)
                    //{
                        foreach (var scv in bondCapacityView)
                        {
                            BondCapacity sc = new BondCapacity(scv);
                            if (sc.CapacityId > 0)
                                db.Entry(sc).State = EntityState.Modified;
                            else
                                db.BondCapacities.Add(sc);
                        }

                        cIndex = db.BondCapacities.Where(l => l.BondPeriodId == BondPeriodId).Count();
                        var bcaps = db.BondCapacities.Where(l => l.BondPeriodId == BondPeriodId).ToList();
                        for (rIndex = 0; rIndex < cIndex; rIndex++)
                        {
                            var scv = bcaps[rIndex];
                            if (db.Entry(scv).State == EntityState.Unchanged)
                                db.BondCapacities.Remove(scv);//.Entry(scv).State = EntityState.Deleted;
                        }
                    //}
                    //else
                    //{
                    //    foreach (var scv in bond.BondPeriods)
                    //    {
                    //        var sc = bondPeriodView.ToList()[rIndex];
                    //        var scview = (BondPeriod)sc.ConvertNotNull(scv);
                    //        db.Entry(scview).State = EntityState.Modified;
                    //        bondPeriodView.ToList()[rIndex]._editStatus = true;

                    //        rIndex++;
                    //    }

                    //    foreach (var scv in bondPeriodView.Where(l => l._editStatus == false))
                    //    {
                    //        var sc = new BondPeriod(scv);
                    //        bond.BondPeriods.Add(sc);
                    //    }
                    //}

                    //bond = (Bond)bondView.ConvertNotNull(bond);
                    //db.Entry(bond).State = EntityState.Modified;

                    //foreach (var bcv in bondCapacityView)
                    //{
                    //    BondCapacity bondCapacity = new BondCapacity(bcv);

                    //    db.BondCapacities.Add(bondCapacity);
                    //}
                    db.SaveChanges();
                    return Content("Success");
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            //ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber");
            //ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondId == bondCapacityView.BondId), "BondPeriodId", "LicensePeriod", bondCapacityView.BondPeriodId);
            //ViewBag.CapacityUnitId = new SelectList(db.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");

            throw new Exception("invalid");
        }

        // GET: /BondCapacity/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BondCapacity bondCapacity = db.BondCapacities.Find(id);
            BondCapacityView bondCapacityView = (BondCapacityView)bondCapacity.Convert(new BondCapacityView());

            if (bondCapacity == null)
            {
                return HttpNotFound();
            }
            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber", bondCapacityView.BondId);
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondId == bondCapacityView.BondId), "BondPeriodId", "LicensePeriod", bondCapacityView.BondPeriodId);
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", bondCapacityView.CapacityUnitId);

            return View(bondCapacityView);
        }

        // POST: /BondCapacity/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CapacityId,BondId,BondPeriodId,BondSerialNo,RMTitle,HSCode,StartDate,EndDate,Capacity,CapacityUnitId,Activity")] BondCapacityView bondCapacityView)
        {
            if (ModelState.IsValid)
            {
                BondCapacity bondCapacity = db.BondCapacities.Find(bondCapacityView.CapacityId);
                bondCapacity = (BondCapacity)bondCapacityView.ConvertNotNull(bondCapacity);

                db.Entry(bondCapacity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BondId = new SelectList(db.Bonds, "BondId", "LicenseNumber", bondCapacityView.BondId);
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondId == bondCapacityView.BondId), "BondPeriodId", "LicensePeriod", bondCapacityView.BondPeriodId);
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", bondCapacityView.CapacityUnitId);

            return View(bondCapacityView);
        }

        // GET: /BondCapacity/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BondCapacity bondCapacity = db.BondCapacities.Find(id);
            if (bondCapacity == null)
            {
                return HttpNotFound();
            }
            return View(bondCapacity);
        }

        // POST: /BondCapacity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BondCapacity bondCapacity = db.BondCapacities.Find(id);
            db.BondCapacities.Remove(bondCapacity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CapacityDetails(int? id)
        {
            var capDetails = db.BondCapacities.Where(l => l.BondPeriodId == id);
            ViewBag.sdate = db.BondPeriods.Find(id).StartDate.ToString("yyyy-MM-dd");
            ViewBag.edate = db.BondPeriods.Find(id).EndDate.ToString("yyyy-MM-dd");

            return PartialView(capDetails);
        }

        public ActionResult PeriodList(int? id)
        {
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondId == id).OrderByDescending(c => c.BondPeriodId), "BondPeriodId", "LicensePeriod");
            return PartialView();
        }
    }
}
