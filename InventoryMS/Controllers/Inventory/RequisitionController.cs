﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class RequisitionController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private EMailCubeEntities dbEM = new EMailCubeEntities();

        // GET: /Requisition/
        [CAuthorize]
        public ActionResult Index()
        {
            var requisitions = (from p in db.PRMains.AsEnumerable()
                                //join q in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals q.QuantityUnitId
                                join r in dbProd.UserFactories.AsEnumerable() on p.FactoryId equals r.FactoryId
                                select new {p, QuantityUnit = "", r.ShortName}).Select(c => new Requisition()
                                {
                                    PRId = c.p.PRId,
                                    PRNo = c.p.PRNo,
                                    PRDate = c.p.PRDate,
                                    TotalQuantity = c.p.TotalQuantity,
                                    QuantityUnit = c.QuantityUnit,
                                    FactoryId = c.p.FactoryId,
                                    FactoryName = c.ShortName,
                                    IsActive = c.p.IsActive,
                                    CreatedBy = c.p.CreatedBy,
                                    RevisedBy = c.p.UpdatedBy,
                                    RevisedTime = c.p.UpdateTime
                                }).ToList();

            return View(requisitions);
        }

        // GET: /Requisition/
        [CAuthorize]
        public ActionResult QueuedRequisition()
        {
            var requisitions = (from p in db.PRMains.AsEnumerable()
                                join r in dbProd.UserFactories.AsEnumerable() on p.FactoryId equals r.FactoryId
                                where p.HODApprovedType == 0 && (p.ApprovedType == 0 || p.ApprovedType == 10)
                                select new { p, QuantityUnit = "", r.ShortName }).Select(c => new Requisition()
                                {
                                    PRId = c.p.PRId,
                                    PRNo = c.p.PRNo,
                                    PRDate = c.p.PRDate,
                                    TotalQuantity = c.p.TotalQuantity,
                                    QuantityUnit = c.QuantityUnit,
                                    FactoryId = c.p.FactoryId,
                                    FactoryName = c.ShortName,
                                    IsActive = c.p.IsActive,
                                    CreatedBy = c.p.CreatedBy,
                                    RevisedBy = c.p.UpdatedBy,
                                    RevisedTime = c.p.UpdateTime
                                }).ToList();

            return View(requisitions);
        }

        // GET: /Requisition/
        [CAuthorize]
        public ActionResult PendingForKGUpdate(int? id)
        {
            if (id == null)
            {
                var requisitions = (from p in db.PRMains.AsEnumerable()
                                    join r in dbProd.UserFactories.AsEnumerable() on p.FactoryId equals r.FactoryId
                                    where p.ApprovedType == 1 && p.PRSubs.Any(c => c.QuantityInKG == 0)
                                    select new { p, QuantityUnit = "", r.ShortName }).Select(c => new Requisition()
                                    {
                                        PRId = c.p.PRId,
                                        PRNo = c.p.PRNo,
                                        PRDate = c.p.PRDate,
                                        TotalQuantity = c.p.TotalQuantity,
                                        QuantityUnit = c.QuantityUnit,
                                        FactoryId = c.p.FactoryId,
                                        FactoryName = c.ShortName,
                                        IsActive = c.p.IsActive,
                                        CreatedBy = c.p.CreatedBy,
                                        RevisedBy = c.p.UpdatedBy,
                                        RevisedTime = c.p.UpdateTime
                                    }).ToList();

                return View(requisitions);
            }

            PRMain prmain = db.PRMains.Find(id);

            if ((prmain == null || prmain.ApprovedType == 0) && clsMain.getCurrentUser() != 4)
            {
                return HttpNotFound();
            }

            Requisition requisition = (Requisition)prmain.Convert(new Requisition());
            requisition.requisitionSub = (from p in prmain.PRSubs.AsEnumerable()
                                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                                          select new { p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit }
                                        ).Select(
                                        c => new RequisitionSub()
                                        {
                                            PRId = c.p.PRId,
                                            PRSubId = c.p.PRSubId,
                                            ProductId = c.p.ProductId,
                                            CurrentStock = c.p.CurrentStock,
                                            StockInTransit = c.p.StockInTransit,
                                            PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                            HistoricalConsumption = c.p.HistoricalConsumption,
                                            HistoricalPeriod = c.p.HistoricalPeriod,
                                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                                            FactoryETA = c.p.FactoryETA,
                                            LeadTime = c.p.LeadTime,
                                            BrandId = c.p.BrandId,
                                            BrandName = c.BrandName,
                                            PreferredSupplierId = c.p.PreferredSupplierId,
                                            SupplierName = c.SupplierName,
                                            Quantity = c.p.Quantity,
                                            QuantityUnit = c.QuantityUnit,
                                            QuantityUnitId = c.p.QuantityUnitId,
                                            QuantityInKG = c.p.QuantityInKG,
                                            KGPerUnit = c.p.KGPerUnit,
                                            CategoryId = c.CategoryId,
                                            ProductName = c.ItemName,
                                            BondBalance = c.p.BondBalance,
                                            CapacityId = c.p.CapacityId,
                                            RowNo = c.p.RowNo,
                                            Remarks = c.p.Remarks
                                        }).ToList();

            var comid = dbProd.Factories.Find(requisition.FactoryId).CompanyId;
            var bp = db.Bonds.Where(l => l.CompanyId == comid).FirstOrDefault().BondPeriods.OrderBy(c => c.EndDate).Last();
            if (bp == null)
            {
                bp = new BondPeriod();
                bp.EndDate = DateTime.Parse("01-Jan-1900");
            }
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName", requisition.FactoryId);
            ViewBag.BLSDate = bp.EndDate.ToString("dd-MMM-yyyy");
            ViewBag.BLEDate = bp.EndDate.AddDays(30).ToString("dd-MMM-yyyy");

            return View("KGEdit", requisition);
        }

        // GET: /Requisition/Details/5
        [CAuthorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PRMain prmain = db.PRMains.Find(id);
            Requisition requisition = (Requisition)prmain.Convert(new Requisition());
            requisition.requisitionSub = (from p in prmain.PRSubs.AsEnumerable()
                                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                                          select new { p, q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit }
                                        ).Select(
                                        c => new RequisitionSub()
                                        {
                                            PRId = c.p.PRId,
                                            PRSubId = c.p.PRSubId,
                                            ProductId = c.p.ProductId,
                                            CurrentStock = c.p.CurrentStock,
                                            StockInTransit = c.p.StockInTransit,
                                            PendingRequisitionQuanityForItem = c.p.PendingRequisitionQuanity,
                                            HistoricalConsumption = c.p.HistoricalConsumption,
                                            HistoricalPeriod = c.p.HistoricalPeriod,
                                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                            FactoryETA = c.p.FactoryETA,
                                            BrandId = c.p.BrandId,
                                            BrandName = c.BrandName,
                                            PreferredSupplierId = c.p.PreferredSupplierId,
                                            SupplierName = c.ShortName,
                                            Quantity = c.p.Quantity,
                                            QuantityUnit = c.QuantityUnit,
                                            QuantityUnitId = c.p.QuantityUnitId,
                                            QuantityInKG = c.p.QuantityInKG,
                                            KGPerUnit = c.p.KGPerUnit,
                                            CategoryId = c.CategoryId,
                                            ProductName = c.ItemName,
                                            RowNo = c.p.RowNo,
                                            Remarks = c.p.Remarks,
                                            IsWithDuty = c.p.IsWithDuty,
                                            BondCapacityCheck = false,
                                            PendingRequisitionQuanity = 0,
                                            BondDeductSourceType = c.p.BondDeductSourceType
                                        }).ToList();

            if (prmain == null || prmain.HODApprovedType > 0 || prmain.ApprovedType > 0 || dbProd.UserFactories.Where(l => l.FactoryId == prmain.FactoryId).Count() == 0)
            {
                return HttpNotFound();
            }

            ViewBag.is_mnsp = 0;
            foreach (var item in prmain.PRSubs)
            {
                if (!dbProd.ItemInfoes.Find(item.ProductId).ItemCode.Substring(0, 2).Equals("01"))
                {
                    ViewBag.is_mnsp = 1;

                    break;
                }
            }

            return View(requisition);
        }

        // GET: /Requisition/Details/5
        [CAuthorize]
        public ActionResult Details_MNSP(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PRMain prmain = db.PRMains.Find(id);
            Requisition requisition = (Requisition)prmain.Convert(new Requisition());
            requisition.requisitionSub = (from p in prmain.PRSubs.AsEnumerable()
                                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                                          select new { p, q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit }
                                        ).Select(
                                        c => new RequisitionSub()
                                        {
                                            PRId = c.p.PRId,
                                            PRSubId = c.p.PRSubId,
                                            ProductId = c.p.ProductId,
                                            CurrentStock = c.p.CurrentStock,
                                            StockInTransit = c.p.StockInTransit,
                                            PendingRequisitionQuanityForItem = c.p.PendingRequisitionQuanity,
                                            HistoricalConsumption = c.p.HistoricalConsumption,
                                            HistoricalPeriod = c.p.HistoricalPeriod,
                                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                            FactoryETA = c.p.FactoryETA,
                                            BrandId = c.p.BrandId,
                                            BrandName = c.BrandName,
                                            PreferredSupplierId = c.p.PreferredSupplierId,
                                            SupplierName = c.ShortName,
                                            Quantity = c.p.Quantity,
                                            QuantityUnit = c.QuantityUnit,
                                            QuantityUnitId = c.p.QuantityUnitId,
                                            QuantityInKG = c.p.QuantityInKG,
                                            KGPerUnit = c.p.KGPerUnit,
                                            CategoryId = c.CategoryId,
                                            ProductName = c.ItemName,
                                            RowNo = c.p.RowNo,
                                            Remarks = c.p.Remarks,
                                            IsWithDuty = c.p.IsWithDuty,
                                            BondCapacityCheck = false,
                                            PendingRequisitionQuanity = 0,
                                            BondDeductSourceType = c.p.BondDeductSourceType
                                        }).ToList();

            if (prmain == null || prmain.HODApprovedType != 1 || prmain.ApprovedType > 0 || dbProd.UserFactories.Where(l => l.FactoryId == prmain.FactoryId).Count() == 0)
            {
                return HttpNotFound();
            }

            ViewBag.is_mnsp = 0;

            return View("Details", requisition);
        }

        // GET: /Requisition/Create
        [CAuthorize]
        public ActionResult Create()
        {
            Requisition pr = new Requisition();
            pr.PRDate = db.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            return View(pr);
        }

        // POST: /Requisition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        //[ValidateAntiForgeryToken]
        public ActionResult Save([Bind(Include = "FactoryId, PRDate, PRMonth, Remarks, Activity")] Requisition requisition, ICollection<RequisitionSub> requisitionSub)
        {
            try
            {
                PRMain prmain = new PRMain(requisition);
                short is_mnsp = 0;

                prmain.PRNo = "RQ/" + dbProd.Factories.Find(prmain.FactoryId).Prefix + "/" + db.PRMains.Where(l => l.Year == prmain.PRDate.Year && l.FactoryId == prmain.FactoryId).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + prmain.PRDate.ToString("MM/yy").ToUpperInvariant();
                prmain.CreatedBy = clsMain.getCurrentUser();
                prmain.CreatedTime = clsMain.getCurrentTime();
                prmain.PRDate = prmain.CreatedTime;

                foreach (var rqSub in requisitionSub)
                {
                    PRSub prsub = new PRSub(rqSub);
                    rqSub.CETA = prmain.PRQDate.AddDays(rqSub.LeadTime.Value).Date;
                    rqSub.OriginalETA = rqSub.FactoryETA;
                    prsub.BondProductId = prsub.ProductId;

                    if (prsub.BondDeductSourceType == 1)
                    {
                        prsub.IsWithDuty = 1;
                    } else if (prsub.BondDeductSourceType == 4)
                    {
                        prsub.IsLocalPurchase = 1;
                    }

                    if (!dbProd.ItemInfoes.Find(rqSub.ProductId).ItemCode.Substring(0, 2).Equals("01"))
                    {
                        is_mnsp = 1;
                    }
                    prmain.PRSubs.Add(prsub);
                }

                db.PRMains.Add(prmain);

                try
                {
                    db.SaveChanges();

                    var row = (from p in db.EmailLibraries.AsEnumerable()
                               join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                               where p.EmailTypeId == 1 && q.FactoryId == prmain.FactoryId
                               select new
                               {
                                   p.TypeName,
                                   p.EmailBody,
                                   q.EmailGroupFrom,
                                   q.EmailGroupTo,
                                   q.EmailGroupCC,
                                   q.EmailGroupBCC,
                                   p.Subject
                               }).FirstOrDefault();

                    if (row != null)
                    {
                        EMailCollectionView MailCollection = new EMailCollectionView();
                        MailCollection.EmailType = row.TypeName;
                        MailCollection.ModuleName = "IPMS";
                        MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                        MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                        MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                        MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                        MailCollection.EmailSubject = row.Subject.Replace("@RequisitionNo", prmain.PRNo);
                        MailCollection.EmailBody = row.EmailBody.Replace("@RequisitionNo", prmain.PRNo)
                            .Replace("@Factory", dbProd.Factories.Find(prmain.FactoryId).ShortName)
                            .Replace("@Date", prmain.PRQDate.ToString("dd-MMM-yyyy"))
                            .Replace("@ApprovalLink", @"http://192.168.0.47/ipms/requisition/details/" + prmain.PRId);
                        MailCollection.EmailAttachmentLink = "";
                        MailCollection.CreateAttachment = 0;
                        MailCollection.CreatedBy = clsMain.getCurrentUser();

                        EMailCollection mail = new EMailCollection(MailCollection);

                        dbEM.EMailCollections.Add(mail);

                        dbEM.SaveChanges();

                        return Json(new { status = "Save successful and sent to HOD (" + MailCollection.EmailTo + ") for " + (is_mnsp == 0 ? "approval" : "forwarding") + Environment.NewLine + "Please contact with him.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = "Save successful without email and sent to HOD for " + (is_mnsp == 0 ? "approval" : "forwarding") + Environment.NewLine + "Please contact with him.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Requisition/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PRMain prmain = db.PRMains.Find(id);
            Requisition requisition = (Requisition)prmain.Convert(new Requisition());
            requisition.requisitionSub = (from p in prmain.PRSubs.AsEnumerable()
                                         join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                                         join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                                         join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                                         join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                                         join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                                         select new {p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit}
                                        ).Select(
                                        c => new RequisitionSub()
                                        {
                                            PRId = c.p.PRId, PRSubId = c.p.PRSubId, ProductId = c.p.ProductId,
                                            CurrentStock = c.p.CurrentStock, StockInTransit = c.p.StockInTransit,
                                            PendingRequisitionQuanity = c.p.PendingRequisitionQuanity, 
                                            HistoricalConsumption = c.p.HistoricalConsumption,
                                            HistoricalPeriod = c.p.HistoricalPeriod,
                                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay, 
                                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                                            FactoryETA = c.p.FactoryETA,
                                            BrandId = c.p.BrandId,
                                            BrandName = c.BrandName,
                                            PreferredSupplierId = c.p.PreferredSupplierId,
                                            SupplierName = c.SupplierName,
                                            Quantity = c.p.Quantity,
                                            QuantityUnit = c.QuantityUnit,
                                            QuantityUnitId = c.p.QuantityUnitId,
                                            CategoryId = c.CategoryId,
                                            ProductName = c.ItemName,
                                            BondBalance = c.p.BondBalance,
                                            CapacityId = c.p.CapacityId,
                                            RowNo = c.p.RowNo,
                                            Remarks = c.p.Remarks
                                        }).ToList();

            if (prmain == null)
            {
                return HttpNotFound();
            }

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName", requisition.FactoryId);
            return View(requisition);
        }

        // GET: /Requisition/QREdit/5
        [CAuthorize]
        public ActionResult QREdit(int? id, string no)
        {
            if (id == null && string.IsNullOrEmpty(no))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRMain prmain = null;

            if (id != null)
                prmain = db.PRMains.Find(id);
            if (prmain == null && !String.IsNullOrEmpty(no))
                prmain = db.PRMains.FirstOrDefault(c => c.PRNo.ToLower().Trim().Equals(no.ToLower().Trim()));
            //if (pimain == null && !String.IsNullOrEmpty(fileno))
            //    pimain = dbFund.PIMains.FirstOrDefault(c => c.PIFileNo.ToLower().Trim().Equals(fileno.ToLower().Trim()));

            if (prmain == null || (clsMain.getCurrentUser() != 4 && prmain.ApprovedType != 0 && prmain.ApprovedType != 10))
            {
                return HttpNotFound();
            }

            Requisition requisition = (Requisition)prmain.Convert(new Requisition());
            requisition.requisitionSub = (from p in prmain.PRSubs.AsEnumerable()
                                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                                          select new { p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit }
                                        ).Select(
                                        c => new RequisitionSub()
                                        {
                                            PRId = c.p.PRId,
                                            PRSubId = c.p.PRSubId,
                                            ProductId = c.p.ProductId,
                                            CurrentStock = c.p.CurrentStock,
                                            StockInTransit = c.p.StockInTransit,
                                            PendingRequisitionQuanityForItem = c.p.PendingRequisitionQuanity,
                                            PendingRequisitionQuanity = 0,
                                            HistoricalConsumption = c.p.HistoricalConsumption,
                                            HistoricalPeriod = c.p.HistoricalPeriod,
                                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                                            FactoryETA = c.p.FactoryETA,
                                            LeadTime = c.p.LeadTime,
                                            BrandId = c.p.BrandId,
                                            BrandName = c.BrandName,
                                            PreferredSupplierId = c.p.PreferredSupplierId,
                                            SupplierName = c.SupplierName,
                                            Quantity = c.p.Quantity,
                                            QuantityUnit = c.QuantityUnit,
                                            QuantityUnitId = c.p.QuantityUnitId,
                                            QuantityInKG = c.p.QuantityInKG,
                                            KGPerUnit = c.p.KGPerUnit,
                                            CategoryId = c.CategoryId,
                                            ProductName = c.ItemName,
                                            BondBalance = c.p.BondBalance,
                                            CapacityId = c.p.CapacityId,
                                            IsWithDuty = c.p.IsWithDuty, 
                                            BondCapacityCheck = false, 
                                            RowNo = c.p.RowNo,
                                            Remarks = c.p.Remarks,
                                            BondDeductSourceType = c.p.BondDeductSourceType
                                        }).ToList();

            var comid = dbProd.Factories.Find(requisition.FactoryId).CompanyId;
            var bp = db.Bonds.Where(l => l.CompanyId == comid).FirstOrDefault().BondPeriods.OrderBy(c => c.EndDate).Last();
            if (bp == null)
            {
                bp = new BondPeriod();
                bp.EndDate = DateTime.Parse("01-Jan-1900");
            }
            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName", requisition.FactoryId);
            ViewBag.BLSDate = bp.EndDate.ToString("dd-MMM-yyyy");
            ViewBag.BLEDate = bp.EndDate.AddDays(30).ToString("dd-MMM-yyyy");

            return View(requisition);
        }

        // POST: /Requisition/QRUpdate
        [HttpPost]
        [CAuthorize]
        public ActionResult QRUpdate([Bind(Include = "PRId, FactoryId, PRMonth, Remarks, Activity")] Requisition requisition, ICollection<RequisitionSub> requisitionSub)
        {
            try
            {
                PRMain prmain = db.PRMains.Find(requisition.PRId);
                prmain = (PRMain)requisition.ConvertNotNull(prmain);
                prmain.UpdatedBy = clsMain.getCurrentUser();
                prmain.UpdateTime = clsMain.getCurrentTime();

                if (requisitionSub != null)
                {
                    foreach (var rqSub in requisitionSub)
                    {
                        rqSub.BondProductId = rqSub.ProductId;
                        if (rqSub.PRSubId > 0)
                        {
                            PRSub prsub = db.PRSubs.Find(rqSub.PRSubId);
                            prsub = (PRSub)rqSub.ConvertNotNull(prsub);

                            if (prsub.BondDeductSourceType == 1)
                            {
                                prsub.IsWithDuty = 1;
                            }
                            else if (prsub.BondDeductSourceType == 4)
                            {
                                prsub.IsLocalPurchase = 1;
                            }

                            db.Entry(prsub).State = EntityState.Modified;
                        }
                        else
                        {
                            PRSub prsub = new PRSub(rqSub);

                            if (prsub.BondDeductSourceType == 1)
                            {
                                prsub.IsWithDuty = 1;
                            }
                            else if (prsub.BondDeductSourceType == 4)
                            {
                                prsub.IsLocalPurchase = 1;
                            }

                            prmain.PRSubs.Add(prsub);
                        }
                    }

                    var unsub = db.PRSubs.Where(l => l.PRId == prmain.PRId).ToList();

                    for (int ri = 0; ri < unsub.Count; ri++)
                    {
                        if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                            db.Entry(unsub[ri]).State = EntityState.Deleted;
                    }

                    db.Entry(prmain).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Json(new { status = "Update successful.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: /Requisition/QRUpdate
        [HttpPost]
        [CAuthorize]
        public ActionResult WeightUpdate(ICollection<RequisitionSub> requisitionSub)
        {
            try
            {
                string prno = "";
                if (requisitionSub != null)
                {
                    foreach (var rqSub in requisitionSub)
                    {
                        if (rqSub.PRSubId > 0)
                        {
                            PRSub prsub = db.PRSubs.Find(rqSub.PRSubId);
                            prsub.QuantityInKG = rqSub.QuantityInKG.Value;
                            prsub.KGPerUnit = rqSub.KGPerUnit.Value;
                            prno = prsub.PRMain.PRNo;

                            db.Entry(prsub).State = EntityState.Modified;
                        }
                    }
                }

                db.SaveChanges();

                return Json(new { status = "Weight update successful.", rqno = prno }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: /Requisition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [CAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PRId,FactoryId,PRDate,Remarks,Activity")] Requisition requisition, ICollection<RequisitionSub> requisitionSub)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(pRMain).State = EntityState.Modified;
                //db.SaveChanges();
                //return RedirectToAction("Index");
            }
            return View();
        }

        // GET: /Requisition/ApprovalPending
        [CAuthorize]
        public ActionResult ApprovalPending()
        {
            var requisitions = (from p in db.PRMains.AsEnumerable()
                                //join q in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals q.QuantityUnitId
                                join r in dbProd.UserFactories.AsEnumerable() on p.FactoryId equals r.FactoryId
                                where p.ApprovedType == 0 && p.HODApprovedType == 0
                                select new { p, QuantityUnit = "", r.ShortName }).Select(c => new Requisition()
                                {
                                    PRId = c.p.PRId,
                                    PRNo = c.p.PRNo,
                                    PRDate = c.p.PRDate,
                                    TotalQuantity = c.p.TotalQuantity,
                                    QuantityUnit = c.QuantityUnit,
                                    FactoryId = c.p.FactoryId,
                                    FactoryName = c.ShortName,
                                    IsActive = c.p.IsActive
                                }).ToList();
            
            ViewBag.is_mnsp = 0;
            return View(requisitions);
        }

        // GET: /Requisition/ApprovalPending
        [CAuthorize]
        public ActionResult PendingApproval()
        {
            var requisitions = (from p in db.PRMains.AsEnumerable()
                                //join q in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals q.QuantityUnitId
                                join r in dbProd.UserFactories.AsEnumerable() on p.FactoryId equals r.FactoryId
                                where p.ApprovedType == 0 && p.HODApprovedType == 1
                                select new { p, QuantityUnit = "", r.ShortName }).Select(c => new Requisition()
                                {
                                    PRId = c.p.PRId,
                                    PRNo = c.p.PRNo,
                                    PRDate = c.p.PRDate,
                                    TotalQuantity = c.p.TotalQuantity,
                                    QuantityUnit = c.QuantityUnit,
                                    FactoryId = c.p.FactoryId,
                                    FactoryName = c.ShortName,
                                    IsActive = c.p.IsActive
                                }).ToList();

            ViewBag.is_mnsp = 1;
            return View("ApprovalPending", requisitions);
        }

        // GET: /Requisition/Approval
        [CAuthorize]
        public ActionResult Approval(int id, ICollection<RequisitionSub> requisitionSub, byte apvtype, short is_mnsp = 0)
        {
            PRMain prmain = db.PRMains.Find(id);
            try
            {
                string app_link = "", _msg = "";
                short fac_id = 0, email_type_id = 0;

                if (is_mnsp == 1)
                {
                    if (prmain.HODApprovedType == 1 && prmain.ApprovedType == 0)
                        return Json(new { status = "Invalid", message = "Requistion # " + prmain.PRNo + " has already been forwarded.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
                    else if (prmain.HODApprovedType == 1 && prmain.ApprovedType == 1)
                        return Json(new { status = "Invalid", message = "Requistion # " + prmain.PRNo + " has already been approved.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
                    else if (prmain.HODApprovedType > 1 || prmain.ApprovedType > 1)
                        return Json(new { status = "Invalid", message = "Requistion # " + prmain.PRNo + " has already been disapproved.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);

                    prmain.HODApprovedType = apvtype;
                    prmain.HODId = clsMain.getCurrentUser();
                    prmain.HODApprovedTime = clsMain.getCurrentTime();
                    // prmain.PRDate = prmain.ApprovedTime;
                    
                    email_type_id = 2;
                    fac_id = prmain.FactoryId;
                    if (apvtype == 1)
                    {
                        _msg = "Requistion # " + prmain.PRNo + " has been approved and sent to sourcing for further processing.";
                    }
                    else
                    {
                        _msg = "Requistion # " + prmain.PRNo + " has been disapproved.";
                    }

                    app_link = @"http://192.168.0.47/ipms/report/mrr?__RequestVerificationToken=sKyQrtf_PxmyNAbQnwxvfNQZzuyx9vk6CPB19h8BO_5WlWJ6gPMsrL1ZoyR7xZEeXro5w4ZWVaXYvikdxR_U0qpOcaJnDuNmP9DPDr2A8sg1&fn=rptRequisitionProfile&FactoryId=" + prmain.FactoryId + "&Year=" + prmain.Year + "&PRId=" + prmain.PRId;
                    short is_app_to_next_approval = 0;
                            
                    foreach (var item in prmain.PRSubs)
                    {
                        if (!dbProd.ItemInfoes.Find(item.ProductId).ItemCode.Substring(0, 2).Equals("01"))
                        {
                            is_app_to_next_approval = 1;

                            break;
                        }
                    }

                    if (is_app_to_next_approval == 1 && apvtype == 1)
                    {
                        app_link = @"http://192.168.0.47/ipms/requisition/details_mnsp/" + prmain.PRId;
                        email_type_id = 11;
                        fac_id = 0;
                        if (apvtype == 1)
                        {
                            _msg = "Requistion # " + prmain.PRNo + " has been forwarded to EVP sir" + Environment.NewLine + "Please contact with him.";
                        }
                        else
                        {
                            _msg = "Requistion # " + prmain.PRNo + " has been disapproved.";
                        }
                    }
                    else
                    {
                        prmain.ApprovedType = apvtype;
                        prmain.ApprovedBy = clsMain.getCurrentUser();
                        prmain.ApprovedTime = clsMain.getCurrentTime();
                    }
                }
                else
                {
                    if (prmain.ApprovedType == 1)
                        return Json(new { status = "Invalid", message = "Requistion # " + prmain.PRNo + " has already been approved.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
                    else if (prmain.ApprovedType > 1)
                        return Json(new { status = "Invalid", message = "Requistion # " + prmain.PRNo +  " has already been disapproved.", rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);

                    app_link = @"http://192.168.0.47/ipms/report/mrr?__RequestVerificationToken=sKyQrtf_PxmyNAbQnwxvfNQZzuyx9vk6CPB19h8BO_5WlWJ6gPMsrL1ZoyR7xZEeXro5w4ZWVaXYvikdxR_U0qpOcaJnDuNmP9DPDr2A8sg1&fn=rptRequisitionProfile&FactoryId=" + prmain.FactoryId + "&Year=" + prmain.Year + "&PRId=" + prmain.PRId;

                    email_type_id = 2;
                    fac_id = prmain.FactoryId;
                    if (apvtype == 1)
                    {
                        _msg = "Requistion # " + prmain.PRNo + " has been approved and sent to sourcing.";
                    }
                    else
                    {
                        _msg = "Requistion # " + prmain.PRNo + " has been disapproved.";
                    }

                    prmain.ApprovedType = apvtype;
                    prmain.ApprovedBy = clsMain.getCurrentUser();
                    prmain.ApprovedTime = clsMain.getCurrentTime();
                    // prmain.PRDate = prmain.ApprovedTime;
                }

                foreach (var sub in requisitionSub)
                {
                    var reqSub = db.PRSubs.Find(sub.PRSubId);
                    reqSub.Quantity = sub.Quantity.Value;
                    reqSub.CETA = prmain.PRDate.AddDays(reqSub.LeadTime).Date;

                    if (prmain.ApprovedTime.AddDays(reqSub.LeadTime) > reqSub.FactoryETA)
                        reqSub.FactoryETA = prmain.ApprovedTime.AddDays(reqSub.LeadTime);
                    
                    reqSub.OriginalETA = reqSub.FactoryETA;

                    db.Entry(reqSub).State = EntityState.Modified;
                }

                db.Entry(prmain).State = EntityState.Modified;
                db.SaveChanges();

                var row = (from p in db.EmailLibraries.AsEnumerable()
                           join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                           where p.EmailTypeId == email_type_id && q.FactoryId == fac_id
                           select new
                           {
                               p.TypeName,
                               p.EmailBody,
                               q.EmailGroupFrom,
                               q.EmailGroupTo,
                               q.EmailGroupCC,
                               q.EmailGroupBCC,
                               p.Subject
                           }).FirstOrDefault();

                EMailCollectionView MailCollection = new EMailCollectionView();
                MailCollection.EmailType = row.TypeName;
                MailCollection.ModuleName = "IPMS";
                MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                if (email_type_id == 2)
                    MailCollection.EmailCC += "; " + clsMain.getEmail(prmain.CreatedBy.ToString());
                MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                MailCollection.EmailSubject = row.Subject.Replace("@RequisitionNo", prmain.PRNo);
                MailCollection.EmailBody = row.EmailBody.Replace("@RequisitionNo", prmain.PRNo)
                    .Replace("@Factory", dbProd.Factories.Find(prmain.FactoryId).ShortName)
                    .Replace("@Date", prmain.PRQDate.ToString("dd-MMM-yyyy"))
                    .Replace("@ApprovalLink", app_link);
                MailCollection.EmailAttachmentLink = "";
                MailCollection.CreateAttachment = 0;
                MailCollection.CreatedBy = clsMain.getCurrentUser();

                EMailCollection mail = new EMailCollection(MailCollection);

                dbEM.EMailCollections.Add(mail);

                dbEM.SaveChanges();

                return Json(new { status = "Success", message = _msg, rqno = prmain.PRNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Requisition/Delete/5
        [CAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRMain pRMain = db.PRMains.Find(id);
            if (pRMain == null)
            {
                return HttpNotFound();
            }
            return View(pRMain);
        }

        // POST: /Requisition/Delete/5
        [CAuthorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRMain pRMain = db.PRMains.Find(id);
            db.PRMains.Remove(pRMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CompanyInfo(int? id)
        {
            var comid = dbProd.Factories.Find(id).CompanyId;
            return Content(dbProd.Companies.Find(comid).CompanyName);
        }

        public ActionResult FactoryDetails(int? id, DateTime? rqdate)
        {
            string rqno = "RQ/" + dbProd.Factories.Find(id).Prefix + "/" + db.PRMains.Where(l => l.Year == rqdate.Value.Year && l.FactoryId == id).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + rqdate.Value.ToString("MM/yy").ToUpperInvariant();

            var comid = dbProd.Factories.Find(id).CompanyId;
            var bp = db.Bonds.Where(l => l.CompanyId == comid).FirstOrDefault().BondPeriods.OrderBy(c => c.EndDate).Last();
            if(bp == null)
            {
                bp = new BondPeriod();
                bp.EndDate = DateTime.Parse("01-Jan-1900");
            }

            return Json(new { comname = dbProd.Companies.Find(comid).CompanyName, blsdate = bp.EndDate.ToString("dd-MMM-yyyy"), bledate = bp.EndDate.AddDays(15).ToString("dd-MMM-yyyy"), rqno = rqno }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConsumptionTemplate(short id, short? facid)
        {
            var reqsub = (from p in db.PRSubs.AsEnumerable()
                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                          where p.PRSubId == id
                          select new { p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit }
                        ).Select(
                        c => new RequisitionSub()
                        {
                            PRId = c.p.PRId,
                            PRSubId = c.p.PRSubId,
                            ProductId = c.p.ProductId,
                            CurrentStock = c.p.CurrentStock,
                            StockInTransit = c.p.StockInTransit,
                            PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                            HistoricalConsumption = c.p.HistoricalConsumption,
                            HistoricalPeriod = c.p.HistoricalPeriod,
                            Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                            Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                            AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                            AverageConsumptionPerDay = c.p.AverageConsumptionPerDay,
                            DaysWithTotalStock = c.p.DaysWithTotalStock,
                            FactoryETA = c.p.FactoryETA,
                            LeadTime = c.p.LeadTime,
                            BrandId = c.p.BrandId,
                            BrandName = c.BrandName,
                            PreferredSupplierId = c.p.PreferredSupplierId,
                            SupplierName = c.SupplierName,
                            Quantity = c.p.Quantity,
                            QuantityUnit = c.QuantityUnit,
                            QuantityUnitId = c.p.QuantityUnitId,
                            CategoryId = c.CategoryId,
                            ProductName = c.ItemName,
                            BondBalance = c.p.BondBalance,
                            CapacityId = c.p.CapacityId,
                            RowNo = c.p.RowNo,
                            Remarks = c.p.Remarks
                        }).FirstOrDefault();

            return PartialView(reqsub);
        }

        public ActionResult RequisitionTemplate(RequisitionSub requisitionSub, short? facid)
        {
            if (requisitionSub.ProductId != null && requisitionSub.ProductId > 0)
            {
                requisitionSub.ItemCode = dbProd.ItemInfoes.Find(requisitionSub.ProductId).ItemCode;
                var qtyIdList = dbProd.ItemInfoes.Find(requisitionSub.ProductId).QuantityUnitList.Split(new string[] { ", " }, StringSplitOptions.None).ToList();
                var QtyList = dbProd.viewQuantityUnits.Where(l => qtyIdList.Contains(l.QuantityUnitId.ToString()));
                ViewBag.QuantityUnit = new SelectList(QtyList, "QuantityUnitId", "QuantityUnit", requisitionSub.QuantityUnitId);
            }
            else
            {
                ViewBag.QuantityUnit = new SelectList(dbProd.viewQuantityUnits.Where(l => l.QuantityUnitId == 0), "QuantityUnitId", "QuantityUnit");
            }

            //if(Session["UserId"].ToString().Equals("42"))
            //{
            //    ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => c.CategoryId == 13), "ItemId", "ItemName");
            //}
            //else
                ViewBag.Product = new SelectList(dbProd.ItemInfoes.Join(dbProd.ItemFactories, p => p.ItemId, q => q.ItemId, (p, q) => new { item = p, factory = q }).Where(l => l.factory.FactoryId == facid).Select(c => c.item).Distinct(), "ItemId", "ItemName", requisitionSub.ProductId);
            
            //if (requisitionSub == null || requisitionSub.CategoryId == null)
            //    ViewBag.Category = new SelectList(dbProd.ItemCategories.Join(dbProd.ItemInfoes.Join(dbProd.ItemFactories, i => i.ItemId, f => f.ItemId, (i, f) => new { Item = i, Fact = f }).Where(c => c.Fact.FactoryId == facid), p => p.CategoryId, q => q.Item.CategoryId, (p, q) => new { cat = p, item = q.Item }).Where(l => l.cat.StageLevel == 4).Select(c => new { CategoryId = c.cat.CategoryId, CategoryName = c.cat.CategoryName }).Distinct(), "CategoryId", "CategoryName");
            //else
            ViewBag.Category = new SelectList(dbProd.ItemCategories.Join(dbProd.ItemInfoes.Join(dbProd.ItemFactories, i => i.ItemId, f => f.ItemId, (i, f) => new { Item = i, Fact = f }).Where(c => c.Fact.FactoryId == facid), p => p.CategoryId, q => q.Item.CategoryId, (p, q) => new { cat = p, item = q.Item }).Where(l => l.cat.StageLevel == 4).Select(c => new { CategoryId = c.cat.CategoryId, CategoryName = c.cat.CategoryName }).Distinct().OrderBy(c => c.CategoryName), "CategoryId", "CategoryName", requisitionSub.CategoryId); //(Session["UserId"].ToString().Equals("42") ? 13 : requisitionSub.CategoryId));
            //ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(c => c.CategoryId == 0), "CategoryId", "CategoryName");
            ViewBag.Supplier = new SelectList(db.Suppliers.Where(c => c.ApprovedType == 1), "SupplierId", "SupplierName", (requisitionSub == null || requisitionSub.PreferredSupplierId == null ? 0 : requisitionSub.PreferredSupplierId));
            ViewBag.Brand = new SelectList(db.Brands, "BrandId", "BrandName", (requisitionSub == null || requisitionSub.BrandId == null ? 0 : requisitionSub.BrandId));

            if (requisitionSub.PRId > 0)
                requisitionSub.CETA = db.PRMains.Find(requisitionSub.PRId).PRQDate.AddDays(Convert.ToInt16(requisitionSub.LeadTime));
            else
                requisitionSub.CETA = clsMain.getCurrentTime().AddDays(Convert.ToInt16(requisitionSub.LeadTime));

            return PartialView(requisitionSub);
        }

        //[HttpPost]
        public ActionResult RowTemplate(RequisitionSub requisitionSub, bool? isbutton, int? iskgupdate)
        {
            isbutton = isbutton ?? false;
            if (requisitionSub != null && requisitionSub.PreferredSupplierId != null)
                requisitionSub.SupplierName = db.Suppliers.Find(requisitionSub.PreferredSupplierId).ShortName;

            ViewBag.isbutton = isbutton;
            ViewBag.iskgupdate = iskgupdate;
            return PartialView(requisitionSub);
        }

        //[HttpPost]
        public ActionResult ProductList(short? id, short? facid, short? value)
        {
            if(facid == null || facid == 0)
                ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(l => id == null || l.CategoryId == id).OrderBy(c => c.ItemName.Length).ThenBy(c => c.ItemName), "ItemId", "ItemName", value);
            else
                ViewBag.Product = new SelectList((from p in dbProd.ItemInfoes.AsEnumerable() 
                                                 join q in dbProd.ItemFactories.AsEnumerable() on p.ItemId equals q.ItemId 
                                                 //join r in db.viewFactories.AsEnumerable() on q.FactoryId equals r.FactoryId
                                                 //join s in db.Bonds.AsEnumerable() on r.CompanyId equals s.CompanyId
                                                 where q.FactoryId == facid && (id == null || p.CategoryId == id)
                                                 select p).OrderBy(c => c.ItemName.Length).ThenBy(c => c.ItemName), "ItemId", "ItemName", value);

            return PartialView();
        }

        //[HttpPost]
        public ActionResult ItemDetails(int id, short? facid, DateTime? fdate, DateTime? tdate, int? prsubid, DateTime? prdate)
        {
            try
            {
                if(prsubid != null && prsubid > 0)
                {
                    var hp = db.PRSubs.Find(prsubid).HistoricalPeriod.Split(new string[] { " - "}, StringSplitOptions.None);
                    fdate = DateTime.Parse(hp[0].Trim());
                    tdate = DateTime.Parse(hp[1].Trim());
                    prdate = db.PRSubs.Find(prsubid).PRMain.PRDate.Date;
                }
                db.Database.CommandTimeout = 300;
                var items = db.Database.SqlQuery<ItemDetails>("Exec prcItemDetails " + id + ", " + facid + ", " + (fdate.HasValue ? "'" + fdate.Value.ToString("dd-MMM-yyyy") + "'" : "null") + ", " + (tdate.HasValue ? "'" + tdate.Value.ToString("dd-MMM-yyyy") + "'" : "null") + ", " + (prdate.HasValue ? "'" + prdate.Value.ToString("dd-MMM-yyyy") + "'" : "''")).ToList();
                ItemDetails item = new ItemDetails();

                int leadtime = (from p in db.PRMains.AsEnumerable()
                                join q in db.PRSubs.AsEnumerable() on p.PRId equals q.PRId
                                where p.FactoryId == facid && q.ProductId == id
                                select new { p.PRDate, q.FactoryETA }).OrderByDescending(c => c.PRDate).Select(c => c.FactoryETA.Subtract(c.PRDate).Days).FirstOrDefault();

                var lclist = db.PipelineMains.Where(l => l.IsLastLot == 1).Select(c => c.LCFileId).Distinct();
                var pl = (from p in db.PipelineMains.AsEnumerable()
                          join q in db.PipelineSubs.AsEnumerable() on p.PLEId equals q.PLEId
                          join r in dbFund.PISubs.AsEnumerable() on q.PIFileSubId equals r.PIFileSubId
                          join s in dbFund.LCMains.AsEnumerable() on p.LCFileId equals s.LCFileId
                          join t in dbProd.viewQuantityUnits.AsEnumerable() on r.QuantityUnitId equals t.QuantityUnitId
                          where (p.IsLotReceived == 0 || (p.IsLotReceived == 1 && p.LotReceivedDate > tdate)) && q.ItemId == id && r.FactoryId == facid
                          select new { LCNo = s.LCNo, ETA = p.StoreArivalDate.ToString("dd-MMM-yyyy"), PLQty = q.StockInTransit.ToString("##,###.000"), QUnit = t.QuantityUnit }
                         ).Union
                         (from p in dbFund.LCMains.AsEnumerable()
                          join q in dbFund.LCWisePIs.AsEnumerable() on p.LCFileId equals q.LCFileId
                          join r in dbFund.PISubs.AsEnumerable() on q.PIFileId equals r.PIFileId
                          join s in db.PRSubs.AsEnumerable() on r.PRSubId equals s.PRSubId
                          join w in dbProd.viewQuantityUnits.AsEnumerable() on r.QuantityUnitId equals w.QuantityUnitId
                          join u in db.PipelineSubs.GroupBy(c => c.PIFileSubId).Select(c => new { PIFileSubId = c.Key, PLQty = c.Sum(l => l.StockInTransit) }).AsEnumerable() on r.PIFileSubId equals u.PIFileSubId into v
                          from t in v.DefaultIfEmpty()
                          where r.PIQuantity - (t == null ? 0 : t.PLQty) > 0 && !lclist.Contains(p.LCFileId)
                                && r.ProductId == id && r.FactoryId == facid
                          select new { p.LCNo, ETA = s.FactoryETA.ToString("dd-MMM-yyyy"), PLQty = (r.PIQuantity - (t == null ? 0 : t.PLQty)).ToString("##,###.000"), QUnit = w.QuantityUnit }
                          //).Union
                          //(from p in db.QuantityApprovalSubs.Include(c => c.PRSub).AsEnumerable()
                          // join q in dbFund.PISubs.AsEnumerable() on p.QApprovalSubId equals q.QApprovalSubId
                          // join r in dbFund.LCWisePIs.AsEnumerable() on q.PIFileId equals r.PIFileId
                          // join s in dbFund.LCMains.AsEnumerable() on r.LCFileId equals s.LCFileId
                          // join t in dbProd.viewQuantityUnits.AsEnumerable() on p.PRSub.QuantityUnitId equals t.QuantityUnitId
                          // where (p.IsLotReceived == 0 || (p.IsLotReceived == 1 && p.LotReceivedDate > tdate)) && q.ItemId == id && r.FactoryId == facid
                          // select new { LCNo = s.LCNo, ETA = p.StoreArivalDate.ToString("dd-MMM-yyyy"), PLQty = q.StockInTransit.ToString("##,###.000"), QUnit = t.QuantityUnit }
                          ).ToList();

                if (items.Count == 0)
                {
                    //items.Add(new ItemDetails());
                }
                else
                {
                    item = items.GroupBy(c => new { c.ItemId, c.ItemCode, c.Pipeline, c.BondBalance, c.CapacityId, c.BondCapacityCheck, c.ReqPendingQuantity, c.ReqPendingQuantityForItem, c.FDate, c.TDate })
                    .Select(c => new ItemDetails()
                    {
                        ItemId = c.Key.ItemId,
                        ItemCode = c.Key.ItemCode,
                        PresentStock = c.Sum(l => l.PresentStock),
                        N3MonthConsmLY = c.Sum(l => l.N3MonthConsmLY),
                        P3MonthConsm = c.Sum(l => l.P3MonthConsm),
                        Pipeline = c.Key.Pipeline,
                        ReqPendingQuantity = c.Key.ReqPendingQuantity,
                        ReqPendingQuantityForItem = c.Key.ReqPendingQuantityForItem,
                        CapacityId = c.Key.CapacityId,
                        BondBalance = c.Key.BondBalance,
                        FDate = c.Key.FDate,
                        TDate = c.Key.TDate,
                        BondCapacityCheck = c.Key.BondCapacityCheck
                    }).FirstOrDefault();
                }

                return Json(new { items = item, pl = pl, lt = leadtime, feta = clsMain.getCurrentTime().AddDays(leadtime).ToString("dd-MMM-yyyy") }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw new Exception("Error");
            }
        }

        [HttpGet]
        public JsonResult GetLeadTime(int id,int categoryId)
        {
            var ItemParam = new SqlParameter("@ItemId", id);
            var CategoryParam = new SqlParameter("@CategoryId", categoryId);
            var leatime = db.Database.SqlQuery<int>("EXEC prcrptAvgLeadTime @ItemId,@CategoryId", ItemParam, CategoryParam);

            return Json(leatime, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SupplierDetails(short id, DateTime? reqDate, short? facid)
        {
            Supplier supplier = db.Suppliers.Find(id);
            DateTime FETA = reqDate.Value;

            if (reqDate.HasValue)
                reqDate = reqDate.Value.AddDays(supplier.LeadTime);
            else
                reqDate = clsMain.getCurrentTime().AddDays(supplier.LeadTime);

            var comid = dbProd.Factories.Find(facid).CompanyId;
            var bp = db.Bonds.Where(l => l.CompanyId == comid).FirstOrDefault().BondPeriods.OrderBy(c => c.EndDate).Last();
            if(bp == null)
            {
                bp = new BondPeriod();
                bp.EndDate = DateTime.Parse("01-Jan-1900");
            }

            if (reqDate >= bp.EndDate && reqDate <= bp.EndDate.AddDays(15))
                FETA = bp.EndDate.AddDays(15);
            else
                FETA = reqDate.Value;

            return Json(new { LeadTime = supplier.LeadTime, BrandId = supplier.BrandId, FETA = FETA.ToString("dd-MMM-yyyy"), CETA = reqDate.Value.ToString("dd-MMM-yyyy") }, JsonRequestBehavior.AllowGet);
            //return Json(supplier, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult QuantityUnitList(short id, short? value)
        {
            var qtyIdList = dbProd.ItemInfoes.Find(id).QuantityUnitList.Split(new string[] { ", " }, StringSplitOptions.None).ToList();
            value = (value == null || value == 0 ? dbProd.ItemInfoes.Find(id).DefaultQUnitId : value);
            var QtyList = dbProd.viewQuantityUnits.Where(l => qtyIdList.Contains(l.QuantityUnitId.ToString()));
            ViewBag.QuantityUnit = new SelectList(QtyList, "QuantityUnitId", "QuantityUnit", value);
            return PartialView();
        }

        [HttpPost]
        public ActionResult QuantityUnitListInJson(short id, short? value)
        {
            var qtylist = dbProd.ItemUnits.Where(c => c.ItemId == id).Select(c => new { c.UnitId, c.KGProportion }).ToList();
            var qty_unit_list = string.Join(", ", qtylist.Select(m => "\"" + m.UnitId + "\"" + ":" + m.KGProportion).ToArray()); 

            return Json(qty_unit_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequisitionDetails(ICollection<RequisitionSub> requisitionSub)
        {
            Requisition requisition = new Requisition();
            requisition.requisitionSub = requisitionSub;

            return PartialView(requisition);
        }

        public ActionResult PRList(short? fid, short? year)
        {
            ViewBag.PRId = new SelectList(db.PRMains.Where(l => l.FactoryId == fid && l.ApprovedTime.Year == year), "PRId", "PRNo");

            return PartialView();
        }

        public JsonResult GetMaxBondate(int FactoryId)
        {

            string sql = "SELECT MAX(C.EndDate) endate FROM dbo.Bond a " +
                "INNER JOIN dbo.viewFactory b ON b.CompanyId = a.CompanyId " +
                "INNER JOIN dbo.BondPeriod C ON C.BondId = a.BondId " +
                "WHERE b.FactoryId = @FactoryId";

            //var maxdate= db.Database.SqlQuery<DateTime>(sql).FirstOrDefault();

            var maxdate = db.Database.SqlQuery<DateTime?>(sql, new SqlParameter("@FactoryId", FactoryId)).FirstOrDefault();


            string formattedDate = maxdate.HasValue ? maxdate.Value.ToString("yyyy-MM-dd") : string.Empty;

            return Json(new { maxDate = formattedDate }, JsonRequestBehavior.AllowGet);
        }
    }
}
