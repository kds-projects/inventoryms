﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class ApprovalController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();

        // GET: /Approval/
        public ActionResult Index()
        {
            var purchaseorders = db.PurchaseOrders.Include(p => p.Bond);
            return View(purchaseorders.ToList());
        }

        // GET: /Approval/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        // GET: /Approval/Create
        [CAuthorize]
        public ActionResult QApprovalPending()
        {
            var v = (from p in db.PurchaseOrders.AsEnumerable()
                     join q in db.PurchaseOrderSubs.AsEnumerable() on p.POId equals q.POId
                     join u in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals u.FactoryId
                     join r in db.QuantityApprovalSubs.GroupBy(c => c.POSubId).Select(c => new { POSubId = c.Key, AppQty = c.Sum(l => l.ApprovedQuantity) }).AsEnumerable() on q.POSubId equals r.POSubId into s
                     from t in s.DefaultIfEmpty()
                     where q.Quantity - (t == null ? 0 : t.AppQty) > 0
                     select p
                ).Distinct().ToList();

            ViewBag.type = "Q";
            return View(v);
        }

        // GET: /Approval/Create
        [CAuthorize]
        public ActionResult QuotationPending()
        {
            var v = (from p in db.QuantityApprovalMains.AsEnumerable()
                     join q in db.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                     join u in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals u.FactoryId
                     join s in db.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals s.QApprovalSubId into t
                     from r in t.DefaultIfEmpty()
                     where r == null //&& q.IsPriceApproved == 0
                     select p
                     ).Distinct().ToList();

            ViewBag.type = "QT";
            return View("PApprovalPending", v);
        }

        // GET: /Approval/Create
        [CAuthorize]
        public ActionResult PApprovalPending()
        {
            var va = (from p in db.QuantityApprovalMains.AsEnumerable()
                      join q in db.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                      join u in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals u.FactoryId
                      join v in db.QuotationMains.AsEnumerable() on q.QApprovalSubId equals v.QApprovalSubId
                      join w in db.QuotationSubs.GroupBy(c => new { c.SupplierId, c.QuotationId }).Select(c => new { QuotationId = c.Key.QuotationId, SupplierCount = c.Count() }).Distinct().AsEnumerable() on v.QuotationId equals w.QuotationId
                      join y in db.PriceApprovalSubs.AsEnumerable() on v.QuotationId equals y.QuotationId into z
                      from x in z.DefaultIfEmpty()
                      where x == null && w.SupplierCount >= 1 //&& q.IsPriceApproved == 0
                      select p
                     ).Distinct().ToList();

            ViewBag.type = "P";
            return View(va);
        }

        // GET: /Approval/Create
        [CAuthorize]
        public ActionResult QuantityApproval(int id)
        {
            PurchaseOrder purchaseOrder = (from p in db.PurchaseOrders.AsEnumerable()
                                           join q in db.PurchaseOrderSubs.AsEnumerable() on p.POId equals q.POId
                                           join r in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals r.FactoryId
                                           where p.POId == id
                                           select p).Distinct().FirstOrDefault();

            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }

            try
            {
                ViewBag.qano = "QA/" + db.PurchaseOrders.Find(id).BondId.ToString("00") + "/" + db.QuantityApprovalMains.Where(l => l.QApprovalDate.Year == DateTime.Today.Year && l.PurchaseOrder.BondId == purchaseOrder.BondId).Select(c => c.SerialNo + 1).DefaultIfEmpty(1).Max().Value.ToString("0000") + "-" + DateTime.Today.ToString("MM/yy").ToUpper();
            }
            catch(Exception ex)
            {
                ViewBag.errormessage = "Error due to : " + ex.Message;
            }

            return View(purchaseOrder);
        }

        [CAuthorize]
        public ActionResult QuantityApprovalSave(QuantityApproval quantityApproval, ICollection<QuantityApprovalDetails> quantityApprovalDetails)
        {
            try
            {
                QuantityApprovalMain qa = new QuantityApprovalMain(quantityApproval);

                PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(qa.POId);
                qa.QApprovalDate = db.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                qa.QApprovalNo = "QA/" + db.PurchaseOrders.Find(qa.POId).BondId.ToString("00") + "/" + db.QuantityApprovalMains.Where(l => l.QApprovalDate.Year == qa.QApprovalDate.Year && l.PurchaseOrder.BondId == purchaseOrder.BondId).Select(c => c.SerialNo + 1).DefaultIfEmpty(1).Max().Value.ToString("0000") + "-" + qa.QApprovalDate.ToString("MM/yy").ToUpper();


                foreach (QuantityApprovalDetails sub in quantityApprovalDetails)
                {
                    QuantityApprovalSub QASub = new QuantityApprovalSub(sub);
                    qa.QuantityApprovalSubs.Add(QASub);
                }

                db.QuantityApprovalMains.Add(qa);

                db.SaveChanges();
                return Json(new { status = "Quantity approved successfully.", qano = qa.QApprovalNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("Quantity approval failed.\nReason: " + ex.Message);
            }
        }

        // GET: /Approval/Edit/5
        [CAuthorize]
        public ActionResult Quotation(int id)
        {
            QuantityApprovalMain quantityApproval = (from p in db.QuantityApprovalMains.AsEnumerable()
                                                     join q in db.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                                                     join r in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals r.FactoryId
                                                     join s in db.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals s.QApprovalSubId into t
                                                     from u in t.DefaultIfEmpty()
                                                     where p.QApprovalId == id && u == null
                                                     select p).Distinct().FirstOrDefault();

            if (quantityApproval == null)
            {
                return HttpNotFound();
            }

            return View(quantityApproval);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [CAuthorize]
        public ActionResult QuotationSave(int? id, ICollection<QuotationView> quotationView, ICollection<QuotationSubView> quotationSubView)
        {
            if (quotationView != null && id > 0)
            {
                for (int i = 0; i < quotationView.ToList().Count; i++)
                {
                    QuotationView qview = quotationView.ToList()[i];

                    if (qview.QuotationId == 0)
                    {
                        QuotationMain quotation = new QuotationMain(qview);

                        if (quotationSubView != null)
                        {
                            foreach (QuotationSubView sub in quotationSubView.Where(l => l.QVId == qview.QVId))
                            {
                                QuotationSub qSub = new QuotationSub(sub);
                                quotation.QuotationSubs.Add(qSub);
                            }

                            db.QuotationMains.Add(quotation);
                        }
                    }
                    else
                    {
                        QuotationMain quotation = db.QuotationMains.Find(qview.QuotationId);
                        quotation = (QuotationMain)qview.ConvertNotNull(quotation);

                        if (quotationSubView != null)
                        {
                            foreach (QuotationSubView sub in quotationSubView.Where(l => l.QVId == qview.QVId && l.QuotationId == quotation.QuotationId))
                            {
                                if (sub.QuotationSubId > 0)
                                {
                                    QuotationSub qSub = db.QuotationSubs.Find(sub.QuotationSubId);
                                    qSub = (QuotationSub)sub.ConvertNotNull(qSub);

                                    db.Entry(qSub).State = EntityState.Modified;
                                }
                                else
                                {
                                    QuotationSub qSub = new QuotationSub(sub);
                                    quotation.QuotationSubs.Add(qSub);
                                }
                            }

                            var unsub = db.QuotationSubs.Where(l => l.QuotationId == quotation.QuotationId && l.QuotationMain.QApprovalSubId == quotation.QApprovalSubId).ToList();

                            for (int ri = 0; ri < unsub.Count; ri++)
                            {
                                if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                                    db.Entry(unsub[ri]).State = EntityState.Deleted;
                            }

                            db.Entry(quotation).State = EntityState.Modified;
                        }
                    }
                }

                var poSubs = db.QuantityApprovalMains.Find(id).QuantityApprovalSubs.Where(l => l.PriceApprovalSubs.Count == 0).Select(c => c.QApprovalSubId).ToList();

                var dQList = db.QuotationMains.Where(l => poSubs.Contains(l.QApprovalSubId));

                foreach(QuotationMain qm in dQList)
                {
                    if (db.Entry(qm).State == EntityState.Unchanged)
                        db.Entry(qm).State = EntityState.Deleted;
                    else if (db.QuotationSubs.Where(l => l.QuotationId == qm.QuotationId).Count() == 0)
                        db.Entry(qm).State = EntityState.Deleted;
                }

                try
                {
                    db.SaveChanges();
                    return Content("Quotation save successful.");
                }
                catch (Exception ex)
                {
                    return Content("Quotation saved failed.\nReason: " + ex.Message);
                }
            }

            //PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(POId);
            //purchaseOrder.quotationView = quotationView;
            //purchaseOrder.quotationSubView = quotationSubView;
            //return View(purchaseOrder);
            return Content("Quotation saved failed.\nReason: No quotation avaliable.");
        }

        // GET: /Approval/Edit/5
        [CAuthorize]
        public ActionResult PriceApproval(int id)
        {
            QuantityApprovalMain quantityApproval = (from p in db.QuantityApprovalMains.AsEnumerable()
                                                     join q in db.QuantityApprovalSubs.AsEnumerable() on p.QApprovalId equals q.QApprovalId
                                                     join u in dbProd.UserFactories.AsEnumerable() on q.PRMain.FactoryId equals u.FactoryId
                                                     join v in db.QuotationMains.AsEnumerable() on q.QApprovalSubId equals v.QApprovalSubId
                                                     join w in db.QuotationSubs.GroupBy(c => new { c.SupplierId, c.QuotationId }).Select(c => new { QuotationId = c.Key.QuotationId, SupplierCount = c.Count() }).Distinct().AsEnumerable() on v.QuotationId equals w.QuotationId
                                                     join y in db.PriceApprovalSubs.AsEnumerable() on q.QApprovalSubId equals y.QApprovalSubId into z
                                                     from x in z.DefaultIfEmpty()
                                                     where x == null && w.SupplierCount >= 1 && p.QApprovalId == id 
                                                     select p).ToList<QuantityApprovalMain>().FirstOrDefault();

            if (quantityApproval == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(quantityApproval);
        }

        // GET: /Approval/Save
        [CAuthorize]
        [HttpPost]
        public ActionResult PriceApprovalSave(int id, ICollection<int> priceApprovalSub)
        {
            try
            {
                QuantityApprovalMain QAMain = db.QuantityApprovalMains.Find(id);
                PriceApprovalMain PAMain = new PriceApprovalMain(QAMain);

                //PAMain.ApprovedType = 1;
                PAMain.ApprovedBy = Int16.Parse(Session["UserId"].ToString());
                PAMain.PApprovalDate = db.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                PAMain.PApprovalNo = "PA/" + QAMain.PurchaseOrder.BondId.ToString("00") + "/" + db.PriceApprovalMains.Where(l => l.PApprovalDate.Year == PAMain.PApprovalDate.Year && l.PurchaseOrder.BondId == PAMain.PurchaseOrder.BondId).Select(c => c.SerialNo + 1).DefaultIfEmpty(1).Max().Value.ToString("0000") + "-" + PAMain.PApprovalDate.ToString("MM/yy").ToUpper();

                foreach (var sub in priceApprovalSub)
                {
                    QuotationSub qtsub = db.QuotationSubs.Find(sub);
                    if (db.PriceApprovalSubs.Where(l => l.QuotationId == qtsub.QuotationId).Count() > 0)
                    {
                        return Json(new { status = "Error", message = "Some item has already approved." }, JsonRequestBehavior.AllowGet);
                    }

                    PriceApprovalSub PASub = new PriceApprovalSub(qtsub.QuotationMain);
                    PASub = (PriceApprovalSub)qtsub.ConvertNotNull(PASub);
                    PASub.ApprovedPrice = qtsub.QuotationPrice;

                    PAMain.PriceApprovalSubs.Add(PASub);

                    QuantityApprovalSub qasub = db.QuantityApprovalSubs.Find(PASub.QApprovalSubId);
                    qasub.IsPriceApproved = 1;
                    db.Entry(qasub).State = EntityState.Modified;
                }

                db.PriceApprovalMains.Add(PAMain);
                db.SaveChanges();

                return Json(new { status = "Success", pano = PAMain.PApprovalNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Approval/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            if (purchaseOrder == null)
            {
                return HttpNotFound();
            }
            return View(purchaseOrder);
        }

        // POST: /Approval/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PurchaseOrder purchaseOrder = db.PurchaseOrders.Find(id);
            db.PurchaseOrders.Remove(purchaseOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult RowTemplate(int? id, string type)
        {
            ICollection<RequisitionView> reqSubs;

            if (type == "P" || type == "QT")
            {
                reqSubs = (from o in db.QuantityApprovalMains.Find(id).QuantityApprovalSubs.AsEnumerable()
                           join po in db.PurchaseOrderSubs.AsEnumerable() on o.POSubId equals po.POSubId
                           join p in db.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                           join uf in dbProd.UserFactories.AsEnumerable() on p.PRMain.FactoryId equals uf.FactoryId
                           join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                           join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                           join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                           join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                           join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                           join v in db.QuotationMains.AsEnumerable() on o.QApprovalSubId equals v.QApprovalSubId into x
                           from w in x.DefaultIfEmpty()
                           join xa in db.PriceApprovalSubs.AsEnumerable() on o.QApprovalSubId equals xa.QApprovalSubId into xb
                           from y in xb.DefaultIfEmpty()
                           where y == null
                           select new { po, p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o, QuotationId = (w == null ? 0 : w.QuotationId) }
                            ).Select(c => new RequisitionView()
                            {
                                POId = c.po.POId,
                                POSubId = c.po.POSubId,
                                QApprovalId = c.o.QApprovalId,
                                QApprovalSubId = c.o.QApprovalSubId,
                                QuotationId = c.QuotationId,
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                PONo = c.po.PurchaseOrder.PONo,
                                PODate = c.po.PurchaseOrder.PODate,
                                QANo = c.o.QuantityApprovalMain.QApprovalNo,
                                QADate = c.o.QuantityApprovalMain.QApprovalDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.po.Quantity,
                                ApprovedQuantity = c.o.ApprovedQuantity,
                                CategoryId = c.CategoryId,
                                CategoryName = c.CategoryName,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.p.Remarks
                            }).ToList();
            }
            else if (type == "PA")
            {
                reqSubs = (from o in db.QuantityApprovalMains.Find(id).QuantityApprovalSubs.AsEnumerable()
                           join po in db.PurchaseOrderSubs.AsEnumerable() on o.POSubId equals po.POSubId
                           join p in db.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                           join uf in dbProd.UserFactories.AsEnumerable() on p.PRMain.FactoryId equals uf.FactoryId
                           join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                           join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                           join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                           join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                           join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                           join v in db.QuotationMains.AsEnumerable() on o.QApprovalSubId equals v.QApprovalSubId
                           join w in db.QuotationSubs.GroupBy(c => new { c.SupplierId, c.QuotationId }).Select(c => new { QuotationId = c.Key.QuotationId, SupplierCount = c.Count() }).Distinct().AsEnumerable() on v.QuotationId equals w.QuotationId 
                           join y in db.PriceApprovalSubs.AsEnumerable() on o.QApprovalSubId equals y.QApprovalSubId into z
                           from x in z.DefaultIfEmpty()
                           where x == null && w.SupplierCount >= 1 
                           select new { po, p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o, QuotationId = (w == null ? 0 : w.QuotationId) }
                            ).Distinct().Select(c => new RequisitionView()
                            {
                                POId = c.po.POId,
                                POSubId = c.po.POSubId,
                                QApprovalId = c.o.QApprovalId,
                                QApprovalSubId = c.o.QApprovalSubId,
                                QuotationId = c.QuotationId,
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                PONo = c.po.PurchaseOrder.PONo,
                                PODate = c.po.PurchaseOrder.PODate,
                                QANo = c.o.QuantityApprovalMain.QApprovalNo,
                                QADate = c.o.QuantityApprovalMain.QApprovalDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.po.Quantity,
                                ApprovedQuantity = c.o.ApprovedQuantity,
                                CategoryId = c.CategoryId,
                                CategoryName = c.CategoryName,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.p.Remarks
                            }).ToList();
            }
            else
            {
                var PO = db.PurchaseOrders.Find(id);
                var prodlist = PO.PurchaseOrderSubs.Join(db.PRSubs, p => p.PRSubId, q => q.PRSubId, (p, q) => new {PO = p, R = q}).Select(c => c.R.ProductId.ToString()).ToList().Aggregate((i,j)=>i + "," +j);
                db.Database.CommandTimeout = 300;
                //var iwbb = db.Database.SqlQuery<ItemWiseBondBalance>("Exec prcItemWiseBondBalance '" + prodlist + "', " + PO.BondId + ", " + PO.BondPeriodId).ToList();
                reqSubs = (from por in db.PurchaseOrders.AsEnumerable()
                           join po in db.PurchaseOrderSubs.AsEnumerable() on por.POId equals po.POId
                           join p in db.PRSubs.AsEnumerable() on po.PRSubId equals p.PRSubId
                           join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                           join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                           join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                           join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                           join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                           join v in db.PRMains.AsEnumerable() on p.PRId equals v.PRId
                           join uf in dbProd.UserFactories.AsEnumerable() on v.FactoryId equals uf.FactoryId
                           join w in db.QuantityApprovalSubs.GroupBy(c => c.POSubId).Select(c => new { POSubId = c.Key, AppQty = c.Sum(l => l.ApprovedQuantity) }).AsEnumerable() on po.POSubId equals w.POSubId into x
                           from y in x.DefaultIfEmpty()
                           //join z in iwbb.AsEnumerable() on s.ItemId equals z.ProductId into za
                           //from zb in za.DefaultIfEmpty()
                           where por.POId == id && po.Quantity - (y == null ? 0 : y.AppQty) > 0
                           //select new { po, p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, v.PRNo, AppQty = (y == null ? 0 : y.AppQty), BondSL = (zb == null ? 0 : zb.CapacityId), BondBalance = (zb == null ? 0 : zb.BalanceQuantity) }
                           select new { po, p, SupplierName = q.ShortName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, v.PRNo, AppQty = (y == null ? 0 : y.AppQty), BondSL = 0, BondBalance = 0 }
                            ).Select(c => new RequisitionView()
                            {
                                POId = c.po.POId,
                                POSubId = c.po.POSubId,
                                PRId = c.p.PRId,
                                PRSubId = c.p.PRSubId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                PONo = c.po.PurchaseOrder.PONo,
                                PODate = c.po.PurchaseOrder.PODate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.po.Quantity,
                                ApprovedQuantity = c.AppQty,
                                CategoryId = c.CategoryId,
                                CategoryName = c.CategoryName,
                                ProductName = c.ItemName,
                                RowNo = c.p.RowNo,
                                Remarks = c.p.Remarks
                            }).Distinct().ToList();
            }

            ViewBag.type = type;
            return PartialView(reqSubs);
        }

        public ActionResult QApprovalDetails(ICollection<POSub> poSub)
        {
            POView po = new POView();
            po.poSub = poSub;

            return PartialView("QApprovalDetails", po);
        }

        public ActionResult PriceQuotation()
        {
            return PartialView();
        }

        public ActionResult QuotationDetails(QuantityApprovalMain quantityApproval)
        {
            return PartialView(quantityApproval);
        }

        public ActionResult ItemWiseQuotationDetails(int id)
        {
            QuotationMain quotationMain = db.QuotationMains.Where(l => l.QApprovalSubId == id).FirstOrDefault();
            return PartialView("PreviewQuotation", quotationMain);
        }

        public ActionResult PreviewQuotation(int? id, ICollection<QuotationView> quotationView, ICollection<QuotationSubView> quotationSubView)
        {
            QuotationPreview qp = new QuotationPreview();

            var requisitionView = (from o in db.QuantityApprovalMains.Find(id).QuantityApprovalSubs.AsEnumerable()
                          join po in db.PurchaseOrderSubs.AsEnumerable() on o.POSubId equals po.POSubId
                          join p in db.PRSubs.AsEnumerable() on o.PRSubId equals p.PRSubId
                          join q in db.Suppliers.AsEnumerable() on p.PreferredSupplierId equals q.SupplierId
                          join r in db.Brands.AsEnumerable() on p.BrandId equals r.BrandId
                          join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                          join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                          join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                          select new { po, p, q.SupplierName, r.BrandName, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, o }
                            ).Select(c => new RequisitionView()
                            {
                                POId = c.po.POId,
                                POSubId = c.po.POSubId,
                                QApprovalId = c.o.QApprovalId,
                                QApprovalSubId = c.o.QApprovalSubId,
                                PRId = c.p.PRId,
                                PRNo = c.p.PRMain.PRNo,
                                PRDate = c.p.PRMain.PRDate,
                                PONo = c.po.PurchaseOrder.PONo,
                                PODate = c.po.PurchaseOrder.PODate,
                                QANo = c.o.QuantityApprovalMain.QApprovalNo,
                                QADate = c.o.QuantityApprovalMain.QApprovalDate,
                                ProductId = c.p.ProductId,
                                CurrentStock = c.p.CurrentStock,
                                StockInTransit = c.p.StockInTransit,
                                PendingRequisitionQuanity = c.p.PendingRequisitionQuanity,
                                HistoricalConsumption = c.p.HistoricalConsumption,
                                HistoricalPeriod = c.p.HistoricalPeriod,
                                Next3MonthAvgConsumptionLastYear = c.p.Next3MonthAvgConsumptionLastYear,
                                Next3MonthAvgConsumptionCurrentYear = c.p.Next3MonthAvgConsumptionCurrentYear,
                                AvgLeadTimeEarlierOrders = c.p.AvgLeadTimeEarlierOrders,
                                LeadTime = c.p.LeadTime,
                                FactoryETA = c.p.FactoryETA,
                                BrandId = c.p.BrandId,
                                BrandName = c.BrandName,
                                PreferredSupplierId = c.p.PreferredSupplierId,
                                SupplierName = c.SupplierName,
                                Quantity = c.p.Quantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                POQuantity = c.p.Quantity,
                                ApprovedQuantity = c.o.ApprovedQuantity,
                                CategoryId = c.CategoryId,
                                ProductName = c.ItemName,
                                RowNo = c.po.RowNo,
                                Remarks = c.p.Remarks
                            }).ToList();

            qp.requisitionView = requisitionView;
            qp.quotationView = quotationView;
            qp.quotationSubView = quotationSubView;

            return PartialView(qp);
        }
    }
}
