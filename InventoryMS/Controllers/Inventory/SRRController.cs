﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using System.Globalization;

namespace InventoryMS.Controllers.Inventory
{
    public class SRRController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private SDMSEntities dbSDMS = new SDMSEntities();
        private EMailCubeEntities dbEM = new EMailCubeEntities();

        // GET: /SRR/
        public ActionResult Index()
        {
            return View(db.SRRMains.ToList());
        }

        // GET: /SRR/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SRRMain sRRMain = db.SRRMains.Find(id);
            if (sRRMain == null)
            {
                return HttpNotFound();
            }
            return View(sRRMain);
        }

        // GET: /Requisition/
        [CAuthorize]
        public ActionResult QueuedTransferRequest()
        {
            var requisitions = (from p in db.SRRMains.AsEnumerable()
                                join r in dbProd.UserFactories.AsEnumerable() on p.ReferenceId equals r.FactoryId
                                where p.ForwardType == 1 && p.ApprovedType == 0 && (p.SRRTypeId != 59)
                                select p).ToList();

            return View(requisitions);
        }

        // GET: /Requisition/
        [CAuthorize]
        public ActionResult TransferApproval(int id)
        {
            var requisition = (from p in db.SRRMains.AsEnumerable()
                               join r in dbProd.UserFactories.AsEnumerable() on p.ReferenceId equals r.FactoryId
                               where p.SRRId == id && p.ForwardType == 1 && p.ApprovedType == 0 && (p.SRRTypeId != 59)
                               select p
                              ).FirstOrDefault();

            if (requisition == null)
            {
                return HttpNotFound();
            }

            return View(requisition);
        }

        // GET: /Requisition/Approval
        [CAuthorize]
        public ActionResult TransferApprovalSave(int id, ICollection<SRRSubView> srrSubs, short apvtype)
        {
            SRRMain srrmain = db.SRRMains.Find(id);
            try
            {
                string app_link = "", _msg = "";
                short fac_id = 0, email_type_id = 0;

                if (srrmain.ForwardType == 1 && srrmain.ApprovedType == 1)
                    return Json(new { status = "Invalid", message = "Issue Requistion # " + srrmain.SRRNo + " has already been approved.", srrno = srrmain.SRRNo }, JsonRequestBehavior.AllowGet);
                else if (srrmain.ForwardType > 1 || srrmain.ApprovedType > 1)
                    return Json(new { status = "Invalid", message = "Issue Requistion # " + srrmain.SRRNo + " has already been disapproved.", srrno = srrmain.SRRNo }, JsonRequestBehavior.AllowGet);

                srrmain.ApprovedType = apvtype;
                srrmain.ApprovedBy = clsMain.getCurrentUser();
                srrmain.ApprovedTime = clsMain.getCurrentTime();

                email_type_id = 2;
                fac_id = srrmain.FactoryId;
                if (apvtype == 1)
                {
                    _msg = "Issue Requistion # " + srrmain.SRRNo + " has been approved and sent to respective store for further processing.";
                }
                else
                {
                    _msg = "Issue Requistion # " + srrmain.SRRNo + " has been disapproved.";
                }

                app_link = @"http://192.168.0.47/ipms/report/srr?__RequestVerificationToken=sKyQrtf_PxmyNAbQnwxvfNQZzuyx9vk6CPB19h8BO_5WlWJ6gPMsrL1ZoyR7xZEeXro5w4ZWVaXYvikdxR_U0qpOcaJnDuNmP9DPDr2A8sg1&fn=rptSRRProfile&FactoryId=" + srrmain.FactoryId + "&Year=" + srrmain.Year + "&PRId=" + srrmain.SRRId;

                var subIds = db.SRRMains.Find(id).SRRSubs.Select(c => c.SRRSubId).ToList();

                foreach (var sub_id in subIds)
                {
                    var srrSub = db.SRRSubs.Find(sub_id);
                    srrSub.SRRQuantity = srrSubs.Where(c => c.SRRSubId == sub_id).DefaultIfEmpty(new SRRSubView() { SRRQuantity = 0 }).First().SRRQuantity.Value;
                    srrSub.NetQuantity = srrSub.SRRQuantity;

                    db.Entry(srrSub).State = EntityState.Modified;
                }

                db.Entry(srrmain).State = EntityState.Modified;
                db.SaveChanges();

                //var row = (from p in db.EmailLibraries.AsEnumerable()
                //           join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                //           where p.EmailTypeId == email_type_id && q.FactoryId == fac_id
                //           select new
                //           {
                //               p.TypeName,
                //               p.EmailBody,
                //               q.EmailGroupFrom,
                //               q.EmailGroupTo,
                //               q.EmailGroupCC,
                //               q.EmailGroupBCC,
                //               p.Subject
                //           }).FirstOrDefault();

                //EMailCollectionView MailCollection = new EMailCollectionView();
                //MailCollection.EmailType = row.TypeName;
                //MailCollection.ModuleName = "IPMS";
                //MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                //MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                //MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                //if (email_type_id == 2)
                //    MailCollection.EmailCC += "; " + clsMain.getEmail(prmain.CreatedBy.ToString());
                //MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                //MailCollection.EmailSubject = row.Subject.Replace("@RequisitionNo", prmain.PRNo);
                //MailCollection.EmailBody = row.EmailBody.Replace("@RequisitionNo", prmain.PRNo)
                //    .Replace("@Factory", dbProd.Factories.Find(prmain.FactoryId).ShortName)
                //    .Replace("@Date", prmain.PRQDate.ToString("dd-MMM-yyyy"))
                //    .Replace("@ApprovalLink", app_link);
                //MailCollection.EmailAttachmentLink = "";
                //MailCollection.CreateAttachment = 0;
                //MailCollection.CreatedBy = clsMain.getCurrentUser();

                //EMailCollection mail = new EMailCollection(MailCollection);

                //dbEM.EMailCollections.Add(mail);

                //dbEM.SaveChanges();

                return Json(new { status = "Success", message = _msg, srrno = srrmain.SRRNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /SRR/Create
        [CAuthorize]
        public ActionResult Create(string id)
        {
            SRRMainView srrView = new SRRMainView();
            srrView.SRRDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.SRRNo = "###/####/0000-" + DateTime.Now.ToString("MM/yy");
            //ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName");
            ViewBag.LC = new SelectList(dbFund.LCMains.Where(l => l.LCFileId == 0), "LCFileId", "LCNo");
            ViewBag.Store = new SelectList(db.Warehouses.Where(c => c.IsActive == 1).ToList(), "WarehouseId", "WarehouseName");
            ViewBag.IssueTo = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.SourceType = new SelectList((new[] { new { id = 1, type = "Import" }, new { id = 2, type = "Local" } }).ToList(), "id", "type", 1);
            ViewBag.IssueType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            //ViewBag.SRRNo = new SelectList((new[] { new { id = 1, type = "SRR-0001/19" }, new { id = 2, type = "SRR-0002/19" } }).ToList(), "id", "type");
            ViewBag.BOMNo = new SelectList((new[] { new { id = 1, type = "BOM-0001/19" }, new { id = 2, type = "BOM-0002/19" } }).ToList(), "id", "type");

            //var bookinglist = db.Database.SqlQuery<PlanDetails>("Exec SalesCube.dbo.prcPlanDetails @Flag = 'BOOKINGLIST', @Id = 1").Select(c => new { BookingId = c.BookingId, BookingNo = c.BookingNo + " >> " + c.PlanDate.ToString("dd-MMM-yyyy") }).Distinct();
            //ViewBag.BookingId = new SelectList(db.Custom_Type.Where(c => c.TypeId == -1 && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.BookingId = new SelectList((new[] { new { id = 1, type = "" } }).Where(c => c.id == 0).ToList(), "id", "type"); //new SelectList(bookinglist, "BookingId", "BookingNo");

            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            
            if (id == null || id.ToLower().Equals("production"))
                return View(srrView);
            else
                return View("SRRForOthers", srrView);
        }

        // GET: /SRR/Create
        [CAuthorize]
        public ActionResult SRRForOthers()
        {
            SRRMain srrView = new SRRMain();
            srrView.SRRDate = clsMain.getCurrentTime();
            //var include_list = new List<short>() { 31, 32, 33, 54, 40, 42, 55 };

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.SRRNo = "###/####/0000-" + DateTime.Now.ToString("MM/yy");
            ViewBag.MaterialType = new SelectList(dbProd.CustomTypes.Where(c => c.Flag.Equals("MATERIALTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.RequisitionType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SRRTYPE") && !c.ShortName.Equals("FLOOR ISSUE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Customer = new SelectList(dbSDMS.CustomerGenInfoes.Where(c => c.IsActive == 1).Select(c => new { c.CustomerId, c.CustomerName }).ToList(), "CustomerId", "CustomerName");
            ViewBag.Lender = new SelectList(db.Payers.Where(c => c.IsActive == 1).ToList(), "PayerId", "PayerName");
            ViewBag.Store = new SelectList(db.Warehouses.Where(c => c.IsActive == 1).ToList(), "WarehouseId", "WarehouseName");
            ViewBag.Purpose = new SelectList(db.SRRMains.Where(c => c.Purpose != null).Select(c => new { Purpose = c.Purpose }).Distinct().ToList(), "Purpose", "Purpose");
            ViewBag.TransferFactory = new SelectList((new[] { new { id = 1, type = "" } }).Where(c => c.id == 0).ToList(), "id", "type");
            ViewBag.BondId = new SelectList(db.Bonds.Where(c => c.IsActive == 1).ToList(), "BondId", "LicenseNumber");
            ViewBag.LocationId = new SelectList(db.Locations.Where(c => c.LocationId < 3).ToList(), "LocationId", "LocationName");
            ViewBag.FactoryList = new SelectList(dbProd.Factories, "FactoryId", "FactoryName");

            return View(srrView);
        }

        // POST: /SRR/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(SRRMainView srrView, ICollection<SRRSubView> srrSubView, ICollection<SRRAgainstPlanView> srrPlanView, string save_flag = "Save")
        {
            try
            {
                //if (db.SRRMains.Where(l => l.SRRNo.Equals(srrView.SRRNo) && l.FactoryId == srrView.FactoryId).Count() > 0)
                //    return Content("Duplicate SRR No.");

                var _message = "";

                SRRMain srrMain = new SRRMain(srrView);
                srrMain.SRRDate = DateTime.Parse(srrMain.SRRDate.ToString("dd-MMM-yyyy") + " " + clsMain.getCurrentTime().ToString("HH:mm:ss"));

                srrMain.SRRNo = srrView.SRRNo.Substring(0, 3) + "/" + dbProd.Factories.Find(srrMain.FactoryId).Prefix + "/" + db.SRRMains.Where(l => l.Year == srrMain.SRRDate.Year && l.FactoryId == srrMain.FactoryId && l.SRRNo.StartsWith(srrView.SRRNo.Substring(0, 3))).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + srrMain.SRRDate.ToString("MM/yy").ToUpperInvariant();

                if (srrMain.SRRTypeId == 65)
                {
                    srrMain.ForwardBy = 1;
                    srrMain.ForwardType = 1;
                    srrMain.ForwardTime = srrMain.EntryDate;
                }
                else
                {
                    srrMain.ForwardBy = 1;
                    srrMain.ForwardType = 1;
                    srrMain.ForwardTime = srrMain.EntryDate;
                    srrMain.ApprovedType = 1;
                    srrMain.ApprovedBy = 1;
                    srrMain.ApprovedTime = srrMain.EntryDate;
                }

                if (srrSubView != null)
                {
                    foreach (SRRSubView sub in srrSubView)
                    {
                        sub.CategoryId = dbProd.ItemInfoes.Find(sub.ProductId).CategoryId;
                        sub.QueueQuantity = sub.SRRQuantity;
                        var _sub = new SRRSub(sub);
                        _sub.OriginCategoryId = sub.CategoryId.Value;
                        _sub.OriginProductId = sub.ProductId.Value;
                        //sub.BIN = "-";
                        srrMain.SRRSubs.Add(_sub);
                    }
                }

                if (srrPlanView != null)
                {
                    foreach (SRRAgainstPlanView plan in srrPlanView)
                    {
                        srrMain.SRRAgainstPlans.Add(new SRRAgainstPlan(plan));
                    }
                }

                db.SRRMains.Add(srrMain);
                db.SaveChanges();

                try
                {
                    if (srrMain.SRRId > 0)
                    {
                        var user = db.Sys_User_Name.Find(clsMain.getCurrentUser());

                        var email_type_list = new List<short>() { };
                        if (srrMain.SRRTypeId == 62 || srrMain.SRRTypeId == 65)
                        {
                            email_type_list.Add(16);
                            email_type_list.Add(17);
                        }
                        else if (srrMain.SRRTypeId == 59)
                        {
                            email_type_list.Add(14);
                        }
                        else
                        {
                            email_type_list.Add(17);
                        }

                        var row = (from p in db.EmailLibraries.AsEnumerable()
                                   join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                                   where email_type_list.Contains(p.EmailTypeId) && q.FactoryId == (srrMain.SRRTypeId == 62 || srrMain.SRRTypeId == 65 ? srrMain.ReferenceId : srrMain.FactoryId)
                                   select new
                                   {
                                       p.TypeName,
                                       p.EmailBody,
                                       q.EmailGroupFrom,
                                       q.EmailGroupTo,
                                       q.EmailGroupCC,
                                       q.EmailGroupBCC,
                                       p.Subject
                                   }).FirstOrDefault();

                        EMailCollectionView MailCollection = new EMailCollectionView();
                        MailCollection.EmailType = row.TypeName;
                        MailCollection.ModuleName = "IPMS";

                        var factory = dbProd.Factories.Find(srrMain.FactoryId);
                        var material_type = dbProd.CustomTypes.Find(srrMain.MaterialTypeId).TypeName;
                        var srr_type = db.Custom_Type.Find(srrMain.SRRTypeId).ShortName;

                        //MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom); // clsMain.getEmail(user.eMail);
                        //MailCollection.EmailTo = "apu.sarwar@kdsgroup.net; luna.dasgupta@kdsgroup.net;"; // clsMain.getEmail(row.EmailGroupTo);
                        //MailCollection.EmailCC = "mohammad.noman@kdsgroup.net"; // clsMain.getEmail(row.EmailGroupCC);
                        //MailCollection.EmailBCC = ""; // clsMain.getEmail(row.EmailGroupBCC + "; " + user.eMail);

                        //MailCollection.EmailFrom = "Inventory & Procurement Management System<ipms.cube@kdsgroup.net>";
                        if (string.IsNullOrEmpty(clsMain.getEmail(user.eMail).Trim()))
                        {
                            MailCollection.EmailFrom = "Inventory & Procurement Management System<ipms.cube@kdsgroup.net>";
                        }
                        else
                        {
                            MailCollection.EmailFrom = clsMain.getEmail(user.eMail);  //clsMain.getEmail(row.EmailGroupFrom); // 
                        }

                        MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo); //"apu.sarwar@kdsgroup.net; luna.dasgupta@kdsgroup.net;"; // 
                        MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);  //"mohammad.noman@kdsgroup.net"; // 
                        MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC + "; " + user.eMail);  //""; // 
                        //MailCollection.EmailTo = "mohammad.noman@kdsgroup.net";

                        MailCollection.EmailSubject = row.Subject.Replace("@ReqNo", srrMain.SRRNo).Replace("@ReqType", srr_type);
                        MailCollection.EmailBody = row.EmailBody
                            .Replace("@ReqDate", srrMain.SRRDate.ToString("dd-MMM-yyyy HH:MM"))
                            .Replace("@ReqNo", srrMain.SRRNo)
                            .Replace("@ReqType", srr_type)
                            .Replace("@Factory", factory.FactoryName)
                            .Replace("@MaterialType", material_type)
                            .Replace("@Remarks", srrMain.Remarks)
                            .Replace("@Source", "-")
                            .Replace("@Officer", user.UserTitle + " (" + user.UserName + ")")
                            .Replace("@ViewLink", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "/report/mrr?fn=rptSRRProfile&SRRId=" + srrMain.SRRId.ToString())
                            .Replace("@ApprovalLink", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "/srr/transferapproval/" + srrMain.SRRId.ToString());

                        MailCollection.EmailAttachmentLink = "";
                        MailCollection.CreateAttachment = 0;
                        MailCollection.CreatedBy = clsMain.getCurrentUser();

                        EMailCollection mail = new EMailCollection(MailCollection);

                        dbEM.EMailCollections.Add(mail);

                        dbEM.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    _message = String.Format("{0} successful without email sending.\nSRR No : " + srrMain.SRRNo, save_flag);
                    return Content(_message);
                }
                return Content("Save successful.\nRequistion No : " + srrMain.SRRNo);
            }
            catch (Exception ex)
            {
                throw new Exception("Save failed.\nReason: " + ex.Message, ex.InnerException);
            }
        }

        // GET: /SRR/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SRRMain sRRMain = db.SRRMains.Find(id);
            if (sRRMain == null)
            {
                return HttpNotFound();
            }
            return View(sRRMain);
        }

        // POST: /SRR/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="SRRId,SRRNo,SRRDate,Month,Year,SerialNo,Remarks,DepartmenttId,EntryUserId,EntryDate,RevisionUserId,RevisionDate,IPAddress,PCName")] SRRMain sRRMain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sRRMain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sRRMain);
        }

        // GET: /SRR/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SRRMain sRRMain = db.SRRMains.Find(id);
            if (sRRMain == null)
            {
                return HttpNotFound();
            }
            return View(sRRMain);
        }

        // POST: /SRR/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SRRMain sRRMain = db.SRRMains.Find(id);
            db.SRRMains.Remove(sRRMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
        public ActionResult FactoryDetails(int? id, DateTime? srrdate, string type = "RMR")
        {
            string srrno = type + "/" + dbProd.Factories.Find(id).Prefix + "/" + db.SRRMains.Where(l => l.Year == srrdate.Value.Year && l.FactoryId == id && l.SRRNo.StartsWith(type)).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + srrdate.Value.ToString("MM/yy").ToUpperInvariant();

            return Json(new { srrno = srrno }, JsonRequestBehavior.AllowGet);
        }



        public ActionResult BookingList(int id, DateTime plan_date)
        {
            var bookinglist = db.Database.SqlQuery<PlanDetails>(string.Format("Exec SalesCube.dbo.prcPlanDetails @Flag = 'BOOKINGLIST', @FactoryId = {0}, @PlanDate = '{1}'", id, plan_date.ToString("dd-MMM-yyyy"))).Select(c => new { c.BookingId, BookingNo = c.BookingNo + " >> " + c.CustomerName }).Distinct();
            ViewBag.BookingId = new SelectList(bookinglist.OrderBy(c => c.BookingNo), "BookingId", "BookingNo");

            return PartialView();
        }

        public ActionResult BookingDetails(int id, DateTime plan_date)
        {
            try
            {
                var bookinglist = db.Database.SqlQuery<PlanDetails>(string.Format("Exec SalesCube.dbo.prcPlanDetails @Flag = 'BOOKINGDETAILS', @Id = {0}, @PlanDate = '{1}'", id, plan_date.ToString("dd-MMM-yyyy"))).Distinct();

                return PartialView(bookinglist);
            }
            catch(Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult PlanForRequistion(ICollection<PlanAgRequision> planlist)
        {
            try
            {
                //var FId = new SqlParameter("@FactoryId", Convert.ToInt32(0));
                //var flag = new SqlParameter("@Flag", "PLANTOREQUISITION");
                //var id = new SqlParameter("@Id", 0);
                
                DataTable dt = new DataTable();
                dt.Columns.Add("PlanId", typeof(int));
                dt.Columns.Add("Quantity", typeof(Decimal));

                foreach(var row in planlist)
                {
                    dt.Rows.Add(row.PlanId, row.Quantity);
                }

                var sqlp = new SqlParameter("@PlanDetails", dt);
                sqlp.SqlDbType = SqlDbType.Structured;
                sqlp.TypeName = "dbo.PlanList";

                var list = dbSDMS.Database.SqlQuery<PlanDetails>("Exec dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", new SqlParameter[] { new SqlParameter("@FactoryId", Convert.ToInt32(0)), new SqlParameter("@Flag", "PLANTOREQUISITION".ToString()), new SqlParameter("@Id", Convert.ToInt32(0)), sqlp });
                //var list = db.Database.SqlQuery<PlanDetails>("Exec SalesCube.dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", FId, flag, id, sqlp);

                return PartialView(list);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult RequistionProduct(ICollection<PlanAgRequision> planlist,  short rowno = 1, short srrtype = 59, short mat_type_id = 86, short factory_id = 0, short? transfer_from = 0)
        {
            try
            {
                var item_list = (from p in dbProd.ItemCategories.AsEnumerable()
                                 join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                                 join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                                 where p.StageLevel == 4 && r.FactoryId == factory_id && r.MaterialTypeId == mat_type_id
                                 //where p.StageLevel == 4 && r.FactoryId == factory_id && q.ItemCode.Substring(0, 2) == "01" ||  q.ItemCode.Substring(0, 2) == "02"
                                 select q
                                ).Distinct();

                if (srrtype == 65)
                {
                    item_list = item_list.Intersect(
                        (from p in dbProd.ItemCategories.AsEnumerable()
                         join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                         join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                         where p.StageLevel == 4 && r.FactoryId == transfer_from && q.ItemCode.Substring(0, 2) == "01" 
                         select q
                        ).Distinct()
                    ).Distinct();
                }

                var cat_list = item_list.Select(c => new { c.ItemCategory.CategoryId, c.ItemCategory.CategoryName }).Distinct();

                ViewBag.Category = new SelectList(cat_list, "CategoryId", "CategoryName");
                //ViewBag.Product = new SelectList(item_list.OrderBy(c => c.ItemName).ThenBy(c => c.ItemName.Length), "ItemId", "ItemName");

                //Adding for ItemCode and ItemName in Product Cuolumn--Monsur
                ViewBag.Product = new SelectList(item_list.OrderBy(c => c.ItemName).ThenBy(c => c.ItemName.Length).Select(c => new { c.ItemId, DisplayText = $"{c.ItemCode} -  {c.ItemName}" }), "ItemId", "DisplayText");

                ViewBag.Brand = new SelectList(db.Brands.Where(c => c.IsActive == 1), "BrandId", "BrandName", 0);

                if (planlist != null && planlist.Count > 0)
                {
                    //var FId = new SqlParameter("@FactoryId", Convert.ToInt32(0));
                    //var flag = new SqlParameter("@Flag", "PLANTOREQUISITION");
                    //var id = new SqlParameter("@Id", 0);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("PlanId", typeof(int));
                    dt.Columns.Add("Quantity", typeof(Decimal));

                    foreach (var row in planlist)
                    {
                        dt.Rows.Add(row.PlanId, row.Quantity);
                    }

                    var sqlp = new SqlParameter("@PlanDetails", dt);
                    sqlp.SqlDbType = SqlDbType.Structured;
                    sqlp.TypeName = "dbo.PlanList";

                    var list = dbSDMS.Database.SqlQuery<SRRSubView>("Exec dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", new SqlParameter[] { new SqlParameter("@FactoryId", Convert.ToInt32(0)), new SqlParameter("@Flag", "REQUISITIONPRODUCT".ToString()), new SqlParameter("@Id", Convert.ToInt32(0)), sqlp }).ToList();
                    //var list = db.Database.SqlQuery<PlanDetails>("Exec SalesCube.dbo.prcPlanDetails @FactoryId, @Flag, @Id, @PlanDetails", FId, flag, id, sqlp);

                    //list = list.ForEach(c => c.IssueType = issuetype);
                    list = list.Select(c => { c.IssueType = srrtype; c.RowNo = rowno++; return c; }).ToList();

                    return PartialView(list);
                }
                else
                {
                    SRRSubView rp = new SRRSubView(true);
                    rp.RowNo = rowno;
                    rp.IssueType = srrtype;
                    rp.CategoryName = "";
                    //rp.MaterialTypeId = material_type_id;
                    rp.MaterialTypeId = mat_type_id;


                    return PartialView(new List<SRRSubView>() { rp });
                }
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '6'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult ProductList(int id, short facid, short rowno)
        {
            ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(c => (c.CategoryId == id || id == 0) && c.ItemCode.Substring(0, 2) == "01").OrderBy(c => c.ItemName).ThenBy(c => c.ItemName.Length), "ItemId", "ItemName");
            ViewBag.rowno = rowno.ToString("00");

            return PartialView();
        }

        public ActionResult ProductDetails(int id, short type, short facid, short facid2)
        {
            try
            {
                var product = (from p in dbProd.ItemInfoes.AsEnumerable()
                               join q in dbProd.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals q.QuantityUnitId
                               where p.ItemId == id
                               select new { q.QuantityUnitId, q.QuantityUnit }
                              ).FirstOrDefault();

                var stock1 = (from p in dbProd.ItemInfoes.AsEnumerable()
                              join q in db.GRRSubs.Where(c => c.GRRMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, PurchaseQty = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on p.ItemId equals q.ProductId into r
                              from s in r.DefaultIfEmpty()
                              join t in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, IssueQty = c.Sum(l => l.Quantity), FloorIssueQty = c.Sum(l => l.IssueMain.IssueTypeId == 38 ? l.Quantity : 0) }).AsEnumerable() on p.ItemId equals t.ProductId into u
                              from v in u.DefaultIfEmpty()
                              join w in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQty = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on p.ItemId equals w.ProductId into x
                              from y in x.DefaultIfEmpty()
                              join xa in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQty = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on p.ItemId equals xa.ProductId into xb
                              from z in xb.DefaultIfEmpty()
                              where p.ItemId == id
                              select new
                              {
                                  StockQty = (s == null ? 0 : s.PurchaseQty) - (v == null ? 0 : v.IssueQty) + (y == null ? 0 : y.ReturnQty),
                                  FloorStock = (v == null ? 0 : v.FloorIssueQty) - (z == null ? 0 : z.ConsumeQty) - (y == null ? 0 : y.ReturnQty)
                              }
                             ).Distinct().FirstOrDefault();

                var srr_include_list = new List<short> { 59, 61, 63, 64 };

                //decimal req_pending = 0, req_pending_2 = 0;

                var req_pending = (from p in dbProd.ItemInfoes.AsEnumerable()
                                   join q in db.SRRSubs.Where(c => (c.SRRMain.FactoryId == facid && srr_include_list.Contains(c.SRRMain.SRRTypeId)) || (c.SRRMain.ReferenceId == facid && c.SRRMain.SRRTypeId == 65)).AsEnumerable() on p.ItemId equals q.ProductId
                                   join t in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid).GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, IssueQty = c.Sum(l => l.Quantity) }).AsEnumerable() on q.SRRSubId equals t.SRRSubId into u
                                   from v in u.DefaultIfEmpty()
                                   where p.ItemId == id
                                   select new
                                   {
                                       PendingQty = q.SRRQuantity - (v == null ? 0 : v.IssueQty)
                                   }
                                  ).Select(c => c.PendingQty).DefaultIfEmpty(0).Sum();

                //if (req_pending_list.Count > 0)
                //{
                //    req_pending = req_pending_list.Sum(c => c.PendingQty);
                //}

                decimal stock2_qty = 0, floor_stock2 = 0;
                if (type == 65)
                {
                    var stock2 = (from p in dbProd.ItemInfoes.AsEnumerable()
                                  join q in db.GRRSubs.Where(c => c.GRRMain.FactoryId == facid2).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, PurchaseQty = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on p.ItemId equals q.ProductId into r
                                  from s in r.DefaultIfEmpty()
                                  join t in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid2).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, IssueQty = c.Sum(l => l.Quantity), FloorIssueQty = c.Sum(l => l.IssueMain.IssueTypeId == 38 ? l.Quantity : 0) }).AsEnumerable() on p.ItemId equals t.ProductId into u
                                  from v in u.DefaultIfEmpty()
                                  join w in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid2).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQty = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on p.ItemId equals w.ProductId into x
                                  from y in x.DefaultIfEmpty()
                                  join xa in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid2).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQty = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on p.ItemId equals xa.ProductId into xb
                                  from z in xb.DefaultIfEmpty()
                                  where p.ItemId == id
                                  select new
                                  {
                                      StockQty = (s == null ? 0 : s.PurchaseQty) - (v == null ? 0 : v.IssueQty) + (y == null ? 0 : y.ReturnQty),
                                      FloorStock = (v == null ? 0 : v.FloorIssueQty) - (z == null ? 0 : z.ConsumeQty) - (y == null ? 0 : y.ReturnQty)
                                  }
                                 ).Distinct().FirstOrDefault();

                    var req_pending_2 = (from p in dbProd.ItemInfoes.AsEnumerable()
                                         //join q in db.SRRSubs.Where(c => c.SRRMain.FactoryId == facid2 && c.SRRMain.SRRTypeId == 65).AsEnumerable() on p.ItemId equals q.ProductId
                                         join q in db.SRRSubs.Where(c => (c.SRRMain.FactoryId == facid2 && srr_include_list.Contains(c.SRRMain.SRRTypeId)) || (c.SRRMain.ReferenceId == facid2 && c.SRRMain.SRRTypeId == 65)).AsEnumerable() on p.ItemId equals q.ProductId
                                        join t in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid2).GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, IssueQty = c.Sum(l => l.Quantity) }).AsEnumerable() on q.SRRSubId equals t.SRRSubId into u
                                        from v in u.DefaultIfEmpty()
                                        where p.ItemId == id
                                        select new
                                        {
                                            PendingQty = q.SRRQuantity - (v == null ? 0 : v.IssueQty)
                                        }
                                        ).Select(c => c.PendingQty).DefaultIfEmpty(0).Sum();

                    //if (req_pending_list_2.Count > 0)
                    //{
                    //    req_pending_2 = req_pending_list_2.Sum(c => c.PendingQty);
                    //}
                    stock2_qty = stock2.StockQty - req_pending_2;
                    floor_stock2 = stock2.FloorStock;
                }

                return Json(new { pend_qty = req_pending.ToString(), unitid = product.QuantityUnitId, unit = product.QuantityUnit, stock1_qty = (stock1.StockQty).ToString("##,##0.0000"), floor_stock1 = stock1.FloorStock.ToString("##,##0.0000"), stock2_qty = stock2_qty.ToString("##,##0.0000"), floor_stock2 = floor_stock2.ToString("##,##0.0000") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        //[HttpPost]
        public ActionResult RowTemplate(ICollection<SRRSub> requisitionSub)
        {
            return PartialView(requisitionSub);
        }

        public ActionResult TestEmail(int id)
        {
            var srrMain = db.SRRMains.Find(id);
            try
            {
                if (srrMain.SRRId > 0)
                {
                    var user = db.Sys_User_Name.Find(clsMain.getCurrentUser());

                    var email_type_list = new List<short>() { };
                    if (srrMain.SRRTypeId == 62 || srrMain.SRRTypeId == 65)
                    {
                        email_type_list.Add(16);
                        email_type_list.Add(17);
                    } 
                    else if (srrMain.SRRTypeId == 59)
                    {
                        email_type_list.Add(14);
                    }
                    else
                    {
                        email_type_list.Add(17);
                    }

                    var row = (from p in db.EmailLibraries.AsEnumerable()
                               join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                               where email_type_list.Contains(p.EmailTypeId) && q.FactoryId == (srrMain.SRRTypeId == 62 || srrMain.SRRTypeId == 65 ? srrMain.ReferenceId : srrMain.FactoryId)
                               select new
                               {
                                   p.TypeName,
                                   p.EmailBody,
                                   q.EmailGroupFrom,
                                   q.EmailGroupTo,
                                   q.EmailGroupCC,
                                   q.EmailGroupBCC,
                                   p.Subject
                               }).FirstOrDefault();

                    EMailCollectionView MailCollection = new EMailCollectionView();
                    MailCollection.EmailType = row.TypeName;
                    MailCollection.ModuleName = "IPMS";

                    var factory = dbProd.Factories.Find(srrMain.FactoryId);
                    var material_type = dbProd.CustomTypes.Find(srrMain.MaterialTypeId).TypeName;
                    var srr_type = db.Custom_Type.Find(srrMain.SRRTypeId).ShortName;

                    //MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom); // clsMain.getEmail(user.eMail);
                    //MailCollection.EmailTo = "apu.sarwar@kdsgroup.net; luna.dasgupta@kdsgroup.net;"; // clsMain.getEmail(row.EmailGroupTo);
                    //MailCollection.EmailCC = "mohammad.noman@kdsgroup.net"; // clsMain.getEmail(row.EmailGroupCC);
                    //MailCollection.EmailBCC = ""; // clsMain.getEmail(row.EmailGroupBCC + "; " + user.eMail);

                    MailCollection.EmailFrom = "Inventory & Procurement Management System<ipms.cube@kdsgroup.net>";
                    //MailCollection.EmailFrom = clsMain.getEmail(user.eMail);  //clsMain.getEmail(row.EmailGroupFrom); // 
                    //MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo); //"apu.sarwar@kdsgroup.net; luna.dasgupta@kdsgroup.net;"; // 
                    //MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);  //"mohammad.noman@kdsgroup.net"; // 
                    //MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC + "; " + user.eMail);  //""; // 
                    MailCollection.EmailTo = "mohammad.noman@kdsgroup.net";

                    MailCollection.EmailSubject = row.Subject.Replace("@ReqNo", srrMain.SRRNo).Replace("@ReqType", srr_type);
                    MailCollection.EmailBody = row.EmailBody
                        .Replace("@ReqDate", srrMain.SRRDate.ToString("dd-MMM-yyyy HH:MM"))
                        .Replace("@ReqNo", srrMain.SRRNo)
                        .Replace("@ReqType", srr_type)
                        .Replace("@Factory", factory.FactoryName)
                        .Replace("@MaterialType", material_type)
                        .Replace("@Remarks", srrMain.Remarks)
                        .Replace("@Source", "-")
                        .Replace("@Officer", user.UserTitle + " (" + user.UserName + ")")
                        .Replace("@ViewLink", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "/report/mrr?fn=rptSRRProfile&SRRId=" + srrMain.SRRId.ToString())
                        .Replace("@ApprovalLink", string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "/srr/transferapproval/" + srrMain.SRRId.ToString());

                    MailCollection.EmailAttachmentLink = "";
                    MailCollection.CreateAttachment = 0;
                    MailCollection.CreatedBy = clsMain.getCurrentUser();

                    EMailCollection mail = new EMailCollection(MailCollection);

                    dbEM.EMailCollections.Add(mail);

                    dbEM.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
                return Content("Email send failed.\nRequistion No : " + srrMain.SRRNo);
            }

            return Content("Email send successful.\nRequistion No : " + srrMain.SRRNo);
        }
    }
}
