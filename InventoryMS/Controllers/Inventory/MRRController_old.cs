﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using System.Globalization;

namespace InventoryMS.Controllers.Inventory
{
    public class MRRController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private EMailCubeEntities dbEM = new EMailCubeEntities();
        private POEntities dbPO = new POEntities();
        private SDMSEntities dbSDMS = new SDMSEntities();

        // GET: /GRR/
        public ActionResult Index()
        {
            return View(db.GRRMains.OrderBy(c => c.GRRNo).ToList());
        }

        // GET: /GRR/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRRMain gRRMain = db.GRRMains.Find(id);
            if (gRRMain == null)
            {
                return HttpNotFound();
            }
            return View(gRRMain);
        }

        // GET: /GRR/Create
        [CAuthorize]
        public ActionResult Create()
        {
            GRRView grrView = new GRRView();
            grrView.GRRNo = "MRR/####/0000-" + DateTime.Now.ToString("MM/yy");
            grrView.GRRDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            //ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName");
            ViewBag.LC = new SelectList(dbFund.LCMains.Where(l => l.LCFileId == 0), "LCFileId", "LCNo");
            ViewBag.Lot = new SelectList(db.PipelineMains.Where(l => l.PLEId == 0), "PLEId", "LotNo");
            ViewBag.SourceType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SOURCETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName", 46);
            ViewBag.ReceiveType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("GRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName", 30);
            ViewBag.PONo = new SelectList((new[] { new { id = 1, type = "MRR-0001/19" }, new { id = 2, type = "MRR-0002/19" } }).ToList(), "id", "type");
            ViewBag.SRRNo = new SelectList((new[] { new { id = 0, type = "-" } }).Where(c => c.id == -1).ToList(), "id", "type");
            ViewBag.InvoiceNo = new SelectList((new[] { new { id = 0, type = "-" } }).Where(c => c.id == -1).ToList(), "id", "type");
            ViewBag.Warehouse = new SelectList(db.Warehouses.Where(c => c.IsActive == 1).ToList(), "WarehouseId", "WarehouseName", 0);
            ViewBag.save_flag = "Save";

            return View(grrView);
        }

        // GET: /GRR/Create
        [CAuthorize]
        public ActionResult Approval(int id)
        {
            GRRView grrView = (GRRView)db.GRRMains.Find(id).Convert(new GRRView());
            if (grrView == null)
            {
                return HttpNotFound();
            }

            if (grrView.PurchaseTypeId == 46)
            {
                var lc = (from p in db.PipelineMains.AsEnumerable()
                          join q in dbFund.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                          join r in dbFund.LCWisePIs.AsEnumerable() on q.LCFileId equals r.LCFileId
                          join s in dbFund.PISubs.AsEnumerable() on r.PIFileId equals s.PIFileId
                          join t in db.PRSubs.AsEnumerable() on s.PRSubId equals t.PRSubId
                          join u in db.PRMains.AsEnumerable() on t.PRId equals u.PRId
                          where /*p.IsLotReceived == 0 &&*/ u.FactoryId == grrView.FactoryId
                          select q).Distinct().ToList();

                ViewBag.LC = new SelectList(lc, "LCFileId", "LCNo", grrView.LCFileId);
                grrView.SupplierName = db.Suppliers.Find(grrView.SupplierId).SupplierName;
                grrView.LotDate = db.PipelineMains.Find(grrView.PLEId).PLEDate;
            }
            else
                ViewBag.LC = new SelectList(dbFund.LCMains.Where(l => l.LCFileId == 0), "LCFileId", "LCNo", grrView.LCFileId);

            var bond_id = (from p in db.Bonds.AsEnumerable()
                           join q in dbProd.Factories.AsEnumerable() on p.CompanyId equals q.CompanyId
                           where q.FactoryId == grrView.FactoryId
                           select p.BondId
                          ).Distinct().First();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName", grrView.FactoryId);
            //ViewBag.LC = new SelectList(dbFund.LCMains.Where(l => l.LCFileId == 0), "LCFileId", "LCNo", grrView.LCFileId);
            ViewBag.Lot = new SelectList(db.PipelineMains.Where(l => l.LCFileId == grrView.LCFileId && (l.IsLotReceived == 0 || l.PLEId == grrView.PLEId)), "PLEId", "LotNo", grrView.PLEId);
            ViewBag.SourceType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("SOURCETYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName", grrView.PurchaseTypeId);
            ViewBag.ReceiveType = new SelectList(db.Custom_Type.Where(c => c.Flag.Equals("GRRTYPE") && c.IsActive == 1).OrderBy(c => c.RankId), "TypeId", "TypeName", grrView.ReceiveTypeId);
            ViewBag.SRRNo = new SelectList((new[] { new { id = 1, type = "-" } }).Where(c => c.id == 0).ToList(), "id", "type", grrView.SRRId);
            ViewBag.MRRNo = new SelectList((new[] { new { id = 1, type = "MRR-0001/19" }, new { id = 2, type = "MRR-0002/19" } }).ToList(), "id", "type");
            ViewBag.Warehouse = new SelectList(db.Warehouses.Where(c => c.IsActive == 1).ToList(), "WarehouseId", "WarehouseName", grrView.WarehouseId);
            ViewBag.save_flag = "Approval";

            return View("Create", grrView);
        }

        // POST: /GRR/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(GRRView grrView, ICollection<GRRSubView> grrSubView, string save_flag = "Save")
        {
            try
            {
                //if (db.GRRMains.Where(l => l.GRRNo.Equals(grrView.GRRNo) && l.FactoryId == grrView.FactoryId).Count() > 0)
                //    return Content("Duplicate GRR No.");

                var _message = "";

                //if (save_flag.Equals("Approval"))
                //{

                //}
                //else
                //{

                //}

                GRRMain grrMain = null;
                var ple_id = grrView.PLEId;  // 18825; // 
                var product_list = grrSubView.Select(c => c.ProductId.ToString()).Aggregate((i, j) => i + ", " + j);

                if (grrView.GRRId == 0)
                {
                    grrMain = new GRRMain(grrView);
                    grrMain.GRRDate = DateTime.Parse(grrView.GRRDate.Value.ToString("dd-MMM-yyyy") + " " + clsMain.getCurrentTime().ToString("HH:mm:ss"));

                    grrMain.GRRNo = "MRR/" + dbProd.Factories.Find(grrMain.FactoryId).Prefix + "/" + db.GRRMains.Where(l => l.Year == grrMain.GRRDate.Year && l.FactoryId == grrMain.FactoryId).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + grrMain.GRRDate.ToString("MM/yy").ToUpperInvariant();

                    var accept = dbFund.LCAcceptances.Where(c => c.PLEId == grrMain.PLEId).FirstOrDefault() ?? new LCAcceptance();
                    var cost = dbFund.LandedCostings.Where(l => l.LCAcceptanceId == accept.LCAcceptanceId).FirstOrDefault();

                    if (accept.LCAcceptanceId > 0 && accept.LandedCostingStatus == 1 && cost != null)
                    {
                        decimal acp_com_rate = 0, lc_com_rate = 0, libor_rate = 0, handling_rate = 0;
                        decimal total_grr_qty = 0, total_grr_value = 0;
                        var pi_list = dbFund.LCWisePIs.Where(c => c.LCFileId == accept.LCMain.LCFileId).Select(c => c.PIFileId).ToList();
                        var saction = dbFund.CSSubs.Where(c => pi_list.Contains(c.CSMain.PIFileId)).First();

                        libor_rate = accept.LiborInterestRate;
                        handling_rate = accept.HCInterestRate;

                        if (accept.LCMain.IssuerBankId == saction.BankId1)
                        {
                            acp_com_rate = saction.AcceptanceCommissionRate1;
                            lc_com_rate = saction.LCCommissionRate1;
                            if (libor_rate == 0)
                            {
                                libor_rate = saction.Libor1;
                            }
                            if (handling_rate == 0)
                            {
                                handling_rate = saction.Handling1;
                            }
                        }
                        else if (accept.LCMain.IssuerBankId == saction.BankId2)
                        {
                            acp_com_rate = saction.AcceptanceCommissionRate2;
                            lc_com_rate = saction.LCCommissionRate2;
                            if (libor_rate == 0)
                            {
                                libor_rate = saction.Libor2;
                            }
                            if (handling_rate == 0)
                            {
                                handling_rate = saction.Handling2;
                            }
                        }
                        else if (accept.LCMain.IssuerBankId == saction.BankId3)
                        {
                            acp_com_rate = saction.AcceptanceCommissionRate3;
                            lc_com_rate = saction.LCCommissionRate3;
                            if (libor_rate == 0)
                            {
                                libor_rate = saction.Libor3;
                            }
                            if (handling_rate == 0)
                            {
                                handling_rate = saction.Handling3;
                            }
                        }

                        var grr_subs = (from p in grrSubView.AsEnumerable() join q in dbFund.PISubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId select new { p.PLESubId, p.GRRQuantity, GRRValue = p.GRRQuantity * q.UnitPrice, q.UnitPrice });

                        total_grr_qty = grr_subs.Sum(c => c.GRRQuantity.Value);
                        total_grr_value = grr_subs.Sum(c => c.GRRValue.Value);

                        var accept_tenure = accept.AcceptanceTenure;  // accept.AcceptanceTenure >= 180 ? 180 : accept.AcceptanceTenure;

                        foreach (GRRSubView sub in grrSubView)
                        {
                            var _sub = grr_subs.Where(c => c.PLESubId == sub.PLESubId).FirstOrDefault();
                            var CIValue = (cost.CostValue * _sub.GRRValue) / total_grr_value;
                            var TotalLandedCosting = cost.LCOpeningCommission + cost.LCOpeningCommissionVAT + cost.SwiftCharges + cost.StampAndStationery + cost.PostageAndCourier +
                                                cost.AmendmentCharges + cost.CreditReportCharges + cost.ShippingGuaranteeCommission + cost.CNFCost + cost.InsurancePremium +
                                                cost.AcceptanceCommission + cost.Duty + cost.AIT + (((((accept.AcceptanceAmount * libor_rate) / 100) * accept_tenure) / 360) * accept.BDTRate) +
                                                (((((accept.AcceptanceAmount * handling_rate) / 100) * accept_tenure) / 360) * accept.BDTRate) +
                                                cost.FreightCharges + cost.Margin + cost.SwiftChargesAcceptanceCommission;
                            sub.LandedPriceBDT = ((CIValue) + ((TotalLandedCosting * CIValue) / cost.CostValue)) / _sub.GRRQuantity;
                            sub.ImportPriceFC = _sub.UnitPrice;
                            sub.CurrencyId = accept.CurrencyId;

                            sub.BIN = "-";
                            grrMain.GRRSubs.Add(new GRRSub(sub));
                        }
                    }
                    else
                    {
                        foreach (GRRSubView sub in grrSubView)
                        {
                            if (grrMain.ReceiveTypeId == 33)
                            {
                                var _rate = db.Database.SqlQuery<Decimal>("Select dbo.fncGetItemAveragePrice(" + sub.ProductId.ToString() + ", " + grrMain.FactoryId.ToString() + ", '" + grrMain.GRRDate.ToString("dd-MMM-yyyy HH:MM") + "')").FirstOrDefault();
                                sub.LandedPriceBDT = _rate;
                                sub.ImportPriceFC = _rate;
                                sub.CurrencyId = 23;
                            }
                            sub.BIN = "-";
                            grrMain.GRRSubs.Add(new GRRSub(sub));

                        }
                    }

                    grrMain.ApprovedType = 1;
                    grrMain.ApprovedBy = 6; //clsMain.getCurrentUser();
                    grrMain.ApprovedTime = grrMain.GRRDate;

                    var _pipeline = db.PipelineMains.Find(grrMain.PLEId);
                    _pipeline.IsLotReceived = 1;
                    _pipeline.LotReceivedDate = grrMain.ApprovedTime.Date;

                    db.GRRMains.Add(grrMain);

                    db.Entry(_pipeline).State = EntityState.Modified;

                    _message = String.Format("{0} successful.\nMRR No : " + grrMain.GRRNo, save_flag);
                }
                else
                {
                    grrMain = db.GRRMains.Find(grrView.GRRId);
                    grrMain = (GRRMain)grrView.ConvertNotNull(grrMain);

                    foreach (var Sub in grrSubView)
                    {
                        if (Sub.GRRSubId > 0)
                        {
                            GRRSub sub = db.GRRSubs.Find(Sub.GRRSubId);
                            sub = (GRRSub)Sub.ConvertNotNull(sub);

                            db.Entry(sub).State = EntityState.Modified;
                        }
                        else
                        {
                            GRRSub sub = new GRRSub(Sub);
                            grrMain.GRRSubs.Add(sub);
                        }
                    }

                    var unsub = db.GRRSubs.Where(l => l.GRRId == grrMain.GRRId).ToList();

                    for (int ri = 0; ri < unsub.Count; ri++)
                    {
                        if (db.Entry(unsub[ri]).State == EntityState.Unchanged)
                            db.Entry(unsub[ri]).State = EntityState.Deleted;
                    }

                    db.Entry(grrMain).State = EntityState.Modified;

                    _message = String.Format("{0} successful.\nMRR No : " + grrMain.GRRNo, save_flag);
                }

                var issue_subs = grrSubView.Where(c => c.IssueSubId > 0).GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, RecQty = c.Sum(l => l.GRRQuantity) });
                foreach(var _sub in issue_subs)
                {
                    var sub = db.IssueSubs.Find(_sub.IssueSubId);
                    if (sub.Quantity <= _sub.RecQty)
                    {
                        sub.Status = 0;
                        db.Entry(sub).State = EntityState.Modified;
                    }
                    else
                    {
                        sub.Status = 1;
                        db.Entry(sub).State = EntityState.Modified;
                    }
                }

                db.SaveChanges();

                db.Database.ExecuteSqlCommand("Exec prcUpdateOnwardsRate 0, '" + grrMain.GRRDate.ToString() + "', '" + product_list + "'");

                try
                {
                    if (grrMain.PurchaseTypeId == 46)
                    {
                        var pldetails = (from p in db.PipelineMains.AsEnumerable()
                                         join q in dbFund.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                                         join r in dbFund.LCWisePIs.AsEnumerable() on q.LCFileId equals r.LCFileId
                                         join u in dbFund.PISubs.AsEnumerable() on r.PIFileId equals u.PIFileId
                                         join s in dbProd.Factories.AsEnumerable() on u.FactoryId equals s.FactoryId
                                         join t in db.Suppliers.AsEnumerable() on q.SupplierId equals t.SupplierId
                                         join v in db.PRSubs.AsEnumerable() on u.PRSubId equals v.PRSubId
                                         join w in db.Custom_Type.AsEnumerable() on p.PackageUnitId equals w.TypeId into x
                                         from y in x.DefaultIfEmpty()
                                         where p.PLEId == ple_id
                                         select new
                                         {
                                             p,
                                             q.LCNo,
                                             q.LCDate,
                                             s.FactoryId,
                                             s.FactoryName,
                                             t.SupplierName,
                                             PRNo = v.PRMain.PRNo,
                                             LotNo = p.LotNo,
                                             PRDate = v.PRMain.PRDate,
                                             BrandName = v.Brand.BrandName,
                                             PortDeliveryDate = p.PortDeliveryDate,
                                             PackUnit = (y == null ? "" : y.TypeName)
                                         });

                        var items = (from p in db.GRRSubs.Where(l => l.GRRMain.PLEId == ple_id).AsEnumerable()
                                     join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                                     join qc in dbProd.ItemCategories.AsEnumerable() on q.CategoryId equals qc.CategoryId
                                     join pi in dbFund.PISubs.AsEnumerable() on p.PIFileSubId equals pi.PIFileSubId
                                     join u in dbProd.viewQuantityUnits.AsEnumerable() on pi.QuantityUnitId equals u.QuantityUnitId
                                     join r in dbProd.ItemVariants.AsEnumerable() on q.ItemId equals r.ItemId into s
                                     from t in s.DefaultIfEmpty()
                                     select new { ItemName = t == null ? q.ItemName : pi.FactoryId == 2 || pi.FactoryId == 16 ? q.ItemName : qc.CategoryName, p.GRRQuantity, p.PipelineQuantity, u.QuantityUnit, p.PLESubId });

                        //var ic = items.Count(); var tq = items.Select(c => c.StockInTransit).Sum();
                        var pipeline = pldetails.FirstOrDefault();
                        var itemlist = items.Select(c => c.ItemName).Distinct().Aggregate((i, j) => i + "," + j);
                        var qty = items.Select(c => new { c.PLESubId, c.QuantityUnit, c.GRRQuantity }).Distinct().GroupBy(c => c.QuantityUnit).Select(c => new { Unit = c.Key, StockInTransit = c.Sum(l => l.GRRQuantity) }).Select(c => c.StockInTransit.ToString("##,##0.00") + " " + c.Unit).Aggregate((i, j) => i + "," + j);
                        var ci_qty = items.Select(c => new { c.PLESubId, c.QuantityUnit, c.PipelineQuantity }).Distinct().GroupBy(c => c.QuantityUnit).Select(c => new { Unit = c.Key, StockInTransit = c.Sum(l => l.PipelineQuantity) }).Select(c => c.StockInTransit.ToString("##,##0.00") + " " + c.Unit).Aggregate((i, j) => i + "," + j);
                        var factory = pldetails.Select(c => c.FactoryName).Distinct().Aggregate((i, j) => i + "," + j);
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                        var user = db.Sys_User_Name.Find(clsMain.getCurrentUser());

                        var row = (from p in db.EmailLibraries.AsEnumerable()
                                   join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                                   where p.EmailTypeId == 12 && q.FactoryId == grrMain.FactoryId
                                   select new
                                   {
                                       p.TypeName,
                                       p.EmailBody,
                                       q.EmailGroupFrom,
                                       q.EmailGroupTo,
                                       q.EmailGroupCC,
                                       q.EmailGroupBCC,
                                       p.Subject
                                   }).FirstOrDefault();

                        EMailCollectionView MailCollection = new EMailCollectionView();
                        MailCollection.EmailType = row.TypeName;
                        MailCollection.ModuleName = "IPMS";

                        //MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom); // clsMain.getEmail(user.eMail);
                        //MailCollection.EmailTo = "apu.sarwar@kdsgroup.net; luna.dasgupta@kdsgroup.net;"; // clsMain.getEmail(row.EmailGroupTo);
                        //MailCollection.EmailCC = "mohammad.noman@kdsgroup.net"; // clsMain.getEmail(row.EmailGroupCC);
                        //MailCollection.EmailBCC = ""; // clsMain.getEmail(row.EmailGroupBCC + "; " + user.eMail);

                        MailCollection.EmailFrom = clsMain.getEmail(user.eMail);  //clsMain.getEmail(row.EmailGroupFrom); // 
                        MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo); //"apu.sarwar@kdsgroup.net; luna.dasgupta@kdsgroup.net;"; // 
                        MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);  //"mohammad.noman@kdsgroup.net"; // 
                        MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC + "; " + user.eMail);  //""; // 
                        //MailCollection.EmailTo = "mohammad.noman@kdsgroup.net";

                        MailCollection.EmailSubject = row.Subject.Replace("@LCNo", pipeline.LCNo);
                        MailCollection.EmailBody = row.EmailBody.Replace("@Addressee", Request.Params["MailAddressee"])
                            .Replace("@MRRDate", grrMain.GRRDate.ToString("dd-MMM-yyyy HH:MM"))
                            .Replace("@MRRNo", grrMain.GRRNo)
                            .Replace("@ReceivedDate", grrMain.ReceiveDate.ToString("dd-MMM-yyyy"))
                            .Replace("@PortDeliveryDate", pipeline.p.PortDeliveryDate.ToString("dd-MMM-yyyy"))
                            .Replace("@PRNo", pipeline.PRNo)
                            .Replace("@PRDate", pipeline.PRDate.ToString("dd-MMM-yyyy"))
                            .Replace("@LCNo", pipeline.LCNo)
                            .Replace("@LotNo", pipeline.LotNo)
                            .Replace("@LCDate", pipeline.LCDate.ToString("dd-MMM-yyyy"))
                            .Replace("@ItemList", itemlist)
                            .Replace("@TotalQty", qty)
                            .Replace("@TotalCIQty", ci_qty)
                            .Replace("@Brand", pipeline.BrandName)
                            .Replace("@PortDeliveryDate", pipeline.PortDeliveryDate.ToString("dd-MMM-yyyy"))
                            .Replace("@Factory", factory)
                            .Replace("@Supplier", pipeline.SupplierName)
                            .Replace("@DeliveryOfficer", textInfo.ToTitleCase(user.ShortName.ToLower()))
                            .Replace("@Remarks", grrMain.Remarks);

                        MailCollection.EmailAttachmentLink = "";
                        MailCollection.CreateAttachment = 0;
                        MailCollection.CreatedBy = clsMain.getCurrentUser();

                        EMailCollection mail = new EMailCollection(MailCollection);

                        dbEM.EMailCollections.Add(mail);

                        dbEM.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    _message = String.Format("{0} successful without email sending.\nMRR No : " + grrMain.GRRNo, save_flag);
                }

                return Content(_message);
            }
            catch(Exception ex)
            {
                return Json(new {status = "Error", message = "Save failed.\nReason: " + ex.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /GRR/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            GRRView grrView = (GRRView)db.GRRMains.Find(id).Convert(new GRRView());
            if (grrView == null)
            {
                return HttpNotFound();
            }

            ViewBag.Factory = new SelectList(db.viewFactories, "FactoryId", "FactoryName", grrView.FactoryId);
            ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName", grrView.SupplierId);

            return View(grrView);
        }

        // POST: /GRR/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Update([Bind(Include = "GRRId,FactoryId,SupplierId,GRRNo,GRRDate,LCNo,LCDate")] GRRView grrView, ICollection<GRRSubView> grrSubView)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Entry(gRRMain).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            return View();
        }

        // GET: /GRR/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GRRMain gRRMain = db.GRRMains.Find(id);
            if (gRRMain == null)
            {
                return HttpNotFound();
            }
            return View(gRRMain);
        }

        // POST: /GRR/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GRRMain gRRMain = db.GRRMains.Find(id);
            db.GRRMains.Remove(gRRMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult FactoryDetails(int? id, DateTime? mrrdate)
        {
            string mrrno = "MRR/" + dbProd.Factories.Find(id).Prefix + "/" + db.GRRMains.Where(l => l.Year == mrrdate.Value.Year && l.FactoryId == id).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + mrrdate.Value.ToString("MM/yy").ToUpperInvariant();

            return Json(new { mrrno = mrrno }, JsonRequestBehavior.AllowGet);
        }

        // GET: LC
        public ActionResult LotList(int id, string type = "create")
        {
            if (type.ToLower().Equals("edit"))
                //ViewBag.LotNos = new SelectList(db.PipelineMains.Where(l => l.LCFileId == id), "PLEId", "LotNo");
                ViewBag.LotNos = new SelectList((
                    from q in db.PipelineMains.Where(l => l.LCFileId == id).AsEnumerable()
                    join r in db.PipelineSubs.AsEnumerable() on q.PLEId equals r.PLEId
                    join s in dbFund.PISubs.AsEnumerable() on r.PIFileSubId equals s.PIFileSubId
                    join t in db.GRRMains.AsEnumerable() on new { q.PLEId, s.FactoryId } equals new { t.PLEId, t.FactoryId } into u
                    from v in u.DefaultIfEmpty()
                    where v == null
                    select q
                    ).Distinct().ToList(), "PLEId", "LotNo");
            else
                ViewBag.LotNos = new SelectList((
                    from p in db.Custom_Type.Where(l => l.Flag.Equals("LOTNO")).AsEnumerable()
                    join q in db.PipelineMains.Where(l => l.LCFileId == id).AsEnumerable() on p.TypeName equals q.LotNo into r
                    from s in r.DefaultIfEmpty()
                    join t in db.GRRMains.AsEnumerable() on s.PLEId equals t.PLEId into u
                    from v in u.DefaultIfEmpty()
                    where s == null && v == null
                    select p
                    ).Distinct().ToList(), "TypeName", "TypeName");

            return PartialView();
        }
        
        // GET: LC
        public ActionResult LotDetails(int id)
        {
            try
            {
                var pl = db.PipelineMains.Find(id);
                var polid = pl.POLId == 0 ? dbFund.LCMains.Find(pl.LCFileId).POLId : pl.POLId;

                var requisition_list = (from p in pl.PipelineSubs.AsEnumerable()
                                        join q in dbFund.PISubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId
                                        join r in db.PRSubs.AsEnumerable() on q.PRSubId equals r.PRSubId
                                        join s in db.PRMains.AsEnumerable() on r.PRId equals s.PRId
                                        select s.PRNo
                                       ).Distinct().ToList().Aggregate((i, j) => i + ", " + j);

                if (pl == null)
                    throw new Exception("Invalid lot no.");

                return Json(new { plno = pl.PLENo, sd = pl.ShipmentDate, bl = pl.BLDate, pe = pl.PortArivalDate, se = pl.StoreArivalDate, 
                    lrd = pl.LotReceivedDate, dab = pl.DocArrivalAtBankDate, fe = pl.FactoryETA, ll = pl.IsLastLot, sr = pl.IsLotReceived, 
                    cl = pl.CurrentLocation, rm = pl.Remarks, ct = clsMain.getCurrentTime(), requisition_list = requisition_list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception("Error");
            }
        }

        // GET: LC
        public ActionResult LotItemDetails(int id, short facid, DateTime mrrdate)
        {
            try
            {
                var bdt_rate = db.CurrencyConversionRates.Where(c => c.RateYear == mrrdate.Year && c.RateMonth == mrrdate.Month).FirstOrDefault().BDTRate;
                var items = (from p in dbFund.PISubs.AsEnumerable()
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                             join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                             join s in db.PipelineSubs.AsEnumerable() on p.PIFileSubId equals s.PIFileSubId
                             join u in dbProd.CustomTypes.AsEnumerable() on p.CurrencyId equals u.TypeId
                             join w in db.PRSubs.AsEnumerable() on p.PRSubId equals w.PRSubId
                             join x in db.PRMains.AsEnumerable() on w.PRId equals x.PRId
                             join z in dbFund.LandedCostings.AsEnumerable() on s.PipelineMain.PLEId equals z.PLEId into xz
                             from y in xz.DefaultIfEmpty()
                             where s.PLEId == id && x.FactoryId == facid && s.StockInTransit > 0
                             select new
                             {
                                 p,
                                 q,
                                 r,
                                 s,
                                 CurrencyType = u.TypeName,
                                 CurrencyId = u.TypeId,
                                 IsLandedCostingDone = y == null ? 0 : 1,
                                 CostValue = y == null ? 0 : y.CostValue,
                                 totalcost = (y == null ? 0 : y.LCOpeningCommission + y.LCOpeningCommissionVAT +
                                 y.SwiftCharges + y.StampAndStationery + y.PostageAndCourier + y.AmendmentCharges +
                                 y.CreditReportCharges + y.ShippingGuaranteeCommission + y.CNFCost +
                                 y.InsurancePremium + y.AcceptanceCommission + y.Duty + y.AIT + y.LIBOR +
                                 y.HandlingCharge + y.CostValue + y.FreightCharges + y.Margin)
                             }
                         ).Select(c => new GRRSubView()
                         {
                             CategoryId = c.q.CategoryId,
                             ProductId = c.q.ItemId,
                             ProductName = c.q.ItemName,
                             PipelineQuantity = c.s.StockInTransit,
                             QuantityUnit = c.r.QuantityUnit,
                             QuantityUnitId = c.r.QuantityUnitId,
                             ImportPriceFC = c.p.UnitPrice,
                             CurrencyCode = c.CurrencyType,
                             CurrencyId = c.CurrencyId,
                             LandedPriceBDT = c.IsLandedCostingDone == 0 ? c.p.UnitPrice * bdt_rate : c.s.StockInTransit == 0 || c.CostValue == 0 ? 0 : ((c.totalcost * (c.p.BDTRate * c.p.UnitPrice * c.s.StockInTransit)) / c.CostValue) / c.s.StockInTransit,
                             GRRQuantity = c.s.StockInTransit,
                             PIFileSubId = c.s.PIFileSubId,
                             PLESubId = c.s.PLESubId,
                             RowNo = c.s.RowNo
                         }).ToList();

                ViewBag.lotdate = db.PipelineMains.Find(id).PortArivalDate.ToString("dd-MMM-yyyy");

                if (items == null)
                    throw new Exception("Invalid lot no.");

                return PartialView("RowTemplate", items);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed.\nReason: " + ex.Message, ex.InnerException);
            }
        }

        [HttpPost]
        public ActionResult SRRList(short id, short comid, int? value, DateTime? mrr_date)
        {
            var srr_type_dict = new Dictionary<short, short>();
            srr_type_dict.Add(31, 65);
            srr_type_dict.Add(54, 62);
            srr_type_dict.Add(35, 63);
            srr_type_dict.Add(32, 60);
            srr_type_dict.Add(33, 61);
            srr_type_dict.Add(58, 62);
            srr_type_dict.Add(34, 1);

            if (id == 31)
            {
                var srr = (from p in db.SRRMains.AsEnumerable()
                          join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                          //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = c }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                          join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                          join s in db.GRRSubs.GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into ss
                          from qs in ss.DefaultIfEmpty()
                          where p.FactoryId == comid && p.SRRTypeId == srr_type_dict[id] && r.IssueMain.IssueSDate <= mrr_date && p.SRRDate >= DateTime.Parse("01-Mar-2022") && r.Quantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                          select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "SRRId", "SRRNo", value ?? '0');
            }

            else if (id == 34)
            {
                var srr = (from p in db.IssueReturnMains.AsEnumerable()
                           join q in db.IssueReturnSubs.AsEnumerable() on p.ReturnId equals q.ReturnId
                           where p.FactoryId == comid
                           select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "ReturnId", "ReturnNo", value ?? '0');
            }

            else if (id == 54)
            {
                var srr = (from p in db.SRRMains.AsEnumerable()
                           join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                           //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = c }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                           join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                           join s in db.GRRSubs.GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into ss
                           from qs in ss.DefaultIfEmpty()
                           where p.FactoryId == comid && p.SRRTypeId == srr_type_dict[id] && r.IssueMain.IssueSDate <= mrr_date && p.SRRDate >= DateTime.Parse("01-Mar-2022") && r.Quantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                           select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "SRRId", "SRRNo", value ?? '0');
            }
            else if (id == 35)
            {
                var srr = (from p in db.SRRMains.AsEnumerable()
                           join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                           //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = c }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                           join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                           join s in db.GRRSubs.GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into ss
                           from qs in ss.DefaultIfEmpty()
                           where p.FactoryId == comid && p.SRRTypeId == srr_type_dict[id] && r.IssueMain.IssueSDate <= mrr_date && r.Quantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                           select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "SRRId", "SRRNo", value ?? '0');
            }
            else if (id == 58)
            {
                var srr = (from p in db.SRRMains.AsEnumerable()
                           join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                           //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = c }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                           join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                           join s in db.GRRSubs.GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into ss
                           from qs in ss.DefaultIfEmpty()
                           where p.ReferenceId == comid && p.SRRTypeId == srr_type_dict[id] && r.IssueMain.IssueSDate <= mrr_date && r.Quantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                           select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "SRRId", "SRRNo", value ?? '0');
            }
            else if (id == 33)
            {
                var srr = (from p in db.SRRMains.AsEnumerable()
                           join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                           join s in db.GRRSubs.GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on q.SRRSubId equals s.SRRSubId into ss
                           from qs in ss.DefaultIfEmpty()
                           where p.FactoryId == comid && p.SRRTypeId == srr_type_dict[id] && p.SRRSDate <= mrr_date && p.SRRDate >= DateTime.Parse("01-Mar-2022") && q.ExchangeQuantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                           select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "SRRId", "SRRNo", value ?? '0');
            }

            


            else
            {
                var srr = (from p in db.SRRMains.AsEnumerable()
                           join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                           join s in db.GRRSubs.GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on q.SRRSubId equals s.SRRSubId into ss
                           from qs in ss.DefaultIfEmpty()
                           where p.FactoryId == comid && p.SRRTypeId == srr_type_dict[id] && p.SRRSDate <= mrr_date && p.SRRDate >= DateTime.Parse("01-Mar-2022") && q.SRRQuantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                           select p).Distinct().ToList();

                ViewBag.SRRNo = new SelectList(srr, "SRRId", "SRRNo", value ?? '0');
            }

            return PartialView();
        }

        // GET: LC
        public ActionResult SRRItemDetails(int id, short type_id)
        {
            try
            {
                var items = new List<GRRSubView>();
                if (type_id == 31)
                {
                    items = (from p in db.SRRMains.AsEnumerable()
                            join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                            join qi in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals qi.ItemId
                            join qu in dbProd.viewQuantityUnits.AsEnumerable() on q.UnitId equals qu.QuantityUnitId
                            //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = c }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                            join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                            join s in db.GRRSubs.GroupBy(c => new {c.SRRSubId, c.IssueSubId}).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into ss
                            from qs in ss.DefaultIfEmpty()
                            where p.SRRId == id && r.Quantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                            select new { q, qi, qu, r, BalanceQuantity = r.Quantity - (qs == null ? 0 : qs.GRRQuantity) }).Distinct().Select(c => new GRRSubView()
                            {
                                CategoryId = c.q.CategoryId,
                                ProductId = c.qi.ItemId,
                                ProductName = c.qi.ItemName,
                                PipelineQuantity = c.BalanceQuantity,
                                QuantityUnit = c.qu.QuantityUnit,
                                QuantityUnitId = c.qu.QuantityUnitId,
                                ImportPriceFC = Convert.ToDecimal(c.r.Rate.ToString("##0.0000")),
                                CurrencyCode = "BDT",
                                CurrencyId = 23,
                                LandedPriceBDT = Convert.ToDecimal(c.r.Rate.ToString("##0.0000")),
                                GRRQuantity = c.BalanceQuantity,
                                SRRSubId = c.q.SRRSubId,
                                IssueSubId = c.r.IssueSubId,
                                Remarks = "RECEVIED AGAINST ISSUE # " + c.r.IssueMain.IssueNo,
                                RowNo = c.q.RowNo
                            }).ToList();
                }
                else if (type_id == 35 || type_id == 54 || type_id == 58)
                {
                    items = (from p in db.SRRMains.AsEnumerable()
                             join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                             join qi in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals qi.ItemId
                             join qu in dbProd.viewQuantityUnits.AsEnumerable() on q.UnitId equals qu.QuantityUnitId
                             //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = (c.Sum(l => l.Quantity) == 0 ? 0 : c.Sum(l => l.Rate * l.Quantity) / c.Sum(l => l.Quantity)) }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                             join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                             join s in db.GRRSubs.GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on r.IssueSubId equals s.IssueSubId into ss
                             from qs in ss.DefaultIfEmpty()
                             where p.SRRId == id && r.Quantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                             select new { q, qi, qu, r, BalanceQuantity = r.Quantity - (qs == null ? 0 : qs.GRRQuantity) }).Distinct().Select(c => new GRRSubView()
                             {
                                 CategoryId = c.q.CategoryId,
                                 ProductId = c.qi.ItemId,
                                 ProductName = c.qi.ItemName,
                                 PipelineQuantity = c.BalanceQuantity,
                                 QuantityUnit = c.qu.QuantityUnit,
                                 QuantityUnitId = c.qu.QuantityUnitId,
                                 ImportPriceFC = c.r.Rate,
                                 CurrencyCode = "BDT",
                                 CurrencyId = 23,
                                 LandedPriceBDT = c.r.Rate,
                                 GRRQuantity = c.BalanceQuantity,
                                 SRRSubId = c.q.SRRSubId,
                                 IssueSubId = c.r.IssueSubId,
                                 Remarks = "RECEVIED AGAINST ISSUE # " + c.r.IssueMain.IssueNo,
                                 RowNo = c.q.RowNo
                             }).ToList();
                }
                else if (type_id == 33)
                {
                    items = (from p in db.SRRMains.AsEnumerable()
                             join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                             join qi in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals qi.ItemId
                             join qu in dbProd.viewQuantityUnits.AsEnumerable() on q.UnitId equals qu.QuantityUnitId
                             //join r in db.IssueSubs.Where(c => c.Status == 1).GroupBy(c => new { c.SRRSubId, c.IssueSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, IssueSubId = c.Key.IssueSubId, IssueQuantity = c.Sum(l => l.Quantity), Rate = (c.Sum(l => l.Quantity) == 0 ? 0 : c.Sum(l => l.Rate * l.Quantity) / c.Sum(l => l.Quantity)) }).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                             //join r in db.IssueSubs.Where(c => c.Status == 1).AsEnumerable() on q.SRRSubId equals r.SRRSubId
                             join s in db.GRRSubs.GroupBy(c => new { c.SRRSubId }).Select(c => new { SRRSubId = c.Key.SRRSubId, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on q.SRRSubId equals s.SRRSubId into ss
                             from qs in ss.DefaultIfEmpty()
                             where p.SRRId == id && q.ExchangeQuantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                             select new { q, qi, qu, p, BalanceQuantity = q.ExchangeQuantity - (qs == null ? 0 : qs.GRRQuantity) }).Distinct().Select(c => new GRRSubView()
                             {
                                 CategoryId = c.q.CategoryId,
                                 ProductId = c.qi.ItemId,
                                 ProductName = c.qi.ItemName,
                                 PipelineQuantity = c.BalanceQuantity,
                                 QuantityUnit = c.qu.QuantityUnit,
                                 QuantityUnitId = c.qu.QuantityUnitId,
                                 ImportPriceFC = 0,
                                 CurrencyCode = "BDT",
                                 CurrencyId = 23,
                                 LandedPriceBDT = 0,
                                 GRRQuantity = c.BalanceQuantity,
                                 SRRSubId = c.q.SRRSubId,
                                 IssueSubId = 0,
                                 Remarks = "RECEVIED AGAINST EXHANGE REQ # " + c.p.SRRNo,
                                 RowNo = c.q.RowNo
                             }).ToList();
                }
                else
                {
                    items = (from p in db.SRRMains.AsEnumerable()
                            join q in db.SRRSubs.AsEnumerable() on p.SRRId equals q.SRRId
                            join qi in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals qi.ItemId
                            join qu in dbProd.viewQuantityUnits.AsEnumerable() on q.UnitId equals qu.QuantityUnitId
                            join s in db.GRRSubs.GroupBy(c => c.SRRSubId).Select(c => new { SRRSubId = c.Key, GRRQuantity = c.Sum(l => l.GRRQuantity) }).AsEnumerable() on q.SRRSubId equals s.SRRSubId into ss
                            from qs in ss.DefaultIfEmpty()
                            where p.SRRId == id && q.SRRQuantity - (qs == null ? 0 : qs.GRRQuantity) > 0
                            select new { q, qi, qu, BalanceQuantity = q.SRRQuantity - (qs == null ? 0 : qs.GRRQuantity) }).Distinct().Select(c => new GRRSubView()
                            {
                                CategoryId = c.q.CategoryId,
                                ProductId = c.qi.ItemId,
                                ProductName = c.qi.ItemName,
                                PipelineQuantity = c.BalanceQuantity,
                                QuantityUnit = c.qu.QuantityUnit,
                                QuantityUnitId = c.qu.QuantityUnitId,
                                ImportPriceFC = 0,
                                CurrencyCode = "BDT",
                                CurrencyId = 23,
                                LandedPriceBDT = 0,
                                GRRQuantity = c.BalanceQuantity,
                                SRRSubId = c.q.SRRSubId,
                                IssueSubId = 0,
                                RowNo = c.q.RowNo
                            }).ToList();
                }

                ViewBag.lotdate = db.SRRMains.Find(id).SRRDate.ToString("dd-MMM-yyyy");

                if (items == null)
                    throw new Exception("Invalid srr no.");

                return PartialView("RowTemplate", items);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed.\nReason: " + ex.Message, ex.InnerException);
            }
        }

        [HttpPost]
        public ActionResult LCDetails(short id)
        {
            var lc =  dbFund.LCMains.Find(id);
            var sup = db.Suppliers.Find(lc.SupplierId);

            return Json(new { supid = lc.SupplierId, supname = sup.SupplierName, lcdate = lc.LCDate.ToString("dd-MMM-yyyy"), shipdate = lc.ShipmentDate }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RefList(short id, short comid, int? value)
        {
            if (id == 30)
            {
                var lc = (from p in db.PipelineMains.AsEnumerable()
                          join q in dbFund.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                          join r in dbFund.LCWisePIs.AsEnumerable() on q.LCFileId equals r.LCFileId
                          join s in dbFund.PISubs.AsEnumerable() on r.PIFileId equals s.PIFileId
                          join t in db.PRSubs.AsEnumerable() on s.PRSubId equals t.PRSubId
                          join u in db.PRMains.AsEnumerable() on t.PRId equals u.PRId
                          join v in db.GRRMains.AsEnumerable() on new { p.PLEId, u.FactoryId } equals new { v.PLEId, v.FactoryId } into w
                          from x in w.DefaultIfEmpty()
                          where /*p.IsLotReceived == 0 &&*/ u.FactoryId == comid && x == null
                          select q).Distinct().ToList();

                ViewBag.LC = new SelectList(lc, "LCFileId", "LCNo", value ?? 0);
            }
            else
                ViewBag.LC = new SelectList(dbFund.LCMains.Where(c => c.LCFileId == 0), "LCFileId", "LCNo", value ?? 0);

            return PartialView();
        }

        [HttpPost]
        public ActionResult POList(short id, short comid, int? value)
        {
            if (id == 30)
            {
                //var cat_list = new List<int>() { 14, 18, 19, 20, 21, 23 };
                //var cc_id = dbProd.Factories.Find(comid).POCostCenterId;
                //var lc = (from p in dbPO.Mrrs.AsEnumerable()
                //          join q in dbPO.Mrr_Details.AsEnumerable() on p.Mrr_ID_Pr.Trim() equals q.Mrr_ID_Pr.Trim()
                //          join r in dbPO.Items.AsEnumerable() on q.Item_ID equals r.Item_ID
                //          join s in dbPO.Cats.AsEnumerable() on r.Cat_ID equals s.Cat_id
                //          join t in dbPO.Requisition_Details.AsEnumerable() on q.rq_det_id.Trim() equals t.Rq_Det_ID.Trim()
                //          join u in dbPO.PO_Requisitions.AsEnumerable() on t.Rq_ID_Pr.Trim() equals u.Rq_ID_Pr.Trim()
                //          join v in dbPO.Cost_Center.AsEnumerable() on u.Account.Trim() equals v.CC_Name.Trim()
                //          join w in db.GRRMains.AsEnumerable() on p.Mrr_ID_Pr.Trim() equals w.MRRId.ToString() into ww
                //          from x in ww.DefaultIfEmpty()
                //          where p.Mrr_Date >= DateTime.Parse("01-Jan-2022") && cat_list.Contains(r.Cat_ID) && v.CC_Id.Trim() == cc_id.Trim() && x == null
                //          select new { p.Mrr_ID, p.Mrr_ID_Pr, v.CC_Id, v.CC_Name }).Distinct().ToList();
                var sli = db.Database.SqlQuery<_SelectListItem>("Exec prcGetSelectListItem " + comid.ToString() + ", 'MRR'").Select(c => new { c.Id, c.Text }).Distinct();

                ViewBag.PONo = new SelectList(sli, "Id", "Text", value ?? '0');
            }
            else
                ViewBag.PONo = new SelectList(dbFund.LCMains.Where(c => c.LCFileId == 0), "LCFileId", "LCNo", value ?? 0);

            return PartialView();
        }

        public ActionResult DetailsTemplate(GRRSubView grrSubView, short? facid)
        {
            var catList = (from p in dbProd.ItemCategories.AsEnumerable()
                           join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                           join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                           join s in dbProd.Factories.AsEnumerable() on r.FactoryId equals s.FactoryId
                           where s.FactoryId == facid && p.StageLevel == 4
                           select p
                          ).Distinct().ToList();

            var qUnitList = (grrSubView.ProductId == 0 ? "" : dbProd.ItemInfoes.Find(grrSubView.ProductId).QuantityUnitList).Replace(" ", "").Split(',');

            ViewBag.Category = new SelectList(catList, "CategoryId", "CategoryName", grrSubView.CategoryId);
            //ViewBag.Product = new SelectList(dbProd.ItemInfoes.Where(l => l.CategoryId == grrSubView.CategoryId), "ItemId", "ItemName", grrSubView.ProductId);
            ViewBag.Product = new SelectList((from p in dbProd.ItemInfoes.AsEnumerable()
                                              join q in dbProd.ItemFactories.AsEnumerable() on p.ItemId equals q.ItemId
                                              where q.FactoryId == facid && p.CategoryId == grrSubView.CategoryId
                                              select p), "ItemId", "ItemName", grrSubView.ProductId);
            ViewBag.QuantityUnit = new SelectList(dbProd.viewQuantityUnits.Where(l => qUnitList.Contains(l.QuantityUnitId.ToString())), "QuantityUnitId", "QuantityUnit", grrSubView.QuantityUnitId);
            ViewBag.Currency = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("CURRCODE")), "TypeId", "TypeName", grrSubView.CurrencyId);

            return PartialView(grrSubView);
        }

        [HttpPost]
        public ActionResult ChallanList(short id, short comid, int? value)
        {
            if (id == 53)
            {
                var sli = db.Database.SqlQuery<_SelectListItem>("Exec prcGetSelectListItem " + comid.ToString() + ", 'CHALLAN'").Select(c => new { c.Id, c.Text }).Distinct();

                ViewBag.InvoiceNo = new SelectList(sli, "Id", "Text", value ?? '0');
            }
            else
                ViewBag.InvoiceNo = new SelectList(dbFund.LCMains.Where(c => c.LCFileId == 0), "LCFileId", "LCNo", value ?? 0);

            return PartialView();
        }

        [HttpPost]
        public ActionResult ChallanDetails(int id)
        {
            var book = dbSDMS.InvoiceMains.Find(id);
            var sup = dbSDMS.CustomerGenInfoes.Find(book.CustomerId);
            var req = db.Database.SqlQuery<PO_Requisition>("Exec prcGetSelectListItemDetails " + id + ", 'CHALLAN'").Select(c => c.Rq_ID).Distinct().Aggregate((i, j) => i + ", " + j);

            return Json(new { supid = sup.CustomerId, supname = sup.CustomerName.ToUpper(), lcdate = book.BookingDate.ToString("dd-MMM-yyyy"), shipdate = book.BookingDate.ToString("dd-MMM-yyyy"), rq_no = book.BookingNo }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChallanProductDetails(int id, short facid)
        {
            ICollection<GRRSubView> mrrsubs = new List<GRRSubView>();

            if (id > 0)
            {
                short row_no = 1, q_unit = 0;
                var _subs = db.Database.SqlQuery<GRRSubView>("Exec prcGetSelectListItemDetails " + id + ", 'CHALLAN'").Distinct();
                mrrsubs = (from p in _subs.AsEnumerable()
                           join v in db.UnitNameConversions.AsEnumerable() on p.QuantityUnit.Trim().ToUpper() equals v.OldUnitName into w
                           from x in w.DefaultIfEmpty()
                           join y in dbProd.viewQuantityUnits.AsEnumerable() on (x == null ? "PCS" : x.NewUnitName).Trim().ToUpper() equals y.QuantityUnit into yy
                           from z in yy.DefaultIfEmpty()
                           select new { p.SRRSubId, p.ProductName, p.ImportPriceFC, p.LandedPriceBDT, p.PipelineQuantity, QuantityUnit = z == null ? "PCS" : z.QuantityUnit, QuantityUnitId = z == null ? q_unit : z.QuantityUnitId }).Distinct().Select(c => new GRRSubView()
                           {
                               CategoryId = 0,
                               ProductId = 0,
                               ProductName = c.ProductName,
                               PipelineQuantity = c.PipelineQuantity,
                               QuantityUnit = c.QuantityUnit,
                               QuantityUnitId = c.QuantityUnitId,
                               GRRQuantity = c.PipelineQuantity,
                               ImportPriceFC = c.ImportPriceFC,
                               CurrencyId = 21,
                               CurrencyCode = "BDT",
                               LandedPriceBDT = c.LandedPriceBDT,
                               RowNo = row_no++,
                               Remarks = c.ProductName,
                               SRRSubId = c.SRRSubId, 
                               IssueSubId = 0,
                               ReceiveThrough = "SALES"
                           }).ToList();
            }

            ViewBag.ProductName = new SelectList(dbProd.ItemInfoes.Join(dbProd.ItemFactories, p => p.ItemId, q => q.ItemId, (p, q) => new { item = p, factory = q }).Where(l => l.factory.FactoryId == facid).Select(c => c.item).Distinct(), "ItemId", "ItemName", 0);

            return PartialView("RowTemplate", mrrsubs);
        }

        [HttpPost]
        public ActionResult PODetails(int id)
        {
            var lc = dbPO.Mrrs.Find(id.ToString());
            var sup = dbPO.PO_Suppliers.Find(lc.Supp_ID.ToString());
            var req = db.Database.SqlQuery<PO_Requisition>("Exec prcGetSelectListItemDetails " + id + ", 'MRR'").Select(c => c.Rq_ID).Distinct().Aggregate((i, j) => i + ", " + j);

            return Json(new { supid = lc.Supp_ID, supname = sup.Supp_Name.ToUpper(), lcdate = lc.Mrr_Date.Value.ToString("dd-MMM-yyyy"), shipdate = lc.Mrr_Date.Value, rq_no = req }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult POProductDetails(int id, short facid)
        {
            ICollection<GRRSubView> mrrsubs = new List<GRRSubView>();
            //var old_to_new_qty = new List<Old2NewQuantityUnit>();
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "KG", NewUnit = "KGS" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "KDS", NewUnit = "KGS" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "PC", NewUnit = "PCS" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "PS", NewUnit = "PCS" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "LTR", NewUnit = "LITTER" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "LITER", NewUnit = "LITTER" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "GAL", NewUnit = "GALLON" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "BOTOL", NewUnit = "BOTTLE" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "CANE", NewUnit = "CAN" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "MTR", NewUnit = "METER" });
            //old_to_new_qty.Add(new Old2NewQuantityUnit() { OldUnit = "YRD", NewUnit = "YARDS" });

            if (id > 0)
            {
                short row_no = 1, q_unit = 0;
                var _subs = db.Database.SqlQuery<GRRSubView>("Exec prcGetSelectListItemDetails " + id + ", 'MRRDET'").Distinct();
                mrrsubs = (from p in _subs.AsEnumerable()
                           join v in db.UnitNameConversions.AsEnumerable() on p.QuantityUnit.Trim().ToUpper() equals v.OldUnitName into w
                           from x in w.DefaultIfEmpty()
                           join y in dbProd.viewQuantityUnits.AsEnumerable() on (x == null ? "PCS" : x.NewUnitName).Trim().ToUpper() equals y.QuantityUnit into yy
                           from z in yy.DefaultIfEmpty()
                           select new { p.SRRSubId, p.ProductName, p.ImportPriceFC, p.PipelineQuantity, QuantityUnit = z == null ? "PCS" : z.QuantityUnit, QuantityUnitId = z == null ? q_unit : z.QuantityUnitId }).Distinct().Select(c => new GRRSubView()
                            {
                                CategoryId = 0,
                                ProductId = 0,
                                ProductName = c.ProductName,
                                PipelineQuantity = c.PipelineQuantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.QuantityUnitId,
                                GRRQuantity = c.PipelineQuantity,
                                ImportPriceFC = c.ImportPriceFC,
                                CurrencyId = 23,
                                CurrencyCode = "BDT",
                                LandedPriceBDT = c.ImportPriceFC,
                                RowNo = row_no++,
                                Remarks = c.ProductName,
                                SRRSubId = c.SRRSubId, 
                                ReceiveThrough = "LPMS"
                            }).ToList();

                //mrrsubs = (from p in dbPO.Mrrs.AsEnumerable()
                //            join q in dbPO.Mrr_Details.AsEnumerable() on p.Mrr_ID_Pr equals q.Mrr_ID_Pr
                //            join r in dbPO.Items.AsEnumerable() on q.Item_ID equals r.Item_ID
                //            join s in dbPO.Cats.AsEnumerable() on r.Cat_ID equals s.Cat_id
                //            join t in dbPO.Requisition_Details.AsEnumerable() on q.rq_det_id equals t.Rq_Det_ID
                //            join u in dbPO.PO_Requisitions.AsEnumerable() on t.Rq_ID_Pr equals u.Rq_ID_Pr
                //            join v in old_to_new_qty.AsEnumerable() on r.Meas_Unit.Trim().ToUpper() equals v.OldUnit into w
                //            from x in w.DefaultIfEmpty()
                //            join y in dbProd.viewQuantityUnits.AsEnumerable() on (x == null ? r.Meas_Unit : x.NewUnit).Trim().ToUpper() equals y.QuantityUnit into yy
                //            from z in yy.DefaultIfEmpty()
                //            where p.Mrr_ID_Pr == id.ToString() && p.Mrr_Date >= DateTime.Parse("01-Jan-2022") && cat_list.Contains(r.Cat_ID) /*p.IsLotReceived == 0 && u.FactoryId == comid && x == null*/
                //            select new { r.Item_Name, q.Rcv_Qty, q.unit_price, QuantityUnit = z == null ? "Test" : z.QuantityUnit, QuantityUnitId = z == null ? q_unit : z.QuantityUnitId }).Distinct().Select(c => new GRRSubView()
                //            {
                //                CategoryId = 0,
                //                ProductId = 0,
                //                ProductName = c.Item_Name,
                //                PipelineQuantity = Convert.ToDecimal(c.Rcv_Qty),
                //                QuantityUnit = c.QuantityUnit,
                //                QuantityUnitId = c.QuantityUnitId,
                //                GRRQuantity = Convert.ToDecimal(c.Rcv_Qty),
                //                ImportPriceFC = Convert.ToDecimal(c.unit_price),
                //                CurrencyId = 23,
                //                CurrencyCode = "BDT",
                //                LandedPriceBDT = Convert.ToDecimal(c.unit_price),
                //                RowNo = row_no++,
                //                Remarks = c.Item_Name,
                //                ReceiveThrough = "MRR"
                //            }).ToList();
            }

            //ViewBag.ProductName = new SelectList(dbProd.ItemInfoes.Join(dbProd.ItemFactories, p => p.ItemId, q => q.ItemId, (p, q) => new { item = p, factory = q }).Where(l => l.factory.FactoryId == facid).Select(c => c.item).Distinct(), "ItemId", "ItemName", 0);

            //Adding for ItemCode and ItemName in Product Cuolumn--Monsur
            ViewBag.ProductName = new SelectList(dbProd.ItemInfoes.Join(dbProd.ItemFactories, p => p.ItemId, q => q.ItemId, (p, q) => new { item = p, factory = q }).Where(l => l.factory.FactoryId == facid).Select(c => new { ItemId = c.item.ItemId, ItemFullName = c.item.ItemCode + " - " + c.item.ItemName }).Distinct(), "ItemId", "ItemFullName", 0);


            return PartialView("RowTemplate", mrrsubs);
        }

        public ActionResult ProductDetails(int id, short type, short facid, short facid2)
        {
            try
            {
                var product = (from p in dbProd.ItemInfoes.AsEnumerable()
                               join q in dbProd.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals q.QuantityUnitId
                               where p.ItemId == id
                               select new { q.QuantityUnitId, q.QuantityUnit }
                              ).FirstOrDefault();

                return Json(new { unitid = product.QuantityUnitId, unit = product.QuantityUnit }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult RowTemplate(int? id)
        {
            ICollection<GRRSubView> grrSubs = new List<GRRSubView>();

            if (id != null)
            {
                grrSubs = (from p in db.GRRSubs.Where(l => l.GRRId == id).AsEnumerable()
                           join s in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals s.ItemId
                           join t in dbProd.ItemCategories.AsEnumerable() on s.CategoryId equals t.CategoryId
                           join u in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals u.QuantityUnitId
                           join w in dbProd.CustomTypes.AsEnumerable() on p.CurrencyId equals w.TypeId
                           select new { p, s.ItemName, t.CategoryId, t.CategoryName, u.QuantityUnit, CurrencyCode = w.TypeName }
                            ).Select(c => new GRRSubView()
                            {
                                CategoryId = c.CategoryId,
                                ProductId = c.p.ProductId,
                                ProductName = c.ItemName,
                                PipelineQuantity = c.p.PipelineQuantity,
                                QuantityUnit = c.QuantityUnit,
                                QuantityUnitId = c.p.QuantityUnitId,
                                GRRQuantity = c.p.GRRQuantity,
                                ImportPriceFC = c.p.ImportPriceFC,
                                CurrencyId = c.p.CurrencyId,
                                CurrencyCode = c.CurrencyCode,
                                LandedPriceBDT = c.p.LandedPriceBDT,
                                RowNo = c.p.RowNo,
                                SRRSubId = 0,
                                IssueSubId = 0,
                                Remarks = c.p.Remarks
                            }).ToList();
            }

            return PartialView(grrSubs);
        }
    }
}
