﻿using InventoryMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.UI.WebControls;

namespace InventoryMS.Controllers
{
    public class SupplierSelectionController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private SDMSEntities dbSDMS = new SDMSEntities();
       
        
        // GET: SupplierSelection
        public ActionResult Index()
        {
            return View(db.SupplierSelectionMains.ToList());
        }


        [CAuthorize]
        public ActionResult Create()
        {
            SupplierSelectionMain supplierSelectionMain = new SupplierSelectionMain();
            supplierSelectionMain.SSEDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.Supplier = new SelectList(db.Suppliers, "SupplierId", "ShortName");
            ViewBag.SupplierSelectionNo = "SSE/####/0000-" + DateTime.Now.ToString("MM/yy");

            ViewBag.Category = new SelectList(dbProd.ItemCategories.Where(c => c.StageLevel.Equals(4) && c.IsActive == 1).OrderBy(c => c.RankId), "CategoryId", "CategoryName");
            ViewBag.Item = new SelectList(dbProd.ItemInfoes, "ItemId", "ItemName");


            return View(supplierSelectionMain);
        }


        [HttpPost]
        [CAuthorize]
        public ActionResult Save(SupplierSelectionMain main, ICollection<SupplierSelectionSub> subs, string prefix = "SSE")
        {
            try
            {
                SupplierSelectionMain smain = new SupplierSelectionMain(main);
                
                smain.SupplierSelectionNo = prefix
               + "/" + dbProd.Factories.Find(smain.FactoryId).Prefix
               + "/" + db.SupplierSelectionMains.Where(l => l.Year == smain.SSEDate.Year && l.FactoryId == smain.FactoryId && l.SupplierSelectionNo.StartsWith(prefix)).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000")
               + "-" + smain.SSEDate.ToString("MM/yy").ToUpperInvariant();

                smain.ApprovedType = 1;
                smain.ApprovedBy = Convert.ToInt16(smain.FactoryId == 3 || smain.FactoryId == 4 || smain.FactoryId == 15 ? 17 : smain.FactoryId == 2 ? 15 : smain.FactoryId == 16 ? 48 : 4);
                smain.ApprovedTime = clsMain.getCurrentTime();
                db.SupplierSelectionMains.Add(smain);

                try
                {
                    db.SaveChanges();
                    foreach (var _sub in subs)
                    {
                        _sub.SupplierSelectionId = smain.SupplierSelectionId;
                    }

                    db.SupplierSelectionSubs.AddRange(subs);
                    db.SaveChanges();

                    return Json(new { status = "Save successful.", cmno = smain.SupplierSelectionNo }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetItemList(int CategoryId)
        {
            var items = dbProd.ItemInfoes.Where(item => item.CategoryId == CategoryId).ToList();
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SupplierSelectionDetails(short rowno = 1, short factory_id = 0)
        {
            try
            {
                var sup_list= db.Suppliers.ToList();
                ViewBag.sup_list = sup_list;

                SupplierSelectionSubView sub = new SupplierSelectionSubView(true);
                sub.RowNo = rowno;
                sub.SuppliersName = "";

                return PartialView(new List<SupplierSelectionSubView>() { sub });
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '6'>Error " + ex.Message + "</td></tr>");
            }
        }


        [HttpPost]
        public JsonResult ReferenceList(DateTime? SSEDate, int id = 0, byte factory_id = 0, string type = "SSE")
        {
            var serialNumber = db.SupplierSelectionMains.Where(l => l.Year == SSEDate.Value.Year && l.FactoryId == factory_id && l.SupplierSelectionNo.StartsWith(type))
                 .Select(c => c.SerialNo + 1)
                 .Max()
                 .GetValueOrDefault(Convert.ToInt16("1"))
                 .ToString("0000");
            string selectionNo = type
                + "/"
                + dbProd.Factories.Find(factory_id).Prefix
                + "/"
                + serialNumber
                + "-" + SSEDate.Value.ToString("MM/yy").ToUpperInvariant();

            return Json(selectionNo, JsonRequestBehavior.AllowGet);
        }
    }
 }


