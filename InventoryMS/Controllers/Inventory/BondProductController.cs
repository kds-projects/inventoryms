﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class BondProductController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();

        // GET: /BondProduct/
        public ActionResult Index()
        {
            var bondproducts = new List<ItemList>();
            return PartialView(bondproducts.ToList());
        }

        // GET: /BondProduct/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BondProduct bondProduct = db.BondProducts.Find(id);
            if (bondProduct == null)
            {
                return HttpNotFound();
            }
            return View(bondProduct);
        }

        // GET: /BondProduct/Create
        public ActionResult Manage()
        {
            ViewBag.BondId = new SelectList(db.Bonds.Select(c => new { c.BondId, LicenseNumber = c.LicenseNumber }), "BondId", "LicenseNumber");
            ViewBag.BondPeriodId = new SelectList(db.BondPeriods.Where(l => l.BondPeriodId == 0), "BondPeriodId", "LicensePeriod");
            ViewBag.CapacityId = new SelectList(db.BondCapacities.Where(l => l.BondId == 0), "CapacityId", "BondSerialNo");
            ViewBag.CategoryId = new SelectList(dbProd.viewItemCategories.Where(l => l.StageLevel == 4), "CategoryId", "CategoryName");
            return View();
        }

        // POST: /BondProduct/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ICollection<BondProductView> bondProductView, int CapacityId)
        {
            if (ModelState.IsValid)
            {
                if (bondProductView != null)
                {
                    foreach (BondProductView bpView in bondProductView)
                    {
                        BondProduct bondProduct = new BondProduct(bpView);
                        bondProduct.CapacityId = CapacityId;

                        db.BondProducts.Add(bondProduct);
                    }
                }

                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            ViewBag.CapacityId = new SelectList(db.BondCapacities, "CapacityId", "LicenseNumber", CapacityId);
            ViewBag.CategoryId = new SelectList(dbProd.viewItemCategories.Where(l => l.StageLevel == 4), "CategoryId", "CategoryName");
            return View(bondProductView);
        }

        // GET: /BondProduct/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BondProduct bondProduct = db.BondProducts.Find(id);
            if (bondProduct == null)
            {
                return HttpNotFound();
            }
            ViewBag.CapacityId = new SelectList(db.BondCapacities, "CapacityId", "LicenseNumber", bondProduct.CapacityId);
            return View(bondProduct);
        }

        // POST: /BondProduct/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="BSPId,CapacityId,ProductId")] BondProduct bondProduct)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bondProduct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CapacityId = new SelectList(db.BondCapacities, "CapacityId", "BondSerialNo", bondProduct.CapacityId);
            return View(bondProduct);
        }

        public ActionResult Delete(int id)
        {
            BondProduct bondProduct = db.BondProducts.Find(id);
            db.BondProducts.Remove(bondProduct);
            try
            {
                db.SaveChanges();

                return Content("success");
            }
            catch (Exception ex)
            {
                throw new Exception("Save failed.\n" + ex.InnerException);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CapacityList(int id)
        {
            ViewBag.CapacityId = new SelectList(db.BondCapacities.Where(l => l.BondPeriodId == id), "CapacityId", "BondSerialNo");

            return PartialView();
        }

        public ActionResult ProductList(short? id, int? capid)
        {
            var bproducts = db.BondProducts.Where(l => l.CategoryId == id && l.CapacityId == capid).Select(c => c.ProductId).ToList();
            var products = dbProd.ItemInfoes.Where(l => l.CategoryId == id && capid > 0).Select(c => new ItemList() { ItemId = c.ItemId, ItemName = c.ItemName, isChecked = bproducts.Contains(c.ItemId) }).ToList();

            return PartialView(products);
        }

        public ActionResult CapProductList(short id)
        {
            var bproducts = (from p in db.BondProducts.AsEnumerable()
                             join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                             join r in dbProd.ItemCategories.AsEnumerable() on q.CategoryId equals r.CategoryId
                             where p.CapacityId == id
                             select new { p, q.ItemName, r.CategoryName }
                                ).Select(c => new ItemList() { BSPId = c.p.BSPId, ItemId = c.p.ProductId, ItemName = c.ItemName, CatName = c.CategoryName }).ToList();

            return PartialView("Index", bproducts);
        }

        [HttpPost]
        public ActionResult UpdateProduct(ICollection<ItemList> itemList, short catid, int capid)
        {
            var id = itemList.Where(l => l.isChecked).Select(c => c.ItemId).ToList();
            var products = db.BondProducts.Where(l => l.CapacityId == capid && l.CategoryId == catid && !id.Contains(l.ProductId)).ToList();
            var newProducts = id.Except(db.BondProducts.Where(l => l.CapacityId == capid && l.CategoryId == catid).Select(c => c.ProductId)).ToList();

            if(products.Count <= newProducts.Count)
            {
                foreach(BondProduct bp in products)
                {
                    bp.ProductId = newProducts[0];
                    db.Entry(bp).State = EntityState.Modified;
                    newProducts.RemoveAt(0);
                }

                for(int c = 0; newProducts.Count > 0; c++)
                {
                    BondProduct bp = new BondProduct();
                    bp.ProductId = newProducts[0];
                    bp.CapacityId = capid;
                    bp.CategoryId = catid;
                    db.BondProducts.Add(bp);
                    newProducts.RemoveAt(0);
                }
            }
            else
            {
                for (int c = 0; c < products.Count; c++)
                {
                    if (newProducts.Count > 0)
                    {
                        products[c].ProductId = newProducts[0];
                        db.Entry(products[c]).State = EntityState.Modified;
                        newProducts.RemoveAt(0);
                    }
                    else
                    {
                        db.Entry(products[c]).State = EntityState.Deleted;
                    }
                }
            }

            try
            {
                db.SaveChanges();

                var bproducts = (from p in db.BondProducts.AsEnumerable()
                                 join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                                 join r in dbProd.ItemCategories.AsEnumerable() on q.CategoryId equals r.CategoryId
                                 where p.CapacityId == capid
                                 select new {p, q.ItemName, r.CategoryName}
                                ).Select(c => new ItemList() { BSPId = c.p.BSPId, ItemId = c.p.ProductId, ItemName = c.ItemName, CatName = c.CategoryName }).ToList();

                return PartialView("Index", bproducts);
            }
            catch (Exception ex)
            {
                throw new Exception("Save failed.\n" + ex.InnerException);
            }
        }
    }
}
