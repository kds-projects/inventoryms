﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class BondController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();

        // GET: /Bond/
        public ActionResult Index()
        {
            var bonds = (from p in db.Bonds.AsEnumerable()
                         join q in dbProd.Companies on p.CompanyId equals q.CompanyId
                         join r in dbProd.viewQuantityUnits on p.CapacityUnitId equals r.QuantityUnitId
                         select new { p, q.CompanyCode, q.CompanyName, r.QuantityUnit }).Select(c => new BondView()
                        {
                            BondId = c.p.BondId,
                            BondCode = c.p.BondCode,
                            CompanyId = c.p.CompanyId,
                            CompanyName = c.CompanyName,
                            LicenseNumber = c.p.LicenseNumber,
                            LicenseRenewalDate = c.p.LicenseRenewalDate,
                            //StartDate = c.p.StartDate,
                            //EndDate = c.p.EndDate,
                            Capacity = c.p.Capacity,
                            CapacityUnitId = c.p.CapacityUnitId,
                            CapacityUnit = c.QuantityUnit,
                            IsActive = c.p.IsActive,
                            LocationName = c.p.Location.LocationName
                        }).ToList();
            return View(bonds);
        }

        // GET: /Bond/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bond bond = db.Bonds.Find(id);
            if (bond == null)
            {
                return HttpNotFound();
            }
            return View(bond);
        }

        // GET: /Bond/Create
        public ActionResult Create()
        {
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "LocationName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit");

            return View();
        }

        // POST: /Bond/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LicenseNumber,LicenseDate,CompanyId,LocationId,LicenseRenewalDate,Capacity,CapacityUnitId,Activity")] BondView bondView, ICollection<BondPeriodView> bondPeriodView)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Bond bond = new Bond(bondView);

                    foreach (BondPeriodView bpv in bondPeriodView)
                    {
                        BondPeriod bp = new BondPeriod(bpv);
                        bp.BondCapacityCheck = true;

                        bond.BondPeriods.Add(bp);
                    }

                    db.Bonds.Add(bond);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            bondView.bondPeriodView = bondPeriodView;

            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "LocationName", bondView.LocationId);
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", bondView.CompanyId);
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", bondView.CapacityUnitId);

            return View(bondView);
        }

        // GET: /Bond/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bond bond = db.Bonds.Find(id);
            if (bond == null)
            {
                return HttpNotFound();
            }

            BondView bondView = (BondView)bond.Convert(new BondView());
            bondView.bondPeriodView = new List<BondPeriodView>();

            foreach (var bp in bond.BondPeriods)
            {
                var bpv = (BondPeriodView)bp.Convert(new BondPeriodView());
                bondView.bondPeriodView.Add(bpv);
            }

            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "LocationName", bondView.LocationId);
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", bondView.CompanyId);
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", bondView.CapacityUnitId);

            return View(bondView);
        }

        // POST: /Bond/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BondId,LicenseNumber,LicenseDate,CompanyId,LocationId,LicenseRenewalDate,Capacity,CapacityUnitId,Activity")] BondView bondView, ICollection<BondPeriodView> bondPeriodView)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Bond bond = db.Bonds.Find(bondView.BondId);

                    int rIndex = 0, cIndex = bond.BondPeriods.Count;
                    if (cIndex >= bondPeriodView.Count)
                    {
                        foreach (var scv in bondPeriodView)
                        {
                            BondPeriod sc = bond.BondPeriods.ToList()[rIndex];
                            sc = (BondPeriod)scv.ConvertNotNull(sc);
                            db.Entry(sc).State = EntityState.Modified;

                            rIndex++;
                        }

                        for (rIndex = 0; rIndex < cIndex; rIndex++)
                        {
                            var scv = bond.BondPeriods.ToList()[rIndex];
                            if (db.Entry(scv).State == EntityState.Unchanged)
                                db.BondPeriods.Remove(scv);//.Entry(scv).State = EntityState.Deleted;
                        }
                    }
                    else
                    {
                        foreach (var scv in bond.BondPeriods)
                        {
                            var sc = bondPeriodView.ToList()[rIndex];
                            var scview = (BondPeriod)sc.ConvertNotNull(scv);
                            db.Entry(scview).State = EntityState.Modified;
                            bondPeriodView.ToList()[rIndex]._editStatus = true;

                            rIndex++;
                        }

                        foreach (var scv in bondPeriodView.Where(l => l._editStatus == false))
                        {
                            var sc = new BondPeriod(scv);
                            bond.BondPeriods.Add(sc);
                        }
                    }

                    bond = (Bond)bondView.ConvertNotNull(bond);
                    db.Entry(bond).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.errormessage = ex.Message;
                }
            }

            bondView.bondPeriodView = bondPeriodView;

            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "LocationName", bondView.LocationId);
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", bondView.CompanyId);
            ViewBag.CapacityUnitId = new SelectList(dbProd.viewQuantityUnits, "QuantityUnitId", "QuantityUnit", bondView.CapacityUnitId);

            return View(bondView);
        }

        // GET: /Bond/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bond bond = db.Bonds.Find(id);
            if (bond == null)
            {
                return HttpNotFound();
            }
            return View(bond);
        }

        // POST: /Bond/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            Bond bond = db.Bonds.Find(id);
            db.Bonds.Remove(bond);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult PeriodDetails(ICollection<BondPeriodView> bondPeriodView)
        {
            return PartialView(bondPeriodView);
        }
    }
}
