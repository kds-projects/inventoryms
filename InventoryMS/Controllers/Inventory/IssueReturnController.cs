﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers.Inventory
{
    public class IssueReturnController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();
        private ProductCubeEntities dbProd = new ProductCubeEntities();
        private FundContext dbFund = new FundContext();
        private SDMSEntities dbSDMS = new SDMSEntities();

        // GET: /IssueReturn/
        public ActionResult Index()
        {
            var issuereturnmains = db.IssueReturnMains;
            return View(issuereturnmains.ToList());
        }

        // GET: /IssueReturn/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueReturnMain issueReturnMain = db.IssueReturnMains.Find(id);
            if (issueReturnMain == null)
            {
                return HttpNotFound();
            }
            return View(issueReturnMain);
        }

        // GET: /IssueReturn/Create
        [CAuthorize]
        public ActionResult Create()
        {
            IssueReturnMain issueReturn = new IssueReturnMain();
            issueReturn.ReturnDate = clsMain.getCurrentTime();

            ViewBag.Factory = new SelectList(dbProd.UserFactories, "FactoryId", "FactoryName");
            ViewBag.ReturnNo = "IRT/####/0000-" + DateTime.Now.ToString("MM/yy");

            return View(issueReturn);
        }

        // POST: /IssueReturn/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(IssueReturnMain main, ICollection<IssueReturnSub> subs)
        {
            try
            {
                IssueReturnMain irmain = new IssueReturnMain(main);
                irmain.ReturnDate = DateTime.Parse(irmain.ReturnDate.ToString("dd-MMM-yyyy") + " " + clsMain.getCurrentTime().ToString("HH:mm:ss"));

                irmain.ReturnNo = "IRT/" + dbProd.Factories.Find(irmain.FactoryId).Prefix + "/" + db.IssueReturnMains.Where(l => l.Year == irmain.ReturnDate.Year && l.FactoryId == irmain.FactoryId).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "-" + irmain.ReturnDate.ToString("MM/yy").ToUpperInvariant();
                irmain.EntryUserId = clsMain.getCurrentUser();
                irmain.EntryDate = clsMain.getCurrentTime();

                foreach (var _sub in subs)
                {
                    IssueReturnSub sub = new IssueReturnSub(_sub);

                    var issue_list = (from p in db.IssueMains.AsEnumerable()
                                      join q in db.IssueSubs.AsEnumerable() on p.IssueId equals q.IssueId
                                      //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                                      join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                                      from t in rs.DefaultIfEmpty()
                                      join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                                      from v in ru.DefaultIfEmpty()
                                      where q.ProductId == sub.ProductId && q.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                                      select new { p.IssueDate, p.IssueId, q.IssueSubId, NetQuantity = q.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) } //q., r.LCFileId, r.IssueLCId, 
                                     ).Distinct().OrderByDescending(c => c.IssueDate);

                    //var return_qty = sub.ReturnQuantity;
                    //short row_no = 1;

                    //foreach (var item in issue_list)
                    //{
                    //    IssueReturnAgainstLC lc = new IssueReturnAgainstLC();
                    //    lc.IssueSubId = item.IssueSubId;
                    //    lc.IssueLCId = 0; // item.IssueLCId;
                    //    lc.ProductId = sub.ProductId;
                    //    lc.LCFileId = 0; // item.LCFileId;
                    //    lc.GRRSubId = 0; // item.GRRSubId;
                    //    lc.FSCId = 0;
                    //    lc.RowNo = row_no++;
                    //    lc.Remarks = "";

                    //    if (return_qty > item.NetQuantity)
                    //    {
                    //        lc.Quantity = item.NetQuantity;
                    //        return_qty -= item.NetQuantity;
                    //    }
                    //    else
                    //    {
                    //        lc.Quantity = return_qty;
                    //        sub.IssueReturnAgainstLCs.Add(lc);
                    //        break;
                    //    }

                    //    sub.IssueReturnAgainstLCs.Add(lc);
                    //}

                    irmain.IssueReturnSubs.Add(sub);
                }

                db.IssueReturnMains.Add(irmain);

                try
                {
                    db.SaveChanges();

                    //var row = (from p in db.EmailLibraries.AsEnumerable()
                    //           join q in db.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                    //           where p.EmailTypeId == 1 && q.FactoryId == irmain.FactoryId
                    //           select new
                    //           {
                    //               p.TypeName,
                    //               p.EmailBody,
                    //               q.EmailGroupFrom,
                    //               q.EmailGroupTo,
                    //               q.EmailGroupCC,
                    //               q.EmailGroupBCC,
                    //               p.Subject
                    //           }).FirstOrDefault();

                    //EMailCollectionView MailCollection = new EMailCollectionView();
                    //MailCollection.EmailType = row.TypeName;
                    //MailCollection.ModuleName = "IPMS";
                    //MailCollection.EmailFrom = clsMain.getEmail(row.EmailGroupFrom);
                    //MailCollection.EmailTo = clsMain.getEmail(row.EmailGroupTo);
                    //MailCollection.EmailCC = clsMain.getEmail(row.EmailGroupCC);
                    //MailCollection.EmailBCC = clsMain.getEmail(row.EmailGroupBCC);
                    //MailCollection.EmailSubject = row.Subject.Replace("@RequisitionNo", irmain.ReturnNo);
                    //MailCollection.EmailBody = row.EmailBody.Replace("@RequisitionNo", irmain.ReturnNo)
                    //    .Replace("@Factory", dbProd.Factories.Find(irmain.FactoryId).ShortName)
                    //    .Replace("@Date", irmain.ReturnDate.ToString("dd-MMM-yyyy"))
                    //    .Replace("@ApprovalLink", @"http://192.168.0.47/ipms/requisition/details/" + irmain.ReturnId);
                    //MailCollection.EmailAttachmentLink = "";
                    //MailCollection.CreateAttachment = 0;
                    //MailCollection.CreatedBy = clsMain.getCurrentUser();

                    //EMailCollection mail = new EMailCollection(MailCollection);

                    //dbEM.EMailCollections.Add(mail);

                    //dbEM.SaveChanges();

                    return Json(new { status = "Save successful.", cmno = irmain.ReturnNo }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /IssueReturn/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueReturnMain issueReturnMain = db.IssueReturnMains.Find(id);
            if (issueReturnMain == null)
            {
                return HttpNotFound();
            }
            ViewBag.IssueId = new SelectList(db.IssueMains, "IssueId", "IssueNo");
            return View(issueReturnMain);
        }

        // POST: /IssueReturn/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ReturnId,IssueId,DepartmentId,ReturnNo,ReturnDate,Month,Year,SerialNo,Remarks,EntryUserId,EntryDate,RevisionUserId,RevisionDate,IPAddress,PCName,ApprovedType,ApprovedBy,ApprovedTime")] IssueReturnMain issueReturnMain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(issueReturnMain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IssueId = new SelectList(db.IssueMains, "IssueId", "IssueNo");
            return View(issueReturnMain);
        }

        // GET: /IssueReturn/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueReturnMain issueReturnMain = db.IssueReturnMains.Find(id);
            if (issueReturnMain == null)
            {
                return HttpNotFound();
            }
            return View(issueReturnMain);
        }

        // POST: /IssueReturn/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IssueReturnMain issueReturnMain = db.IssueReturnMains.Find(id);
            db.IssueReturnMains.Remove(issueReturnMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ReturnDetails(short rowno = 1, short factory_id = 0)
        {
            try
            {
                var item_list = db.Database.SqlQuery<_SelectListItem>("Exec prcGetSelectListItem " + factory_id.ToString() + ", 'CONSUMPTION'").Distinct();

                var cat_list = item_list.Select(c => new { CategoryId = c.Id2, CategoryName = c.Text2 }).Distinct();

                //var cid_list = (from p in dbProd.ItemCategories.AsEnumerable()
                //                join q in dbProd.ItemInfoes.AsEnumerable() on p.CategoryId equals q.CategoryId
                //                join r in dbProd.ItemFactories.AsEnumerable() on q.ItemId equals r.ItemId
                //                where p.StageLevel == 4 && r.FactoryId == factory_id && p.CategoryCode.Substring(0, 2) == "01"
                //                select p.CategoryId
                //               ).Distinct();

                //var item_list = (from p in db.IssueMains.AsEnumerable()
                //                join q in db.IssueSubs.AsEnumerable() on p.IssueId equals q.IssueId
                //                join i in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals i.ItemId
                //                join ic in dbProd.ItemCategories.AsEnumerable() on i.CategoryId equals ic.CategoryId
                //                //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                //                join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                //                from t in rs.DefaultIfEmpty()
                //                join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                //                from v in ru.DefaultIfEmpty()
                //                where /*cid_list.Contains(i.CategoryId) &&*/ p.FactoryId == factory_id && q.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                //                select i
                //               ).Distinct();

                //Dictionary<int, string> d = new Dictionary<int, string>();
                //foreach (var item in cat_list)
                //{
                //    d.Add(item.CategoryId, item.CategoryName);
                //}

                //var cat_list = item_list.Select(c => new { c.ItemCategory.CategoryId, c.ItemCategory.CategoryName }).Distinct();

                ViewBag.CategoryList = cat_list.OrderBy(c => c.CategoryName);
                ViewBag.Category = new SelectList(cat_list.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");
                ViewBag.Product = new SelectList(item_list.OrderBy(c => c.Text).ThenBy(c => c.Text.Length), "Id", "Text");
                ViewBag.Brand = new SelectList(db.Brands.Where(c => c.IsActive == 1), "BrandId", "BrandName", 0);

                ReturnSubView sub = new ReturnSubView(true);
                sub.RowNo = rowno;
                sub.CategoryName = "";

                return PartialView(new List<ReturnSubView>() { sub });
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '6'>Error " + ex.Message + "</td></tr>");
            }
        }

        public ActionResult ProductList(int id, short facid, short rowno)
        {
            var product = (from i in dbProd.ItemInfoes.AsEnumerable()
                           join qu in dbProd.viewQuantityUnits.AsEnumerable() on i.DefaultQUnitId equals qu.QuantityUnitId
                           join q in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid && c.IssueMain.IssueTypeId == 38).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, Quantity = c.Sum(l => l.Quantity) }).AsEnumerable() on i.ItemId equals q.ProductId
                           //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                           //join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                           //from t in rs.DefaultIfEmpty()
                           //join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                           //from v in ru.DefaultIfEmpty()
                           join s in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on i.ItemId equals s.ProductId into rs
                           from t in rs.DefaultIfEmpty()
                           join u in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on i.ItemId equals u.ProductId into ru
                           from v in ru.DefaultIfEmpty()
                           where (i.CategoryId == id || id == 0) && q.Quantity - (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                           select i
                          ).Distinct();

            ViewBag.Product = new SelectList(product.OrderBy(c => c.ItemName).ThenBy(c => c.ItemName.Length), "ItemId", "ItemName");
            ViewBag.rowno = rowno.ToString("00");

            return PartialView();
        }

        public ActionResult ProductDetails(int id, short facid)
        {
            try
            {
                //var product = (from p in dbProd.ItemInfoes.AsEnumerable()
                //               join q in dbProd.viewQuantityUnits.AsEnumerable() on p.DefaultQUnitId equals q.QuantityUnitId
                //               where p.ItemId == id
                //               select new { q.QuantityUnitId, q.QuantityUnit }
                //              ).FirstOrDefault();
                var product = (from i in dbProd.ItemInfoes.AsEnumerable()
                               join qu in dbProd.viewQuantityUnits.AsEnumerable() on i.DefaultQUnitId equals qu.QuantityUnitId
                               join q in db.IssueSubs.Where(c => c.IssueMain.FactoryId == facid && c.IssueMain.IssueTypeId == 38).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, Quantity = c.Sum(l => l.Quantity) }).AsEnumerable() on i.ItemId equals q.ProductId
                               //join r in db.IssueAgainstLCs.AsEnumerable() on q.IssueSubId equals r.IssueSubId
                               //join s in db.IssueReturnAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on q.IssueSubId equals s.IssueSubId into rs
                               //from t in rs.DefaultIfEmpty()
                               //join u in db.ConsumptionAgIssues.GroupBy(c => c.IssueSubId).Select(c => new { IssueSubId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on q.IssueSubId equals u.IssueSubId into ru
                               //from v in ru.DefaultIfEmpty()
                               join s in db.IssueReturnSubs.Where(c => c.IssueReturnMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ReturnQuantity = c.Sum(l => l.ReturnQuantity) }).AsEnumerable() on i.ItemId equals s.ProductId into rs
                               from t in rs.DefaultIfEmpty()
                               join u in db.ConsumptionSubs.Where(c => c.ConsumptionMain.FactoryId == facid).GroupBy(c => c.ProductId).Select(c => new { ProductId = c.Key, ConsumeQuantity = c.Sum(l => l.ConsumeQuantity) }).AsEnumerable() on i.ItemId equals u.ProductId into ru
                               from v in ru.DefaultIfEmpty()
                               where i.ItemId == id && q.Quantity + (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) > 0
                               select new { qu.QuantityUnitId, qu.QuantityUnit, balqty = q.Quantity - (t == null ? 0 : t.ReturnQuantity) - (v == null ? 0 : v.ConsumeQuantity) }
                              ).FirstOrDefault();

                return Json(new { unitid = product.QuantityUnitId, unit = product.QuantityUnit, balqty = product.balqty.ToString("##,##0.0000") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("<tr><td colspan = '15'>Error " + ex.Message + "</td></tr>");
            }
        }
    }
}
