﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;

namespace InventoryMS.Controllers
{
    public class LocationController : Controller
    {
        private InventoryCubeEntities db = new InventoryCubeEntities();

        // GET: /Location/
        public ActionResult Index()
        {
            var locations = (from l in db.Locations
                             join cnt in db.Countries
                             on l.CountryId equals cnt.CountryId
                             select new
                             {
                                 LocationId = l.LocationId,
                                 LocationName = l.LocationName,
                                 ShortName = l.ShortName,
                                 Prefix = l.Prefix,
                                 Details = l.Details,
                                 IsActive = l.IsActive,
                                 CountryName = cnt.CountryName
                             }
                            ).Select(c => new LocationView()
                            {
                                LocationId = c.LocationId,
                                LocationName = c.LocationName,
                                ShortName = c.ShortName,
                                Prefix = c.Prefix,
                                Details = c.Details,
                                IsActive = c.IsActive,
                                CountryName = c.CountryName
                            }).ToList();

            return View(locations);
        }

        // GET: /Location/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // GET: /Location/Create
        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName");
            return View();
        }

        // POST: /Location/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="LocationName,ShortName,Prefix,Details,CountryId,Activity")] LocationView locationView)
        {
            if (ModelState.IsValid)
            {
                Location location = new Location(locationView);

                db.Locations.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName", locationView.CountryId);
            return View(locationView);
        }

        // GET: /Location/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            LocationView locationView = (LocationView)location.Convert(new LocationView());
            if (locationView == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName", location.CountryId);
            return View(locationView);
        }

        // POST: /Location/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LocationId,LocationName,ShortName,Prefix,Details,CountryId,Activity")] LocationView locationView)
        {
            if (ModelState.IsValid)
            {
                Location location = db.Locations.Find(locationView.LocationId);
                location = (Location)locationView.Convert(location);

                db.Entry(location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName", locationView.CountryId);
            return View(locationView);
        }

        // GET: /Location/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: /Location/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
