﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using System.Web.Routing;
using System.Web.Mvc.Filters;

namespace InventoryMS.Controllers
{
    public class CAuthorizeAttribute : AuthorizeAttribute
    {
        InventoryCubeEntities context = new InventoryCubeEntities(); // my entity  
        private readonly string[] allowedroles;
        //List<Sys_User_Name> user = new List<Sys_User_Name>();
        bool authorize = false;

        public CAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        public CAuthorizeAttribute()
        {
            
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //foreach (var role in allowedroles)
            //{
                //var user = context.AppUser.Where(m => m.UserID == GetUser.CurrentUser/* getting user form current context */ && m.Role == role &&
                //m.IsActive == true); // checking active users with allowed roles.  
                //if (user.Count() > 0)
                //{
                //    authorize = true; /* return true if Entity has current user(active) with specific role */
                //}
            //}
            return authorize;
        } 
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;
            authorize = false;

            if (HttpContext.Current.Session["UserId"] != null && int.Parse(HttpContext.Current.Session["UserId"].ToString()) != 0)
            {
                short userId = short.Parse(HttpContext.Current.Session["UserId"].ToString());
                if (Roles == "Admin")
                    authorize = context.Sys_User_Name.Any(m => m.UserId == userId && m.UserTypeId == 1); // checking active users with allowed roles.
                else
                    authorize = context.Sys_User_Name.Any(m => m.UserId == userId); // checking active users with allowed roles.  

                HttpContext.Current.Session["UserId"] = userId.ToString();
                HttpContext.Current.Session.Timeout = 120;
            }
            //else
            //{
            //    authorize = true;
            //    if (HttpContext.Current.Session["UserId"] == null)
            //    {
            //        //Redirecting the user to the Login View of Account Controller  
            //        filterContext.Result = new RedirectToRouteResult(
            //            new RouteValueDictionary  
            //            {  
            //                 { "controller", "User" },  
            //                 { "action", "Login" }  
            //            });
            //    }
            //}

            base.OnAuthorization(filterContext);
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                //Redirecting the user to the Login View of Account Controller  
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary  
                {  
                     { "controller", "User" },  
                     { "action", "Login" }  
                });
            }
        } 

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            List<string> controller = new List<string>() { "home", "user" };


            //if (HttpContext.Current.Session["UserId"] == null)
            //{
            //    //Redirecting the user to the Login View of Account Controller  
            //    filterContext.Result = new RedirectToRouteResult(
            //        new RouteValueDictionary  
            //            {  
            //                 { "controller", "User" },  
            //                 { "action", "Login" }  
            //            });
            //} 
            //else 
                if (controller.Contains(filterContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString().ToLower()))
                filterContext.Result = new HttpUnauthorizedResult();
            else if (!filterContext.HttpContext.Request.IsAjaxRequest())
                filterContext.Result = new HttpUnauthorizedResult();
                //filterContext.Controller.ViewData["OpenAuthorizationPopup"] = true;
            else
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        status = "NotAuthorized"//,
                        //LogOnUrl = new UrlHelper(filterContext.RequestContext).Action("PartilaLogin", "User")
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
        }
    }

    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        InventoryCubeEntities context = new InventoryCubeEntities(); // my entity  
        private readonly string[] allowedroles;
        List<Sys_User_Name> user = new List<Sys_User_Name>();
        public AdminAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        public AdminAuthorizeAttribute()
        {

        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            //foreach (var role in allowedroles)
            //{
            //var user = context.AppUser.Where(m => m.UserID == GetUser.CurrentUser/* getting user form current context */ && m.Role == role &&
            //m.IsActive == true); // checking active users with allowed roles.  
            if (user.Count() > 0)
            {
                authorize = true; /* return true if Entity has current user(active) with specific role */
            }
            //}
            return authorize;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;
            short userId = short.Parse(HttpContext.Current.Session["UserId"] == null ? "0" : HttpContext.Current.Session["UserId"].ToString());

            user = context.Sys_User_Name.Where(m => m.UserId == userId).ToList(); // checking active users with allowed roles.  

            base.OnAuthorization(filterContext);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }  
}