﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.Serialization.Diagnostics;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryMS.Models;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using System.IO;
using System.Net.Mail;
using InventoryMS.Controllers.Inventory;
using System.Collections.Specialized;

namespace InventoryMS.Controllers
{
    //[Authorize(Roles = "User")]
    public class HomeController : Controller
    {
        //[Authorize]
        [CAuthorize]
        public ActionResult Index()
        {
            InventoryCubeEntities db = new InventoryCubeEntities();
            //Assembly asm = Assembly.GetAssembly(typeof(InventoryMS.MvcApplication));
            //ListDictionary dict = new ListDictionary();
            //dict.Add("IssueController", 30);
            //dict.Add("SRRController", 31);
            //dict.Add("IssueReturnController", 32);
            //dict.Add("ConsumptionController", 33);

            ////SRRController
            //var controlleractionlist = asm.GetTypes()
            //        .Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type))
            //        .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
            //        .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
            //        .Where(m => m.DeclaringType.Name == "IssueController" || m.DeclaringType.Name == "SRRController" || m.DeclaringType.Name == "IssueReturnController" || m.DeclaringType.Name == "ConsumptionController")
            //        .Select(x => new
            //        {
            //            ControllerName = x.DeclaringType.Name,
            //            ControllerId = dict[x.DeclaringType.Name],
            //            ActionName = x.Name,
            //            ParameterList = (x.GetParameters().Count() > 0 ? x.GetParameters().Select(c => c.Name).Aggregate((i, j) => i + ", " + j) : ""),
            //            ReturnType = x.ReturnType.Name,
            //            AttributeList = String.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", "")))
            //        })
            //        .OrderBy(x => x.ControllerName).ThenBy(x => x.ActionName).ToList();

            //foreach (var a in controlleractionlist.OrderBy(l => l.ControllerId).Distinct())
            //{
            //    User_Action ua = new User_Action(new User_Action());
            //    ua.ControllerId = Convert.ToInt16(a.ControllerId);
            //    ua.ActionName = a.ActionName;
            //    ua.AttributeList = a.AttributeList;
            //    ua.ReturnType = a.ReturnType;
            //    ua.ParameterList = a.ParameterList;
            //    ua.CreatedBy = clsMain.getCurrentUser();
            //    ua.CreatedTime = clsMain.getCurrentTime();
            //    ua.UpdatedBy = 0; ua.UpdateTime = DateTime.Parse("01-Jan-1900");
            //    ua.IPAddress = clsMain.GetUser_IP();
            //    ua.PCName = clsMain.GetUser_PCName();

            //    db.User_Action.Add(ua);
            //}
            //var abc = 1;

            //foreach (var c in controlleractionlist.Select(c => c.ControllerName).Distinct())
            //{
            //    User_Controller uc = new User_Controller(new User_Controller());
            //    uc.ControllerName = c.Replace("Controller", "");
            //    uc.CreatedBy = clsMain.getCurrentUser();
            //    uc.CreatedTime = clsMain.getCurrentTime();
            //    uc.UpdatedBy = 0; uc.UpdateTime = DateTime.Parse("01-Jan-1900");
            //    uc.IPAddress = clsMain.GetUser_IP();
            //    uc.PCName = clsMain.GetUser_PCName();

            //    foreach (var a in controlleractionlist.Where(l => l.ControllerName == c).Distinct())
            //    {
            //        User_Action ua = new User_Action(new User_Action());
            //        ua.ActionName = a.ActionName;
            //        ua.AttributeList = a.AttributeList;
            //        ua.ReturnType = a.ReturnType;
            //        ua.ParameterList = a.ParameterList;
            //        ua.CreatedBy = clsMain.getCurrentUser();
            //        ua.CreatedTime = clsMain.getCurrentTime();
            //        ua.UpdatedBy = 0; ua.UpdateTime = DateTime.Parse("01-Jan-1900");
            //        ua.IPAddress = clsMain.GetUser_IP();
            //        ua.PCName = clsMain.GetUser_PCName();

            //        uc.User_Action.Add(ua);
            //    }

            //    db.User_Controller.Add(uc);
            //}

            ////foreach (var c in controlleractionlist)
            ////{
            ////    var uc = db.User_Action.Where(l => l.ActionName.ToLower() == c.ActionName.ToLower() && l.User_Controller.ControllerName.ToLower() == c.ControllerName.ToLower().Replace("controller", "") && l.AttributeList.ToLower() == c.AttributeList.ToLower()).FirstOrDefault();

            ////    if (uc != null)
            ////    {
            ////        uc.ParameterList = c.ParameterList;

            ////        db.Entry(uc).State = EntityState.Modified;
            ////    }
            ////}

            //db.SaveChanges();

            ////var cntrl = db.MenuTypes.ToList();
            ////SendMail();
            ////clsMailProcess.SendMail();

            //string methods = "";
            ////Type t = typeof(HomeController);
            ////MethodInfo[] mi = t.GetMethods();
            ////foreach (MethodInfo m in mi)
            ////{
            ////    if (m.IsPublic)
            ////        if (typeof(ActionResult).IsAssignableFrom(m.ReturnParameter.ParameterType))
            ////            methods = m.Name + Environment.NewLine + methods;
            ////}

            ViewBag.ActionList = "";
            return View();
        }

        //[Authorize]
        [CAuthorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [CAuthorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public void SendMail()
        {
            StiReport report = new StiReport();

            string rptName = Server.MapPath("~/Reports/rptMARSummary.mrt");

            if (!System.IO.File.Exists(rptName))
            {
                return;
            }
            else
            {
                report.Load(rptName);

                //List<String> ParameterList = dataRow["ParameterList"].ToString().Split(',').ToList();
                //List<String> ParameterValueList = dataRow["ParameterValueList"].ToString().Split(',').ToList();

                Dictionary<string, string> Parameters = new Dictionary<string, string>();

                //for (int ri = 0; ri < ParameterList.Count; ri++)
                //{
                Parameters.Add("FactoryId", "1");
                Parameters.Add("FDate", "15-May-2017");
                //}

                foreach (StiVariable pf in report.Dictionary.Variables)
                {
                    if (Parameters.ContainsKey(pf.Name.Replace("_", "")))
                    {
                        var param = Parameters.Where(l => l.Key == pf.Name.Replace("_", "")).FirstOrDefault();

                        if (!String.IsNullOrEmpty(param.Value))
                        {
                            if (pf.Type == typeof(string))
                            {
                                report.Dictionary.Variables[pf.Name].Value = "\"" + param.Value + "\"";
                            }
                            else if (pf.Type.BaseType == typeof(System.ValueType))
                            {
                                if (pf.Type == typeof(DateTime))
                                {
                                    report.Dictionary.Variables[pf.Name].Value = "" + DateTime.Parse(param.Value).ToString("yyyy-MM-dd") + "";
                                }
                                else
                                {
                                    report.Dictionary.Variables[pf.Name].Value = param.Value;
                                }
                            }
                        }
                        else
                        {
                            if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                            {
                                pf.Value = "0";
                            }
                        }
                    }
                    else
                    {
                        if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                        {
                            pf.Value = "0";
                        }
                    }
                }

                StiSqlDatabase db = (StiSqlDatabase)report.Dictionary.Databases["IMPS"];

                if (db == null)
                    db = (StiSqlDatabase)report.Dictionary.Databases["IPMS"];

                db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=InventoryCube;User Id=sa;Password=dbsrv170@sdk;";
                db.PromptUserNameAndPassword = false;

                //report.Compile();
                report.Render(); 

                Stream attachmentStream = new MemoryStream();
                report.ExportDocument(StiExportFormat.Pdf, attachmentStream);
                attachmentStream.Seek(0, SeekOrigin.Begin);
                Attachment attachment = new Attachment(attachmentStream, "rptMARSummary.pdf");

                MailSend("ipms@kdsgroup.net", "mohammad.noman@kdsgroup.net".Split(';'), "".Split(';'),
                                    "".Split(';'), "Test Mail", "FYI", null, new Attachment[] { attachment });
                            
            }
        }
            
        public static void MailSend(string mailFrom, string[] mailTo, string[] mailCC, string[] mailBCC, string subject, string body, string[] attachmentLink, Attachment[] attachmentList)
        {
            try
            {
                //if (!clsDBConfig.GetReportLogon().Server.ToUpper().Equals("192.168.0.181") && !clsDBConfig.GetReportLogon().Server.ToUpper().Equals("203.82.207.170")) return;

                if (mailFrom == null)
                    return;
                if (subject == null)
                    subject = "=N/A=";
                if (body == null)
                    body = "-";

                if (mailTo == null)
                    mailTo = new string[] { };
                if (mailCC == null)
                    mailCC = new string[] { };
                if (mailBCC == null)
                    mailBCC = new string[] { };
                if (attachmentLink == null)
                    attachmentLink = new string[] { };
                if (attachmentList == null)
                    attachmentList = new Attachment[] { };

                MailMessage mail = new MailMessage();

                //if (!mailFrom.Contains("<"))
                //    mailFrom = "CUBE | SDMS (Sales & Distribution Management System)<" + mailFrom + ">;";

                mail.From = new MailAddress(mailFrom.Trim());

                foreach (string address in mailTo.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.To.Add(address.Trim());
                }
                foreach (string cc in mailCC.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.CC.Add(cc.Trim());
                }
                foreach (string bcc in mailBCC.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.Bcc.Add(bcc.Trim());
                }
                //mail.Bcc.Add("mohammad.noman@kdsgroup.net");

                mail.Subject = subject.Replace(Environment.NewLine, "").Replace("\n", "").Trim();
                mail.Body = body.Trim();
                mail.IsBodyHtml = true;

                foreach (string strAttachment in attachmentLink.Where(o => o.Trim() != "" && !o.Trim().Equals("-")))
                {
                    mail.Attachments.Add(new Attachment(strAttachment.Trim()));
                }

                foreach (Attachment attachment in attachmentList.Where(o => o != null))
                {
                    mail.Attachments.Add(attachment);
                }

                SmtpClient SmtpServer = new SmtpClient("203.82.207.6");
                SmtpServer.Credentials = new System.Net.NetworkCredential("mohammad.noman@kdsgroup.net", "bakalam");
                SmtpServer.Port = 25;
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);

                SmtpServer = null;
                //MessageBox.Show("Mail Send Successfully.");
            }
            catch (Exception ex)
            {
                //WriteLog(ex.Message + ", Mail Subject: " + subject, EventLogEntryType.Error);
                throw new Exception("Mail Send Failed. " + ex.Message + ", Mail Subject: " + subject);
            }
        }
    }
}
