﻿using System.Web;
using System.Web.Optimization;

namespace InventoryMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/datatable/js").Include(
                      //"~/Scripts/jquery.dataTables.js",
                      //"~/Scripts/DT_bootstrap.js",
                      //"~/Scripts/tables.js",

                      "~/common/theme/plugins/datatables/jquery.dataTables.min.js",
                      "~/common/theme/plugins/datatables/dataTables.bootstrap.min.js",
                      "~/common/theme/plugins/datatables/DataTable.js"

                      ));

            bundles.Add(new ScriptBundle("~/common/headerscripts").Include(
                      "~/common/theme/plugins/jQuery/jQuery-2.1.4.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/common/footerscripts").Include(
                        "~/common/theme/plugins/jQueryUI/jquery-ui.min.js",
                        "~/common/bootstrap/js/bootstrap.min.js",
                        "~/common/theme/plugins/select2/select2.full.min.js",
                        "~/common/theme/plugins/raphael/raphael-min.js",
                        "~/common/theme/plugins/bootstrap-swich/js/bootstrap-switch.js",
                        "~/common/theme/plugins/morris/morris.min.js",
                        "~/common/theme/plugins/sparkline/jquery.sparkline.min.js",
                        "~/common/theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                        "~/common/theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                        "~/common/theme/plugins/knob/jquery.knob.js",
                        "~/common/theme/plugins/moment/moment.min.js",
                        "~/common/theme/plugins/input-mask/jquery.inputmask.js",
                        "~/common/theme/plugins/input-mask/jquery.inputmask.date.extensions.js",
                        "~/common/theme/plugins/input-mask/jquery.inputmask.extensions.js",
                        "~/common/theme/plugins/daterangepicker/jquery.daterangepicker.js",
                        "~/common/theme/plugins/datepicker/bootstrap-datepicker.js",
                        "~/common/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                        "~/common/theme/plugins/slimScroll/jquery.slimscroll.min.js",
                        "~/common/theme/plugins/fastclick/fastclick.min.js",
                        "~/common/theme/js/app.min.js",
                        "~/common/js/myjs.js"
                      ));

            bundles.Add(new StyleBundle("~/common/css").Include(
                        "~/common/theme/plugins/select2/select2.css",
                        "~/Common/bootstrap/css/bootstrap.css",
                        "~/common/theme/font-awesome-4.4.0/css/font-awesome.min.css",
                        "~/common/theme/ionicons-2.0.1/css/ionicons.min.css",
                        "~/common/css/style.css",
                        //"~/common/theme/plugins/iCheck/flat/blue.css",
                        "~/common/theme/plugins/bootstrap-swich/css/bootstrap-switch.css",
                        "~/common/theme/plugins/morris/morris.css",
                        "~/common/theme/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                        "~/common/theme/plugins/daterangepicker/daterangepicker.css",
                        "~/common/theme/plugins/datepicker/datepicker3.css",
                        //"~/common/theme/plugins/daterangepicker/daterangepicker-bs3.css", 	
                        "~/common/theme/plugins//bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",
                        "~/common/theme/css/AdminLTE.css",
                        "~/common/theme/css/skins/_all-skins.min.css"
                      ));

            bundles.Add(new StyleBundle("~/datatable/css").Include(
                      "~/common/theme/plugins/datatables/dataTables.bootstrap.css"));

            bundles.Add(new StyleBundle("~/vendor/css").Include(
                //"~/Content/app-assets/css/fonts.css",
                "~/Content/app-assets/css/vendors.css",
                "~/Content/app-assets/vendors/css/forms/icheck/icheck.css",
                "~/Content/app-assets/vendors/css/forms/icheck/custom.css",
                "~/Content/app-assets/vendors/css/charts/morris.css",
                "~/Content/app-assets/vendors/css/extensions/unslider.css",
                "~/Content/app-assets/vendors/css/weather-icons/climacons.min.css",
                "~/Content/app-assets/vendors/css/forms/selects/select2.min.css",
                "~/Content/app-assets/css/app.css",
                "~/Content/app-assets/vendors/css/ui/jquery-ui.min.css",
                "~/app-assets/css/plugins/ui/jqueryui.css",
                //"~/Content/app-assets/css/pages/users.css",
                //"~/Content/app-assets/css/plugins/calendars/clndr.css",
                //"~/Content/assets/css/style.css"
                
                "~/Content/app-assets/css/core/menu/menu-types/horizontal-top-icon-menu.css",
                "~/Content/app-assets/css/core/colors/palette-climacon.css",
                "~/Content/app-assets/css/pages/users.css",
                "~/Content/assets/css/style.css",
                "~/Content/app-assets/css/style.css"));


            //BundleTable.EnableOptimizations = true;
        }
    }
}
